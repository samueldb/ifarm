//-------------------------------------------------------------------
/*/{Protheus.doc} MA410COR
Ponto de entrada para altera��o de legenda de pedidos venda
@author  Samuel Dantas  
@since   date
@version version
/*/
//-------------------------------------------------------------------
User Function MA410COR()
    //Ignoro cores vindas do ParamIXB
    Local aCoresPE := {}
                                                                         
    aAdd(aCoresPE,{"C5_YSTATUS == 'F'", "BR_VERMELHO"   , "Pedido finalizado."})
    aAdd(aCoresPE,{"C5_YSTATUS == 'A'", "BR_VERDE"      , "Pedido aceito."})
    aAdd(aCoresPE,{"C5_YSTATUS == 'S'", "BR_AZUL"       , "Saiu para entrega."})
    aAdd(aCoresPE,{"C5_YSTATUS == 'P'", "BR_AMARELO"    , "Pendente"})
    aAdd(aCoresPE,{"C5_YSTATUS == 'E'", "BR_BRANCO"     , "Em separa��o"})
    aAdd(aCoresPE,{"C5_YSTATUS == 'G'", "BR_PRETO"      , "Aguardando"})

return aCoresPE 

User Function MA410LEG()
    //Ignoro legendas vindas do ParamIXB
    Local aLegNew := {}

    AADD( aLegNew, {"BR_VERMELHO"   ,"Pedido finalizado."  } )
    AADD( aLegNew, {"BR_VERDE"      ,"Pedido aceito."      } )
    AADD( aLegNew, {"BR_AZUL"       ,"Saiu para entrega."  } )
    AADD( aLegNew, {"BR_AMARELO"    ,"Pendente"            } )
    AADD( aLegNew, {"BR_BRANCO"     ,"Em separa��o"        } )
    AADD( aLegNew, {"BR_PRETO"      ,"Aguardando"          } )

Return aLegNew 