//-------------------------------------------------------------------
/*/{Protheus.doc} M410LIOK
Ponto de entrada chamado na atualiza��o do item do pedido de venda. (LinhaOk)
@author  Samuel Dantas
@since   11/06/2019
@version version
/*/
//-------------------------------------------------------------------
User Function M410LIOK ()    
    Local nVlrTotal := 0
    Local nPerFrete := 0
    Local nVlrFrete := 0
    Local nPosTotal := 0 
    Local nPosProd  := 0
    Local nPosQtd   := 0
    Local nI        := 1
    Local _nPreco   := 0

    //S� executa quando tiver sido chamado pelo protheus direto, sem ser execauto
    If FunName() == "MATA410"
        
        nPosTotal   := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_VALOR'})
        nPosProd    := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_PRODUTO'})
        nPosQtd     := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_QTDVEN'})
        
        SB1->(DbSetOrder(1))
        SB1->(DbSeek(xFilial("SB1") + aCols[n][nPosProd] ))

        // � necess�rio posicionar SA1 antes de chamar getPercPlano
        SA1->(DbSetOrder(1))
        If SA1->(DbSeek(xFilial("SA1") + M->C5_CLIENTE + M->C5_LOJACLI ))
            nPercDesc := StaticCall(IFAWS005, getPercPlano,  SB1->B1_COD, SB1->B1_GRUPO, .F. ) //Percentual por produto
            nPerFrete := StaticCall(IFAWS005, getPercPlano,  SB1->B1_COD, SB1->B1_GRUPO, .T. ) // Percentutal de frete
            If nPercDesc > 0
                Z18->(DbSetOrder(4))
                If Z18->(DbSeek(xFilial("Z18") + SB1->B1_COD))
                    //Preecnhe o preco
                    _nPreco := Z18->Z18_PRECO - Z18->Z18_PRECO * nPercDesc / 100   
                    _nPreco := Round(_nPreco, 2)
                    GDFieldPut("C6_PRCVEN",_nPreco)
                    GDFieldPut("C6_VALOR", _nPreco*aCols[n][nPosQtd])
                EndIf
            Endif
        EndIf  
    
        Z01->(DbSetOrder(1))
        Z01->(DbSeek(xFilial('Z01')))

        //Calcula o valor total dos itens        
        For nI := 1 To len(aCols)
            nVlrTotal += aCols[nI][nPosTotal]
        Next

        //Guarda total do pedido em campo virtual
        M->C5_YTOTPED := nVlrTotal

        //Variavel que define se recalcula ou nao o frete
        //So calcula novamente se nao tiver zerado
        lRecalFrete := M->C5_FRETE > 0
        
        //Se o frete for diferente do gravado no campo na alteracao ou diferente do frete padrao na inclusao
        If (M->C5_FRETE <> SC5->C5_FRETE .AND. ALTERA) 
            lRecalFrete :=  MsgYesNo('O frete foi alterado manualmente, deseja recalcular?')
        EndIf
        
        //Verifica se o frete deve ser recalculado
        If lRecalFrete

            //Verifica se tem perecentual de desconto
            If nPerFrete > 0            
                nVlrFrete := Z01->Z01_FRETE - Z01->Z01_FRETE * (nPerFrete/100)
                M->C5_FRETE := nVlrFrete
            Else
                If nVlrTotal >= Z01->Z01_PEDMIN
                    M->C5_FRETE := 0
                Else
                    M->C5_FRETE := Iif((Z01->Z01_FRETE - M->C5_YDESCFR) > 0, Z01->Z01_FRETE - M->C5_YDESCFR , 0)
                EndIf  
            EndIf
                   
        EndIf
        
        //Atualiza  tela de pedido de vendas
        If oGetPV <> Nil
            oGetPV:Refresh()
        Endif	
    EndIf
    
Return .T.