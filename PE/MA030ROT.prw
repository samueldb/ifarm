//-------------------------------------------------------------------
/*/{Protheus.doc} MA030ROT
Ponto de entrada no cadastro de clientes
Aiciona mais acoes ao cadastro
@author  Sidney Sales
@since   26/12/18
@version 1.0
/*/
//-------------------------------------------------------------------
User Function MA030ROT
	Local aRotAux	:= {}
	aAdd(aRotAux,{ "Enderešos do Cliente", "U_fEndCli()", 0, 4, 0, Nil})
Return aRotAux

//-------------------------------------------------------------------
/*/{Protheus.doc} fEndCli
description
@author  author
@since   26/12/2018
@version 1.0
/*/
//-------------------------------------------------------------------
User Function fEndCli()

    Private cCadastro := 'Enderešos do Cliente'

    Private aRotina := {{'Pesquisar' ,'AxPesqui',0,1} ,;
                       {'Visualizar' ,'AxVisual',0,2} ,;
                       {'Incluir'   ,'AxInclui',0,3} ,;
                       {'Alterar'   ,'AxAltera',0,4} ,;
                       {'Excluir'   ,'AxDeleta',0,5} }

    Private cDelFunc := '.T.' // Validacao para a exclusao. Pode-se utilizar ExecBlock

    Private cString := 'Z09'
    DbSelectArea(cString)
    DbSetOrder(1)
    DbSelectArea(cString)

    Private A1_VM_MARC  := ''
    Private A1_CODMARC  := ''
    Private A1_OBS      := ''
    
    set filter to SA1->A1_COD == Z09->Z09_CODSA1 .AND. SA1->A1_LOJA == Z09->Z09_LOJA
    mBrowse( 6,1,22,75,cString)
    set filter to
    
Return
