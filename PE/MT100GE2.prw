//-------------------------------------------------------------------
/*/{Protheus.doc} MT100GE2
Ponto de entrada na gravacao dos titulos a pagar. Deduz o valor
do titulo para o percentual do associado.
@author  Sidney Sales
@since   18/12/2018
@version 1.0
/*/
//-------------------------------------------------------------------
User Function MT100GE2()
    //N�o utilizado a partir de 12/06/2019

    // Local nValor    := 0
    // Local aAreaSC7  := SC7->(GetArea())    
    // Local aAreaZ01  := Z01->(GetArea())    
    // Local aAreaZ02  := Z02->(GetArea())    

    // SC7->(DbSetOrder(1))
    // Z01->(DbSetOrder(1))
    // Z02->(DbSetOrder(3))
    
    // //Seta o pedido de compra
    // If SC7->(DbSeek(xFilial('SC7') + SD1->D1_PEDIDO + SD1->D1_ITEMPC ))        
    //     //Se o pedido de compras for de uma associacao, posiciona a associacao e faz o calculo que tem que reduzir. 
    //     If !Empty(SC7->C7_YCODZ01)
    //         If Z01->(DbSeek(xFilial('Z01') + SC7->C7_YCODZ01)) .AND.  Z02->(DbSeek(xFilial('Z02') + SC7->C7_YCODZ01 + SD1->D1_FORNECE + SD1->D1_LOJA))
    //             nValor := SE2->E2_VALOR * Z02->Z02_PERLUC / 100 
    //             SE2->E2_VALOR   := nValor 
    //             SE2->E2_SALDO   := nValor
    //             SE2->E2_VLCRUZ  := nValor
    //         EndIf
    //     EndIf    
    // EndIf

    // RestArea(aAreaSC7)
    // RestArea(aAreaZ01)
    // RestArea(aAreaZ02)

Return
