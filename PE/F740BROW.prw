//-------------------------------------------------------------------
/*/{Protheus.doc} func
    Ponto de entrada chamado para filtrar browser da tela de fun���es do contras a receber
    @type  user Function
    @author Samuel Dantas
    @since   26/06/2020
    @version 1.0
/*/
//-------------------------------------------------------------------
User Function F740BROW
    Local bCondicao 
    If FUNNAME() == "MATA410"
        bCondicao := {||E1_FILIAL = SE1->E1_FILIAL .AND. E1_PEDIDO = SE1->E1_PEDIDO }

        DbSelectArea("SE1")   
        DbSetOrder(1)   
        DbSetFilter(bCondicao," E1_FILIAL = '"+SE1->E1_FILIAL+"' .AND. E1_PEDIDO = '"+SE1->E1_PEDIDO+"' ")
    EndIf
Return