//-------------------------------------------------------------------
/*/{Protheus.doc} M030ALT
Ponto de enetrada na alteracao de cliente
@author  Sidney Sales
@since   10/06/19
@version 1.0
/*/
//-------------------------------------------------------------------
User Function M030ALT
    Local lRet := .T.
    
    //Verifica se o endere�o � vazio e preenche com um cadastro de endere�o, caso exista
    If Empty(SA1->A1_END)
        Z09->(DbSetOrder(1))//Z09_FILIAL+Z09_CODSA1+Z09_LOJA+Z09_CODIGO                                                                                                                           
        If Z09->(DbSeek(xFilial('Z09') + SA1->A1_COD + SA1->A1_LOJA))
            RecLock('SA1', .F.)
                SA1->A1_END := Z09->Z09_RUA
                SA1->A1_EST := Z09->Z09_EST
                SA1->A1_MUN := Z09->Z09_MUN
            SA1->(MsUnLock())
        EndIf
    EndIf        
    
Return lRet