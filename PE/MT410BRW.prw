#INCLUDE 'PROTHEUS.CH'
#INCLUDE 'FILEIO.CH'
#INCLUDE 'TOPCONN.CH'
#INCLUDE 'TBICONN.CH'
//-------------------------------------------------------------------
/*/{Protheus.doc} function
Ponto de entra para inclus�o de a��es no cadastro de produtos
@author  Samuel Dantas Batista
@since   11/12/2018
@version version
/*/
//-------------------------------------------------------------------
User function MT410BRW  ()
    SetaAtalhos()
Return 

Static Function SetaAtalhos()
	SetKey(VK_F5, { || U_M410MNUa('P') })
	SetKey(VK_F6, { || U_M410MNUa('A') })
	SetKey(VK_F7, { || U_M410MNUa('S') })
	SetKey(VK_F8, { || U_M410MNUa('F') })
	SetKey(VK_F9, { || U_M410MNUd()	   })
Return