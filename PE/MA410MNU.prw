#INCLUDE 'Rwmake.ch'
#INCLUDE 'Protheus.ch'
#INCLUDE 'TbIconn.ch'
#INCLUDE 'Topconn.ch'

//-------------------------------------------------------------------
/*/{Protheus.doc} MA410MNU
Ponto de entrada para inclus�o de a��o no menu de pedidos de venda
@author  Samuel Dantas  
@since   14/12/2018
/*/
//-------------------------------------------------------------------
User function MA410MNU ()
    Local aOpcoes       := {}
    Local aImp          := {}
    
    aadd(aOpcoes,{'F5 - Pendente',"U_M410MNUa('P')", 0 , 4,0,NIL})   
    aadd(aOpcoes,{'F6 - Aceito'  ,"U_M410MNUa('A')", 0 , 4,0,NIL})   
    aadd(aOpcoes,{'F7 - Saiu pra entrega',"U_M410MNUa('S')", 0 , 4,0,NIL})   
    aadd(aOpcoes,{'F8 - Finalizado',"U_M410MNUa('F')", 0 , 4,0,NIL})   
    aadd(aOpcoes,{'Aguardando',"U_M410MNUa('G')", 0 , 4,0,NIL})   

    aAdd(aImp, {'Pedido Selecionado'       ,"u_M410MNUr(.T.)"  , 0 , 2,0,NIL} )
    aAdd(aImp, {'Escolher Pedidos'         ,"u_M410MNUr(.F.)"  , 0 , 2,0,NIL} )
    aAdd(aImp, {'Lista Separa��o'          ,"u_M410MNUr(.T., .T.)"  , 0 , 2,0,NIL} )
    aAdd(aImp, {'Escolher Lista'           ,"u_M410MNUr(.F., .T.)"  , 0 , 2,0,NIL} )
    
    aadd(aRotina,{'Alterar Status'  ,aOpcoes            , 0 , 2,0,NIL})   
    aadd(aRotina,{'Log Status'      ,"u_M410MNUb()"     , 0 , 2,0,NIL})   
    aadd(aRotina,{'Imprimir'        ,aImp               , 0 , 4,0,NIL})   
    aadd(aRotina,{'Passar Cart�o'   ,"u_M410MNUc()"     , 0 , 4,0,NIL})   
    aadd(aRotina,{'F9 - Abrir WattsApp',"U_M410MNUd()"  , 0 , 4,0,NIL})   
    aadd(aRotina,{'Enviar por WattsApp' ,"U_IFAF004C()"  , 0 , 4,0,NIL})   
    aadd(aRotina,{'Remover da Separa��o',"U_M410MNUe()" , 0 , 4,0,NIL})   
    aadd(aRotina,{'Titulo a receber',"U_M410MNUf()" , 0 , 4,0,NIL})   
Return 

User Function M410MNUr(lSelecionado, lLista)
    Local aSC5      := {}
	Local aPergs	:= {}
	Local aRet  	:= {}
	Local aMvPar	:= {}
    
    Default lLista  := .F.
    
    If lSelecionado
        U_IFAR0001(,,lLista)
    else
        
        //Grava os parametros MV_PAR da rotina
        u_gravapar(@aMvPar, .F.)

        aAdd(aPergs, {1,"Pedido de", '      ','@!',"VAZIO()",'',".T.",50,.F.})
        aAdd(aPergs, {1,"Pedido At�",'ZZZZZZ','@!',"VAZIO()",'',".T.",50,.F.})
        aAdd(aPergs, {2,"Status", 'Aceito',{'Aceito','Saiu pra entrega', 'Pendende', 'Finalizado'},50,'.T.',.F.})
        
        If ParamBox(aPergs ,"Impress�o de pedidos",aRet,,,,,,,.F.,.F.)
            cQuery := " SELECT R_E_C_N_O_ AS RECNOSC5 FROM " + RetSqlTab('SC5')
            cQuery += " WHERE " + RetSqlDel('SC5')
            cQuery += " AND C5_FILIAL = '" + xFilial('SC5') + "' "
            cQuery += " AND C5_NUM BETWEEN '" + aRet[1] + "' AND '" + aRet[2] + "' "
            cQuery += " AND C5_YSTATUS = '" + Left(aRet[3],1) + "' "

            If Select('QRYC5') > 0
                QRYC5->(DbCloseArea())
            EndIf

            TcQuery cQuery New Alias 'QRYC5'

            While QRYC5->(!Eof())
                aAdd(aSC5, QRYC5->RECNOSC5)
                QRYC5->(DbSkip())
            EndDo

        EndIf

        u_gravapar(@aMvPar, .T.)

        U_IFAR0001(.F., aSC5, lLista)

    EndIf

Return

//Funcao auxiliar para alterar o status do pedido
User Function M410MNUa(cNovoStatus)
    Local cStatusPara   := u_X3_CBOX("C5_YSTATUS", cNovoStatus)
    Local cStatusDe     := u_X3_CBOX("C5_YSTATUS", SC5->C5_YSTATUS)
    Local cStatusCartao     := ""
    Local cAprovados   := "approved,authorized"
    Local cCodDebito   := Alltrim(SuperGetMv("MS_CODDEBI",.F.,"000005"))
    Local cCodCredito  := Alltrim(SuperGetMv("MS_CODCRED",.F.,"000007"))
    Local cNomeEntrega := Alltrim(SuperGetMv('MS_ENTREGA', .F., 'Wisner'))
    Local cCodMsgAc := Alltrim(SuperGetMv('MS_MSGACEI', .F., '000011')) // C�digo da mensagem de aceito (Z33)
    Local cCodMsgEn := Alltrim(SuperGetMv('MS_MSGENT', .F., '000012')) // C�digo da mensagem de saiu p/entrega (Z33)
    Local cMsg         := ''
    Local cStatusAtual := SC5->C5_YSTATUS
    Local lEnviaWatts  := .T.
    
    If cNovoStatus == 'S'
        //Debito ou cr�dito
        If SC5->C5_YFORMPG == cCodDebito .OR. SC5->C5_YFORMPG == cCodCredito
            cStatusCartao := StaticCall(IFAWS004, getStatus,  Alltrim(SC5->C5_YSTATMP) ) //Percentual por produto
            If !( UPPER(cStatusCartao) == 'APROVADO' .OR. UPPER(cStatusCartao) == 'AUTORIZADO')
                MsgAlert('Venda efetuada via cart�o est� com status '+cStatusCartao+'. Necess�rio realizar o pagamento antes de mudar status para saiu para entrega.')
                Return
            EndIf
        EndIf
    EndIf

    If ! SC5->C5_YSTATUS $ 'P,A' .AND. cNovoStatus == 'G'
        MsgAlert('S� � poss�vel mudar o status para aguardando em pedidos Pendentes ou Aceitos')
        return
    EndIf
    
    If SC5->C5_YSTATUS == 'A' .AND. cNovoStatus == 'P' .AND. SC5->C5_YSEPAR == 'S'
        MsgAlert('N�o � poss�vel mudar o status de aceito para pendente pois o mesmo j� foi adicionado a uma separa��o. ')
        Return
    EndIf
    
    If SC5->C5_YSTATUS == 'P' .AND. cNovoStatus != 'A'
        MsgAlert('S� � permitida mudan�a de status de pedidos pendentes para Aceito.')
        Return
    EndIf

    If cNovoStatus $ 'S,F' .AND. !Empty(SC5->C5_YTOKEN) .AND. ! Alltrim(SC5->C5_YSTATMP) $ cAprovados .AND. (SC5->C5_YFORMPG == cCodDebito .OR. SC5->C5_YFORMPG == cCodCredito)
        MsgAlert('N�o � poss�vel a mudan�a de status pois o pagamento com cart�o ainda n�o foi aprovado.')
        Return
    EndIf

    If SC5->C5_YSTATUS <> cNovoStatus
        If MsgYesNo('Confirma a mudan�a para o status ' + cStatusPara)
            
            //Se ja aceitou uma vez, nao envia novamente
            If jaACeitou(SC5->C5_NUM)
                lEnviaWatts := .F.
            Else
                lEnviaWatts := .T.
            EndIf
            
            RecLock('SC5', .F.)
                SC5->C5_YSTATUS := cNovoStatus
                If cNovoStatus == 'F'
                    SC5->C5_YDTFIM := dDataBase
                EndIf
            SC5->(MsUnLock())

            RecLock('Z08',.T.)
                Z08->Z08_FILIAL := xFilial('Z08')
                Z08->Z08_CODSC5 := SC5->C5_NUM
                Z08->Z08_DATA   := dDataBase
                Z08->Z08_HORA   := Time()
                Z08->Z08_DE     := cStatusDe
                Z08->Z08_PARA   := cStatusPara
                z08->Z08_USUARI := __cUserID + ' - ' + cUserName
            Z08->(MsUnLock())
                    
            If cNovoStatus $ 'A,S'                    
                SA1->(DbSetOrder(1))            
                SA1->(DbSeek(xFilial('SA1') + SC5->C5_CLIENTE + SC5->C5_LOJACLI))
                nPos     := At(' ',SA1->A1_NOME)
                cNomeCli := Alltrim(SubStr(SA1->A1_NOME, 1, nPos))

                Z09->(DbSetOrder(2))            
                Z09->(DbSeek(xFilial("Z09") + SC5->C5_YCODZ09 ))
                Z33->(DbSetOrder(1))
               
                If cNovoStatus == 'A'    
                    //Pedido Aceite         
                    If Z33->(DbSeek(xFilial("Z33") + cCodMsgAc))
                        cMsg := &(Alltrim(Z33->Z33_MSG))
                    EndIf    
                ElseIf cNovoStatus == 'S'
                    //Pedido Saiu para entrega
                     If Z33->(DbSeek(xFilial("Z33") + cCodMsgEn))
                        cMsg := &(Alltrim(Z33->Z33_MSG))
                    EndIf
                EndIf   
            
                If (cNovoStatus == 'A' .AND. lEnviaWatts) .OR. cNovoStatus != 'A'
                    StaticCall(IFAF0004, SendWatts, cMsg, .F.)
                EndIf

            EndIf

        EndIf
    Else
        MsgAlert('O status atual j� � ' + cStatusPara)
    EndIf

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} M410MNUb
Browse listando as mudan�as de status
@author  Sidney Sales
@since   24/12/2018
@version 1.0
/*/
//-------------------------------------------------------------------
User Function M410MNUb()

    Private cCadastro := 'Log de mudan�a de status do pedido'

    Private aRotina := {{'Pesquisar' ,'AxPesqui',0,1} ,;
                       {'Visualizar' ,'AxVisual',0,2} ,;
                       {'Excluir' ,'AxDeleta',0,5} }

    Private cDelFunc := '.T.' // Validacao para a exclusao. Pode-se utilizar ExecBlock

    Private cString := 'Z08'

    DbSelectArea(cString)
    DbSetOrder(1)
    DbSelectArea(cString)
    
    Set Filter To Z08->Z08_CODSC5 == SC5->C5_NUM 
    mBrowse( 6,1,22,75,cString)
    Set Filter To 

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} M410MNUc
Funcao para 
@author  author
@since   23/04/2019
@version 1.0
/*/
//-------------------------------------------------------------------
User Function M410MNUc()
    Local cAprovados  := "approved,authorized"
    
    If Empty(SC5->C5_YTOKEN)
        MsgAlert("Esse pedido n�o possui token de autoriza��o com cart�o, n�o � poss�vel solicitar autoriza��o.","Aten��o")
        Return
    ElseIf Alltrim(SC5->C5_YSTATMP) $ cAprovados 
        ApMsgInfo('O pedido j� foi aprovado/autorizado')
    Else
        MsAguarde({|| U_IFAF0005() }, 'Aguarde...')
    EndIf

    If Alltrim(SC5->C5_YSTATMP) $ cAprovados
        MsAguarde({|| geraNCC()}, 'Verificando NCC...')
    EndIf

Return

Static Function geraNCC
    Local cQuery, nVlrNCC 
    Local aDados        := {}
    Local cNatureza     := SuperGetMv('MS_NATNCC', .F., 'CREDITO')
    Local nDiaVcto      := SuperGetMv('MS_DIASNCC', .F., 30)

    Private lMsErroAuto := .F.

    cQuery := " SELECT R_E_C_N_O_ AS RECNOSE1 FROM " + RetSqlTab('SE1')
    cQuery += " WHERE " + RetSqlDel('SE1')
    cQuery += " AND E1_YIDMP = '" + SC5->C5_YIDMP + "' "

    If Select('QRY') > 0
        QRY->(DbCloseArea())
    EndIf
    
    TcQuery cQuery New Alias 'QRY'

    If QRY->(Eof())
        
        cNumNCC := u_uGetSXENum("SE1","E1_NUM")

		nVlrNCC := StaticCall(IFAF0005, totalPedido)
        
        aAdd(aDados, {"E1_PREFIXO" 	, 'NCC'					,Nil}) 
		aAdd(aDados, {"E1_NUM"	  	, 'NCC' + cNumNCC       ,Nil})
		aAdd(aDados, {"E1_PARCELA" 	, '001'					,Nil})
		aAdd(aDados, {"E1_TIPO"	  	, 'NCC'					,Nil})
		aAdd(aDados, {"E1_NATUREZ" 	, cNatureza				,Nil})
		aAdd(aDados, {"E1_CLIENTE" 	, SC5->C5_CLIENTE		,Nil})
		aAdd(aDados, {"E1_LOJA"	  	, SC5->C5_LOJACLI		,Nil})
		aAdd(aDados, {"E1_EMISSAO" 	, dDataBase		    	,Nil})
		aAdd(aDados, {"E1_VENCTO"  	, dDataBase + nDiaVcto	,Nil})
		aAdd(aDados, {"E1_VALOR"	, nVlrNCC 				,Nil})    
		aAdd(aDados, {"E1_YIDMP" 	, SC5->C5_YIDMP 		,Nil})    
		aAdd(aDados, {"E1_YCODSC5" 	, SC5->C5_NUM   		,Nil})    

		lMsErroAuto := .F.			                                 					                                         	
		
		MsExecAuto( { |x,y,z| FINA040(x,y)}, aDados, 3)		
 
 		If lMsErroAuto            
			MsgAlert('Erro na gera��o de NCC para o cliente.', 'Aten��o')
            MostraErro()
		Else
            ApMsgInfo('Gerado a NCC ' + SE1->E1_NUM + ' para o cliente. ')
        EndIf

    EndIf

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} M410MNUd
Rotina que abre o wattsapp web para envio de mensagem   
@author  Sidney Sales
@since   12/06/19
@version 1.0
/*/
//-------------------------------------------------------------------
User Function M410MNUd
    Local cURL
    
    SA1->(DbSetOrder(1))
    SA1->(DbSeek(xFilial('SA1') + SC5->C5_CLIENTE + SC5->C5_LOJACLI))    
    
    cURL := "https://api.whatsapp.com/send?phone=55"+Alltrim(SA1->A1_TEL)+"&text=Ol� " + Alltrim(Alltrim(SA1->A1_NOME)) + ', tudo bem?'    
    ShellExecute( "Open", cURL, "", "C:\", 1 )
    
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} M410MNUe
Funcao que remove o pedido de um rateio
@author  Sidney
@since   05/11/19
@version 1.0
/*/
//-------------------------------------------------------------------
User Function M410MNUe
    Local aAreaZ11  := Z11->(GetArea())    
    Z11->(DbSetOrder(1))

    If SC5->C5_YSTATUS == 'A' 
        If SC5->C5_YSEPAR == 'S'
            If Z11->(DbSeek(xFilial('Z11') + SC5->C5_YCODZ11)) .AND. Z11->Z11_STATUS == 'F'
                MsgAlert('N�o � poss�vel remover o pedido da separa��o ' + SC5->C5_YCODZ11 + ' pois a mesma j� foi finalizada.')
                Return
            EndIf            
            If MsgYesNo('Tem certeza que deseja remover o pedido da separa��o ' + SC5->C5_YCODZ11 + '?')
                RecLock('SC5',.F.)
                    SC5->C5_YSEPAR  := Space(TAMSX3("C5_YSEPAR")[1])
                    SC5->C5_YCODZ11 := Space(TAMSX3("C5_YCODZ11")[1])
                SC5->(MsUnLock())
                ApMsgInfo('Pedido removido com sucesso.')
            Endif
        Else
            MsgAlert('Esse pedido ainda n�o est� em separa��o.')
        EndIf 
    Else
        MsgAlert('O pedido ainda n�o foi aceito, ent�o ele n�o pode estar em uma separa��o.')
    EndIf

    RestArea(aAreaZ11)

Return

Static Function jaACeitou(cC5NUM)
    Local cQuery 
    Local lRet := .F.
    
    cQuery := " SELECT Z08_CODSC5 FROM " + RetSqlTab('Z08')
    cQuery += " WHERE RTRIM(Z08_DE) = 'Pendente' AND RTRIM(Z08_PARA) = 'Aceito' "
    cQuery += " AND D_E_L_E_T_ = '' "
    cQuery += " AND Z08_CODSC5 = '" + cC5NUM + "' "

    If Select('QRYC5') > 0
        QRYC5->(DbCloseArea())
    EndIf

    TcQuery cQuery New Alias 'QRYC5'

    If QRYC5->(!Eof())
        lRet := .T.
    EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} M410MNUf
    Rediraciona para o contas a receber do pedido
    @type  Static Function
    @author Samuel Dantas
    @since   26/06/2020
    @version 1.0
/*/
//-------------------------------------------------------------------
User Function M410MNUf()
    Local aAreaSE1 := SE1->(GetArea())
    Local aAreaSC5 := SC5->(GetArea())
    Local aAreaSC6 := SC6->(GetArea())
    
    cQuery := " SELECT R_E_C_N_O_ AS RECNO FROM "  + RetSqlName('SE1') + " SE1"
    cQuery += " WHERE SE1.D_E_L_E_T_ <> '*' AND E1_FILIAL ='"+xFilial('SE1')+"' AND E1_PEDIDO = '"+SC5->C5_NUM+"' "
    
    If Select('QRY') > 0
        QRY->(dbclosearea())
    EndIf
    
    TcQuery cQuery New Alias 'QRY'
    
    If QRY->(!Eof())
        
        If QRY->RECNO > 0
            SE1->(DbGoTo(QRY->RECNO))
            FinA740()
        EndIf

        QRY->(dbSkip())
    EndIf

    SE1->(RestArea(aAreaSE1))
    SC5->(RestArea(aAreaSC5))
    SC6->(RestArea(aAreaSC6))

Return

User Function TestmSG()
    Local cNomeEntrega := ""
    Local cNomeCli := ""
    If Empty(FunName())
        PREPARE ENVIRONMENT EMPRESA '01' FILIAL '01'
    EndIf
    cCodMsgAc := Alltrim(SuperGetMv('MS_MSGACEI', .F., '000011')) // C�digo da mensagem de aceito (Z33)
    cCodMsgEn := Alltrim(SuperGetMv('MS_MSGENT', .F., '000012')) // C�digo da mensagem de saiu p/entrega (Z33)
    cNomeEntrega := Alltrim(SuperGetMv('MS_ENTREGA', .F., 'Wisner'))
    SC5->(DbSetOrder(1))
    SC5->(DbSeek(xFilial("SC5") + "010272" ))
    
    SA1->(DbSetOrder(1))            
    SA1->(DbSeek(xFilial('SA1') + SC5->C5_CLIENTE + SC5->C5_LOJACLI))
    nPos     := At(' ',SA1->A1_NOME)
    cNomeCli := Alltrim(SubStr(SA1->A1_NOME, 1, nPos))

    Z09->(DbSetOrder(2))            
    Z09->(DbSeek(xFilial("Z09") + SC5->C5_YCODZ09 ))
    Z33->(DbSetOrder(1))
    //Pedido Aceite
    If Z33->(DbSeek(xFilial("Z33") + cCodMsgAc))
        cMsgAcei := &(Alltrim(Z33->Z33_MSG))
    EndIf
    //Pedido Saiu para entrega
    If Z33->(DbSeek(xFilial("Z33") + cCodMsgEn))
        cMsgSaiu := &(Alltrim(Z33->Z33_MSG))
    EndIf
    
Return
