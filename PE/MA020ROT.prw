//-------------------------------------------------------------------
/*/{Protheus.doc} MA020ROT
Ponto de entra para adicionar uma a��o a tela de Fornecedor
@author  Samuel Dantas  
@since   11/12/2018
@version version
/*/
//-------------------------------------------------------------------
User Function MA020ROT
	Local aRotAux	:= {}
	aAdd(aRotAux,{ "Produto p/ associado"	, "U_IFAA0003()" , 0, 2, 0, Nil})
	aAdd(aRotAux,{ "Estoque Venda"			, "U_IFAA0005()" , 0, 2, 0, Nil})
	aAdd(aRotAux, {"Anexos","u_Anexos('SA2ANEXOS',SA2->A2_FILIAL + '-' + Alltrim(SA2->A2_COD) + '-' + Alltrim(SA2->A2_LOJA))", 0, 4, , .F. })
Return aRotAux


