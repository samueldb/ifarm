#INCLUDE 'Rwmake.ch'
#INCLUDE 'Protheus.ch'
#INCLUDE 'TbIconn.ch'
#INCLUDE 'Topconn.ch'

//-------------------------------------------------------------------
/*/{Protheus.doc} M020INC
Ponto de entrada na inclusao do fornecedor. Cria um armazem
e vincula ao mesmo. 
@author  Sidney Sales
@since   18/12/2018
@version 1.0
/*/
//-------------------------------------------------------------------

User Function M020INC()          
    Local aAreaNNR := NNR->(GetArea())
    
    If MsgYesNo('Deseja cadastrar o armaz�m para o vendedor?')
        NNR->(dbSetOrder(1))
        
        cNNR_CODIGO := GetSxeNum('NNR', 'NNR_CODIGO')
        ConfirmSx8()
        
        If Empty(SA2->A2_YLOCAL)
            RecLock('NNR',.T.)
                NNR->NNR_FILIAL	:= xFilial('NNR')
                NNR->NNR_CODIGO	:= cNNR_CODIGO
                NNR->NNR_DESCRI	:= 'Armazem ' + SA2->A2_NREDUZ
                NNR->NNR_TIPO	:= '1'
            NNR->(msUnlock())
            RecLock('SA2', .F.)
                SA2->A2_YLOCAL := cNNR_CODIGO
            SA2->(MsUnlock())
        EndIf    
    EndIf
    
    RestArea(aAreaNNR)

Return

