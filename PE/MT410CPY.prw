//Bibliotecas
#INCLUDE 'Rwmake.ch'
#INCLUDE 'Protheus.ch'
#INCLUDE 'TbIconn.ch'
#INCLUDE 'Topconn.ch'

 
/*------------------------------------------------------------------------------------------------------*
 | P.E.:  MT410CPY                                                                                      |
 | Desc:  Na c�pia do Pedido de Venda, zera os campos especificos                                       |
 | Links: http://tdn.totvs.com/pages/releaseview.action�pageId=6784349                                  |
 *------------------------------------------------------------------------------------------------------*/
User Function MT410CPY()
    Local aArea        := GetArea()
    Local lRet            := .T.
    Local nPosQtd    := GdFieldPos("C6_YQTD")
    Local nPosVlr    := GdFieldPos("C6_YVALOR")
    Local nLinAtu        := 0
    
    //M->C5_YCODZ09 := Space(TAMSX3("C5_YCODZ09")[1])
    //M->C5_YFORMPG := Space(TAMSX3("C5_YFORMPG")[1])
    //M->C5_YCODZ11 := Space(TAMSX3("C5_YCODZ11")[1])
    //M->C5_YHRENT  := Space(TAMSX3("C5_YHRENT")[1])
    //M->C5_YDIAENT := Space(TAMSX3("C5_YDIAENT")[1])
    
    M->C5_YSTATUS := 'P'
    M->C5_YDTENTR := CtoD("") 
    M->C5_CONDPAG := SC5->C5_CONDPAG    
    M->C5_YMAPA   := Space(TAMSX3("C5_YMAPA")[1])
    M->C5_YSEPAR  := Space(TAMSX3("C5_YSEPAR")[1])
    M->C5_YDESCFR := 0
    M->C5_YDESCPE := 0
    M->C5_YTROCO  := 0
    M->C5_YENTREG := .F.
    M->C5_YOBS    := Space(TAMSX3("C5_YOBS")[1])
    M->C5_YTOKEN  := Space(TAMSX3("C5_YTOKEN")[1])
    M->C5_YSTATMP := Space(TAMSX3("C5_YSTATMP")[1])
    M->C5_YMETOD  := Space(TAMSX3("C5_YMETOD")[1])
    M->C5_YPARCEL := 0
    M->C5_YIDMP   := Space(TAMSX3("C5_YIDMP")[1])
    M->C5_YULTMSG := Space(TAMSX3("C5_YULTMSG")[1])
    M->C5_YDTFIM  := CtoD("") 
    M->C5_YCODZ12 := Space(TAMSX3("C5_YCODZ12")[1])
    M->C5_YHRSZ12 := Space(TAMSX3("C5_YHRSZ12")[1])
    M->C5_YCODZ24 := Space(TAMSX3("C5_YCODZ24")[1])
    M->C5_YCODZ11 := Space(TAMSX3("C5_YCODZ11")[1])
    
    //Percorrendo linhas da grid 
    For nLinAtu := 1 To Len(aCols)
        //Se encontrar o campo na grid, sobrep�e o valor
        If nPosQtd > 0
            aCols[nLinAtu][nPosQtd] := 0
        EndIf
        If nPosVlr > 0
            aCols[nLinAtu][nPosVlr] := 0
        EndIf
    Next nLinAtu
 
    RestArea(aArea)
Return lRet
