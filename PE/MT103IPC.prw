//-------------------------------------------------------------------
/*/{Protheus.doc} MT103IPC
Ponto de entrada para preenchimento da descri��o do produto na tela de prenota
@author  Sidney 
@since   07/03/19
@version 1.0
/*/
//-------------------------------------------------------------------

User Function MT103IPC
	Local nItem 	:= PARAMIXB[1]
	Local nPosCod 	:= AsCan(aHeader,{|x|Alltrim(x[2])=="D1_COD"})
	Local nPosDes 	:= AsCan(aHeader,{|x|Alltrim(x[2])=="D1_DESCRIC"})  

	If nPosCod > 0 .And. nItem > 0
		aCols[nItem,nPosDes] := SC7->C7_DESCRI
	Endif
	
Return