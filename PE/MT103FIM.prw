#include 'protheus.ch'
#include 'parmtype.ch'
#include 'tbiconn.ch'
#include 'topconn.ch'
#include "TOTVS.CH"

/*/{Protheus.doc} MT103FIM
    Ponto de entrada chamada depois da classifica��o de uma nota
    @type  User Function
    @author Samuel Dantas
    @since 18/12/2018
    @return 
 /*/
 User Function MT103FIM
    
    Local nOpcao := PARAMIXB[1]   // Op��o Escolhida pelo usuario no aRotina 
    Local nConfirma := PARAMIXB[2]   // Se o usuario confirmou a opera��o de grava��o da NFECODIGO DE APLICA��O DO USUARIO
    Local nPosPrd   :=  aScan(aHeader,{|x| AllTrim(x[2]) == 'D1_COD'})
    Local nPosDsc   :=  aScan(aHeader,{|x| AllTrim(x[2]) == 'D1_DESCRIC'})
    Local nPosQtd   :=  aScan(aHeader,{|x| AllTrim(x[2]) == 'D1_QUANT'})
    Local nPosPrc   :=  aScan(aHeader,{|x| AllTrim(x[2]) == 'D1_VUNIT'})
    Local nPosForn  := aScan(aHeader,{|x| AllTrim(x[2]) == 'D1_FORNECE'})
    Local nPosLoja  := aScan(aHeader,{|x| AllTrim(x[2]) == 'D1_LOJA'})
    Local nPosNum   := aScan(aHeader,{|x| AllTrim(x[2]) == 'D1_PEDIDO'})
    Local cMsg      := ""
    Local cDesc     := ""

    // Se for altera��o ou inclus�o e status confirmado, verifica modifica��es
    If (nOpcao == 3 .OR. nOpcao == 4)  .AND. nConfirma == 1
        SC7->(DbSetOrder(2)) // C7_FILIAL + C7_PRODUTO + C7_FORNECE + C7_LOJA + C7_NUM
        For nX :=1 To Len( aCols )     
            
            cProduto := aCols[nX][nPosPrd]
            cFornece := SF1->F1_FORNECE
            cLoja    := SF1->F1_LOJA
            cDscProd := aCols[nX][nPosDsc]
            cNum     := aCols[nX][nPosNum]
            nPrcUni  := aCols[nX][nPosPrc] 
            nQuant   := aCols[nX][nPosQtd]
            cMsg     := ""
            
            If SC7->(DbSeek( xFilial("SC7") + cProduto + cFornece + cLoja + cNum))
                //Verifica mudan�a em quantidade ou pre�o unit�rio
                If (SC7->C7_QUANT != nQuant ) .OR. (nPrcUni != SC7->C7_PRECO)
                    If (SC7->C7_QUANT != nQuant )
                        cDesc :=  " Diverg�ncia na quantidade do produto "
                    EndIf
                    If (nPrcUni != SC7->C7_PRECO)
                        cDesc :=  IIF(EMPTY(cDesc)," Diverg�ncia no pre�o do produto ", " Diverg�ncia no pre�o e na quantidade do produto ")
                    EndIf
                    cMsg += cDesc + CRLF
                    cMsg += " Foi realizada altera��o no produto : "+ cProduto + " - " + cDscProd + CRLF
                    cMsg += " Quantidade no pedido de compra: "+ cValToChar(SC7->C7_QUANT) +  CRLF
                    cMsg += " Quantidade na nota: "+cValToChar(nQuant) + ". " + CRLF
                    cMsg += " Pre�o unit�rio no pedido de compra: "+ cValToChar(SC7->C7_PRECO) +  CRLF
                    cMsg += " Pre�o unit�rio na nota: "+cValToChar(nPrcUni) + ". " + CRLF
                    RecLock('Z07', .T.)
                        Z07->Z07_FILIAL := xFilial("Z07")
                        Z07->Z07_CODIGO := GETSXENUM("Z07","Z07_CODIGO")       
                        Z07->Z07_DESC   := cDesc       
                        Z07->Z07_MEMO   := cMsg
                        Z07->Z07_SC7NUM := cNum
                        Z07->Z07_STATUS := "1"
                    Z07->(MsUnLock())
                    ConfirmSx8()
                EndIf

            EndIf

        Next nX
    EndIf

    If !Empty(cDesc)
        MsgAlert("Aten��o, houve diverg�ncia entre o pedido e a nota de entrada. A diverg�ncia foi adicionada a lista de a��es.", "Aten��o")
    EndIf

Return 