//-------------------------------------------------------------------
/*/{Protheus.doc} FA070POS
Ponto de entrada para alteracao do banco agencia e conta que sera
baixado
@author  Sidney Sales
@since   13/03/2020
@version 1.0
/*/
//-------------------------------------------------------------------
User Function FA070POS()
     
     cBanco     := SuperGetMV('MS_BANCO', .F., '001' )
     cAgencia   := SuperGetMV('MS_AGENCIA', .F., '2870' )
     cConta     := SuperGetMV('MS_CONTA', .F., '20767' )

Return