#INCLUDE 'Rwmake.ch'
#INCLUDE 'Protheus.ch'
#INCLUDE 'TbIconn.ch'
#INCLUDE 'Topconn.ch'

//-------------------------------------------------------------------
/*/{Protheus.doc} function
Ponto de entra para inclus�o de a��es no cadastro de produtos
@author  Samuel Dantas Batista
@since   11/12/2018
@version version
/*/
//-------------------------------------------------------------------
User function MT010BRW  ()
    Local aRotUser	:= {}
	aAdd(aRotUser,{ "Foto"	                , "U_fImages()"     , 0, 4, 0, Nil})
Return aRotUser

//-------------------------------------------------------------------
/*/{Protheus.doc} fHistPrc
Tela para mostrar o hist�rico de altera��es de pre�os dos protudos
@author  Sidney Sales
@since   26/12/2018
@version 1.0
/*/
//-------------------------------------------------------------------
User Function fHistPrc
    Private cCadastro := 'Hist�rico de Altera��o de Pre�os'

    Private aRotina := {{'Pesquisar' ,'AxPesqui',0,1} ,;
                       {'Visualizar' ,'AxVisual',0,2} }

    Private cDelFunc := '.T.' // Validacao para a exclusao. Pode-se utilizar ExecBlock

    Private cString := 'Z10'
    DbSelectArea(cString)
    DbSetOrder(1)
    DbSelectArea(cString)

    set filter to Z10->Z10_CODSB1 == SB1->B1_COD
    
    mBrowse( 6,1,22,75,cString)

    set filter to


Return

User Function fImages
    Local cEnd           := Alltrim(SuperGetMV('MS_ENDHTTP',.F.,'http://187.111.253.19:9051'))            
    Local cDirWeb        := Alltrim(SuperGetMV('MS_DIRIMGS',.F.,'/web/'))
    Local cDirLocal      := StrTran(cDirWeb, '/','\')
    
    Local cProd          := cFilAnt + '-'+ SB1->B1_COD
    Private lNotFound := .F.
    Private cNomeImagem := cProd + '.png'

    cPagina     := cEnd + '/pages/' + cProd + ".html"
    cNotFound   := cEnd + '/imgs/'+ "404.png"
    
    criahtml(cDirLocal, cEnd, cProd)
    
    If !File(cDirLocal + 'imgs\' + cNomeImagem)
        __CopyFile(cNotFound,cDirLocal + 'imgs\' + cNomeImagem)
    EndIf
    
    DEFINE DIALOG oDlg TITLE "Imagem" FROM 180,180 TO 550,700 PIXEL
      
    
        oTIBrowser := TIBrowser():New(0,0,260,170,cPagina,oDlg)	
        
        oTIBrowser:Align := CONTROL_ALIGN_ALLCLIENT

        TButton():New( 172, 052, "Inserir/Alterar", oDlg,;
            {|| InserirAlterar(cDirLocal), oTIBrowser:Navigate(cPagina) },40,010,,,.F.,.T.,.F.,,.F.,,,.F. )
              
        TButton():New( 172, 102, "Excluir", oDlg,;
            {|| Excluir(cDirLocal) },40,010,,,.F.,.T.,.F.,,.F.,,,.F. )
             
        // Dispon�vel apenas em vers�es superiores a 7.00.170117A - 17.2.0.2
        TButton():New( 172, 152, "Sair", oDlg,;
            {|| oDlg:End() },40,010,,,.F.,.T.,.F.,,.F.,,,.F. )
              
    ACTIVATE DIALOG oDlg CENTERED


Return

//-------------------------------------------------------------------
/*/{Protheus.doc} inseriralterar
Insere ou altera uma imagem de um produto
@author Sidney Sales
@since   17/01/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function inseriralterar(cDir)
    Local cImgFile      := cGetFile("Imagem PNG|*.png","Abrir imagem do disco",0,"\",.F., NIL ,.T.)
    
    __CopyFile(cImgFile,cDir + '\imgs\' + cNomeImagem)

Return 

Static Function criahtml(cDirLocal, cEnd, cProd)
    Local cHtml
    
    cHtml := '<!DOCTYPE HTML>'
    cHtml += '<html>'
    cHtml += '<head>'
    cHtml += '<title>Title of the document</title>'
    cHtml += '</head>'
    cHtml += '<body>'
    cHtml += '<img src="'+cEnd+'/imgs/'+cProd + '.png">'
    cHtml += '</body>'
    cHtml += '</html>'

    MemoWrit(cDirLocal + 'pages\' + cProd + '.html', cHtml)

return

Static function Excluir (cDir)
    oDlg:End()
    FErase( cDir + 'pages\' + StrTran(cNomeImagem,"png","html"))
    FErase( cDir + 'imgs\' + cNomeImagem)

Return