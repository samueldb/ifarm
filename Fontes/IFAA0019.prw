
#Include 'Protheus.ch'
#Include 'Parmtype.ch'
#include "FWMVCDEF.CH"
#include "topconn.CH"

/*/{Protheus.doc} IFAA0019
Tela de categorias do associado
@author Samuel Dantas
@since 03/01/2019
@version 1.0
/*/
User Function IFAA0019()
	Local oBrowse
	Private cString 		:= "Z23"  

	//Montagem do Browse principal	
	oBrowse := FWMBrowse():New()

	//Define alias principal
	oBrowse:SetAlias(cString)
	oBrowse:SetDescription('Categorias do associado')
	oBrowse:SetMenuDef('IFAA0019')

	oBrowse:Activate()
Return

/*/{Protheus.doc} MenuDef
Retorna o menu principal
@author Samuel Dantas
@since 03/01/2019
@version undefined
@return aRotina, Array com os dados para os botoes do browse
/*/ 
Static Function MenuDef()
	Local aRotina := {}
    
    aAdd( aRotina, { 'Visualizar'		, 'VIEWDEF.IFAA0019'	, 0, 2, 0, NIL } ) 
    aAdd( aRotina, { 'Incluir' 			, 'VIEWDEF.IFAA0019'	, 0, 3, 0, NIL } )
    aAdd( aRotina, { 'Alterar' 			, 'VIEWDEF.IFAA0019'	, 0, 4, 0, NIL } )
    aAdd( aRotina, { 'Excluir' 			, "VIEWDEF.IFAA0019"	, 0, 5, 0, NIL } )

Return aRotina

/*/{Protheus.doc} ModelDef
Construcao do modelo de dados
@author Samuel Dantas
@since 03/01/2019
@version 1.0
@return oModel, Retorna o objeto do modelo de dados
/*/
Static Function ModelDef()
	Local oModel
	Local oStruZ23 := FWFormStruct(1,"Z23")	

    oModel := MPFormModel():New("IFAA019", /*bPreValidacao*/,, /* GravaDados */, /*bCancel*/ )

	//Cria a estrutura principal(Z23)
	oModel:addFields('MASTERZ23',,oStruZ23)
	
	//Adiciona a chave
	oModel:SetPrimaryKey({'Z23_FILIAL', 'Z23_CODIGO'})
	
	//Define a descricao dos modelos
	oModel:GetModel( 'MASTERZ23' ):SetDescription( 'Categorias do associado' )

    // AntesDeTudo
    // oModel:SetVldActivate( {|oModel| .T. } )

Return oModel

/*/{Protheus.doc} ViewDef
Monta o view do modelo
@author Samuel Dantas
@since 03/01/2019
@version 1.0
/*/
Static Function ViewDef()
	Local oModel := ModelDef()
	Local oView
	Local oStrZ23:= FWFormStruct(2, 'Z23')

	oView := FWFormView():New()
	oView:SetModel(oModel)

    //Adiciona no nosso View um controle do tipo FormFields(antiga enchoice)
	oView:AddField('FORM_Z23' , oStrZ23,'MASTERZ23' ) 

    // 30% cabec e 70% para as abas
	oView:CreateHorizontalBox('SUPERIOR', 100)	
	
	oView:SetOwnerView('FORM_Z23', 'SUPERIOR')

Return oView