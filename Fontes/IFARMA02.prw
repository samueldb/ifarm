#INCLUDE 'Rwmake.ch'
#INCLUDE 'Protheus.ch'
#INCLUDE 'TbIconn.ch'
#INCLUDE 'Topconn.ch'
//-------------------------------------------------------------------
/*/{Protheus.doc} IFARMA02
    Tela para monitoramento de status dos pedidos
    @type  Static Function
    @author Samuel Dantas
    @since   06/10/2020
    @version 1.0
/*/
//-------------------------------------------------------------------
User Function IFARMA02()
    Local oDlg
    Local aHeader   := {}
    Local aCols     := {}
    Local cTitulo   := ''
	Local aRet		:= {} 
    Local oLayer    := FWLayer():New()
    Local cStatus   := Alltrim(SuperGetMv("MS_STATUS",.F., "P,A,E,S")) //P=Pendente;A=Aceito;S=Saiu para Entrega;F=Finalizado;R=Recusado;E=Em Separacao;G=Aguardando
    Private oBrw
    Private aPedsProd   := {}
    Private cPerg := PadR('U_IFARMA02', 10)

    If Empty(FunName())
        PREPARE ENVIRONMENT EMPRESA '01' FILIAL '01'
    EndIf
    
    ValidPerg()

    If ! Pergunte(cPerg, .T.)
        Return
    EndIf
    
    Z01->(DbSeek(xFilial('Z01')))

    aHeader := {}
    aCols   := {}


    aHeader := GetStatus()[1]
    aCols := GetStatus()[2]

	oDlg := FwDialogModal():New()
	
	oDlg:SetTitle(cTitulo)
	
	oDlg:enableAllClient()
	oDlg:CreateDialog()	

	oPanelModal	:= oDlg:GetPanelMain()

    //Cria o layer na tela
    oLayer:init(oPanelModal,.F.)

    //Panel01
    oLayer:addLine('lin01',100,.F.)
    oLayer:addCollumn('col01',100,.F.,'lin01')
    oLayer:addWindow('col01','win01','Produtos',100,.F.,.F.,{|| }, 'lin01',{|| })
    oPanel01 := oLayer:getWinPanel('col01','win01', 'lin01')

    oBrw:= MsNewGetDados():New(050,010,200,390,0,/*Fun��o LinhaOk*/,/*Fun��o TudoOk*/,/*cIniPos*/,,,999,,,,oPanel01,aHeader,aCols)
    oBrw:oBrowse:Refresh()
    oBrw:oBrowse:Align := CONTROL_ALIGN_ALLCLIENT

	oDlg:AddButton('Atualizar', {|| MSAguarde( { || AtuDados() }, "Aguarde" ,"Atualizando", .F.) }, 'Atualizar',, .T., .T., .T., ) 
	oDlg:AddButton('Ver Pedidos', {|| VerPedidos(oBrw:Acols[oBrw:nAt][1]) }, 'Ver Pedidos',, .T., .T., .T., ) 
    
    oDlg:Activate()

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} GetStatus
    Retorna o aCols e o aHeader para constru��o da tela
    @type  User Function
    @author Samuel Dantas
    @since   06/10/2020
    @version 1.0
/*/
//-------------------------------------------------------------------
Static Function GetStatus(cStatus)
    Local aStatus   
    Local nI        := 1
    Local aHeader   := {}
    Local aProdutos := {}
    Local aCols     := {}

    If Empty(FunName())
        PREPARE ENVIRONMENT EMPRESA '01' FILIAL '01'
    EndIf

    cStatus   := Alltrim(SuperGetMv("MS_STATUS",.F., "P,A,E,S")) //P=Pendente;A=Aceito;S=Saiu para Entrega;F=Finalizado;R=Recusado;E=Em Separacao;G=Aguardando
    aStatus   := StrTokArr(cStatus,",")
    
    //Array usado para busca a descri��o do status
    aCombo := RetSX3Box(GetSX3Cache("C5_YSTATUS", "X3_CBOX"),,,1)

    aAdd(aHeader,{ "Produto"    , "C6_PRODUTO"   ,"",15,0,"","�","C","","V" })
    aAdd(aHeader,{ "Descricao"  , "C6_DESCRI"   ,"",15,0,"","�","C","","V" })

    For nI := 1 to len(aStatus)
        nPos := aScan(aCombo,{|x| AllTrim(x[2]) == aStatus[nI]})
        
        If nPos > 0
            aAdd(aHeader,{ Alltrim(aCombo[nPos][3])    , aStatus[nI]   ,"",15,0,"","�","N","","V" })
        EndIf
        
    Next
    
    aAdd(aHeader,{ "Total"    , "total"   ,"",15,0,"","�","N","","V" })

    aProdutos   := GetProdutos(cStatus)
    aCols       := GetACols(aProdutos, aHeader)

Return { aHeader, aCols }

//-------------------------------------------------------------------
/*/{Protheus.doc} AtuDados
    Atualiza informa��es da tela
    @type  User Function
    @author Samuel Dantas
    @since   06/10/2020
    @version 1.0
/*/
//-------------------------------------------------------------------
Static Function AtuDados()
    Local cStatus   := Alltrim(SuperGetMv("MS_STATUS",.F., "P,A,E,S")) //P=Pendente;A=Aceito;S=Saiu para Entrega;F=Finalizado;R=Recusado;E=Em Separacao;G=Aguardando
    Local aDados    := GetStatus(cStatus)
    
    oBrw:aCols := aDados[2]
    oBrw:oBrowse:Refresh()

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} GetProdutos
    Busca a quantidade de pedidos por status. Agrupado por produto e
    status.
    @type  User Function
    @author Samuel Dantas
    @since   06/10/2020
    @version 1.0
/*/
//-------------------------------------------------------------------
Static Function GetProdutos(cStatus)
    Local aStatus   := StrTokArr(cStatus,",")
    Local nJ        := 1
    Local aRet      := {}
    Default cStatus   := ""
    cStatus := ""
    
    For nJ := 1 To len(aStatus)
        cStatus += IIF(Empty(cStatus), "'"+aStatus[nJ]+"'", ",'"+aStatus[nJ]+"'" )
    Next

    cQry := " SELECT C6_PRODUTO, C6_DESCRI, C5_YSTATUS, SUM(C6_QTDVEN) AS QTD FROM "  + RetSqlName('SC5') + " SC5"
    cQry += " INNER JOIN "  + RetSqlName('SC6') + " SC6 ON C5_FILIAL = C6_FILIAL AND C5_NUM = C6_NUM AND SC5.D_E_L_E_T_ =  SC6.D_E_L_E_T_ "
    cQry += " WHERE SC5.D_E_L_E_T_ <> '*' AND C5_YSTATUS IN ("+cStatus+") "
    cQry += " AND C5_EMISSAO BETWEEN '"+DtoS(MV_PAR01)+"' AND '"+DtoS(MV_PAR02)+"' "
    cQry += " GROUP BY C6_PRODUTO, C6_DESCRI, C5_YSTATUS ORDER BY C6_DESCRI "
    
    If Select('QRY') > 0
        QRY->(dbclosearea())
    EndIf
    
    TcQuery cQry New Alias 'QRY'
    
    While QRY->(!Eof())
        
        If len(aRet) > 0
            nPos := aScan(aRet,{|x| AllTrim(x[1]) == QRY->C6_PRODUTO})
            If nPos > 0
                aAdd(aRet[nPos][2], {QRY->C5_YSTATUS,QRY->QTD} )
            Else
                aAdd(aRet, { Alltrim(QRY->C6_PRODUTO), {{QRY->C5_YSTATUS,QRY->QTD}} })
            EndIf
            
        Else
            aAdd(aRet, { Alltrim(QRY->C6_PRODUTO), {{QRY->C5_YSTATUS,QRY->QTD}} })
        EndIf

        QRY->(dbSkip())
    EndDo
    
Return aRet


//-------------------------------------------------------------------
/*/{Protheus.doc} GetACols
    Organiza os produtos na estrutura do aCols e retorna o aCols.
    @type  User Function
    @author Samuel Dantas
    @since   06/10/2020
    @version 1.0
/*/
//-------------------------------------------------------------------
Static Function GetACols(aProdutos, aHeader)
    Local nX :=  1
    Local nJ :=  1
    Local aRet :=  {}
    Local aCols :=  {}
    Local nTotal
    Default aProdutos := {}

    For nX := 1 to len(aProdutos)
        aCols   :=  {}
        aSts    := aProdutos[nX][2]
        nTotal  := 0
        aAdd(aCols, aProdutos[nX][1] )
        aAdd(aCols, Alltrim(Posicione("SB1", 1, xFilial("SB1") + PADR(aProdutos[nX][1],LEN(SB1->B1_COD)),"B1_DESC")))
        
        For nJ := 3 to len(aHeader)
            nPos := aScan(aSts, {|x| AllTrim(x[1]) == aHeader[nJ][2] })
            If nPos > 0
                nTotal += aSts[nPos][2]
                aAdd(aCols,aSts[nPos][2])
            Else
                aAdd(aCols, 0 )
            EndIf
        Next

        aCols[len(aCols)] := nTotal 
        
        aAdd(aCols, .F. )
        aAdd(aRet, aCols)
    Next

Return aRet

//-------------------------------------------------------------------
/*/{Protheus.doc} VerPedidos
    Tela de visualiza��o dos pedidos a patir da linha posicionada.
    @type  User Function
    @author Samuel Dantas
    @since   06/10/2020
    @version 1.0
/*/
//-------------------------------------------------------------------
Static Function VerPedidos(cProduto)
    Local oDlg      
    Local oPanel01
    Local oPanelModal
    Local oLayer
    Local oBrwPeds
    Local i
    Local aHeader  := {}
    Local aCols    := {}
    
    //Cabecalho da grid de titulos
    aAdd(aHeader,{'Pedido'			,10, ''				})
    aAdd(aHeader,{'Cliente'	        ,50, ''	            })
    aAdd(aHeader,{'Quantidade'		,10, '' 		    })
    aAdd(aHeader,{'Status'		,10, '' 		    })
    SC5->(DbSetOrder(1))
    SA1->(DbSetOrder(1))
    SB1->(DbSetOrder(1))
    
    nTotProds := 0
    If !Empty(cProduto)
        aPedsProd := GetPedPro(cProduto)
    EndIF

    For i := 1 to Len(aPedsProd)
        If aPedsProd[i][2] == cProduto
            If SC5->(Dbseek(xFilial('SC5') + aPedsProd[i][1]))
                SB1->(DbSeek(xFilial('SB1') + cProduto))
                SA1->(DbSeek(xFilial('SA1') + SC5->C5_CLIENTE + SC5->C5_LOJACLI))
                nTotProds += aPedsProd[i][3]
                aAdd(aCols, {SC5->C5_NUM, SA1->A1_NOME, aPedsProd[i][3], aPedsProd[i][4] })
            EndIf
        EndIf
    Next

    oDlg := FwDialogModal():New()
    oDlg:SetTitle('Pedidos com o Produto: ' +  Alltrim(SB1->B1_DESC) + ' | TOTAL: ' + cValToChar(nTotProds) )
    oDlg:SetSize(250,400)
    oDlg:CreateDialog()	
    oDlg:addCloseButton( , "Fechar")
    oPanelModal	:= oDlg:GetPanelMain()

    oLayer := FwLayer():New()
    oLayer:Init(oPanelModal)

    //Linha dos Filtros
    oLayer:addLine("lin01",100,.T.)		
    oLayer:AddCollumn('col01', 100, .T., 'lin01')	
    oLayer:addWindow("col01","win01",'Pedidos',100,.F.,.F.,{|| }, "lin01",{|| })
    oPanel01 := oLayer:getWinPanel('col01', 'win01', "lin01")

    oBrwPeds 	:= uFwBrowse():create(oPanel01,,aCols,,aHeader,, ,, .F.)	

    oBrwPeds:disableConfig()
    oBrwPeds:disableFilter()
    oBrwPeds:disableReport()

    oBrwPeds:Activate()

    //Ativa a tela principal
    oDlg:Activate()	

Return


//-------------------------------------------------------------------
/*/{Protheus.doc} GetPedPro
    Busca pedidos a partir da linha selecionada.
    @type  Static Function
    @author Samuel Dantas
    @since   06/10/2020
    @version 1.0
/*/
//-------------------------------------------------------------------
 Static Function GetPedPro(cCodProd)
    Local cStatus   := Alltrim(SuperGetMv("MS_STATUS",.F., "P,A,E,S"))
    Local aStatus   := StrTokArr(cStatus,",")
    Local nJ        := 1
    Local aRet      := {}
    Local aPedsProd := {}
    Local cQuery    := ""
    
    cStatus := ""
    For nJ := 1 To len(aStatus)
        cStatus += IIF(Empty(cStatus), "'"+aStatus[nJ]+"'", ",'"+aStatus[nJ]+"'" )
    Next

    //Retorna todos os pedidos, produtos e quantidades ainda nao separados
    cQuery := " SELECT C6_NUM, C6_PRODUTO, C6_DESCRI, C6_QTDVEN, C5_YSTATUS, C6_VALOR FROM " + RetSqlTab("SC6")
    cQuery += " INNER JOIN " + RetSqlTab('SC5') + " ON C5_FILIAL = C6_FILIAL AND C5_NUM = C6_NUM AND SC5.D_E_L_E_T_ = SC6.D_E_L_E_T_ "
    cQuery += " WHERE SC5.D_E_L_E_T_ <> '*' "
    cQuery += " AND C6_PRODUTO = '" + cCodProd + "' " 
    cQuery += " AND C5_YSTATUS IN ("+cStatus+") "
    cQuery += " AND C5_EMISSAO BETWEEN '"+DtoS(MV_PAR01)+"' AND '"+DtoS(MV_PAR02)+"' "

    If Select('QSC6') > 0
        QSC6->(dbclosearea())
    EndIf

    TcQuery cQuery New Alias 'QSC6'
    
    aCombo := RetSX3Box(GetSX3Cache("C5_YSTATUS", "X3_CBOX"),,,1)
    
    //Percorre agrupando produtos iguais, somando as quantidades.
    While QSC6->(!Eof())
        cStatus := QSC6->C5_YSTATUS
        nPos := aScan(aCombo,{|x| AllTrim(x[2]) == Alltrim(cStatus) })
        
        If nPos > 0
            cStatus := Alltrim(aCombo[nPos][3])
        EndIf

        aAdd(aPedsProd, {QSC6->C6_NUM, QSC6->C6_PRODUTO, QSC6->C6_QTDVEN, cStatus})
                        
        QSC6->(DbSkip())

    EndDo
    
Return aPedsProd

//-------------------------------------------------------------------
/*/{Protheus.doc} ValidPerg
Criacao das perguntas do relatorio
@author  author
@since   06/10/2020
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ValidPerg
    Local aRegs    := {}
    Local aAreaSX1 := SX1->(GetArea())
    Local i, j

    SX1->(DbSetOrder(1))

    // Numeracao dos campos:
    // 01 -> X1_GRUPO   02 -> X1_ORDEM    03 -> X1_PERGUNT  04 -> X1_PERSPA  05 -> X1_PERENG
    // 06 -> X1_VARIAVL 07 -> X1_TIPO     08 -> X1_TAMANHO  09 -> X1_DECIMAL 10 -> X1_PRESEL
    // 11 -> X1_GSC     12 -> X1_VALID    13 -> X1_VAR01    14 -> X1_DEF01   15 -> X1_DEFSPA1
    // 16 -> X1_DEFENG1 17 -> X1_CNT01    18 -> X1_VAR02    19 -> X1_DEF02   20 -> X1_DEFSPA2
    // 21 -> X1_DEFENG2 22 -> X1_CNT02    23 -> X1_VAR03    24 -> X1_DEF03   25 -> X1_DEFSPA3
    // 26 -> X1_DEFENG3 27 -> X1_CNT03    28 -> X1_VAR04    29 -> X1_DEF04   30 -> X1_DEFSPA4
    // 31 -> X1_DEFENG4 32 -> X1_CNT04    33 -> X1_VAR05    34 -> X1_DEF05   35 -> X1_DEFSPA5
    // 36 -> X1_DEFENG5 37 -> X1_CNT05    38 -> X1_F3       39 -> X1_GRPSXG

    aAdd(aRegs, {cPerg, "01", "Data de?"  , "", "", "mv_ch1", 'D', 8, 0, 0, 'G', "", "MV_PAR01", "",  "", "", "", "", "",    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""})
    aAdd(aRegs, {cPerg, "02", "Data at�?" , "", "", "mv_ch2", 'D', 8, 0, 0, 'G', "", "MV_PAR02", "",  "", "", "", "", "",    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""})

    For i := 1 To Len(aRegs)
        If ! SX1->(DbSeek(cPerg+aRegs[i,2]))
            RecLock("SX1", .T.)

            For j :=1 to SX1->(FCount())
                If j <= Len(aRegs[i])
                    SX1->(FieldPut(j,aRegs[i,j]))
                EndIf
            Next

            SX1->(MsUnlock())
        EndIf
    Next

    SX1->(RestArea(aAreaSX1))
Return
