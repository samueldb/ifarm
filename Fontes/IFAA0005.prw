#Include 'Protheus.ch'
#Include 'TbIconn.ch'
#Include 'Topconn.ch'
//-------------------------------------------------------------------
/*/{Protheus.doc} IFAA0005
Tela para atualiza��o de estoque para cada produto relacionado a um 
fornecedor
@author  Samuel Dantas
@since   12/12/2018
/*/
//-------------------------------------------------------------------
User Function IFAA0005()
	Local oDlg
	Local aHeader			:= {}
	Local aCols 			:= {}
	Local aAux 				:= {}
	Local cRet				:= ''
	Local cTitulo			:= ''
	Local lRet				:= .F.
	Local nX 				:= 980
	Local nY 				:= 560
	Private oLayer			:= FWLayer():New()

    //Posi��es no aCols
    Private nCodSB1         := 2
    Private cDesc           := 3
    Private nEstoque        := 4

	Private oBrwFor, oBrwProd
	Private aColsDefault
	Private lSoVisual		:= .F.
	Private cDecima		:= ''

	cTitulo += "Cadastro virtual de estoque - " + SA2->A2_NREDUZ

	//Cria a tela
	oDlg := FwDialogModal():New()
	oDlg:SetTitle(cTitulo)
	oDlg:SetSubTitle("Ultima Consulta: " + Dtoc(SA2->A2_YDATA) + " - " + SA2->A2_YHORA	)		
	oDlg:enableAllClient()
	oDlg:CreateDialog()	

	oPanelModal	:= oDlg:GetPanelMain()

	
	//Cria o layer na tela	
	oLayer:init(oPanelModal,.F.)

	//Panel para digitacao dos fornecedores
	oLayer:addLine("lin01",100,.F.)
	oLayer:addCollumn("col01",100,.F.,"lin01")
	oLayer:addWindow("col01","win01",'Produtos',100,.F.,.F.,{|| }, "lin01",{|| })
	oPanel01 := oLayer:getWinPanel("col01","win01", "lin01")

	aHeader	:= {}
	aCols	:= {}

	//Cria header dos produtos
	aAdd(aHeader,	{"Item"							,"cItem"		,"",06,0,"","�","C",""	,"V",,,".F."  })
	aAdd(aHeader,	{"Produto"						,"Z05_CODSB1"   ,"",15,0,"","�","C",""  ,"V",,,".F."  })
	aAdd(aHeader,	{"Descricao"					,"Z05_DESC"     ,"",50,0,"","�","C",""	,"V",,,".F."  })
	aAdd(aHeader,	{"Ped.Venda"	    		,"nQtdPenden"	,"@E 99,999,999.99",10,2,"","�","N",""	,"F" ,,,".F."  })
	aAdd(aHeader,	{"Lja.Virtual"	    				,"nVlrEstqv1"	,"@E 99,999,999.99",10,2,"","�","N",""	,"V"  })
	aAdd(aHeader,	{"Lja.Outros"	    		,"nVlrEstqv2"	,"@E 99,999,999.99",10,2,"","�","N",""	,"F" ,,,".F."  })
	aAdd(aHeader,	{"Entreposto"	    		,"nVlrEstqe1"	,"@E 99,999,999.99",10,2,"","�","N",""	,"V"  })
	aAdd(aHeader,	{"Entr.Outros"	    ,"nVlrEstqe2"	,"@E 99,999,999.99",10,2,"","�","N",""	,"F" ,,,".F."  })
	aAdd(aHeader,	{"Est.Campo"	    		,"nVlrEstqc1"	,"@E 99,999,999.99",10,2,"","�","N",""	,"V"  })
	aAdd(aHeader,	{"Est.Outros"	    ,"nVlrEstqc2"	,"@E 99,999,999.99",10,2,"","�","N",""	,"F" ,,,".F."  })

    aCols := getProdutos()

	oBrwProd:= MsNewGetDados():New(050,010,200,390,Iif(lSoVisual,, GD_UPDATE),,,"+cItem",,,999,,,,oPanel01,aHeader,aCols)
	oBrwProd:oBrowse:Refresh()
	oBrwProd:oBrowse:Align := CONTROL_ALIGN_ALLCLIENT    

	oDlg:AddButton('Cancelar'	, {|| lRet := .F., oDlg:Deactivate() }, 'Cancelar', , .T., .T., .T., ) 
	oDlg:AddButton('Confirmar'	, {|| lRet := .T., oDlg:Deactivate() }, 'Confirmar',, .T., .T., .T., ) 

	oDlg:Activate()

	If lRet
		If !Empty(SA2->A2_YLOCAL)
			MsAguarde({|| MoveEstoq(5,"SA2->A2_YLOCAL") }, 'Gerando movimenta��es para loja virtual...')
		Else
			ApMsgInfo("N�o foi gerada movimenta��o para estoque de loja virtal. Cadastre um armazem par estoque de loja virtual. ")
		EndIf
		If !Empty(SA2->A2_YLOCAL1)
			MsAguarde({|| MoveEstoq(7,"SA2->A2_YLOCAL1") }, 'Gerando movimenta��es para Entreposto ...')
		Else
			ApMsgInfo("N�o foi gerada movimenta��o para estoque entreposto. Cadastre um armazem para estoque entreposto. ")
		EndIf
		If !Empty(SA2->A2_YLOCAL2)
			MsAguarde({|| MoveEstoq(9,"SA2->A2_YLOCAL2") }, 'Gerando movimenta��es para campo...')
		Else
			ApMsgInfo("N�o foi gerada movimenta��o para estoque de campo. Cadastre um armazem para estoque de campo. ")
		EndIf
		
		RecLock('SA2', .F.)
			SA2->A2_YDATA := dDataBase
			SA2->A2_YHORA := Time()
		SA2->(MsUnLock())
	Endif

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} getProdutos
Busca informa��es de produto relacionados a cada fonecedor para ser
utilizado no aCOls
@author  Samuel Dantas
@since   12/12/2018
/*/
//-------------------------------------------------------------------
Static function getProdutos ()
    Local aEstOut := {}
    Local cSeek := ''
    Local cItem := '001'
	Local i
	Local aRet := {}
	Local nEstOut := 0
	Local nEst1 := 0
	Local nEst2 := 0
	Local nEst3 := 0
	Local nPendentes := 0
    
	//Posiciona tabela Z05 (FORNECEDOR e PRODUTO)
	Z05->(DbGoTop())
    Z05->(DbSetOrder(1))
    Z05->(DbSeek( cSeek := xFilial("Z05") + SA2->A2_COD + SA2->A2_LOJA))
    While (Z05->(!EOF())) .AND. cSeek == xFilial("Z05") + Z05->Z05_CODSA2 + Z05->Z05_LOJA
		SB1->(DBSeek(xFilial('SB1') +Z05->Z05_CODSB1 ))
		SB2->(DbGoTop())
	
		nEst1 := 0
		nEst2 := 0
		nEst3 := 0
		nPendentes := QtdPedAbertos()
	
		//Seta SB2 para buscar saldo atual de cada produto
		aEstOut := CalcOutros(SA2->A2_COD,SA2->A2_LOJA)
		If SB2->(DbSeek(xFilial("SB2") + Z05->Z05_CODSB1 + SA2->A2_YLOCAL)) 
			nEst1 := SaldoSb2()
		EndIf
		If SB2->(DbSeek(xFilial("SB2") + Z05->Z05_CODSB1 + SA2->A2_YLOCAL1)) 
			nEst2 := SaldoSb2()
		EndIf
		If SB2->(DbSeek(xFilial("SB2") + Z05->Z05_CODSB1 + SA2->A2_YLOCAL2)) 
			nEst3 := SaldoSb2()
		EndIf

		aAdd(aRet,{cItem,Z05->Z05_CODSB1, SB1->B1_DESC, nPendentes, nEst1, aEstOut[1], nEst2 ,aEstOut[2], nEst3, aEstOut[3], .F.})		
        cItem := Soma1(cItem)
        Z05->(DbSkip())
    EndDo

	//Ordenar pela descricao
	aAux := aSort(aRet, , , { | x,y | x[3] < y[3] } )
	
	aRet := {}
	For i := 1 to Len(aAux)
		aAdd(aRet,{StrZero(i,3), aAux[i][2], aAux[i][3], aAux[i][4], aAux[i][5], aAux[i][6], aAux[i][7], aAux[i][8], aAux[i][9],aAux[i][10], .F.})		
	Next

Return aRet

//-------------------------------------------------------------------
/*/{Protheus.doc} MoveEstoq
Realiza movimentacoes de entrada e saida de estoques
@author  Samuel Dantas
@since   12/12/2018
/*/
//-------------------------------------------------------------------
Static Function MoveEstoq(nPosEstoque,cCampoEst)
    Local cMovEntr		:= PADR(AllTrim(SuperGetMv("MS_MVENTR",.F.,"001")),3) //Parametro para TM de entrada
    Local cMovSaid		:= PADR(AllTrim(SuperGetMv("MS_MVSAID",.F.,"501")),3) //Parametro para TM de saida
    Local lEntrada 		:= .F.
	Local lMsErroAuto	:= .F.
	Local qtdEstoque	:= 0 
	Local aCols 		:= oBrwProd:aCols
	Local nOpc 			:= 3
	Local aItens 		:= {}
	Local lAlterado 	:= .F.
	Local i, cProduto, nSaldoSB2, cTM, _aCab
	 
	Begin Transaction             	
		
		//Percorre o acols gerando as movimentacoes de ajuste, entrada ou saida de estoque
		For i := 1 To len(aCols)
			
			//Recupera o produto e quantidade atual
			cProduto 	:=	aCols[i][nCodSB1] 
			qtdEstoque 	:=	aCols[i][nPosEstoque]

			SB2->(DbSetOrder(1))

			//Verifica se ja tem saldo, se nao tiver cria
			If !  SB2->(DbSeek(xFilial("SB2") + aCols[i][nCodSB1] + &cCampoEst ))
				CriaSb2(cProduto, &cCampoEst)
			EndIf

			//Retorna o saldo
			nSaldoSB2 := SaldoSb2()

			//Se houver diferenca entre os estoques, ira fazer a movimetacao
			If nSaldoSB2 <> qtdEstoque

				//Entrada
				If nSaldoSB2 < qtdEstoque
					cTM			:= cMovEntr
					qtdEstoque	:= qtdEstoque - nSaldoSB2
				//Saida
				Else
					cTM			:= cMovSaid
					qtdEstoque	:= nSaldoSB2 - qtdEstoque 
				Endif


				//Prepara o execauto
				_aCab := { 	{ "D3_TM"		, cTM , Nil },;
							{"D3_EMISSAO" 	,ddatabase, NIL}} 

				SB1->(DbSetOrder(1))
				SB1->(DbSeek(xFilial("SB1") + cProduto ))

				If qtdEstoque > 0    
					aVetor := {} 
					aadd( aVetor, { "D3_COD"   	, cProduto  	, Nil } ) 
					aadd( aVetor, { "D3_UM"    	, SB1->B1_UM     		, Nil } ) 
					aadd( aVetor, { "D3_QUANT" 	, qtdEstoque 			, Nil } ) 
					aadd( aVetor, { "D3_LOCAL" 	,  &cCampoEst 		, Nil } )

					aAdd(aItens,aVetor)

					MSExecAuto( { |x,y,z| MATA241(x ,y,z) },_aCab, aItens, nOpc ) 

					lAlterado := .T.			
					aItens := {}

					//Caso ocorra erro, desfaz a movimentacao
					If lMsErroAuto 
						MostraErro() 
						DisarmTransaction() 
						Return .F.
					Endif 
				
				EndIf

			EndIf
		
		Next

	End Transaction 
	
	//Confirma a alteracao, quando alterar
	If (lAlterado)
    	ApMsgInfo("Estoque de produtos alterado com sucesso.", 'Aten��o')
	Endif
	
Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} CalcOutros
Calcula valor em estoque em outros fornecedores
@author  Samuel Dantas
@since   17/07/2019
@version version
/*/
//-------------------------------------------------------------------
Static Function CalcOutros (cCodSA2, cLoja)
	Local aRet := { 0 , 0 , 0}
	Local aAreaZ05 := Z05->(GetArea())
	Local aAreaSA2 := SA2->(GetArea())
	Local aAreaSB2 := SB2->(GetArea())
	Default cCodSA2 := ""
	Default cLoja 	:= ""
	
	cQuery := " SELECT R_E_C_N_O_ AS RECNO FROM "  + RetSqlName('Z05') + " Z05"
	cQuery += " WHERE Z05.D_E_L_E_T_ <> '*' "
	cQuery += " AND Z05_CODSA2+Z05_LOJA <> '"+cCodSA2+cLoja+"' "
	cQuery += " AND Z05_CODSB1 = '"+SB1->B1_COD+"' "
	
	If Select('QRY') > 0
		QRY->(dbclosearea())
	EndIf
	
	TcQuery cQuery New Alias 'QRY'
	SA2->(DbSetOrder(1))
	While QRY->(!Eof())
		Z05->(DbGoTo(QRY->RECNO))
		SB2->(DbGoTop())
		
		//Seta SB2 para buscar saldo atual de cada produto
		SA2->(DbSeek(xFilial("SA2") + Z05->Z05_CODSA2 + Z05->Z05_LOJA)) 
		If SB2->(DbSeek(xFilial("SB2") + Z05->Z05_CODSB1 + SA2->A2_YLOCAL)) 
			aRet[1] += SaldoSb2()
		EndIf
		If SB2->(DbSeek(xFilial("SB2") + Z05->Z05_CODSB1 + SA2->A2_YLOCAL1)) 
			aRet[2] += SaldoSb2()
		EndIf
		If SB2->(DbSeek(xFilial("SB2") + Z05->Z05_CODSB1 + SA2->A2_YLOCAL2)) 
			aRet[3] += SaldoSb2()
		EndIf
	
		QRY->(dbSkip())
	EndDo
	
	Z05->(RestArea(aAreaZ05))
	SA2->(RestArea(aAreaSA2))
	SB2->(RestArea(aAreaSB2))

Return aRet

//-------------------------------------------------------------------
/*/{Protheus.doc} QtdPedAbertos
Fun��o respons�vel por retornar a quantidade do produto que foi aceito
por�m n�o foi separado
@author  Samuel Dantas
@since   17/07/2019
@version version
/*/
//-------------------------------------------------------------------
Static Function QtdPedAbertos()
	Local nRet := 0
	
	cQry := " SELECT SUM(C6_QTDVEN) AS TOTAL FROM "  + RetSqlName('SC5') + " SC5 "
	cQry += " INNER JOIN "  + RetSqlName('SC6') + " SC6 ON C6_NUM = C5_NUM AND SC6.D_E_L_E_T_ = SC5.D_E_L_E_T_ "
	cQry += " WHERE SC5.D_E_L_E_T_ <> '*'"
	cQry += " AND C6_PRODUTO = '"+SB1->B1_COD+"' "
	cQry += " AND C5_YSTATUS IN ('A','P') "
	
	If Select('QRYSC5') > 0
		QRYSC5->(dbclosearea())
	EndIf
	
	TcQuery cQry New Alias 'QRYSC5'
	
	While QRYSC5->(!Eof())
		nRet += QRYSC5->TOTAL
		QRYSC5->(dbSkip())
	EndDo

Return nRet