//-------------------------------------------------------------------
/*/{Protheus.doc} IFAA0010
Cadastro de Horarios
@author  Sidney Sales
@since   26/12/2018
@version 1.0
/*/
//-------------------------------------------------------------------
User Function IFAA0010()

    Private cCadastro := 'Cadastro de Hor�rios de Entrega'

    Private aRotina := {{'Pesquisar' ,'AxPesqui',0,1} ,;
                       {'Visualizar' ,'AxVisual',0,2} ,;
                       {'Incluir' ,'AxInclui',0,3} ,;
                       {'Alterar' ,'AxAltera',0,4} ,;
                       {'Excluir' ,'AxDeleta',0,5} }


    Private cDelFunc := '.T.' // Validacao para a exclusao. Pode-se utilizar ExecBlock

    Private cString := 'Z12'
    DbSelectArea(cString)
    DbSetOrder(1)
    DbSelectArea(cString)

    mBrowse( 6,1,22,75,cString)

Return
