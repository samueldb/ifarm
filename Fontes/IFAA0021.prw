
#Include 'Protheus.ch'
#Include 'Parmtype.ch'
#include "FWMVCDEF.CH"
#include "topconn.CH"

User Function IFAA0021()
	Local oBrowse
	Private cString 		:= "Z29"  

	//Montagem do Browse principal	
	oBrowse := FWMBrowse():New()

	//Define alias principal
	oBrowse:SetAlias(cString)
	oBrowse:SetDescription('Dias e hor�rios de entrega')
	oBrowse:SetMenuDef('IFAA0021')

	oBrowse:Activate()

	SetKey(VK_F6,{||})

Return

/*/{Protheus.doc} MenuDef
Retorna o menu principal
@author Samuel Dantas
@since 03/01/2019
@version undefined
@return aRotina, Array com os dados para os botoes do browse
/*/ 
Static Function MenuDef()
	Local aRotina 	:= {}
	Local nTotal	:= 0
	
	Z29->(DbGoTop())
    aAdd( aRotina, { 'Visualizar'		, 'VIEWDEF.IFAA0021'	, 0, 2, 0, NIL } ) 
	If Z29->(EoF())
		aAdd( aRotina, { 'Incluir' 			, 'VIEWDEF.IFAA0021'	, 0, 3, 0, NIL } )
	EndIf
    aAdd( aRotina, { 'Alterar' 			, 'VIEWDEF.IFAA0021'	, 0, 4, 0, NIL } )
    aAdd( aRotina, { 'Excluir' 			, "VIEWDEF.IFAA0021"	, 0, 5, 0, NIL } )

Return aRotina

/*/{Protheus.doc} ModelDef
Construcao do modelo de dados
@author Samuel Dantas
@since 03/01/2019
@version 1.0
@return oModel, Retorna o objeto do modelo de dados
/*/
Static Function ModelDef()
	Local oModel
	Local oStruZ29 := FWFormStruct(1,"Z29")	
	Local oStruZ30 := FWFormStruct(1,"Z30")
	Local oStruZ31 := FWFormStruct(1,"Z31")
	

    oModel := MPFormModel():New("IFAA021", /*bPreValidacao*/,, /* GravaDados */, /*bCancel*/ )

	//Cria a estrutura principal(Z29)
	oModel:addFields('MASTERZ29',,oStruZ29)

	//Adiciona a chave
	oModel:SetPrimaryKey({'Z29_FILIAL', 'Z29_CODIGO'})

    oModel:AddGrid('Z30DETAIL','MASTERZ29',oStruZ30, /*bPreValidacao*/,, /*bPreVal*/, /*bPosVal*/, /*BLoad*/ )
	//Define a relacao entre as tabelas
	oModel:SetRelation('Z30DETAIL',{{'Z30_FILIAL','xFilial("Z30")'},{'Z30_CODZ29','Z29_CODIGO'}},Z30->(IndexKey(2)))

    aCamposZ31 := {}
	aAdd(aCamposZ31,{ 'Z31_FILIAL'	, 'xFilial("Z31")' 	})
	aAdd(aCamposZ31,{ 'Z31_CODZ29'	, "Z29_CODIGO" 		})
	
    //Cria estrutura de grid para os itens
	oModel:AddGrid('Z31DETAIL', 'MASTERZ29', oStruZ31)
	//Define a relacao entre as tabelas
	oModel:SetRelation('Z31DETAIL', aCamposZ31, Z31->(IndexKey(2)))
    
	//Define a descricao dos modelos
	oModel:GetModel( 'MASTERZ29' ):SetDescription( 'Dias de entrega' )
	oModel:GetModel( 'Z30DETAIL' ):SetDescription( 'Dias e horarios de entrega' )
	oModel:GetModel( 'Z31DETAIL' ):SetDescription( 'Datas exce��es' )

	//Define que o preenchimento da grid e' opcional
	oModel:GetModel('Z30DETAIL'):SetOptional( .T. )
	oModel:GetModel('Z31DETAIL'):SetOptional( .T. )

    //Define que a linha nao podera ter o conteudo repetido
	oModel:GetModel('Z30DETAIL'):SetUniqueLine({'Z30_FILIAL', 'Z30_DIA','Z30_CODZ12','Z30_CODZ29'})
	oModel:GetModel('Z31DETAIL'):SetUniqueLine({'Z31_FILIAL','Z31_DATA'})

    // AntesDeTudo
    // oModel:SetVldActivate( {|oModel| .T. } )
	//Atualiza quantidade em estoque na Z31
	// U_IFAA015A()
Return oModel

/*/{Protheus.doc} ViewDef
Monta o view do modelo
@author Samuel Dantas
@since 03/01/2019
@version 1.0
/*/
Static Function ViewDef()
	Local oModel := ModelDef()
	Local oView
	Local oStrZ29:= FWFormStruct(2, 'Z29')
	Local oStrZ30:= FWFormStruct(2, 'Z30')
	Local oStrZ31:= FWFormStruct(2, 'Z31')

	oView := FWFormView():New()
	oView:SetModel(oModel)
	
    //Adiciona no nosso View um controle do tipo FormFields(antiga enchoice)
	oView:AddField('FORM_Z29' , oStrZ29,'MASTERZ29' ) 

    //Adiciona no nosso View um controle do tipo FormGrid(antiga newgetdados)
    oView:AddGrid( 'GRID_Z30', oStrZ30, 'Z30DETAIL' )
    oView:AddGrid( 'GRID_Z31', oStrZ31, 'Z31DETAIL' )

    // Define campos que terao Auto Incremento
    // oView:AddIncrementField( 'GRID_Z30', 'Z30_ITEM' )
    oView:AddIncrementField( 'GRID_Z31', 'Z31_ITEM' )

    // 30% cabec e 70% para as abas
	oView:CreateHorizontalBox('SUPERIOR', 10)	
	oView:CreateHorizontalBox( 'INFERIOR', 90)
	
	// Criar "box" vertical para receber algum elemento da view
	oView:CreateVerticalBox( 'INFERIORESQ', 80, 'INFERIOR' )
	oView:CreateVerticalBox( 'INFERIORDIR', 20, 'INFERIOR' )


    // Cria Folder na View
    // oView:CreateFolder( 'PASTA_INFERIOR' ,'INFERIOR' )

    // Crias as pastas (abas)
    // oView:AddSheet( 'PASTA_INFERIOR'    , 'ABA_Z30'  , "Categorias" )
    // oView:AddSheet( 'PASTA_INFERIOR'    , 'ABA_Z31'  , "Produtos" )

    // Criar "box" horizontal com 100% dentro das Abas
    // oView:CreateHorizontalBox( 'ITENS' 		,100,,, 'PASTA_INFERIOR', 'ABA_Z30' )
    // oView:CreateHorizontalBox( 'DETALHES'   ,100,,, 'PASTA_INFERIOR', 'ABA_Z31' )

    // Liga a identificacao do componente
    oView:EnableTitleView('GRID_Z30','Dias e horarios do plano ')
    oView:EnableTitleView('GRID_Z31','Datas exce��es ')

    // Relaciona o ID da View com o "box" para exibicao
	oView:SetOwnerView('FORM_Z29', 'SUPERIOR')
	oView:SetOwnerView('GRID_Z30', 'INFERIORESQ' )
	oView:SetOwnerView('GRID_Z31', 'INFERIORDIR' )
	
Return oView