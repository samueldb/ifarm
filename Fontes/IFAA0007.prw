#Include 'Protheus.ch'
#Include 'TbIconn.ch'
#Include 'Topconn.ch'
//-------------------------------------------------------------------
/*/{Protheus.doc} IFAA0007
Tela para separacao de produtos. Gera pedidos de compras de acordo
com os fornecedores escolhidos.
@author  Samuel Dantas
@since   12/12/2018
/*/
//-------------------------------------------------------------------
User Function IFAA0007(nOpcao)
    Local oDlg, i, cFornece, nPosFor, nQtdFornece
    Local aHeader   := {}
    Local aCols     := {}
    Local cTitulo   := ''
    Local lRet      := .F.
    Local nX        := msAdvSize(.F.)[5]
    Local nY        := msAdvSize(.F.)[6]
    Local aFinal    := {}

	Local aPergs 	:= {}   
	Local aRet		:= {}     
    Local lEfetivar := .F.
    Local lGerou    := .F.
    Local oLayer    := FWLayer():New()

    Private aProdutos   := {}
    Private oBrw
    Private aSC6  
    Private nLinhaAtu   := 0
    
    Private aPedsProd   := {}
    
    Default nOpcao  := 0

    Z01->(DbSeek(xFilial('Z01')))

    aHeader := {}
    aCols   := {}

    aAdd(aHeader,{"Produto"     ,"C6_PRODUTO"   ,"",15,0,"","�","C","","V" })
    aAdd(aHeader,{"Descri��o"   ,"C6_DESCRI"    ,"",30,0,"","�","C","","V" })
    aAdd(aHeader,{"Quantidade"  ,"C6_QTDVEN"    ,"@E 999,999.99",10,2,"","�","N","","V",,,,".F."})
    
    //Preeche os aHeader com os dados dos fornecedores da associacao
    Z02->(DbSetOrder(1))
    Z02->(DbSeek( cSeek := xFilial("Z02") + Z01->Z01_CODIGO ))
    
    nQtdFornece := 0
    //Usa o codigo do fornecedor como nome do campo
    While Z02->(!EOF()) .AND. cSeek == Z02->Z02_FILIAL + Z02->Z02_CODZ01
        _cNomeFor := Posicione("SA2",1,xFilial("SA2")+Z02->(Z02_CODSA2 + Z02_LOJA),"A2_NREDUZ")
        aAdd(aHeader,{'PED - ' + Alltrim(_cNomeFor),"F" + Z02->(Z02_CODSA2 + Z02_LOJA), "@E 999,999.99",10,2,"U_IFAA007B()","�","N","","V" })
        aAdd(aHeader,{'EST - ' + Alltrim(_cNomeFor),"E" + Z02->(Z02_CODSA2 + Z02_LOJA), "@E 999,999.99",10,2,"U_IFAA007B()","�","N","","V",,,".F." })
        nQtdFornece++
        Z02->(DbSkip())
    EndDo
    
    //Preenche o acols com os dados dos pedidos
    aCols := GetPedidos(aHeader, nQtdFornece, nOpcao)

    //Se nao tiver pedidos no filtro, mostra msg pro usuario e encerra
    If Empty(aCols)
        MsgAlert("Aten��o, n�o existem pedidos para separa��o.", "Aten��o")
        Return
    EndIf

    cTitulo += 'Separa��o de Produtos - Pedidos'

    If nX == 0
        nX  := 800
        nY  := 600
    EndIf


    If Z01->(Eof())
        MsgAlert('Por favor realize o cadastro de associa��o antes de realizar uma separa��o.', 'Aten��o')
        Return
    EndIf

    If nOpcao == 4 .AND. Z11->Z11_STATUS <> 'A'
        MsgAlert("S� � poss�vel alterar uma separa��o com status ABERTA.")
        Return
    EndIf

	oDlg := FwDialogModal():New()
	
	oDlg:SetTitle(cTitulo)
	
	oDlg:enableAllClient()
	oDlg:CreateDialog()	

	oPanelModal	:= oDlg:GetPanelMain()

    //Cria o layer na tela
    oLayer:init(oPanelModal,.F.)

    //Panel01
    oLayer:addLine('lin01',100,.F.)
    oLayer:addCollumn('col01',100,.F.,'lin01')
    oLayer:addWindow('col01','win01','Produtos',100,.F.,.F.,{|| }, 'lin01',{|| })
    oPanel01 := oLayer:getWinPanel('col01','win01', 'lin01')

    oBrw:= MsNewGetDados():New(050,010,200,390,GD_UPDATE,/*Fun��o LinhaOk*/,/*Fun��o TudoOk*/,/*cIniPos*/,,,999,,,,oPanel01,aHeader,aCols)
    oBrw:oBrowse:Refresh()
    oBrw:oBrowse:Align := CONTROL_ALIGN_ALLCLIENT

	oDlg:AddButton('Efetivar'   , {|| lEfetivar := lRet := U_IFAA007c(@aFinal), Iif(lRet, oDlg:Deactivate(),) }, 'Efetivar',, .T., .T., .T., ) 
	oDlg:AddButton('Cancelar'	, {|| lRet := .F., oDlg:Deactivate() }, 'Cancelar', , .T., .T., .T., ) 
	oDlg:AddButton('Salvar'	    , {|| lRet := U_IFAA007c(@aFinal), Iif(lRet, oDlg:Deactivate(),) }, 'Salvar',, .T., .T., .T., ) 
	oDlg:AddButton('Ver Pedidos', {|| VerPedidos(oBrw:Acols[oBrw:nAt][1]) }, 'Ver Pedidos',, .T., .T., .T., ) 

    oDlg:Activate()

    If lRet .AND. lEfetivar
        If ! MsgYesNo('Aten��o, esse processo n�o poder� ser retornado. Confirma a efetiva��o?')
            MsgAlert('Opera��o cancelada pelo usu�rio.', 'Aten��o')
            Return
        EndIf
    EndIf

    //Verifica se houveram dados para gerar os pedidos
    If !Empty(aFinal)
        lContinua := .T.

        If lEfetivar
            lContinua := validaEstoque(aFinal)
        EndIf
        
        If lContinua
            MsAguarde({|| lGerou := GerarPedidos(aFinal, nOpcao, lEfetivar)}, 'Aguarde, processando separa��o... ')
            If lGerou
                ApMsgInfo('Separa��o '+Iif(lEfetivar, 'efetivada', 'salva')+' com sucesso.')
            Else
                MsgAlert('Erros foram encontrados na sepera��o. A serpa��o n�o foi ' + Iif(lEfetivar, 'efetivar', 'salvar') + ' a separa��o. ')
            EndIf
        EndIf
    ElseIf nOpcao == 4
        MsgAlert('N�o foram realizadas altera��es na separa��o.')
    Else
        MsgAlert('Pedidos n�o separados. Opera��o cancelada pelo usu�rio.', 'Aten��o')            
    EndIf

Return

Static Function corLinha
	Local nCor := 16777215 //COR BRANCA PADRAO	

    If nLinhaAtu == 0
        If oBrw:aCols[oBrw:oBrowse:nAt][Len(oBrw:aheader)+1]
		    nCor := 12632256
        EndIf
    Else
	//Se for primeiro registro entao pinta com a cor CLR_HCYAN
        If oBrw:oBrowse:nAt <> nLinhaAtu
            nCor := 16776960         
            nLinhaAtu := oBrw:nAt
        //Se nao, se nao for o primeiro registro e estiver deletado pinta com
        //a cor de linha deletada CLR_HGRAY	
        Else
            nCor      := 16777215			
            If oBrw:aCols[oBrw:oBrowse:nAt][Len(oBrw:aheader)+1]
                nCor := 12632256
            EndIf
        EndIf	       

    EndIf

Return nCor

Static Function ChangeLine
    if nLinhaAtu == 0
        nLinhaAtu = -1
    EndIf
Return

//validacao do campo, para saber se o produto esta cadastrado para o fornecedor
User function IFAA007B()
    Local cProduto  := gdFieldGet("C6_PRODUTO",n) 
    Local cDesProd  := gdFieldGet("C6_DESCRI",n) 
    Local cForAux   := StrTran(ReadVar(), 'M->F','')
    Local lRet      := .T.
    
    SA2->(DbSetOrder(1))
    SA2->(DbSeek(xFilial('SA2') + cForAux))
    Z05->(DbSetOrder(1))
    If ! Z05->(DbSeek(xFilial('Z05') + cForAux + cProduto))
        MsgAlert('O produto ' + Alltrim(cDesProd)+ ' n�o est� cadastrado para o fornecedor ' + Alltrim(SA2->A2_NREDUZ) + '. N�o � poss�vel separar para o fornecedor')
        lRet := .F.
    EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} IFAA007c
Funcao que prepara o array para geracao dos pedidos de compra
@author  Samuel Dantas
@since   12/12/2018
/*/
//-------------------------------------------------------------------
User Function IFAA007c(aFinal)
    Local i, j, nQtdProd, cDescPro
    Local cFornece, nPosFor
    Local lRet  := .T.
    
    //Zera o array recebido, para montar o retorno    
    aFinal  := {}
    
    //Percorre o array 
    For i := 1 to Len(oBrw:aCols)

        //Verifica os dados do produto
        nQtdProd    :=  0
        cProduto    := oBrw:aCols[i][1] 
        cDescPro    := oBrw:aCols[i][2] 

        //A partir da 4 posicao, pega as quantidade de cada fornecedor
        For j := 4 to Len(oBrw:aHeader)
            
            //Despreza os campos onde informa o estoque do produtor
            If Left(oBrw:aHeader[j][2],1) == 'E'
                loop
            EndIf

            //Pega os dados do fornecedor, o nome do campo �  F+Codigo e Loja do fornece
            cFornece    := SubStr(oBrw:aHeader[j][2],2)
            nPosFor     := aScan(aFinal, {|x| x[1] == cFornece })    
            
            //Verifica se sera pedido algo para esse produto para esse fornecedor 
            nQtdProd    += oBrw:aCols[i][j]
            
            //Se tiver dado na quantidade atual, adicionara ao array 
            If oBrw:aCols[i][j] > 0
                
                //Se ainda nao existir um array para o fornecedor, cria uma linha para ele
                If nPosFor == 0
                    aAdd(aFinal, {cFornece })
                    nPosFor := Len(aFinal)  
                EndIf

                //adiciona o produto ao fornecedor
                aAdd(aFinal[nPosFor], {cProduto, oBrw:aCols[i][j] } )                 

            EndIf            

        Next

        //Consiste o total digitado para o produto com a dos pedidos
        If nQtdProd <> oBrw:aCols[i][3]
            MsgALert('Aten��o, o produto ' + Alltrim(cDescPro) + ' foi solicitado ' + Alltrim(Transform(oBrw:aCols[i][3], "@e 999,999.99")) + ;
                ' e est� sendo separado ' + Alltrim(Transform(nQtdProd, "@e 999,999.99") ) + '. Opera��o cancelada.' , 'Aten��o')      
            aFinal  := {}         
            lRet    := .F.            
        EndIf
    Next    

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} getPedidos
Retorna as quantidade e quais os pedidos que ainda nao foram separados 
para a asssocia��o
@author  Samuel Dantas
@since   12/12/2018
/*/
//-------------------------------------------------------------------
Static function GetPedidos(aHeader, nQtdFornece, nOpcao)
    Local aCols         := {} 
    Local aColsAux      := {}
    Local i, cQuery, j, cFornece
    
    //Retorna todos os pedidos, produtos e quantidades ainda nao separados
    cQuery := " SELECT C6_NUM, C6_PRODUTO, C6_DESCRI, C6_QTDVEN, C6_VALOR FROM " + RetSqlTab("SC6")
    cQuery += " INNER JOIN " + RetSqlTab('SC5') + " ON C5_FILIAL = C6_FILIAL AND C5_NUM = C6_NUM AND SC5.D_E_L_E_T_ = SC6.D_E_L_E_T_ "
    cQuery += " WHERE SC5.D_E_L_E_T_ <> '*' "
    cQuery += " AND C5_YCODZ01 = '" + Z01->Z01_CODIGO + "' " 
    
    If nOpcao == 4
        // cQuery += " AND C5_YCODZ11 = '"+Z11->Z11_CODIGO+"' "

        cQrySC5 := " SELECT * FROM "  + RetSqlName('SC5') + " SC5 "
        cQrySC5 += " WHERE SC5.D_E_L_E_T_ <> '*'"
        cQrySC5 += " AND C5_YSEPAR <> 'S'"
        cQrySC5 += " AND C5_YSTATUS != 'P'"
        
        If Select('QRYSC5') > 0
            QRYSC5->(dbclosearea())
        EndIf
        
        TcQuery cQrySC5 New Alias 'QRYSC5'
        
        If QRYSC5->(!Eof())
            If MSGYESNO( "Existem pedidos em aberto que ainda n�o foram inclu�dos no rateio. Deseja inclu�los nesse rateio?", "Novos pedidos" )
                cQuery += " AND ( C5_YCODZ11 = '"+Z11->Z11_CODIGO+"' OR ( C5_YSEPAR <> 'S' AND C5_YSTATUS != 'P' ) )"
            Else
                cQuery += " AND C5_YCODZ11 = '"+Z11->Z11_CODIGO+"' "
            EndIf
        Else
            cQuery += " AND C5_YCODZ11 = '"+Z11->Z11_CODIGO+"' "
        EndIf

        
    Else
        cQuery += " AND C5_YSEPAR <> 'S'"
        cQuery += " AND C5_YSTATUS != 'P'"
    EndIf
    
    cQuery += " ORDER BY C6_DESCRI "
    
    If Select('QRY') > 0
        QRY->(DbCloseArea())
    EndIf

    TcQuery cQuery New Alias 'QRY'

    aSC6    := {}
    aAuxPed := {}

    //Percorre agrupando produtos iguais, somando as quantidades.
    While QRY->(!Eof())
        
        aAdd(aPedsProd, {QRY->C6_NUM, QRY->C6_PRODUTO, C6_QTDVEN})

        //Verifica se ja adicionou ou produto no array            
        nPosProd := aScan(aAuxPed, {|x| x[1] == QRY->C6_PRODUTO })

        If nPosProd == 0
            aAdd(aAuxPed, {QRY->C6_PRODUTO, QRY->C6_DESCRI, 0, 0} )
            nPosProd := Len(aAuxPed)
        EndIf

        aAuxPed[nPosProd][3] += QRY->C6_QTDVEN
        aAuxPed[nPosProd][4] += QRY->C6_VALOR
        
        //Verifica se ja botou o pedido no array que tera os pedidos do intervalo
        nPosSC6 := aScan(aSC6, QRY->C6_NUM)

        If nPosSC6 == 0
            aAdd(aSC6, QRY->C6_NUM)
        EndIf
                        
        QRY->(DbSkip())

    EndDo

    
    //Cria o aCols que mostrara ao usuario a quantidade pedida e os fornecedores
    For i := 1 to Len(aAuxPed)

        aColsAux    := {}

        aAdd(aColsAux, aAuxPed[i][1]) //produto
        aAdd(aColsAux, aAuxPed[i][2]) //descricao
        aAdd(aColsAux, aAuxPed[i][3]) //quantidade

        //Adiciona um item a mais para cada fornecedor(associado)
        For j := 1 to (nQtdFornece*2) Step 2
            
            //Posicao do fornecedor
            cFornece    := SubStr(aHeader[j+3][2],2)
            
            If nOpcao == 4
                //retorna a quantidade do fornecedor, caso esteja alterando
                nQuant      := getQtdPedFor(aAuxPed[i][1], cFornece)
            Else
                nQuant      := 0
            EndIf            
            //campo de quantidade a pedir
            aAdd(aColsAux, nQuant)
            
            //Retorna a quantidade do estoque
            nEstoque    := U_IFAF001C(aAuxPed[i][1], cFornece)
            aAdd(aColsAux, nEstoque)

        Next

        aAdd(aColsAux, .F.)
        aAdd(aCols, aColsAux)
    Next        

    aProdutos := aAuxPed
    
Return aCols

//-------------------------------------------------------------------
/*/{Protheus.doc} gerarPedidos
    (long_description)
    @type  Static Function
    @author Sidney Sales
    @since   18/12/2018
    @version 1.0
/*/
//-------------------------------------------------------------------
Static Function gerarPedidos(aFinal, nOpcao, lEfetivar)
    Local i, j, aCab, aItem    
    Local cCondPag  := SuperGetMv('MS_CONDPG', .F., '001')
    Local aAreaZ01  := Z01->(GetArea())
    Local aAreaZ02  := Z02->(GetArea())
    Local lRet      := .T.
    Local aCabSD3   := {}
    Local aItensSD3 := {}
    Local aItemSD3  := {}
    Local cMovSaid	:= PADR(AllTrim(SuperGetMv("MS_MVSAID",.F.,"501")),3) //Parametro para TM de saida
    Local aRet      := {}
    Local aPergs    := {}
    
    Default lEfetivar   := .F.
    Private lMsErroAuto := .F.
    SX5->(DbSetOrder(1))
    
    Begin Transaction

        If nOpcao == 4        

            cCodZ11 := Z11->Z11_CODIGO
            
            cQueryDel := " SELECT R_E_C_N_O_ AS SC7RECNO FROM " + RetSqlName('SC7')
            cQueryDel += " WHERE D_E_L_E_T_ <> '*' "
            cQueryDel += " AND C7_YCODZ01 = '" + Z01->Z01_CODIGO + "' "        
            cQueryDel += " AND C7_YCODZ11 = '" + cCodZ11 + "' "
            
            If Select("QRYDEL") > 0
                QRYDEL->(DbCloseArea())
            EndIf

            TcQuery cQueryDel New Alias 'QRYDEL' 

            While QRYDEL->(!Eof())
                SC7->(DbGoto(QRYDEL->SC7RECNO))
                RecLock('SC7', .F.)
                    SC7->(DbDelete())
                SC7->(MsUnLock())
                QRYDEL->(DbSkip())
            EndDo

        Else

            cCodZ11 := GetSxeNum('Z11', 'Z11_CODIGO')
            ConfirmSX8()

            RecLock('Z11', .T.)    
                Z11->Z11_FILIAL := xFilial("Z11")
                Z11->Z11_STATUS := 'A'
                Z11->Z11_CODIGO := cCodZ11
                Z11->Z11_DATA   := dDataBase
                Z11->Z11_HORA   := Time()
                Z11->Z11_USER   := __cUserID + ' - ' + cUserName
            Z11->(MsUnLock())

        EndIf
        
        If lEfetivar
        
            aAdd(aPergs, {1,"S�rie", Z11->Z11_YSERIE,'@!','.T.','','.T.',50,.F.})
            
            If ParamBox(aPergs ,"Informa a Serie p/ NFs de Sa�da",aRet,,,,,,,.F.,.F.)
                If SX5->(DbSeek(xFilial('SX5') + '01' + aRet[1]))
                    RecLock('Z11', .F.)
                        Z11->Z11_YSERIE := aRet[1]
                    Z11->(MsUnLock())
                EndIf
            EndIf

            If Empty(Z11->Z11_YSERIE)
                MsgAlert('A s�rie digitada � inv�lida. Opera��o cancelada.')
                Return .F.
            EndIf

        EndIf

        //Percorre o array gerando os pedidos de compras
        For i := 1 to Len(aFinal)
            
            aCab        := {}
            aItem       := {}
            aItensSD3   := {}

            //Pega o fornecedor
            cAuxForn    := Left(aFinal[i][1], Len(SA2->A2_COD)) 
            cAuxLoja    := Right(aFinal[i][1], Len(SA2->A2_LOJA)) 

            Z01->(DbSetOrder(1))
            Z02->(DbSetOrder(3))//FILIAL + CODZ01 + CODSA2 + LOJA

            Z01->(Dbseek(xFilial('Z01') ))
            Z02->(DbSeek(xFilial('Z02') + Z01->Z01_CODIGO + cAuxForn + cAuxLoja))

            SA2->(DbSetOrder(1))
            SA2->(DbSeek(xFilial('SA2') + cAuxForn + cAuxLoja))
            
            If !Empty(SA2->A2_COND)
                cCondPag := SA2->A2_COND
            Else
                cCondPag := Alltrim(SuperGetMv('MS_CONDPG',.F.,'001'))
            EndIf
           
            //Cabecalho do pedido
            aAdd(aCab, {"C7_EMISSAO" ,dDataBase  		                                                ,Nil})
            aAdd(aCab, {"C7_FORNECE" ,cAuxForn   		                                                ,Nil})
            aAdd(aCab, {"C7_LOJA"    ,cAuxLoja 			                                                ,Nil})
            aAdd(aCab, {"C7_COND"    ,cCondPag		 	                                                ,Nil})
            aAdd(aCab, {"C7_YCODZ01" ,Z01->Z01_CODIGO	                                                ,Nil})
            aAdd(aCab, {"C7_YCODZ11" ,cCodZ11       	                                                ,Nil})
            
            
            
            //A partir do segundo item, estao os dados dos itens do fornecedor atual
            For j := 2 to Len(aFinal[i])
                
                //Incrementa o item
                cItem   := Strzero(j-1, Len(SC7->C7_ITEM))
                
                //Seta o produto atual
                SB1->(DbSetOrder(1))
                SB1->(DbSeek(xFilial('SB1') +  aFinal[i][j][1]))
                
                //Quantidade que sera gerada
                nQuant      := aFinal[i][j][2]
                
                //Busca a posicao do produto no array
                nPosProd    := aScan(aProdutos, {|x| x[1] == SB1->B1_COD})
                
                //Caso encontre o produto(Sempre devera encontrar), calcula o pre�o que sera comprado ao vendedor
                If nPosProd > 0
                    
                    //Calcula o preco medio vendido
                    nPreco      := Round(aProdutos[nPosProd][4] / aProdutos[nPosProd][3],2)
                    nPrecoMEDIO := nPreco
                    
                    //Precos e percentuais especificos do fornecedor
                    nPercFOR    := Z05->Z05_PERCEN
                    nPrecoFOR   := nPreco

                    Z05->(DbSetOrder(1)) //Z05_FILIAL, Z05_CODSA2, Z05_LOJA, Z05_CODSB1, R_E_C_N_O_, D_E_L_E_T_
                    
                    //Seta o cadastro do produto por fornecedor, e verifica se tem preco ou perecentual especifico pra ele
                    If Z05->(DbSeek(xFilial('Z05') + cAuxForn + cAuxLoja + SB1->B1_COD )) .AND.  (Z05->Z05_PERCEN > 0 .OR. Z05->Z05_PRECO > 0)
                        If Z05->Z05_PERCEN > 0
                            nPreco := nPreco * Z05->Z05_PERCEN / 100
                        ElseIf Z05->Z05_PRECO > 0
                            nPreco := Z05->Z05_PRECO
                        EndIf
                        nPercFOR    := Z05->Z05_PERCEN
                        nPrecoFOR   := Z05->Z05_PRECO
                    Else
                        nPreco      := nPreco * Z02->Z02_PERLUC / 100
                        nPercFOR    := Z02->Z02_PERLUC
                    EndIf
                    
                Else   
                    nPreco := 0
                EndIf

                //Prepara o item do pedido de venda, o armazem sera o armazem da associacao
                AADD(aItem,{{"C7_PRODUTO"    ,SB1->B1_COD                                                           ,Nil},;
                                {"C7_ITEM"   ,cItem						                                            ,Nil},;
                                {"C7_QUANT"  ,nQuant		 			                                            ,Nil},;
                                {"C7_PRECO"  ,ROUND(nPreco,2)     				                                            ,Nil},;
                                {"C7_TOTAL"  ,ROUND(nPreco,2) * nQuant		                                                ,Nil},;
                                {"C7_YCODZ01",Z01->Z01_CODIGO   		                                            ,Nil},;
                                {"C7_YCODZ11",cCodZ11              		                                            ,Nil},;
                                {"C7_YNFORNE",POSICIONE("SA2",1,xFilial("SA2")+cAuxForn+cAuxLoja,"A2_NOME")         ,Nil},;
                                {"C7_YPERCEN",nPercFOR                                                              ,Nil},;
                                {"C7_YPRECO" ,nPrecoFOR                                                             ,Nil},;
                                {"C7_YPRCMED",nPrecoMEDIO                                                           ,Nil},;
                                {"C7_LOCAL"  ,Z01->Z01_CODNNR   		                                            ,Nil}})
                                
                
                //Preenche os campos para a movimentacao interna
                aItemSD3 := {}

                aadd( aItemSD3, { "D3_COD"   	, SB1->B1_COD  	        , Nil } ) 
                aadd( aItemSD3, { "D3_UM"    	, SB1->B1_UM     		, Nil } ) 
                aadd( aItemSD3, { "D3_QUANT" 	, nQuant 			    , Nil } ) 
                aadd( aItemSD3, { "D3_LOCAL" 	, SA2->A2_YLOCAL 		, Nil } )
                
                aAdd(aItensSD3,aItemSD3)

            Next
            
            lMsErroAuto := .F.
            
            //Se for efetivar ira fazer a movimentacao interna
            If lEfetivar
                //Prepara o execauto do SD3(Movimento interno de saida para o fornecedor)
                aCabSD3 := { { "D3_TM"		, cMovSaid , Nil },;
                            {"D3_EMISSAO" 	,ddatabase, NIL}} 
                    

                MSExecAuto( { |x,y,z| MATA241(x ,y,z) },aCabSD3, aItensSD3, 3 )             
            
                If lMsErroAuto
                    lRet := .F.
                    MostraErro()
                    DisarmTransaction()
                    Exit
                EndIF
            EndIf

            //Chamado rotina automatica        
            MATA120(1,aCab,aItem,3)
            
            If lMsErroAuto
                lRet := .F.
                MostraErro()
                DisarmTransaction()
                Exit
            EndIF

        Next

        //Caso nao de erro ao final do processamento, grava como 'separados' todos os pedidos de venda. 
        If lRet
            SC5->(DbSetOrder(1))
            For i := 1 to Len(aSC6)
                If SC5->(DbSeek(xFilial('SC5') + aSC6[i]))
                    RecLock('SC5', .F.)
                        SC5->C5_YSEPAR  := 'S'
                        SC5->C5_YCODZ11 := cCodZ11
                        SC5->C5_YSTATUS := 'E'
                    SC5->(MsUnLock())
                EndIf
            Next
        EndIf
    
    
    If lRet .AND. lEfetivar

        //Gera nota fiscal de entrada
        lRet := GeraNFEntrada()
        
        //gera nota fiscal de saida
        If lRet
            lRet := GeraNFSaida()
        EndIf

        If lRet
            lRet := GeraTitulos()
        EndIf

        //desfaz a transa��o caso ocorra algum erro
        If !lRet
            DisarmTransaction()
        Else
            RecLock('Z11', .F.)
                Z11->Z11_STATUS := 'F'
            Z11->(MsUnLock())
        EndIf

    
    EndIf
    
    End Transaction

    RestArea(aAreaZ01)
    RestArea(aAreaZ02)

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} GeraTitulos
description
@author  Samuel Dantas
@since   14/06/2019
@version version
/*/
//-------------------------------------------------------------------
Static Function GeraTitulos()

    Local aArray := {}
    Local  cQuery   := ""
    Local  cCodFor      := PadR(AllTrim(SuperGetMV("MS_FOIFARM",.F.,"000000000")),  TAMSX3("A2_COD")[1]  )
    Local  cLojaFor     := PadR(AllTrim(SuperGetMV("MS_LOIFARM",.F.,"0000"))     ,  TAMSX3("A2_LOJA")[1]  )
    Local  _cNatureza   := AllTrim(SuperGetMV("MS_NATIFAR",.F.,"OUTROS"))
    Local  lRet := .T.
    Local  _cNum := ""
    PRIVATE lMsErroAuto := .F.

    cQuery := " SELECT R_E_C_N_O_ AS RECNOSF2 FROM " + RetSqlTab('SF2')
    cQuery += " WHERE " + RetSqlDel('SF2')
    cQuery += " AND F2_YCODZ11 = '" + Z11->Z11_CODIGO + "' "
    cQuery += " AND SF2.D_E_L_E_T_ <> '*' "

    If Select('QRYSF2') > 0
        QRYSF2->(DbCloseArea())
    EndIf
    
    TcQuery cQuery New alias 'QRYSF2'
    
    Z01->(DbSetOrder(1))    
    Z02->(DbSetOrder(2))    
    If Z02->(DbSeek(xFilial("Z02") + cCodFor + cLojaFor ))    
        While QRYSF2->(!Eof())
            SF2->(DbGoTo(QRYSF2->RECNOSF2))
            SC6->(DbSetOrder(4)) //C6_FILIAL+C6_NOTA+C6_SERIE
            If SC6->(DbSeek(SC6->(xFilial("SC6")+SF2->F2_DOC+SF2->F2_SERIE))) // Busca o pedido de origem
                _cNum := SC6->C6_NUM
            EndIf
            aArray := { { "E2_PREFIXO"  , "FOR"                                     , NIL },;
                        { "E2_NUM"      , SF2->F2_DOC                               , NIL },;
                        { "E2_TIPO"     , "NF"                                      , NIL },;
                        { "E2_NATUREZ"  , _cNatureza                                , NIL },;
                        { "E2_FORNECE"  , cCodFor                                   , NIL },;
                        { "E2_LOJA"     , cLojaFor                                  , NIL },;
                        { "E2_EMISSAO"  , SF2->F2_EMISSAO                           , NIL },;
                        { "E2_VENCTO"   , SF2->F2_EMISSAO                           , NIL },;
                        { "E2_VENCREA"  , SF2->F2_EMISSAO                           , NIL },;
                        { "E2_HIST"     , "PAGAMENTO PARA ASSOCIA��O"               , NIL },;
                        { "E2_YNUMSC6"  , _cNum                                     , NIL },;
                        { "E2_VALOR"    , SF2->F2_VALBRUT * (Z02->Z02_PERLUC/100)   , NIL } }
            
            MsExecAuto( { |x,y,z| FINA050(x,y,z)}, aArray,, 3)  // 3 - Inclusao, 4 - Altera��o, 5 - Exclus�o
            
            If lMsErroAuto
                MostraErro()
                lRet := .F.
                exit
            Endif
            
            QRYSF2->(DbSkip())
        EndDo
    Else
        Alert("Fornecedor IFARM n�o encontrado. N�o foi poss�vel gerar os titulos de pagamento.")
        lRet := .F.
    EndIf

Return lRet

Static Function GeraNFEntrada
	Local oNFEntrada	:= TNYXNFENTRADA():New()	
	Local cTes			:= SuperGetMv("MS_TESENTR",.F.,"001")	
    Local cEspecie      := SuperGetMv("MS_ESPENTR",.F.,"NF")	
	Local oSF1          := CRUD():New('SF1', nil, 1)    
    Local cSerie        := SuperGetMv('MS_SERENTR', .F., '1')
    Local cCondPag      := SuperGetMv('MS_CONDENT',.F.,'001')
    Local cC7_NUM       := ''
    Local lRet          := .T.
    Local aCamposSF1    := {}
    Local aCamposSD1    := {} 
    Local aAuxSD1       := {} 
    Local aAreaSA2      := SA2->(GetArea())
    SA2->(DbSetOrder(1))
    
    cQuery := " SELECT R_E_C_N_O_ AS RECNOSC7 FROM " + RetSqlTab('SC7') 
    cQuery += " WHERE " + RetSqlDel('SC7')
    cQuery += " AND C7_YCODZ01 = '" + Z01->Z01_CODIGO + "' "
    cQuery += " AND C7_YCODZ11 = '" + Z11->Z11_CODIGO + "' "
    cQuery += " ORDER BY C7_FILIAL, C7_NUM "

    If Select('QRYSC7') > 0
        QRYSC7->(DbCloseArea())
    EndIf

    TcQuery cQuery New Alias 'QRYSC7'

    cC7_NUM := ''
    nItem   := 0
    
    While QRYSC7->(!Eof())
        
        SC7->(DbGoTo(QRYSC7->RECNOSC7))
        
        If cC7_NUM <> SC7->C7_NUM
            aCamposSF1    := {}
            aCamposSD1    := {} 

            cC7_NUM := SC7->C7_NUM
            cDoc    := u_uGetSxeNum('SF1', 'F1_DOC')
            cFornec := SC7->C7_FORNECE
            cLoja   := SC7->C7_LOJA

            If SA2->(DbSeek( xFilial("SA2") + SC7->(C7_FORNECE+C7_LOJA)))
                If !Empty(SA2->A2_COND)
                    cCondPag := SA2->A2_COND
                Else
                    cCondPag := Alltrim(SuperGetMv('MS_CONDENT',.F.,'001'))
                EndIf
            Else
                cCondPag := Alltrim(SuperGetMv('MS_CONDENT',.F.,'001'))
            EndIf

            aAdd(aCamposSF1,{'F1_FILIAL'	, cFilAnt   , nil})
            aAdd(aCamposSF1,{'F1_TIPO'		, 'N'       , nil})
            aAdd(aCamposSF1,{'F1_FORMUL'	, 'N'       , nil})
            aAdd(aCamposSF1,{'F1_DOC'		, cDoc      , nil})
            aAdd(aCamposSF1,{'F1_SERIE'		, cSerie    , nil})
            aAdd(aCamposSF1,{'F1_EMISSAO'	, dDatabase , nil})
            aAdd(aCamposSF1,{'F1_FORNECE'	, cFornec   , nil})
            aAdd(aCamposSF1,{'F1_LOJA' 		, cLoja	    , nil})
            aAdd(aCamposSF1,{'F1_ESPECIE'	, cEspecie  , nil})
            aAdd(aCamposSF1,{'F1_COND'		, cCondPag  , nil})
            aAdd(aCamposSF1,{'F1_YCODZ11'	, Z11->Z11_CODIGO, nil})

        EndIf

        nItem++
        aAuxSD1 := {}
        aAdd(aAuxSD1, {'D1_COD'		, SC7->C7_PRODUTO	, nil})
        aAdd(aAuxSD1, {'D1_ITEM'	, StrZero(nItem,4)  , nil})
        aAdd(aAuxSD1, {'D1_LOCAL'	, Z01->Z01_CODNNR   , nil})
        aAdd(aAuxSD1, {'D1_QUANT'	, SC7->C7_QUANT     , nil})
        aAdd(aAuxSD1, {'D1_VUNIT'	, SC7->C7_PRECO     , nil})
        aAdd(aAuxSD1, {'D1_TOTAL'	, SC7->C7_TOTAL     , nil})
        aAdd(aAuxSD1, {'D1_TES'		, cTes              , nil})
        aAdd(aAuxSD1, {'D1_DESCRIC'	, SC7->C7_DESCRI    , nil})
        aAdd(aAuxSD1, {'D1_PEDIDO'	, SC7->C7_NUM       , nil})
        aAdd(aAuxSD1, {'D1_ITEMPC'	, SC7->C7_ITEM      , nil})
        aAdd(aAuxSD1, {'D1_YPERCEN'	, SC7->C7_YPERCEN   , nil})
        aAdd(aAuxSD1, {'D1_YPRECO'	, SC7->C7_YPRECO    , nil})
        aAdd(aAuxSD1, {'D1_YPRCMED'	, SC7->C7_YPRCMED   , nil})

		aAdd(aCamposSD1, aAuxSD1)

        QRYSC7->(DbSkip())
        
        SC7->(DbGoTo(QRYSC7->RECNOSC7))
        
        If cC7_NUM <> SC7->C7_NUM

            lMsErroAuto := .F.
            
			msExecAuto({|x,y,z| MATA103(x,y,z) }, aCamposSF1, aCamposSD1, 3)            
            
            If lMsErroAuto
                lRet := .F.
                MostraErro()
                Exit
            Endif                
        
        EndIf

    EndDo

    SA2->(RestArea(aAreaSA2))
Return lRet

Static Function GeraNFSaida
   	Local oPedido	:= TNYXPEDIDOS():New() 
    Local cSerie    := SuperGetMV("MS_SERSAID",.F., "001")
    Local lRet      := .T.
    Local aRecnos   := {}
    Local cMsg   := ""

    //Prepapara as propriedades do objeto pedidos
	oPedido:SERIENF	        := Z11->Z11_YSERIE                       //SERIE DA NOTA
	oPedido:TIPONF		    := 'N'						    //TIPO DA NOTA
	oPedido:FORMULNF	    := Space(len(SF2->F2_FORMUL))	//FORMULARIO PROPRIO	
    oPedido:aC5CUSTOMFIELDS := {}
    oPedido:aC5toF2			:= {{'C5_YCODZ11','F2_YCODZ11'}}
    oPedido:lCRIASB2        := .F.

    cQuery := " SELECT R_E_C_N_O_ AS RECNOSC5 FROM " + RetSqlTab('SC5')
    cQuery += " WHERE " + RetSqlDel('SC5')
    cQuery += " AND C5_YCODZ11 = '" + Z11->Z11_CODIGO + "' "

    If Select('QRYSC5') > 0
        QRYSC5->(DbCloseArea())
    EndIf
    
    TcQuery cQuery New alias 'QRYSC5'
    
    While QRYSC5->(!Eof())
        aAdd(aRecnos, QRYSC5->RECNOSC5)
        //Atualiza endere�o do cliente
        U_IFAF007B(QRYSC5->RECNOSC5)
        //Cria o objeto que realizara a inclusao do cabecalho
        oCrud := crud():new('SC5', QRYSC5->RECNOSC5, 1)
        aAdd(oPedido:SC5, oCrud)  
        
        QRYSC5->(DbSkip())

    EndDo

    //Gera libera��o
    lRet := GerarLiberacao(aRecnos)
    
    If lRet
        If ! oPedido:GERANF()
            MostraErro()
            lRet := .F.
        Else
            lRet := oPedido:REPLICARCAMPOS()

            cMsg := validLiberacao(aRecnos)
            If !EMPTY(cMsg)
                Aviso("Valida��o de libera��o de pedido",cMsg)
            // MostraErro(cMsg)
                lRet := .F.
            EndIf
        EndIf
    EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} GerarLiberacao
Gera a libera��o do pedido antes do faturamento.
@author  Samuel Dantas
@since   03/06/2019
@version version
/*/
//-------------------------------------------------------------------
Static Function GerarLiberacao(aRecnos)

    Local aAreaSC5  := SC5->(GetArea())
    Local aAreaSC6  := SC6->(GetArea())
    Local aAreaSC9  := SC9->(GetArea())
    Local cMsg      := ""    
    Local cRecnos   := ""    
    Local cQuery    := ""
    Local nI        := 1
    Local cAnterior := ""
    Local cAtual    := ""
    Local lRet      := .T.
    Default aRecnos := {}

    If len(aRecnos) > 0
        For nI := 1 To len(aRecnos)
            cRecnos += IIF(nI == 1, cValToChar(aRecnos[nI]), ","+cValToChar(aRecnos[nI]))
            SC5->(DbGoTo(aRecnos[nI]))

            aPvlNfs		:= {}
            aBloqueio		:= {}
            
            SC9->(DbSetOrder(1))
            SC6->(DbSetOrder(1)) //FILIAL+NUMERO+ITEM
            SC6->(DbSeek(xFilial("SC6")+SC5->C5_NUM))
            While SC6->(!EOF()) .and. SC6->C6_FILIAL == xFilial("SC6") .and. SC6->C6_NUM == SC5->C5_NUM
        
                lCredito := .t.
                lEstoque := .t.
                lLiber   := .t.
                lTransf := .f.
            
                If GetMv("MV_ESTNEG") == "S"
                    lAvEst := .f.
                Else
                    lAvEst := .t.
                Endif

                If SC9->(DbSeek(xFilial("SC9") + SC6->(C6_NUM+C6_ITEM) ))
                    RecLock('SC9', .F.)
                        SC9->(DbDelete())
                    SC9->(MsUnLock())
                EndIf
                nQtdLib := SC6->C6_QTDVEN
                nQtdLib := MaLibDoFat(SC6->(RecNo()),nQtdLib,@lCredito,@lEstoque,.F.,lAvEst,lLiber,lTransf)

                If SC6->C6_QTDVEN != nQtdLib
                    lRet := .F.
                    return lRet
                EndIf

                SC6->(DbSkip())
            EndDo
        Next
        
    EndIf
    SC5->(RestArea(aAreaSC5))
    SC6->(RestArea(aAreaSC6))
    SC9->(RestArea(aAreaSC9))
Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} validLiberacao
Valida se houve a libera��o do pedido antes do faturamento.
@author  Samuel Dantas
@since   03/06/2019
@version version
/*/
//-------------------------------------------------------------------
Static Function validLiberacao(aRecnos)

    Local aAreaSC5  := SC5->(GetArea())
    Local aAreaSC6  := SC6->(GetArea())
    Local aAreaSC9  := SC9->(GetArea())
    Local cMsg      := ""    
    Local cRecnos   := ""    
    Local cQuery    := ""
    Local nI        := 1
    Local cAnterior := ""
    Local cAtual    := ""
    Default aRecnos := {}

    If len(aRecnos) > 0
        For nI := 1 To len(aRecnos)
            cRecnos += IIF(nI == 1, cValToChar(aRecnos[nI]), ","+cValToChar(aRecnos[nI]))
        Next
        
        cQuery := " SELECT DISTINCT C5_NUM, C6_ITEM, IsNull(C9_DATALIB, '') C9_DATALIB, IsNull(C9_PEDIDO, '') C9_PEDIDO FROM "  + RetSqlName('SC5') + " SC5 " 
        cQuery += " INNER JOIN "  + RetSqlName('SC6') + " SC6 ON C5_FILIAL = C6_FILIAL AND C5_NUM = C6_NUM AND SC5.D_E_L_E_T_ = SC6.D_E_L_E_T_ "
        cQuery += " LEFT JOIN "  + RetSqlName('SC9') + " SC9 ON C6_FILIAL = C9_FILIAL AND C6_NUM = C9_PEDIDO AND C6_ITEM = C9_ITEM AND SC6.D_E_L_E_T_ = SC9.D_E_L_E_T_  "
        cQuery += " WHERE SC5.D_E_L_E_T_ <> '*' "
        cQuery += " AND SC5.R_E_C_N_O_ IN ("+cRecnos+") AND ( C9_BLEST IN ('02','03') OR C9_BLCRED IN ('02','03') ) ORDER BY C5_NUM "
        
        If Select('QRYVALID') > 0
            QRYVALID->(dbclosearea())
        EndIf
        
        TcQuery cQuery New Alias 'QRYVALID'

        While QRYVALID->(!Eof())

            cMsg += " � necess�rio realizar a libera��o do pedido "+QRYVALID->C5_NUM+". "+CRLF 

            QRYVALID->(dbSkip())
        EndDo
        
    EndIf
    SC5->(RestArea(aAreaSC5))
    SC6->(RestArea(aAreaSC6))
    SC9->(RestArea(aAreaSC9))
Return cMsg

Static Function getQtdPedFor(cProduto, cFornece)
    Local cQuery 
    Local nRet  := 0
    
    cQuery := " SELECT C7_QUANT FROM " + RetSqlTab('SC7')
    cQuery += " WHERE D_E_L_E_T_ <> '*' "
    cQuery += " AND " + RetSqlDel('SC7')
    cQuery += " AND C7_PRODUTO = '" + cProduto + "' "
    cQuery += " AND C7_FORNECE + C7_LOJA = '" + cFornece + "' "
    cQuery += " AND C7_YCODZ11 = '" + Z11->Z11_CODIGO + "' "
    cQuery += " AND C7_YCODZ01 = '" + Z01->Z01_CODIGO + "' "

    If Select('QRYSC7') > 0
        QRYSC7->(DbCloseArea())
    EndIf

    TcQuery cQuery New Alias 'QRYSC7'

    If QRYSC7->(!Eof())
        nRet := QRYSC7->C7_QUANT
    EndIf

Return nRet

Static Function validaEstoque(aFinal)
    Local cErros    := ""
    Local lRet      := .T.
    Local i, j, cAuxForn, cAuxLoja
    
    For i := 1 to Len(aFinal)

        //Pega o fornecedor
        cAuxForn    := Left(aFinal[i][1], Len(SA2->A2_COD)) 
        cAuxLoja    := Right(aFinal[i][1], Len(SA2->A2_LOJA)) 

        SA2->(DbSetOrder(1))
        SA2->(DbSeek(xFilial('SA2') + cAuxForn + cAuxLoja))
        
        //A partir do segundo item, estao os dados dos itens do fornecedor atual
        For j := 2 to Len(aFinal[i])
            
            //Seta o produto atual
            SB1->(DbSetOrder(1))
            SB1->(DbSeek(xFilial('SB1') +  aFinal[i][j][1]))
            
            //Quantidade que sera gerada
            nQuant      := aFinal[i][j][2]
            SB2->(DbSeek(xFilial("SB2") + SB1->B1_COD + SA2->A2_YLOCAL))
            nSaldoFor   := SaldoSb2()
            
            If nQuant > nSaldoFor
                cErros += "O Fornecedor:  " + Alltrim(SA2->A2_NREDUZ) +  " n�o possui saldo suficiente para o produto " +;
                 Alltrim(SB1->B1_DESC) + "(Saldo faltante: "+cValToChar(nQuant - nSaldoFor)+")" + Chr(13) + Chr(10) + Chr(13) + Chr(10)                 
            EndIf        
        Next
    Next

    If !Empty(cErros)
        cErros := "A opera��o n�o pode ser conclu�da pois foram encontrados os seguintes erros: " + Chr(13) + Chr(10) + Chr(13) + Chr(10) + cErros
        MsgAlert(cErros, 'Aten��o')
        lRet := .F.
    EndIf

Return lRet

Static Function VerPedidos(cProduto)
    Local oDlg      
    Local oPanel01
    Local oPanelModal
    Local oLayer
    Local oBrwPeds
    Local i
    Local aHeader  := {}
    Local aCols    := {}
    
    //Cabecalho da grid de titulos
    aAdd(aHeader,{'Pedido'			,10, ''				})
    aAdd(aHeader,{'Cliente'	        ,50, ''	            })
    aAdd(aHeader,{'Quantidade'		,10, '' 		    })
    SC5->(DbSetOrder(1))
    SA1->(DbSetOrder(1))
    SB1->(DbSetOrder(1))
    
    nTotProds := 0
    
    For i := 1 to Len(aPedsProd)
        If aPedsProd[i][2] == cProduto
            If SC5->(Dbseek(xFilial('SC5') + aPedsProd[i][1]))
                SB1->(DbSeek(xFilial('SB1') + cProduto))
                SA1->(DbSeek(xFilial('SA1') + SC5->C5_CLIENTE + SC5->C5_LOJACLI))
                nTotProds += aPedsProd[i][3]
                aAdd(aCols, {SC5->C5_NUM, SA1->A1_NOME, aPedsProd[i][3]})
            EndIf
        EndIf
    Next

    oDlg := FwDialogModal():New()
    oDlg:SetTitle('Pedidos com o Produto: ' +  Alltrim(SB1->B1_DESC) + ' | TOTAL: ' + cValToChar(nTotProds) )
    oDlg:SetSize(250,400)
    oDlg:CreateDialog()	
    oDlg:addCloseButton( , "Fechar")
    oPanelModal	:= oDlg:GetPanelMain()

    oLayer := FwLayer():New()
    oLayer:Init(oPanelModal)

    //Linha dos Filtros
    oLayer:addLine("lin01",100,.T.)		
    oLayer:AddCollumn('col01', 100, .T., 'lin01')	
    oLayer:addWindow("col01","win01",'Pedidos',100,.F.,.F.,{|| }, "lin01",{|| })
    oPanel01 := oLayer:getWinPanel('col01', 'win01', "lin01")

    oBrwPeds 	:= uFwBrowse():create(oPanel01,,aCols,,aHeader,, ,, .F.)	

    oBrwPeds:disableConfig()
    oBrwPeds:disableFilter()
    oBrwPeds:disableReport()

    oBrwPeds:Activate()

    //Ativa a tela principal
    oDlg:Activate()	

Return
