#INCLUDE 'Rwmake.ch'
#INCLUDE 'Protheus.ch'
#INCLUDE 'TbIconn.ch'
#INCLUDE 'Topconn.ch'

//-------------------------------------------------------------------
/*/{Protheus.doc} IFAA0004
description
@author  Samuel Dantas
@since   11/12/2018
@version 1.0
/*/
//-------------------------------------------------------------------
User Function IFAA0004()
    Private cCadastro := 'Produtos Finais'

    Private aRotina := {{'Pesquisar' ,'AxPesqui',0,1} ,;
                       {'Visualizar' ,'AxVisual',0,2} ,;
                       {'Incluir' ,'AxInclui',0,3} ,;
                       {'Alterar' ,'AxAltera',0,4} ,;
                       {'Excluir' ,'AxDeleta',0,5} }


    Private cDelFunc := '.T.' // Validacao para a exclusao. Pode-se utilizar ExecBlock

    Private cString := 'Z06'
    DbSelectArea(cString)
    DbSetOrder(1)
    DbSelectArea(cString)

    Set Filter To Z06->Z06_CODSB1 == SB1->B1_COD
    
    mBrowse( 6,1,22,75,cString)

    Set Filter To

Return
