#INCLUDE 'Rwmake.ch'
#INCLUDE 'Protheus.ch'
#INCLUDE 'TbIconn.ch'
#INCLUDE 'Topconn.ch'

 //-------------------------------------------------------------------
 /*/{Protheus.doc} Funcao para retornar a descri��o da condi��o de pgto
  @author  Sidney Sales
 @since   20/01/2020
 @version 1.0
 /*/
 //-------------------------------------------------------------------
 User Function IFAF0007()
    Local cQuery
    Local cRet := ''
      
    cQuery := " SELECT C5_YFORMPG FROM " + RetSqlTab('SC5')
    cQuery += " WHERE D_E_L_E_T_ = ''"
    cQuery += " AND C5_NOTA = '" + SE1->E1_NUM + "' AND C5_SERIE = '" + SE1->E1_PREFIXO + "' "

    If Select("QRYE1") > 0
        QRYE1->(DbCloseArea())
    EndIf

    TcQuery cQuery New Alias 'QRYE1'

    If QRYE1->(!Eof())
        Z15->(DbSetOrder(1))
        If Z15->(DbSeek(xFIlial('Z15') + QRYE1->C5_YFORMPG))
            cRet := Alltrim(Z15->Z15_NOME)
        EndIf
    EndIf

 Return cRet


 //-------------------------------------------------------------------
 /*/{Protheus.doc} IFAF007A
     Fun��o utilizada para atualiza��o do endere�o do cliente.
     @type  User Function
     @author Samuel Dantas
     @since   17/07/2020
     @version 1.0
 /*/
 //-------------------------------------------------------------------
User Function IFAF007A(cCodCli,cLoja)
    Local aAreaSA1 
    Local aAreaSX5 
    Default cCodCli := ""
    Default cLoja   := ""
    
    If Empty(FunName())
        PREPARE ENVIRONMENT EMPRESA '01' FILIAL '01'
    EndIf

    aAreaSA1 := SA1->(GetArea())
    aAreaSX5 := SX5->(GetArea())

    SA1->(DbSetOrder(1))
    Z09->(DbSetOrder(2))
    //Se n�o foi chamada via Job
    If !Empty(cCodCli+cLoja)
        cCodCli := PADR(cCodCli, len(SA1->A1_COD))
        cLoja   := PADR(cLoja, len(SA1->A1_LOJA))
        If SA1->(DbSeek(xFIlial("SA1") + cCodCli + cLoja ))
            PopulaSA1()
        EndIf
    Else //Popula toda tabela SA1 que possui v�nculo com Z09
        SA1->(DbGoTop())
        While SA1->(!EoF())
            PopulaSA1()
            SA1->(DbSkip())
        EndDo
    EndIf

    SA1->(RestArea(aAreaSA1))
    SX5->(RestArea(aAreaSX5))
Return


//-------------------------------------------------------------------
 /*/{Protheus.doc} IFAF007B
     Fun��o utilizada para atualiza��o do endere�o do cliente a partir 
     pedido de VEMDA.
     @type  User Function
     @author Samuel Dantas
     @since   17/07/2020
     @version 1.0
 /*/
 //-------------------------------------------------------------------
User Function IFAF007B(nRecnoSC5)
    Local aAreaSA1 
    Local aAreaSC5 
    Default nRecnoSC5 := 0
    
    If Empty(FunName())
        PREPARE ENVIRONMENT EMPRESA '01' FILIAL '01'
    EndIf

    aAreaSA1 := SA1->(GetArea())
    aAreaSC5 := SC5->(GetArea())
    aAreaSX5 := SX5->(GetArea())
    //Posiciona SC5
    SC5->(DbGoTo(nRecnoSC5))    
    //Seta ordens da tabela
    SA1->(DbSetOrder(1))
    Z09->(DbSetOrder(2))
    SX5->(DbSetOrder(1))
    
    cCodCli := PADR(SC5->C5_CLIENTE, len(SA1->A1_COD))
    cLoja   := PADR(SC5->C5_LOJACLI, len(SA1->A1_LOJA))

    //Posiciona cliente
    If SA1->(DbSeek(xFilial("SA1") + cCodCli + cLoja ))
        If !Empty(SC5->C5_YCODZ09) .AND. Z09->(DbSeek(xFIlial('Z09') +PADR(SC5->(C5_YCODZ09),len(Z09->Z09_CODIGO)) ))
            RecLock('SA1', .F.)
                SA1->A1_END     := Alltrim(Z09->Z09_RUA)+" "+ Alltrim(Z09->Z09_NUMERO)
                SA1->A1_BAIRRO  := Alltrim(Z09->Z09_BAIRRO)
                SA1->A1_EST     := UPPER(Alltrim(Z09->Z09_EST))
                SA1->A1_CEP     := Alltrim(Z09->Z09_CEP)
                SA1->A1_MUN     := UPPER(Alltrim(Z09->Z09_MUN))
                SA1->A1_PAIS    := "105"
                SA1->A1_COD_MUN := POSICIONE("CC2",4,xFilial('CC2')+UPPER(Alltrim(Z09->Z09_EST))+UPPER(Alltrim(Z09->Z09_MUN)),"CC2_CODMUN" )
                
                //Nome do estado por extenso
                If SX5->(DbSeek(xFIlial("SX5")+"12"+ SA1->A1_EST))
                    SA1->A1_ESTADO := Alltrim(SX5->X5_DESCRI)
                EndIf
                If Empty(SA1->A1_INSCR)
                    SA1->A1_INSCR := "ISENTO"
                EndIf

                If Empty(SA1->A1_INSCRM)
                    SA1->A1_INSCRM := "ISENTO"
                EndIf
            SA1->(MsUnLock())
        EndIf
    EndIf

    SX5->(RestArea(aAreaSX5))
    SA1->(RestArea(aAreaSA1))
    SC5->(RestArea(aAreaSC5))
Return


//-------------------------------------------------------------------
/*/{Protheus.doc} PopulaSA1
    M�todo usado para popular dados do endere�o do cliente, com base
    no cadastro da Z09
    @type  Static Function
    @author Samuel Dantas
    @since   17/07/2020
    @version 1.0
/*/
//-------------------------------------------------------------------
Static Function PopulaSA1()
    
    Local cQuery := ""

    cQuery := " SELECT TOP 1 C5_YCODZ09 FROM "  + RetSqlName('SC5') + " SC5"
    cQuery += " WHERE SC5.D_E_L_E_T_ <> '*' AND C5_CLIENTE =  '"+SA1->A1_COD+"' AND C5_LOJACLI = '"+SA1->A1_LOJA+"' ORDER BY C5_EMISSAO DESC "
    
    If Select('QSC5') > 0
        QSC5->(dbclosearea())
    EndIf
    
    TcQuery cQuery New Alias 'QSC5'
    
    While QSC5->(!Eof())
        If !Empty(QSC5->C5_YCODZ09) .AND. Z09->(DbSeek(xFIlial('Z09') +PADR(QSC5->(C5_YCODZ09),len(Z09->Z09_CODIGO)) ))
            RecLock('SA1', .F.)
                SA1->A1_END     := Alltrim(Z09->Z09_RUA)+" "+ Alltrim(Z09->Z09_NUMERO)
                SA1->A1_BAIRRO  := Alltrim(Z09->Z09_BAIRRO)
                SA1->A1_EST     := UPPER(Alltrim(Z09->Z09_EST))
                SA1->A1_CEP     := Alltrim(Z09->Z09_CEP)
                SA1->A1_MUN     := UPPER(Alltrim(Z09->Z09_MUN))
                SA1->A1_PAIS    := "105"
                SA1->A1_COD_MUN := POSICIONE("CC2",4,xFilial('CC2')+UPPER(Alltrim(Z09->Z09_EST))+UPPER(Alltrim(Z09->Z09_MUN)),"CC2_CODMUN" )
                
                //Nome do estado por extenso
                If SX5->(DbSeek(xFIlial("SX5")+"12"+ SA1->A1_EST))
                    SA1->A1_ESTADO := Alltrim(SX5->X5_DESCRI)
                EndIf

                If Empty(SA1->A1_INSCR)
                    SA1->A1_INSCR := "ISENTO"
                EndIf

                If Empty(SA1->A1_INSCRM)
                    SA1->A1_INSCRM := "ISENTO"
                EndIf
                
            SA1->(MsUnLock())
        EndIf
        QSC5->(dbSkip())
    EndDo

Return



//-------------------------------------------------------------------
/*/{Protheus.doc} PopulaSA1
    M�todo usado para popular dados do endere�o do cliente, com base
    no cadastro da Z09
    @type  Static Function
    @author Samuel Dantas
    @since   17/07/2020
    @version 1.0
/*/
//-------------------------------------------------------------------
// User Function LimpaSa1()
    
//     Local cQuery := ""
//     Local nCount := 0
//     If Empty(FunName())
//         PREPARE ENVIRONMENT EMPRESA '01' FILIAL '01'
//     EndIf
    
    
//     SA1->(DbSetOrder(1))
//     Z09->(DbSetOrder(2))
//     SA1->(DbGoTop())
//     While SA1->(!EoF())
        
//         cQuery := " SELECT TOP 1 C5_YCODZ09 FROM "  + RetSqlName('SC5') + " SC5"
//         cQuery += " WHERE SC5.D_E_L_E_T_ <> '*' AND C5_CLIENTE =  '"+SA1->A1_COD+"' AND C5_LOJACLI = '"+SA1->A1_LOJA+"' ORDER BY C5_EMISSAO DESC "
        
//         If Select('QSC5') > 0
//             QSC5->(dbclosearea())
//         EndIf
        
//         TcQuery cQuery New Alias 'QSC5'
        
//         While QSC5->(!Eof())
//             If !Empty(QSC5->C5_YCODZ09) .AND. Z09->(DbSeek(xFIlial('Z09') +PADR(QSC5->(C5_YCODZ09),len(Z09->Z09_CODIGO)) ))
//                 RecLock('SA1', .F.)
//                     SA1->A1_END     := ""
//                     SA1->A1_BAIRRO  := ""
//                     SA1->A1_EST     := ""
//                     SA1->A1_CEP     := ""
//                     SA1->A1_MUN     := ""
//                     SA1->A1_PAIS    := ""
//                     SA1->A1_COD_MUN := ""
//                     SA1->A1_ESTADO := ""
                    
//                     If Empty(SA1->A1_INSCR)
//                         SA1->A1_INSCR := ""
//                     EndIf

//                     If Empty(SA1->A1_INSCRM)
//                         SA1->A1_INSCRM := ""
//                     EndIf
                    
//                 SA1->(MsUnLock())
//                 nCount += 1
//             EndIf
//             QSC5->(dbSkip())
//         EndDo
//         SA1->(DbSkip())
//     EndDo
// Return
