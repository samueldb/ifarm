#INCLUDE 'Rwmake.ch'
#INCLUDE 'Protheus.ch'
#INCLUDE 'TbIconn.ch'
#INCLUDE 'Topconn.ch'
//-------------------------------------------------------------------
/*/{Protheus.doc} IFAF0005
Funcao para comunicação com o mercado pago
@author  Sidney Sales
@since   23/04/2019
@version 1.0
/*/
//-------------------------------------------------------------------
User Function IFAF0005(nRecNoSC5)   
	Local aHeader   := {}
    Local cID       := ''
    Local cULTMSG   := ''
    Local cStatus   := ''
    Local cToken    
    Local oRest     

    Default nRecNoSC5   := SC5->(Recno())
    //Posiciona o SE5
	SC5->(DbGoTo(nRecNoSC5))

    //Posiciona o cliente
    SA1->(DbSeek(xFilial('SA1') + SC5->C5_CLIENTE + SC5->C5_LOJACLI))
    
    //Cabecalho da requisicao
    aAdd(aHeader, "Content-Type : application/json")
    
    //Token de acesso do mercado pago
    cToken  := Alltrim(SuperGetMV("MS_TOKENMP"  , .F., "TEST-7863235948118309-042310-d45196dae2d5f2ce1c56417fbb9677a2-319192752"))

    //URI Base para api do mercado pago
    cURLMP  := Alltrim(SuperGetMV("MS_URLMP"    , .F., "https://api.mercadopago.com/v1/payments"))    

    //Cria o objeto rest de comunicacao na URL
    oRest   := FwRest():new(cURLMP)
    
    //Monta o objeto(Corpo) que sera enviaro
    oJson := JsonObject():new()
    
    oJson['token']                  := Alltrim(SC5->C5_YTOKEN)
    oJson['payment_method_id']      := Alltrim(SC5->C5_YMETOD)
    oJson['transaction_amount']     := totalPedido()
    oJson['installments']           := SC5->C5_YPARCEL
    
    //Objeto do pagador
    oPayer  :=  JsonObject():new()
    oPayer['email']                 := Alltrim(SA1->A1_YEMAIL)
    oJson['payer']                  := oPayer
    
    oPayerAdd   :=  JsonObject():new()
    oPayerAux   := JsonObject():new()
    
    //oPayerAdd['registration_date'] := FWTIMESTAMP(3, SA1->A1_DTCAD)
    
    aNome := StrTokArr2(SA1->A1_NOME, ' ')
    If Len(aNome) >= 2
        oPayerAux['first_name'] := aNome[1]
        oPayerAux['last_name']  := aNome[2]
    EndIf   

    oPhone  :=  JsonObject():new()    
    oPhone['area_code'] := Left(SA1->A1_TEL,2)
    oPhone['number']    := Alltrim(SubString(SA1->A1_TEL,3,10))
    
    oPayerAux['phone']  := oPhone

    Z09->(DbSetOrder(1))
    If Z09->(DbSeek(xFilial('Z09') + SA1->A1_COD + SA1->A1_LOJA))
        oAddress  :=  JsonObject():new()    
        oAddress['zip_code']        := Alltrim(Z09->Z09_CEP)
        oAddress['street_name']     := Alltrim(Z09->Z09_RUA)
        oAddress['street_number']   := Val(Z09->Z09_NUMERO)            
        oPayerAux['address']  := oAddress
    EndIf

    SC6->(DbSetOrder(1))
    SC6->(DbSeek(cSeekSC6 := xFilial("SC6") + SC5->C5_NUM))
    aItems := {}
    
    While SC6->(!Eof()) .AND. SC6->(C6_FILIAL + C6_NUM) == cSeekSC6
        oItem := JsonObject():new()
        oItem['id'] := SC6->C6_PRODUTO
        oItem['title']  := Alltrim(SC6->C6_DESCRI)
        oItem['description'] := Alltrim(SC6->C6_DESCRI)
        oItem['quantity'] := SC6->C6_QTDVEN
        oItem['unit_price'] := SC6->C6_PRCVEN
        aAdd(aItems, oItem)
        SC6->(DbSkip())
    EndDo

    oPayerAdd['items'] := aItems    
    oPayerAdd['payer'] := oPayerAux    
    oJson['additional_info'] := oPayerAdd
    
    //Da o post no campos do corpo
    cJson   := oJson:ToJson()
    
    oRest:SetPostParams(cJson)
    oResult := JsonObject():new()
    
    //Se nao tiver ID, ira realizar o post incluindo o pagamento
    If Empty(SC5->C5_YIDMP)

        //Seta o token de acesso
        oRest:setPath("?access_token="+cToken)

        //Aplica o metodo post
        If oRest:Post(aHeader)
            oResult:FromJson(oRest:GetResult())
            cID     := oResult['id']
            cStatus := oResult['status']         
            cULTMSG := oResult['status_detail']   
        Else        

            //Verifica o erro
            oResult:FromJson(oRest:GetResult())
            cErro := oResult['message']

            //Caso nao tenha erro, tenta capturar o erro do objeto
            If Empty(cErro)
                cErro := oRest:GetLastError()
            EndIf

            cULTMSG     := cErro
            MsgAlert('Erro na comunicação com o mercado pago. ' + cErro)

        EndIf
    Else

        //Seta o ID + token de acesso
        oRest:setPath("/"+Alltrim(SC5->C5_YIDMP)+"/?access_token="+cToken )    

        //Realiza o get, para verificar o status atual do pagamento
        If oRest:get(aHeader)
            oResult:FromJson(oRest:GetResult())                
            cStatus := oResult['status']         
            cULTMSG := oResult['status_detail']                
        Else        
            oResult:FromJson(oRest:GetResult())
            cErro := oResult['message']
            If Empty(cErro)
                cErro := oRest:GetLastError()
            EndIf
            cULTMSG     := cErro            
            MsgAlert('Erro na comunicação com o mercado pago. ' + cErro)
        EndIf           
    EndIf

    RecLock('SC5', .F.)

        If !Empty(cID)
            SC5->C5_YIDMP   := cValToChar(cID)
        EndIf

        If !Empty(cStatus)
            SC5->C5_YSTATMP := cStatus  
        EndIf

        SC5->C5_YULTMSG := cULTMSG

    SC5->(MsUnLock())

    Aviso('Retorno MP',  Alltrim(SC5->C5_YSTATMP) + ' - ' + cULTMSG)

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} totalPedido
Funcao que retornara o valor do pedido
@author  Sidney Sales
@since   23/04/19
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function totalPedido
    Local aAreaSC6  := SC6->(GetArea())
    Local cSeekSC6 
    Local nSubtot   := 0   
    
    SC6->(DbSeek(cSeekSC6 := xFilial('SC6') + SC5->C5_NUM))
    
    While (SC6->(!EOF())) .AND. cSeekSC6 == SC6->(C6_FILIAL + C6_NUM)
        nSubtot += SC6->C6_QTDVEN * SC6->C6_PRCVEN
        SC6->(DbSkip())
    EndDo

    nTotal := nSubtot + SC5->(C5_FRETE - C5_YDESCFR)

Return nTotal