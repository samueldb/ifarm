//-------------------------------------------------------------------
/*/{Protheus.doc} IFAA0011
Cadastro de hor�rio dos estabelecimentos
@author  Samuel Dantas
@since   27/12/2018
@version 1.0
/*/
//-------------------------------------------------------------------
User Function IFAA0011()
    Local i   
    Private cCadastro   := 'Horarios do estabelecimento'
    Private aRotina     := {{'Pesquisar' ,'AxPesqui',0,1} ,;
                            {'Visualizar' ,'AxVisual',0,2} ,;
                            {'Alterar' ,'AxAltera',0,4} }

    Private cDelFunc    := '.T.' // Validacao para a exclusao. Pode-se utilizar ExecBlock
    Private cString     := 'Z13'

    
    Z13->(DbSeek(xFilial('Z13')))
    
    If Z13->(Eof())

        For i := 1 to 7
            RecLock('Z13', .T.)
                Z13->Z13_FILIAL := xFilial('Z13')
                Z13->Z13_DIASEM := cValToChar(i)
                Z13->Z13_INICIO := '00:00'
                Z13->Z13_FIM := '23:59'
            Z13->(MsUnLock())
        Next

    EndIf

    DbSelectArea(cString)
    DbSetOrder(1)
    DbSelectArea(cString)

    mBrowse( 6,1,22,75,cString)

Return

