#Include 'Protheus.ch'
#Include 'TbIconn.ch'
#Include 'Topconn.ch'
//-------------------------------------------------------------------
/*/{Protheus.doc} IFAA0006
Tela para atualiza��o de pre�o de venda dos produtos
@author  Samuel Dantas
@since   12/12/2018
/*/
//-------------------------------------------------------------------
User Function IFAA0006()
	Local oDlg
	Local aHeader			:= {}
	Local aCols 			:= {}
	Local aAux 				:= {}
	Local cRet				:= ''
	Local cTitulo			:= ''
	Local lRet				:= .F.
	Local nX 				:= 1080
	Local nY 				:= 660

    nX := msAdvSize(.F.)[5]
    nY := msAdvSize(.F.)[6]

	Private oLayer			:= FWLayer():New()

    //Posi��es no aCols
    Private nCodProd        := 2
    Private nPrv1           := 4
    Private nPrvPR          := 5

	Private oBrwProd


	cTitulo += "Altera��o de pre�o de produtos"
    
	oDlg := FwDialogModal():New()
	
	oDlg:SetTitle(cTitulo)
	
	oDlg:enableAllClient()
	oDlg:CreateDialog()	

	oPanelModal	:= oDlg:GetPanelMain()


	//Cria o layer
	oLayer:init(oPanelModal,.F.)

	//Panel parados produtos�
	oLayer:addLine("lin01",100,.F.)
	oLayer:addCollumn("col01",100,.F.,"lin01")
	oLayer:addWindow("col01","win01",'Produtos',100,.F.,.F.,{|| }, "lin01",{|| })
	oPanel01 := oLayer:getWinPanel("col01","win01", "lin01")

	aHeader	:= {}
	aCols	:= {}

	//Cria header dos produtos
	aAdd(aHeader,	{"Item"			    ,"cItem"		,"",06,0,"","�","C",""	,"V",,,".F."  })
	aAdd(aHeader,	{"Produto"		    ,"B1_COD"       ,"",15,0,"","�","C",""  ,"V",,,".F."  })
	aAdd(aHeader,	{"Descricao"	    ,"B1_DESC"      ,"",50,0,"","�","C",""	,"V",,,".F."  })
	aAdd(aHeader,	{"Pre�o de"		    ,"B1_YPRCDE"	,"@E 999,999,999,999,999.99",18,2,"U_IFAA006A(.F.)","�","N",""	,"V",,,,".T." })
	aAdd(aHeader,	{"Pre�o Por"	    ,"B1_PRV1"	    ,"@E 999,999,999,999,999.99",18,2,"U_IFAA006A(.T.)","�","N",""	,"V",,,,".T." })

	//Funcao que popula o acols
	aCols := getProdutos()

	//Cria a grid dos produtos
	oBrwProd:= MsNewGetDados():New(050,010,200,390,GD_UPDATE,,,"+cItem",,,999,,,,oPanel01,aHeader,aCols)
	oBrwProd:oBrowse:Refresh()
	oBrwProd:oBrowse:Align := CONTROL_ALIGN_ALLCLIENT
    
	oDlg:AddButton('Sair'		, {|| oDlg:Deactivate() }, 'Sair', , .T., .T., .T., ) 

	oDlg:Activate()

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} IFAA006A
Funcao para atualizar o cadastro do SB1 ao digitar o valor
@author  Samuel Dantas
@since   12/12/2018
/*/
//-------------------------------------------------------------------
User Function IFAA006A(lPrcPromo)    
    Local aCols 	:= oBrwProd:aCols
    Local nPosAtu 	:= oBrwProd:nAt

    SB1->(DbGoTop())
    SB1->(DbSetOrder(1))

    If SB1->(DbSeek(xFilial("SB1") + aCols[nPosAtu][nCodProd]))
        nPorAnt := nPorAtu := SB1->B1_PRV1
        nDeAnt 	:= nDeAtu := SB1->B1_YPRCDE
		Reclock("SB1",.F.)
            //Atualiza campo de valor promocional
			If lPrcPromo
				nPorAnt	:= SB1->B1_PRV1
				nPorAtu	:= &(readvar())
				SB1->B1_PRV1	:= nPorAtu
            Else
                nDeAnt	:= SB1->B1_YPRCDE
				nDeAtu	:= &(readvar())
                SB1->B1_YPRCDE 	:= nDeAtu
            Endif			
        SB1->(MsUnlock())

		Reclock('Z10', .T.)
			Z10->Z10_FILIAL	:= xFilial("SB1")
			Z10->Z10_CODSB1	:= SB1->B1_COD
			Z10->Z10_DATA	:= dDataBase
			Z10->Z10_HORA	:= Time()
			Z10->Z10_USUARI	:= __cUserID + ' - ' + cUserName
			Z10->Z10_DEANT	:= nDeAnt
			Z10->Z10_PORANT	:= nPorAnt
			Z10->Z10_DEATU	:= nDeAtu
			Z10->Z10_PORATU	:= nPorAtu
		Z10->(MsUnlock())

    Endif

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} getProdutos
Busca informa��es de produto relacionados a cada fonecedor para ser
utilizado no aCOls
@author  Samuel Dantas
@since   12/12/2018
/*/
//-------------------------------------------------------------------
Static function getProdutos ()
    Local aRet := {}
    Local cSeek := ''
    Local cItem := '000001'

    //Posiciona tabela Z05 (FORNECEDOR e PRODUTO)
	SB1->(DbGoTop())
    SB1->(DbSetOrder(1))

    While (SB1->(!EOF()))
		//Carrega para o pre�o de o valor do pre�o atual, e por, vem zerado para ser preenchido
		aAdd(aRet,{cItem,SB1->B1_COD, SB1->B1_DESC, SB1->B1_YPRCDE,SB1->B1_PRV1, .F.})
        cItem := Soma1(cItem)
        SB1->(DbSkip())
    EndDo

Return aRet