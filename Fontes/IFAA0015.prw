
#Include 'Protheus.ch'
#Include 'Parmtype.ch'
#include "FWMVCDEF.CH"
#include "topconn.CH"

/*/{Protheus.doc} IFAA0015
Tela de loja virtual
- Cabecalho
Z17 - Cardapio 
- Filhas
Z18 - Produtos
Z21 - Categorias
@author Samuel Dantas
@since 03/01/2019
@version 1.0
/*/
User Function IFAA0015()
	Local oBrowse
	Private cString 		:= "Z17"  

	//Montagem do Browse principal	
	oBrowse := FWMBrowse():New()

	//Define alias principal
	oBrowse:SetAlias(cString)
	oBrowse:SetDescription('Loja Virtual')
	oBrowse:SetMenuDef('IFAA0015')

	oBrowse:Activate()

	SetKey(VK_F6,{||})

Return

/*/{Protheus.doc} MenuDef
Retorna o menu principal
@author Samuel Dantas
@since 03/01/2019
@version undefined
@return aRotina, Array com os dados para os botoes do browse
/*/ 
Static Function MenuDef()
	Local aRotina := {}
    
    aAdd( aRotina, { 'Visualizar'		, 'VIEWDEF.IFAA0015'	, 0, 2, 0, NIL } ) 
    aAdd( aRotina, { 'Incluir' 			, 'VIEWDEF.IFAA0015'	, 0, 3, 0, NIL } )
    aAdd( aRotina, { 'Alterar' 			, 'VIEWDEF.IFAA0015'	, 0, 4, 0, NIL } )
    aAdd( aRotina, { 'Excluir' 			, "VIEWDEF.IFAA0015"	, 0, 5, 0, NIL } )
    aAdd( aRotina, { 'Todos os Produtos', "U_IFAA015B"	, 0, 2, 0, NIL } )

Return aRotina

/*/{Protheus.doc} ModelDef
Construcao do modelo de dados
@author Samuel Dantas
@since 03/01/2019
@version 1.0
@return oModel, Retorna o objeto do modelo de dados
/*/
Static Function ModelDef()
	Local oModel
	Local oStruZ17 := FWFormStruct(1,"Z17")	
	Local oStruZ18 := FWFormStruct(1,"Z18")
	Local oStruZ21 := FWFormStruct(1,"Z21")

    oModel := MPFormModel():New("IFAA015", /*bPreValidacao*/,, /* GravaDados */, /*bCancel*/ )

	//Cria a estrutura principal(Z17)
	oModel:addFields('MASTERZ17',,oStruZ17)

	//Adiciona a chave
	oModel:SetPrimaryKey({'Z17_FILIAL', 'Z17_CODIGO'})

    oModel:AddGrid('Z21DETAIL','MASTERZ17',oStruZ21, /*bPreValidacao*/,, /*bPreVal*/, /*bPosVal*/, /*BLoad*/ )
	//Define a relacao entre as tabelas
	oModel:SetRelation('Z21DETAIL',{{'Z21_FILIAL','xFilial("Z21")'},{'Z21_CODZ17','Z17_CODIGO'}},Z21->(IndexKey(1)))

    aCamposZ18 := {}
	aAdd(aCamposZ18,{ 'Z18_FILIAL'	, "Z17_FILIAL" })
	aAdd(aCamposZ18,{ 'Z18_CODZ17'	, "Z17_CODIGO" })
	aAdd(aCamposZ18,{ 'Z18_CODZ21'	, "Z21_ITEM"    })

    //Cria estrutura de grid para os itens
	oModel:AddGrid('Z18DETAIL', 'Z21DETAIL', oStruZ18)
	
	
	//Define a relacao entre as tabelas, o parametro de indice ordena a grid. 
	oModel:SetRelation('Z18DETAIL', aCamposZ18, Z18->(IndexKey(5)))
    
	//Define a descricao dos modelos
	oModel:GetModel( 'MASTERZ17' ):SetDescription( 'Loja Virtual' )
	oModel:GetModel( 'Z21DETAIL' ):SetDescription( 'Categoria' )
	oModel:GetModel( 'Z18DETAIL' ):SetDescription( 'Produto' )

	//Define que o preenchimento da grid e' opcional
	oModel:GetModel('Z21DETAIL'):SetOptional( .T. )
	oModel:GetModel('Z18DETAIL'):SetOptional( .T. )

    //Define que a linha nao podera ter o conteudo repetido
	oModel:GetModel('Z21DETAIL'):SetUniqueLine({'Z21_FILIAL','Z21_CODZ17', 'Z21_CODSBM'})
	oModel:GetModel('Z18DETAIL'):SetUniqueLine({'Z18_FILIAL','Z18_CODZ17', 'Z18_CODSB1'})

    // AntesDeTudo
    // oModel:SetVldActivate( {|oModel| .T. } )
	//Atualiza quantidade em estoque na Z18
	U_IFAA015A()
Return oModel

/*/{Protheus.doc} ViewDef
Monta o view do modelo
@author Samuel Dantas
@since 03/01/2019
@version 1.0
/*/
Static Function ViewDef()
	Local oModel := ModelDef()
	Local oView
	Local oStrZ17:= FWFormStruct(2, 'Z17')
	Local oStrZ21:= FWFormStruct(2, 'Z21')
	Local oStrZ18:= FWFormStruct(2, 'Z18')

	oView := FWFormView():New()
	oView:SetModel(oModel)

	
	oView:addUserButton( 'Imagem(F6)', 'Imagens', { |oView| U_IFAF0003( oModel:GetModel('Z18DETAIL'):GetValue("Z18_CODSB1"),;
																		oModel:GetModel('Z18DETAIL'):GetValue("Z18_CODZ21"), ;
																		oModel:GetModel('Z18DETAIL'):GetValue("Z18_CODZ17"), ;
											 							oModel:GetModel('Z18DETAIL'):GetValue("Z18_ITEM"))})
	oView:addUserButton( 'Mudar Categoria', 'MudarCategoria', { |oView| MudarCategoria( oModel:GetModel('Z18DETAIL'):GetValue("Z18_CODSB1"),;
																		oModel:GetModel('Z18DETAIL'):GetValue("Z18_CODZ21"), ;
																		oModel:GetModel('Z18DETAIL'):GetValue("Z18_CODZ17"), ;
											 							oModel:GetModel('Z18DETAIL'):GetValue("Z18_ITEM"))})

	SetKey(VK_F6, { || U_IFAF0003(  oModel:GetModel('Z18DETAIL'):GetValue("Z18_CODSB1"),;
									oModel:GetModel('Z18DETAIL'):GetValue("Z18_CODZ21"), ;
									oModel:GetModel('Z18DETAIL'):GetValue("Z18_CODZ17"), ;
									oModel:GetModel('Z18DETAIL'):GetValue("Z18_ITEM"))})
	
    //Adiciona no nosso View um controle do tipo FormFields(antiga enchoice)
	oView:AddField('FORM_Z17' , oStrZ17,'MASTERZ17' ) 

    //Adiciona no nosso View um controle do tipo FormGrid(antiga newgetdados)
    oView:AddGrid( 'GRID_Z21', oStrZ21, 'Z21DETAIL' )
    oView:AddGrid( 'GRID_Z18', oStrZ18, 'Z18DETAIL' )

    // Define campos que terao Auto Incremento
    oView:AddIncrementField( 'GRID_Z21', 'Z21_ITEM' )
    oView:AddIncrementField( 'GRID_Z18', 'Z18_ITEM' )

    // 30% cabec e 70% para as abas
	oView:CreateHorizontalBox('SUPERIOR', 30)	
	oView:CreateHorizontalBox( 'INFERIOR', 70 )
	
    // Cria Folder na View
    oView:CreateFolder( 'PASTA_INFERIOR' ,'INFERIOR' )

    // Crias as pastas (abas)
    oView:AddSheet( 'PASTA_INFERIOR'    , 'ABA_Z21'  , "Categorias" )
    oView:AddSheet( 'PASTA_INFERIOR'    , 'ABA_Z18'  , "Produtos" )

    // Criar "box" horizontal com 100% dentro das Abas
    oView:CreateHorizontalBox( 'ITENS' 		,100,,, 'PASTA_INFERIOR', 'ABA_Z21' )
    oView:CreateHorizontalBox( 'DETALHES'   ,100,,, 'PASTA_INFERIOR', 'ABA_Z18' )

    // Liga a identificacao do componente
    // oView:EnableTitleView('GRID_Z21','Itens |')

    // Relaciona o ID da View com o "box" para exibicao
	oView:SetOwnerView('FORM_Z17', 'SUPERIOR')
	oView:SetOwnerView('GRID_Z21', 'ITENS'   )
	oView:SetOwnerView('GRID_Z18', 'DETALHES' )
	
Return oView

Static Function MudarCategoria(cCodProd, cZ18_CODZ21, cZ18_CODZ17, cZ18_ITEM)  
    Local cDirWeb        := Alltrim(SuperGetMV('MS_DIRIMGS',.F.,'/web/'))
    Local cDirLocal      := StrTran(cDirWeb, '/','\')    	
	Local aAreaZ18		 := Z18->(GetArea())
	Local aMvPar		 := {}
	Local aPergs		 := {}
	Local aRet			 := {}
	Local oModel := FwModelActive()
	
	u_gravapar(@aMvPar, .F.)

	Z18->(DbSetOrder(2)) //FILIAL+CODZ21+CODZ17+ITEM

	If Z18->(DbSeek(xFilial('Z18') + cZ18_CODZ21 + cZ18_CODZ17 + cZ18_ITEM )) .AND. MsgYesNo('Deseja realmente mudar a categoria do ' + Alltrim(Z18->Z18_DESC))
		
		nRecZ18 	:= Z18->(RecNo())		
		Z21->(DbSeek(xFilial('Z21') + cZ18_CODZ17 + cZ18_CODZ21)) 		
		cCategOld 	:= Alltrim(Z21->Z21_NOME)	

		aAdd(aPergs, {1,"Categoria"	    ,Space(Len(Z21->Z21_ITEM)),'@!','.T.','Z21','.T.',100,.F.})
        
        If ParamBox(aPergs ,"Informe a nova categoria",aRet,,,,,,,.F.,.F.)

			If Z21->(DbSeek(xFilial('Z21') + cZ18_CODZ17 + aRet[1])) 
	
				If MsgYesNo('Confirma a mudan�a de categoria de ' + cCategOld + ' para ' + Alltrim(Z21->Z21_NOME))

					cQryAux := " SELECT * FROM " + RetSqlName('Z18') + " Z18 "
					cQryAux += " WHERE D_E_L_E_T_ = '' AND R_E_C_N_O_ = " + cValToChar(Z18->(RecNo()))

					If Select('QRYZ18') > 0
						QRYZ18->(DbCloseArea())
					EndIf
					
					TcQuery cQryAux New Alias 'QRYZ18'

					cProxZ18 := U_Max('Z18', 'Z18_ITEM', "Z18_CODZ17 = '" + Z18->Z18_CODZ17 + "' AND Z18_CODZ21 = '" + Z18->Z18_CODZ21 + "'", '000001')

					RecLock('Z18', .T.)
						Z18->Z18_CODZ21	:= Z21->Z21_ITEM
						Z18->Z18_ITEM 	:= cProxZ18
						Z18->Z18_FILIAL := QRYZ18->Z18_FILIAL
						Z18->Z18_CODSB1 := QRYZ18->Z18_CODSB1
						Z18->Z18_DESC	:= QRYZ18->Z18_DESC
						Z18->Z18_CODZ17	:= QRYZ18->Z18_CODZ17 					
						Z18->Z18_PRECO	:= QRYZ18->Z18_PRECO
						Z18->Z18_PRCDE	:= QRYZ18->Z18_PRCDE
						Z18->Z18_QTDEST	:= QRYZ18->Z18_QTDEST
						Z18->Z18_URLIMG := QRYZ18->Z18_URLIMG
					Z18->(MsUnLock())

					Z18->(DbGoTo(nRecZ18))
					
					RecLock('Z18', .F.)
						Z18->(DbDelete())
					Z18->(MsUnLock())

					oModel:DeActivate()
					oModel:Activate()

					ApMsgInfo('Altera��o de categoria efetuada com sucesso. A tela ser� recarregada.' + Chr(13) + Chr(10) + 'Valide se a imagem do produto est� correta na nova categoria.', 'Aten��o')

				Else
					MsgAlert('Opera��o cancelada.')
				EndIf
			Else
				MsgAlert("A categoria informada " + aRet[1] + " n�o � v�lida.", "Aten��o")
			EndIf

		Else
			MsgAlert('Opera��o cancelada.')
		EndIf
		
	Else
		MsgAlert('Opera��o cancelada.')
	EndIf

	RestArea(aAreaZ18)

	u_gravapar(@aMvPar, .T.)

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} IFAA015A
Atualiza dados do estoque dos produtos do cardapio
@author  Samuel Dantas
@since   date
@version version
/*/
//-------------------------------------------------------------------
User function IFAA015A ()
	Local cSeek := ""
	
	Z18->(DbSetOrder(1))
	Z18->(DbSeek(cSeek := xFilial("Z18") + Z17->Z17_CODIGO))
	
	While Z18->(!EOF()) .AND. cSeek == Z18->Z18_FILIAL + Z18->Z18_CODZ17
		
		RecLock('Z18', .F.)
			Z18->Z18_QTDEST := U_IFAF001C(Z18->Z18_CODSB1)
		Z18->(MsUnLock())

		Z18->(DbSkip())
	EndDo
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} IFAA015B
Funcao para listar todos os produtos para impressao
@author  Sidney Sales
@since   24/06/19
@version 1.0
/*/
//-------------------------------------------------------------------
User Function IFAA015B

    Private cCadastro := 'Listagem de Produtos'

    Private aRotina := {{'Pesquisar' ,'AxPesqui',0,1} ,;
                       {'Visualizar' ,'AxVisual',0,2}}

    Private cDelFunc := '.T.' // Validacao para a exclusao. Pode-se utilizar ExecBlock

    Private cString := 'Z18'

    DbSelectArea(cString)
    DbSetOrder(1)
    DbSelectArea(cString)

    mBrowse( 6,1,22,75,cString)

Return