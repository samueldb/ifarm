
#Include 'Protheus.ch'
#Include 'Parmtype.ch'
#include "FWMVCDEF.CH"
#include "topconn.CH"

/*/{Protheus.doc} IFAA0020
Tela de planos
- Cabecalho
Z24 - Plano 
- Filhas
Z26 - Clientes
Z25 - Produtos
Z27 - Frete
@author Samuel Dantas
@since 03/01/2019
@version 1.0
/*/
User Function IFAA0020()
	Local oBrowse
	Private cString 		:= "Z24"  

	//Montagem do Browse principal	
	oBrowse := FWMBrowse():New()

	//Define alias principal
	oBrowse:SetAlias(cString)
	oBrowse:SetDescription('Planos')
	oBrowse:SetMenuDef('IFAA0020')

	oBrowse:Activate()

	SetKey(VK_F6,{||})

Return

/*/{Protheus.doc} MenuDef
Retorna o menu principal
@author Samuel Dantas
@since 03/01/2019
@version undefined
@return aRotina, Array com os dados para os botoes do browse
/*/ 
Static Function MenuDef()
	Local aRotina := {}
    
    aAdd( aRotina, { 'Visualizar'		, 'VIEWDEF.IFAA0020'	, 0, 2, 0, NIL } ) 
    aAdd( aRotina, { 'Incluir' 			, 'VIEWDEF.IFAA0020'	, 0, 3, 0, NIL } )
    aAdd( aRotina, { 'Alterar' 			, 'VIEWDEF.IFAA0020'	, 0, 4, 0, NIL } )
    aAdd( aRotina, { 'Excluir' 			, "VIEWDEF.IFAA0020"	, 0, 5, 0, NIL } )

Return aRotina

/*/{Protheus.doc} ModelDef
Construcao do modelo de dados
@author Samuel Dantas
@since 03/01/2019
@version 1.0
@return oModel, Retorna o objeto do modelo de dados
/*/
Static Function ModelDef()
	Local oModel
	Local oStruZ24 := FWFormStruct(1,"Z24")	
	Local oStruZ25 := FWFormStruct(1,"Z25")
	Local oStruZ26 := FWFormStruct(1,"Z26")
	

    oModel := MPFormModel():New("IFAA020", /*bPreValidacao*/,, /* GravaDados */, /*bCancel*/ )

	//Cria a estrutura principal(Z24)
	oModel:addFields('MASTERZ24',,oStruZ24)

	//Adiciona a chave
	oModel:SetPrimaryKey({'Z24_FILIAL', 'Z24_CODIGO'})

    oModel:AddGrid('Z25DETAIL','MASTERZ24',oStruZ25, /*bPreValidacao*/,, /*bPreVal*/, /*bPosVal*/, /*BLoad*/ )
	//Define a relacao entre as tabelas
	oModel:SetRelation('Z25DETAIL',{{'Z25_FILIAL','xFilial("Z25")'},{'Z25_CODZ24','Z24_CODIGO'}},Z25->(IndexKey(1)))

    aCamposZ26 := {}
	aAdd(aCamposZ26,{ 'Z26_FILIAL'	, 'xFilial("Z26")' 	})
	aAdd(aCamposZ26,{ 'Z26_CODZ24'	, "Z24_CODIGO" 		})
	
    //Cria estrutura de grid para os itens
	oModel:AddGrid('Z26DETAIL', 'MASTERZ24', oStruZ26)
	//Define a relacao entre as tabelas
	oModel:SetRelation('Z26DETAIL', aCamposZ26, Z26->(IndexKey(1)))
    
	//Define a descricao dos modelos
	oModel:GetModel( 'MASTERZ24' ):SetDescription( 'Planos' )
	oModel:GetModel( 'Z25DETAIL' ):SetDescription( 'Clientes do plano' )
	oModel:GetModel( 'Z26DETAIL' ):SetDescription( 'Produtos do plano' )

	//Define que o preenchimento da grid e' opcional
	oModel:GetModel('Z25DETAIL'):SetOptional( .T. )
	oModel:GetModel('Z26DETAIL'):SetOptional( .T. )

    //Define que a linha nao podera ter o conteudo repetido
	oModel:GetModel('Z25DETAIL'):SetUniqueLine({'Z25_FILIAL','Z25_CODZ24', 'Z25_CODSA1','Z25_LOJA'})
	oModel:GetModel('Z26DETAIL'):SetUniqueLine({'Z26_FILIAL','Z26_CODZ24', 'Z26_CODSBM','Z26_CODSB1'})

    // AntesDeTudo
    // oModel:SetVldActivate( {|oModel| .T. } )
	//Atualiza quantidade em estoque na Z26
	// U_IFAA015A()
Return oModel

/*/{Protheus.doc} ViewDef
Monta o view do modelo
@author Samuel Dantas
@since 03/01/2019
@version 1.0
/*/
Static Function ViewDef()
	Local oModel := ModelDef()
	Local oView
	Local oStrZ24:= FWFormStruct(2, 'Z24')
	Local oStrZ25:= FWFormStruct(2, 'Z25')
	Local oStrZ26:= FWFormStruct(2, 'Z26')

	oView := FWFormView():New()
	oView:SetModel(oModel)
	
    //Adiciona no nosso View um controle do tipo FormFields(antiga enchoice)
	oView:AddField('FORM_Z24' , oStrZ24,'MASTERZ24' ) 

    //Adiciona no nosso View um controle do tipo FormGrid(antiga newgetdados)
    oView:AddGrid( 'GRID_Z25', oStrZ25, 'Z25DETAIL' )
    oView:AddGrid( 'GRID_Z26', oStrZ26, 'Z26DETAIL' )

    // Define campos que terao Auto Incremento
    oView:AddIncrementField( 'GRID_Z25', 'Z25_ITEM' )
    oView:AddIncrementField( 'GRID_Z26', 'Z26_ITEM' )

    // 30% cabec e 70% para as abas
	oView:CreateHorizontalBox('SUPERIOR', 20)	
	oView:CreateHorizontalBox( 'INFERIOR', 80)
	
	// Criar "box" vertical para receber algum elemento da view
	oView:CreateVerticalBox( 'INFERIORESQ', 30, 'INFERIOR' )
	oView:CreateVerticalBox( 'INFERIORDIR', 70, 'INFERIOR' )


    // Cria Folder na View
    // oView:CreateFolder( 'PASTA_INFERIOR' ,'INFERIOR' )

    // Crias as pastas (abas)
    // oView:AddSheet( 'PASTA_INFERIOR'    , 'ABA_Z25'  , "Categorias" )
    // oView:AddSheet( 'PASTA_INFERIOR'    , 'ABA_Z26'  , "Produtos" )

    // Criar "box" horizontal com 100% dentro das Abas
    // oView:CreateHorizontalBox( 'ITENS' 		,100,,, 'PASTA_INFERIOR', 'ABA_Z25' )
    // oView:CreateHorizontalBox( 'DETALHES'   ,100,,, 'PASTA_INFERIOR', 'ABA_Z26' )

    // Liga a identificacao do componente
    oView:EnableTitleView('GRID_Z25','Participantes do plano ')
    oView:EnableTitleView('GRID_Z26','Itens do plano ')

    // Relaciona o ID da View com o "box" para exibicao
	oView:SetOwnerView('FORM_Z24', 'SUPERIOR')
	oView:SetOwnerView('GRID_Z25', 'INFERIORESQ' )
	oView:SetOwnerView('GRID_Z26', 'INFERIORDIR' )
	
Return oView