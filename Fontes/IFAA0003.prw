#INCLUDE 'Rwmake.ch'
#INCLUDE 'Protheus.ch'
#INCLUDE 'TbIconn.ch'
#INCLUDE 'Topconn.ch'

//-------------------------------------------------------------------
/*/{Protheus.doc} IFAA0003
Tela para cadastro de produtos relacionados a um fornecedor
@author  Samuel Dantas
@since   11/12/2018
@version 1.0
/*/
//-------------------------------------------------------------------
User Function IFAA0003()
    Private cCadastro := 'Produtos do fornecedor ' + SA2->A2_COD + ' - ' + Alltrim(SA2->A2_NOME)

    Private aRotina := {{'Pesquisar' ,'AxPesqui',0,1} ,;
                       {'Visualizar' ,'AxVisual',0,2} ,;
                       {'Incluir' ,'AxInclui',0,3} ,;
                       {'Alterar' ,'AxAltera',0,4} ,;
                       {'Excluir' ,'AxDeleta',0,5} }

    Private cDelFunc := '.T.' // Validacao para a exclusao. Pode-se utilizar ExecBlock

    Private cString := 'Z05'

    atuProdutos()

    DbSelectArea(cString)
    DbSetOrder(2)
    DbSelectArea(cString)

    Set Filter To ( Z05->Z05_CODSA2 == SA2->A2_COD .AND. Z05->Z05_LOJA == SA2->A2_LOJA )
    mBrowse( 6,1,22,75,cString)
    Set Filter to

Return

Static Function atuProdutos

    Z05->(DbSetOrder(1))
    SB1->(DbSetOrder(1))
    Z05->(DBGoTop())
    While Z05->(!Eof())
        SB1->(DBSeek(xFilial('SB1') + Z05->Z05_CODSB1))
        RecLock('Z05', .F.)
            Z05->Z05_DESC := SB1->B1_DESC
        Z05->(MsUnLock())
        Z05->(DbSkip())    
    EndDo
Return