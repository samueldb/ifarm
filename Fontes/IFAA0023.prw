#Include 'Protheus.ch'
#Include 'Parmtype.ch'
#include "FWMVCDEF.CH"
#include "TOPCONN.CH"
#INCLUDE 'Rwmake.ch'
#INCLUDE 'TbIconn.ch'
#include "Fileio.ch"

//-------------------------------------------------------------------
/*/{Protheus.doc} IFAA0023
Tela para cadastro de envio de watts app
@author  Sidney Sales
@since   23/03/2020
@version 1.0
/*/
//-------------------------------------------------------------------
User Function IFAA0023()
	Local oBrowse
	//Montagem do Browse principal	
	oBrowse := FWMBrowse():New()
	oBrowse:SetAlias('Z33')
	oBrowse:SetDescription('Cadastro Mensagens Watts')
	oBrowse:SetMenuDef('IFAA0023')
	oBrowse:AddLegend( "Z33_STATUS=='A'", "GREEN"	,	"Ativo"   ) // "Ativo"
	oBrowse:AddLegend( "Z33_STATUS=='I'", "RED"	    ,	"Inativo" ) // "Inativo"
	oBrowse:Activate()
Return

//Montagem do menu 
Static Function MenuDef()

	Local aRotina := {}

	aAdd( aRotina, { 'Visualizar'	, 'VIEWDEF.IFAA0023'	, 0, 2, 0, NIL } )
	aAdd( aRotina, { 'Incluir' 		, 'VIEWDEF.IFAA0023'    , 0, 3, 0, NIL } )
	aAdd( aRotina, { 'Alterar' 		, 'VIEWDEF.IFAA0023'	, 0, 4, 0, NIL } )
	aAdd( aRotina, { 'Excluir' 		, 'VIEWDEF.IFAA0023'	, 0, 5, 0, NIL } )	
	aAdd( aRotina, { 'Copiar' 		, 'VIEWDEF.IFAA0023'	, 0, 9, 0, NIL } )
	aAdd( aRotina, { 'Enviar' 		, 'U_IFAA023A()'    	, 0, 9, 0, NIL } )

Return aRotina

//Construcao do mdelo
Static Function ModelDef()
	Local oModel
	Local oStruZ33 := FWFormStruct(1,"Z33")

	oModel := MPFormModel():New("MD_Z33") 
	oModel:addFields('MASTERZ33',,oStruZ33)
	oModel:SetPrimaryKey({'Z33_FILIAL', 'Z33_CODIGO'})

Return oModel

//Construcao da visualizacao
Static Function ViewDef()
	Local oModel := ModelDef()
	Local oView
	Local oStrZ33:= FWFormStruct(2, 'Z33')

	oView := FWFormView():New()
	oView:SetModel(oModel)

	oView:addUserButton( 'Imagem(F6)', 'Imagens', { |oView| U_IFAA023C(.F.)})
	SetKey(VK_F6, { || U_IFAA023C(.F.)})

	oView:AddField('FORM_Z33' , oStrZ33,'MASTERZ33' ) 
	oView:CreateHorizontalBox( 'BOX_FORM_Z33', 100)
	oView:SetOwnerView('FORM_Z33','BOX_FORM_Z33')

Return oView

//-------------------------------------------------------------------
/*/{Protheus.doc} IFAA023A
Funcao que monta a tela para envio
@author  Sidney Sales
@since   23/03/2020
@version 1.0
/*/
//-------------------------------------------------------------------
User Function IFAA023A
    Local oDlg
	Local oPanel01
	Local oPanelModal
	
    Private aDados  := {}
    Private cMark   := GetMark()
    Private oBrwTabs

	If Z33->Z33_ENVIMG == 'S' 
    	U_IFAA023C(.T.)
	EndIf

	aDados := retDados()
	
    oDlg := FwDialogModal():New()
  	oDlg:SetTitle('Envio de Mensagens via Watssapp')
    oDlg:SetSize(400,650)
	
	oDlg:CreateDialog()	
	oDlg:addCloseButton( , "Fechar")
	oDlg:AddButton('Enviar'	, {||  Enviar() },'Enviar' ,, .T., .T., .T., ) 
	
	oPanelModal	:= oDlg:GetPanelMain()
	
	oLayer := FwLayer():New()
	oLayer:Init(oPanelModal)

	// Se possui imagem, inicia com uma linha para imagem
	// If Z33->Z33_ENVIMG == 'S' 
	// 	//Linha dos Filtros
	// 	oLayer:addLine("lin01",050,.T.)		
	// 	oLayer:AddCollumn('col01', 100, .T., 'lin01')	
	// 	oLayer:addWindow("col01","win01",'Imagem a ser enviada' ,100,.F.,.F.,{|| }, "lin01",{|| })
	// 	oPanel01 := oLayer:getWinPanel('col01', 'win01', "lin01")

	// 	// Usando o New
	// 	oTBitmap1 := TBitmap():New(1,225,150,150,,Alltrim(Z33->Z33_URLIMG),.T.,oPanel01,;
	// 		{|| .T. },,.F.,.F.,,,.F.,,.T.,,.F.)
	// 	oTBitmap1:lStretch:= .f.
	// 	oTBitmap1:lAutoSize := .T.
	// 	// oTBitmap1::lScroll := .T.
	// EndIf
	

	//Linha dos Filtros
	oLayer:addLine("lin02", 100 ,.T.)		
	oLayer:AddCollumn('col01', 100, .T., 'lin02')	
	oLayer:addWindow("col01","win02",'Mensagens a Enviar - ' + Z33->Z33_DESCRI,100,.F.,.F.,{|| }, "lin02",{|| })
	oPanel02 := oLayer:getWinPanel('col01', 'win02', "lin02")

	//Cria array com os campo de marcacao
	aMark	:= {{ "{|| if(self:oData:aArray[self:nAt, 1] == cMark, 'LBOK','LBNO') }", "{|| u_IFAA023b(.F.) }", "{|| u_IFAA023b(.T.)  }" }}	
	oBrwTabs 	:= uFwBrowse():create(oPanel02,,aDados[2],,aDados[1],, aMark,, .F.)	

	oBrwTabs:disableConfig()
	oBrwTabs:disableFilter()
	oBrwTabs:disableReport()

	oBrwTabs:Activate()
	 
    //Ativa a tela principal
	oDlg:Activate()	
		                                 
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} IFAA023B
Funcao que marca e desmarca os itens
@author  Sidney Sales
@since   23/03/2020
@version 1.0
/*/
//-------------------------------------------------------------------
User Function IFAA023b(lTodos)
	Local aArray:= oBrwTabs:oData:aArray 
	Local nAt	:= oBrwTabs:nAt

	If lTodos
		For i := 1 to Len(aArray)
			if aArray[i, 1] == cMark
				aArray[i, 1] := '  '
			else
				aArray[i, 1] := cMark		
			EndIf
		Next
	Else
		if aArray[oBrwTabs:nAt, 1] == cMarK
			aArray[oBrwTabs:nAt, 1] := '  '
		else
			aArray[oBrwTabs:nAt, 1] := cMark		
		EndIf           
	EndIf

	oBrwTabs:Refresh()

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} retDados
Retorna os dados para a grid
@author  Sidney Sales
@since   23/03/2020
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function retDados()
	Local aHeader	:= {}
	Local aCols		:= {}
    Local aTabs     := {}
    Local nI        := 1
    Local bBlock 	:= ErrorBlock()
	Local lErroMsg  := .F.
	Local lErroQry  := .F.

    //Cabecalho da grid de titulos
    aAdd(aHeader,{''			,2, '' 				})
    aAdd(aHeader,{'Cliente'	    ,10, ''	            })
    aAdd(aHeader,{'Nome'		,10, '' 		    })
    aAdd(aHeader,{'Watts'		,10, '' 		    })
    aAdd(aHeader,{'Mensagem'	,50, '' 		    })


	//funcao que valida os erros em tempo de execucao
	ErrorBlock( {|e| ChecErro(e)})

	//Abre o controle para caso ocorra erro na formula
	BEGIN SEQUENCE

	cQuery := StrTran(Alltrim(Z33->Z33_SQL),Chr(13)+Chr(10), '')
    cQuery := &(Alltrim(Z33->Z33_SQL))

		ErrorBlock(bBlock)

		RECOVER       		

		//caso ocorra erros vem para o ponto
		lErroQry := .T.
		ErrorBlock(bBlock)

	END SEQUENCE 


    If Select('QRY') > 0
        QRY->(DbCloseArea())
    EndIf

    TcQuery cQuery New Alias 'QRY'

	Z34->(DbSetOrder(1))//Z34_FILIAL, Z34_CODZ33, Z34_CLIENT, R_E_C_N_O_, D_E_L_E_T_

	//Abre o controle para caso ocorra erro na formula
	BEGIN SEQUENCE

    While QRY->(!Eof())        
        
		SA1->(DbGoTo(QRY->CLIENTE))
		
		If ! Z34->(DbSeek(xFilial('Z34') + Z33->Z33_CODIGO + SA1->A1_COD)) .OR. Z33->Z33_REENVI == 'S'
        	
			cAuxMsg := StrTran(Alltrim(Z33->Z33_MSG), Chr(13) + Chr(10), '@@XX')
			If Right(cAuxMsg,4) == '@@XX'
				cAuxMsg := Left(cAuxMsg, Len(cAuxMsg)-4)
			EndIf
			
			cAuxMsg := &(cAuxMsg)			
			cAuxMsg := StrTran(cAuxMsg, '@@XX', Chr(13) + Chr(10))
			
			If Z33->Z33_PARA == 'G' //G=Grupo;C=Cliente                                                                                                               
				cTelGrupo := SA1->A1_YGROUP				
				If Empty(cTelGrupo)
					cTelGrupo := SA1->A1_TEL
				EndIf
			Else
				cTelGrupo := SA1->A1_TEL
			EndIf
			
			aAdd(aCols, {'  ', SA1->A1_COD, SA1->A1_NOME, Alltrim(cTelGrupo), cAuxMsg})
        
		EndIf
		
		QRY->(DbSkip())
    EndDo

		ErrorBlock(bBlock)

		RECOVER       		

		//caso ocorra erros vem para o ponto
		lErroMsg := .T.
		ErrorBlock(bBlock)

	END SEQUENCE 	


	If lErroQry
		MsgAlert('Aten��o, foram encontrados erros na query, revise-a e tente novamente.')
	EndIf
	If lErroQry
		MsgAlert('Aten��o, foram encontrados erros na mensagem, revise-a e tente novamente.')
	EndIf

	If Empty(aCols)
        aAdd(aCols, {'','','','','',.F.})
    EndIf
		
Return { aHeader, aCols }


Static Function Enviar()
	Local lEnd := .F.
    Private oProcess := MsNewProcess():New( {|lEnd| AtivaEnvio(),"Enviando Mensagens"} )
	oProcess:Activate()
Return

Static Function AtivaEnvio()
	Local aColsAux 	:= oBrwTabs:oData:aArray
	Local nTotal	:= 0
	Local nEnviados	:= 0
	Local nI

	Private nErros		:= 0
	Private nSucessos	:= 0

	//Conta quantos registros serao enviados
	For nI := 1 To len(aColsAux)
	    If aColsAux[nI][1] == cMark
			nTotal++
		EndIf
	Next

	If MsgYesNo('Confirma o envio de ' + cValToChar(nTotal) + ' mensagens?')

		oProcess:SetRegua1(nTotal)

		//Processa enviando cada mensagem		
		For nI := 1 To len(aColsAux)
			If aColsAux[nI][1] == cMark
				nEnviados++
				oProcess:IncRegua1("Enviando " + cValToChar(nEnviados) + '/' + cValToChar(nTotal))	
				oProcess:SetRegua2(1)
				oProcess:IncRegua2("Enviando para " + aColsAux[nI][4])	
				sendToClient(aColsAux[nI][5], aColsAux[nI][4], aColsAux[nI][2], Z33->Z33_CODIGO, Z33->Z33_PARA == 'G', Z33->Z33_ENVIMG == 'S', Z33->Z33_URLIMG )
			EndIf
		Next

		RecLock('Z33', .F.)
			Z33->Z33_DTULTE := dDataBase
			Z33->Z33_HRULTE := Time()
		Z33->(MsUnLock())

		ApMsgInfo('Processamento concluido('+cValToChar(nTotal)+')' + Chr(13) + Chr(10) + 'Total com Sucesso: ' + cValToChar(nSucessos) ;
																	+ Chr(13) + Chr(10) + 'Total com Erros: '   + cValToChar(nErros))
	Else 
		MsgAlert("Opera��o cancelada")
	EndIf

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} sendToClient
Funcao que faz o envio via wattsapp
@author  Sidney Sales
@since   23/03/2020
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function sendToClient(cMsg, cTel, cCli, cTabMsg, lParaGrupo, lEnvImg ,cUrlImg)
	Local cInstancia    := SuperGetMv('MS_INSTAWA',.F., 'instance49121')
	Local cToken		:= Alltrim(SuperGetMv('MS_TOKWAPP',.F.,'oiiojqorkkj64ezm'))
	Local cUrl          := Alltrim(SuperGetMv('MS_URLWAPP',.F.,'https://eu14.chat-api.com/')) + cInstancia
	Local cPath		    := '/sendMessage'     
	Local aHeader		:= {}

	Default cTabMsg 	:= ''
	Default cCli		:= ''
	Default lParaGrupo  := .F.
	Default lEnvImg  := .F.
	Default cUrlImg  := ""
	
	If lEnvImg
		cPath		    := '/sendFile'  
	EndIf

	aAdd(aHeader, 'Content-Type: application/json')

	oRest   := FwRest():new(cUrl)

	oRest:setPath(cPath + '?token=' + cToken )

	oJson 	:= JsonObject():new()
	oResult := JsonObject():new()
		
	If lParaGrupo 
		If Len(cTel) < 20
			oJson["phone"]	:= '55' + Alltrim(cTel)
		Else
			oJson["chatId"]	:= Alltrim(cTel)
		EndIf
	Else
		oJson["phone"]	:= '55' + Alltrim(cTel)
	EndIf
	
	If lEnvImg
		oJson["body"]	:= EBase64(cUrlImg)
	    oJson["caption"]	:= cMsg //EBase64("C:\temp\image_2020_05_19T11_16_23_411Z.png")
		oJson["filename"]	:= "feirinha_organica.png"
	Else
		oJson["body"]	:= cMsg
	EndIf

	oRest:SetPostParams(EncodeUtf8(oJson:ToJson()))
						
	If oRest:Post(aHeader)        
		cResult := oRest:GetResult()
		oResult:FromJson(cResult)	
		If oResult['sent']
			cStatus := 'S'
			nSucessos++
		Else
			cStatus := 'E'
			nErros++
		EndIf		
	Else
		cStatus := 'E'
		nErros++
	EndIf

	//Grava o log para nao enviar novamente para o mesmo cliente a mesma mensagem
	If !Empty(cTabMsg)
		RecLock('Z34', .T.)
			Z34->Z34_FILIAL := xFilial('Z34')
			Z34->Z34_CODZ33 := cTabMsg
			Z34->Z34_CLIENT := cCli
			Z34->Z34_TEL 	:= cTel
			Z34->Z34_MSG 	:= cMsg
			Z34->Z34_DATA 	:= dDataBase
			Z34->Z34_HORA 	:= Time()
			Z34->Z34_STATUS	:= cStatus
		Z34->(MsUnLock())
	EndIf	
	
Return        

//-------------------------------------------------------------------
/*/{Protheus.doc} sendToClient
Funcao que faz o envio via wattsapp
@author  Samuel Dantas
@since   23/03/2020
@version 1.0
/*/
//-------------------------------------------------------------------
User Function sendFile(cMsg, cTel, cCli)
	Local cInstancia    := ""
	Local cToken		:= ""
	Local cUrl          := ""
	Local cPath		    := '/sendFile'     
	Local aHeader		:= {}

	Local cTabMsg 	:= ''
	Local cCli		:= ''
	Local lParaGrupo  := .F.

	If Empty(FunName())
		PREPARE ENVIRONMENT EMPRESA '01' FILIAL '01'
	EndIf
	
	
	cInstancia    := SuperGetMv('MS_INSTAWA',.F., 'instance49121')
	cToken		:= Alltrim(SuperGetMv('MS_TOKWAPP',.F.,'oiiojqorkkj64ezm'))
	cUrl          := Alltrim(SuperGetMv('MS_URLWAPP',.F.,'https://eu14.chat-api.com/')) + cInstancia
	cPath		    := '/sendFile'     
	cTel := "84996314238"
	aAdd(aHeader, 'Content-Type: application/json')

	oRest   := FwRest():new(cUrl)

	oRest:setPath(cPath + '?token=' + cToken )

	oJson 	:= JsonObject():new()
	oResult := JsonObject():new()
		
	
	oJson["phone"]	:= '55' + Alltrim(cTel)
	oJson["phone"] := val(oJson["phone"])
	cMsg := "Teste"
	// oJson["filename"]	:= "521112829.html"
	// oJson["caption"]	:= cMsg
	oJson["body"]	:= EBase64("\msg_imgs\campos.png")
	oJson["caption"]	:= "teste https://google.com"//EBase64("C:\temp\image_2020_05_19T11_16_23_411Z.png")
	oJson["filename"]	:= "image_2020_05_19T11_16_23_411Z.png"
	// oJson["description"]	:= "testes"
	// cBase64 :=	
	oRest:SetPostParams(EncodeUtf8(oJson:ToJson()))
						
	If oRest:Post(aHeader)        
		cResult := oRest:GetResult()
		oResult:FromJson(cResult)	
		If oResult['sent']
			cStatus := 'S'
			nSucessos++
		Else
			cStatus := 'E'
			nErros++
		EndIf		
	Else
		cStatus := 'E'
		nErros++
	EndIf

	//Grava o log para nao enviar novamente para o mesmo cliente a mesma mensagem
	If !Empty(cTabMsg)
		RecLock('Z34', .T.)
			Z34->Z34_FILIAL := xFilial('Z34')
			Z34->Z34_CODZ33 := cTabMsg
			Z34->Z34_CLIENT := cCli
			Z34->Z34_TEL 	:= cTel
			Z34->Z34_MSG 	:= cMsg
			Z34->Z34_DATA 	:= dDataBase
			Z34->Z34_HORA 	:= Time()
			Z34->Z34_STATUS	:= cStatus
		Z34->(MsUnLock())
	EndIf	
	
Return  

Static Function EBase64(cFile)
	Local cTexto := ""
	Local aFile := {}
	Default  cFile := ""
	
	aFile := STRTOKARR2( cFile, "." )
	If len(aFile) > 1 
		nHandle := fopen(cFile , FO_READWRITE + FO_SHARED )
		cString := ""
		FRead( nHandle, cString, 9000000 ) //Carrega na vari�vel cString, a string ASCII do arquivo.

		cTexto := "data:image/"+IIF('png' $ aFile[2] ,"png", "jpeg" )+";base64,"+ Encode64(cString) //Converte o arquivo para BASE64
		

		fclose(nHandle)
	EndIf

Return cTexto


User Function IFAA023C(lPreview)
	Local cUrlImg := ""
	Local cTitulo := ""
	Local lRet := .F.
	Default lPreview := .F.
	
	cTitulo := IIF(lPreview, "Imagem a ser enviada", "Selecione uma imagem" ) 
	cUrlImg := IIF(lPreview, Alltrim(Z33->Z33_URLIMG), FWFldGet("Z33_URLIMG") ) 
	cImgFile := ""
	IF !Empty(cUrlImg )
		//Cria uma c�pia do arquivo utilizando cTexto em um processo inverso(Decode64) para validar a convers�o.    
		cImgFile := Alltrim(cUrlImg)
	EndIf
	
	DEFINE DIALOG oDlg TITLE cTitulo FROM 180,180 TO 650,800 PIXEL
 	oTBar := TBar():New( oDlg, 25, 32, .T.,,,, .F. )
    // Usando o New
    oTBitmap1 := TBitmap():New(01,01,260,184,,cImgFile,.T.,oDlg,;
        {|| .T. },,.T.,.F.,,,.F.,,.T.,,.F.)
    oTBitmap1:lAutoSize := .T.
	
    // oTBitmap1:lScroll := .T.
	oDlg:nHeight 	:= oTBitmap1:nClientHeight
	oDlg:nWidth		:= oTBitmap1:nClientWidth

	oBtn1 := TButton():New( 50, 052, "Salvar\Alterar", oTBar,;
		{|| lRet := SalvarImg(lPreview)},40,010,,,.F.,.T.,.F.,,.F.,,,.F. )
		
	// Dispon�vel apenas em vers�es superiores a 7.00.170117A - 17.2.0.2
	oBtn2 :=  TButton():New( 50, 152, IIF(lPreview,"Continuar","Sair" ), oTBar,;
		{|| lRet := SairTela() },40,010,,,.F.,.T.,.F.,,.F.,,,.F. )
 
    ACTIVATE DIALOG oDlg CENTERED

	If lRet 
		U_IFAA023C(lPreview)
	EndIf
	
Return .t.

Static Function SairTela()
	oDlg:End()
Return .F.

Static Function SalvarImg(lPreview)
	Local cFile 	:= ""
	Local cFilename := ""
	Local cFileServ := ""
	Local aFile 	:= {}
	Local oModel    := FWModelActive()
	Local oView    := FWViewActive()
	Default lPreview := .F.
	
	If !EXISTDIR( "\msg_imgs\" )
		MakeDir("\msg_imgs\")
	EndIf
	
	cFile      := cGetFile("Imagem PNG|*.png |Imagem JPG|*.Jpg","Abrir imagem do disco",0, "C:\temp\",.t., 16 ,.T.)
	// cFile := oTBitmap1:cBmpFile
	aFile := STRTOKARR( cFile, "\" )
	
	If File(cFile) .AND. len(aFile) > 1
		cFileName := aFile[len(aFile)]
		
		CpyT2S(cFile,"\msg_imgs")
		If lPreview
			cFileServ := "\msg_imgs\"+Alltrim(Z33->Z33_CODIGO)+StrTran(Time(),":","")+"."+StrTokArr(cFileName,".")[2]
			frename("\msg_imgs\"+cFileName , cFileServ )
			RecLock('Z33', .F.)
				Z33->Z33_URLIMG := cFileServ
			Z33->(MsUnLock())
			oTBitmap1:cBmpFile := cFileServ
		Else
			cFileServ := "\msg_imgs\"+Alltrim(FWFldGet("Z33_CODIGO"))+StrTran(Time(),":","")+"."+StrTokArr(cFileName,".")[2]
			frename("\msg_imgs\"+cFileName , cFileServ )
			// FWFldPut("Z33_URLIMG", cFileServ)
			oZ33Model := oModel:getModel("MASTERZ33")
			oTBitmap1:cBmpFile := cFileServ
			//Seta mudan�a de valor no formul�rio
			oZ33Model:SetValue("Z33_URLIMG",cFileServ)
			oView:lModify := .T.
		EndIf
		oDlg:end()
		// oDlg:nHeight 	:= oTBitmap1:nClientHeight
		// oDlg:nWidth		:= oTBitmap1:nClientWidth
		
	EndIf
Return .T.