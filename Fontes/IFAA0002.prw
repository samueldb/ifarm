
//-------------------------------------------------------------------
/*/{Protheus.doc} IFAA0002
Tela de visualiza��o de hist�rico de associa��es
@author  Samuel Dantas  
@since   10/12/2018
/*/
//-------------------------------------------------------------------
User Function IFAA0002()

	Private cCadastro := "Hist�rio de Associa��es"
	
	Private aRotina := { {"Pesquisar"		,"axPesqui"						  	,0,1},;
						 {"Visualizar"		,"u_modeloaba(2,'IFAA002B')"		,0,2}}
	
	Private cDelFunc 	:= ".T."
	Private cString 	:= "Z03"
	
	dbSelectArea("Z03")
	dbSetOrder(1)

	//���������������������������������
	//�Inicio do trecho do ModeloAba()�
	//���������������������������������
	SetPrvt(u_modeloaba(0)) // declara as variaveis como private
	U_IFAA002B() // preenche os valores

	dbSelectArea(cString)

	Set Filter To Z01->Z01_CODIGO == Z03->Z03_CODIGO

	mBrowse( 6,1,22,75,cString,,,,,,)

	Set Filter To 

Return

User Function IFAA002B()
	
	aPrivates	:= u_modeloaba(1)
	
	for i:= 1 to len(aPrivates)
		&(aPrivates[i][1])	:= &(aPrivates[i][2])
	next
	//���Ŀ
	//�Pai�
	//�����
	cCabec	:= 'Z03'
	cTitulo	:= cCadastro
	
	//������Ŀ
	//�Filhas�
	//��������
	aAdd(aFilhas	, 'Z04')
	aAdd(aNomes		, 'Produtos para Remarcar')
	aAdd(aOrdem 	, 1) 
	aAdd(_aSeek		, "Z03->Z03_FILIAL + Z03->Z03_YCHAVE")
	aAdd(aWhile		, {|| Z04_FILIAL + Z04_YCHAVE })
	aAdd(aIniPos	, '+Z04_CODZ01')
	aAdd(aOrdItem	, 1) 
	aLinha	:= {}
		aAdd(aLinha	, {'Z04_FILIAL', "xFilial('Z04')"		 		})
		aAdd(aLinha	, {'Z04_YCHAVE', "GdFieldGet('Z04_YCHAVE',n)"})
	aAdd(_aChave	, aLinha)
	aAdd(aReadOnly	, .T.)                 

Return