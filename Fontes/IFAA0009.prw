
#Include 'Protheus.ch'
#Include 'Parmtype.ch'
#include "FWMVCDEF.CH"
#include "topconn.CH"

/*/{Protheus.doc} IFAA0009
Visualiza��o de separa��o de produtos
@author Samuel Dantas
@since 03/01/2019
@version 1.0
/*/
User Function IFAA0009()
	Local oBrowse
	Private cString 		:= "Z11"  

	//Montagem do Browse principal	
	oBrowse := FWMBrowse():New()
		
	oBrowse:AddLegend("Z11_STATUS == 'A'", "BR_VERDE")
	oBrowse:AddLegend("Z11_STATUS == 'F'", "BR_VERMELHO")
	
	//Define alias principal
	oBrowse:SetAlias(cString)
	oBrowse:SetDescription('Separa��es de Pedidos')
	oBrowse:SetMenuDef('IFAA0009')

	oBrowse:Activate()
Return

/*/{Protheus.doc} MenuDef
Retorna o menu principal
@author Samuel Dantas
@since 03/01/2019
@version undefined
@return aRotina, Array com os dados para os botoes do browse
/*/ 
Static Function MenuDef()
	Local aRotina := {}
    
    aAdd( aRotina, { 'Visualizar'		, 'VIEWDEF.IFAA0009'	, 0, 2, 0, NIL } )
    aAdd( aRotina, { 'Ratear pedidos' 	, "U_IFAA0007()"		, 0, 3, 0, NIL } )
    aAdd( aRotina, { 'Alterar/Efetivar' , "U_IFAA0007(4)"		, 0, 3, 0, NIL } )
    //aAdd( aRotina, { 'Excluir' 			, "VIEWDEF.IFAA0009"	, 0, 5, 0, NIL } )
    aAdd( aRotina, { 'Imp. Pedidos'		, "U_IFAA009B()"		, 0, 5, 0, NIL } )
    aAdd( aRotina, { 'Imp. Romaneio'	, "U_IFAR0002()"		, 0, 5, 0, NIL } )
    aAdd( aRotina, { 'Imp. Rom.80Col'	, "U_IFAR0002('.T.')"		, 0, 5, 0, NIL } )

Return aRotina

/*/{Protheus.doc} ModelDef
Construcao do modelo de dados
@author Samuel Dantas
@since 03/01/2019
@version 1.0
@return oModel, Retorna o objeto do modelo de dados
/*/
Static Function ModelDef()
	Local oModel
	Local oStruZ11 := FWFormStruct(1,"Z11")	
	Local oStruSC5 := FWFormStruct(1,"SC5")
	Local oStruSC7 := FWFormStruct(1,"SC7")
	Local oStruZ07 := FWFormStruct(1,"Z07")
    oModel := MPFormModel():New("IFAA009", /*bPreValidacao*/,, /* GravaDados */, /*bCancel*/ )

	//Cria a estrutura principal(Z11)
	oModel:addFields('MASTERZ11',,oStruZ11)

	//Adiciona a chave
	oModel:SetPrimaryKey({'Z11_FILIAL', 'Z11_CODIGO'})

    oModel:AddGrid('SC7DETAIL','MASTERZ11',oStruSC7, /*bPreValidacao*/,, /*bPreVal*/, /*bPosVal*/, /*BLoad*/ )
	//Define a relacao entre as tabelas
	oModel:SetRelation('SC7DETAIL',{{'C7_FILIAL','xFilial("SC7")'},{'C7_YCODZ11','Z11_CODIGO'}},"C7_FILIAL+C7_YCODZ11")
                                          
    aCamposSC5 := {}
	aAdd(aCamposSC5,{ 'C5_FILIAL'	, 'xFilial("SC5")' })
	aAdd(aCamposSC5,{ 'C5_YCODZ11'	, "Z11_CODIGO"    })

    oModel:AddGrid('SC5DETAIL','MASTERZ11',oStruSC5, /*bPreValidacao*/,, /*bPreVal*/, /*bPosVal*/, /*BLoad*/ )
	
	//Define a relacao entre as tabelas
	oModel:SetRelation('SC5DETAIL',{{'C5_FILIAL','xFilial("SC5")'},{'C5_YCODZ11','Z11_CODIGO'}},"C5_FILIAL+C5_YCODZ11")
	                                      
    aCamposZ07 := {}
	aAdd(aCamposZ07,{ 'Z07_FILIAL'	, 'xFilial("Z07")' })
	aAdd(aCamposZ07,{ 'Z07_CODZ11'	, "Z11_CODIGO"    })

    //Cria estrutura de grid para os itens
	oModel:AddGrid('Z07DETAIL', 'SC7DETAIL', oStruZ07)
	//Define a relacao entre as tabelas
	oModel:SetRelation('Z07DETAIL', aCamposZ07, "Z07_FILIAL+Z07_CODZ11")
    
	//Define a descricao dos modelos
	oModel:GetModel( 'MASTERZ11' ):SetDescription( 'Separa��es de Pedidos' )
	oModel:GetModel( 'SC5DETAIL' ):SetDescription( 'Pedidos de venda' )
    oModel:GetModel( 'SC7DETAIL' ):SetDescription( 'Pedidos de compra' )
    oModel:GetModel( 'Z07DETAIL' ):SetDescription( 'A��es geradas na separa��o' )

	//Define que o preenchimento da grid e' opcional
	oModel:GetModel('SC7DETAIL'):SetOptional( .T. )
	oModel:GetModel('SC5DETAIL'):SetOptional( .T. )
	oModel:GetModel('Z07DETAIL'):SetOptional( .T. )

    //Define que a linha nao podera ter o conteudo repetido
	// oModel:GetModel('SC7DETAIL'):SetUniqueLine({'C7_FILIAL','C7_CODZ11', 'C7_CODSBM'})
	// oModel:GetModel('SC5DETAIL'):SetUniqueLine({'C5_FILIAL','C5_CODZ11', 'C5_CODSB1'})

    // AntesDeTudo
    // oModel:SetVldActivate( {|oModel| .T. } )

Return oModel

/*/{Protheus.doc} ViewDef
Monta o view do modelo
@author Samuel Dantas
@since 03/01/2019
@version 1.0
/*/
Static Function ViewDef()
	Local oModel := ModelDef()
	Local oView
	Local oStrZ11:= FWFormStruct(2, 'Z11')
	Local oStrSC7:= FWFormStruct(2, 'SC7')
	Local oStrSC5:= FWFormStruct(2, 'SC5')
	Local oStrZ07:= FWFormStruct(2, 'Z07')
	Local oModelSC7 := oModel:GetModel('SC7DETAIL')
	oView := FWFormView():New()
	oView:SetModel(oModel)
	oView:addUserButton( 'Ver Ped Compra (F6)', 'Pedido', { |oView| U_IFAA009A(oModelSC7:GetValue("C7_NUM"),oModelSC7:GetValue("C7_ITEM")) } )
	SetKey(VK_F6, { || U_IFAA009A(oModelSC7:GetValue("C7_NUM"),oModelSC7:GetValue("C7_ITEM")) })
    //Adiciona no nosso View um controle do tipo FormFields(antiga enchoice)
	oView:AddField('FORM_Z11' , oStrZ11,'MASTERZ11' ) 
	
    //Adiciona no nosso View um controle do tipo FormGrid(antiga newgetdados)
    oView:AddGrid( 'GRID_SC5', oStrSC5, 'SC5DETAIL' )
    oView:AddGrid( 'GRID_SC7', oStrSC7, 'SC7DETAIL' )
    oView:AddGrid( 'GRID_Z07', oStrZ07, 'Z07DETAIL' )
    
    // Define campos que terao Auto Incremento
    // oView:AddIncrementField( 'GRID_SC5', 'C5_ITEM' )
    // oView:AddIncrementField( 'GRID_SC7', 'C7_ITEM' )
    
    // 30% cabec e 70% para as abas
	oView:CreateHorizontalBox('SUPERIOR', 30)	
	oView:CreateHorizontalBox( 'INFERIOR', 70 )
	
    // Cria Folder na View
    oView:CreateFolder( 'PASTA_INFERIOR' ,'INFERIOR' )

    // Crias as pastas (abas)
    oView:AddSheet( 'PASTA_INFERIOR'    , 'ABA_SC5'  , "Pedidos de venda" )
    oView:AddSheet( 'PASTA_INFERIOR'    , 'ABA_SC7'  , "Pedidos de compra" )
    oView:AddSheet( 'PASTA_INFERIOR'    , 'ABA_Z07'  , "A��es geradas na separa��o" )
    

    // Criar "box" horizontal com 100% dentro das Abas
    oView:CreateHorizontalBox( 'DETALHES'   ,100,,, 'PASTA_INFERIOR', 'ABA_SC5' )
    oView:CreateHorizontalBox( 'ITENS' 		,100,,, 'PASTA_INFERIOR', 'ABA_SC7' )
    oView:CreateHorizontalBox( 'ACOES' 		,100,,, 'PASTA_INFERIOR', 'ABA_Z07' )
    

    // Liga a identificacao do componente
    // oView:EnableTitleView('GRID_SC7','Itens |')

    // Relaciona o ID da View com o "box" para exibicao
	oView:SetOwnerView('FORM_Z11', 'SUPERIOR')
	oView:SetOwnerView('GRID_SC7', 'ITENS'   )
	oView:SetOwnerView('GRID_SC5', 'DETALHES' )
	oView:SetOwnerView('GRID_Z07', 'ACOES' )

Return oView

User function IFAA009A (cNum,cItem)
	Private  l120Auto 	:= .F.
	Private aRotina		:= {}
	Private nTipoPed	:= 1
	Private cCadastro	:= "Pedidos de compra"
	
	aAdd(aRotina,{"Pesquisar","PesqBrw"   , 0, 1, 0, .F. }) //"Pesquisar"
	aAdd(aRotina,{"Visualizar","A120Pedido", 0, 2, 0, Nil }) //"Visualizar"
	aAdd(aRotina,{"Incluir","A120Pedido", 0, 3, 0, Nil }) //"Incluir"
	aAdd(aRotina,{"Alterar","A120Pedido", 0, 4, 6, Nil }) //"Alterar"
	aAdd(aRotina,{"Excluir","A120Pedido", 0, 5, 7, Nil }) //"Excluir"

	SC7->(DbSetOrder(1))
	If SC7->(DbSeek(xFilial("SC7")+cNum+cItem)) 
		A120Pedido("SC7",SC7->(Recno()),2,,.F.,.F.,,)
	EndIf

Return .T.

//-------------------------------------------------------------------
/*/{Protheus.doc} func
description
@author  author
@since   11/02/2019
@version 1.0
/*/
//-------------------------------------------------------------------
User Function func()

	Private cCadastro := 'name'

	Private aRotina := {{'Pesquisar' ,'AxPesqui',0,1} ,;
					   {'Visualizar' ,'AxVisual',0,2} ,;
					   {'Incluir' ,'AxInclui',0,3} ,;
					   {'Alterar' ,'AxAltera',0,4} ,;
					   {'Excluir' ,'AxDeleta',0,5} }


	Private cDelFunc := '.T.' // Validacao para a exclusao. Pode-se utilizar ExecBlock

	Private cString := 'Z12'
	DbSelectArea(cString)
	DbSetOrder(1)
	DbSelectArea(cString)

	mBrowse( 6,1,22,75,cString)

Return

User Function IFAA009B
	Local cQuery
	Local aRecnos := {}
	
	cQuery := " SELECT R_E_C_N_O_ AS RECNOSC5 FROM " + RetSqlTab('SC5')
	cQuery += " WHERE " + RetSqlDel('SC5')
	cQuery += " AND C5_YCODZ11 = '" + Z11->Z11_CODIGO + "' "

	If Select('QRYSC5') > 0 
		QRYSC5->(DbCloseArea())
	EndIf
	
	TcQuery cQuery New Alias 'QRYSC5'

	While QRYSC5->(!Eof())
		aAdd(aRecnos, QRYSC5->RECNOSC5)
		QRYSC5->(DbSkip())
	EndDo

	U_IFAR0001(.F.,aRecnos)

Return
