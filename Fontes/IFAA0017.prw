//-------------------------------------------------------------------
/*/{Protheus.doc} IFAA0017
Cadastro de avalia��o
@author  Samuel Dantas
@since   07/01/2019
@version 1.0
/*/
//-------------------------------------------------------------------
User Function IFAA0017()

    Private cCadastro := 'Avalia��es'

    Private aRotina := {{'Pesquisar' ,'AxPesqui',0,1} ,;
                       {'Visualizar' ,'AxVisual',0,2} ,;
                       {'Alterar' ,'AxAltera',0,4} ,;
                       {'Excluir' ,'AxDeleta',0,5} }


    Private cDelFunc := '.T.' // Validacao para a exclusao. Pode-se utilizar ExecBlock

    Private cString := 'Z19'
    DbSelectArea(cString)
    DbSetOrder(1)
    DbSelectArea(cString)

    mBrowse( 6,1,22,75,cString)

Return
