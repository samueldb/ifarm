//-------------------------------------------------------------------
/*/{Protheus.doc} IFAA0016  
Cadastro de endere�os da associa��o
@author  Sidney Sales
@since   03/01/2018
@version 1.0
/*/
//-------------------------------------------------------------------
User Function IFAA0016  ()

    Private cCadastro := 'Endere�os da Associacao'

    Private aRotina := {{'Pesquisar' ,'AxPesqui',0,1} ,;
                       {'Visualizar' ,'AxVisual',0,2} ,;
                       {'Incluir' ,'AxInclui',0,3} ,;
                       {'Alterar' ,'AxAltera',0,4}}


    Private cDelFunc := '.T.' // Validacao para a exclusao. Pode-se utilizar ExecBlock

    Private cString := 'Z20'
    DbSelectArea(cString)
    DbSetOrder(1)
    DbSelectArea(cString)

    mBrowse( 6,1,22,75,cString)

Return
