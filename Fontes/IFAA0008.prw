//-------------------------------------------------------------------
/*/{Protheus.doc} IFAA0008
Tela de visualiza��o de altera��es e diferen�a em pedidos de compra
e nota fiscal de entrada
@author  Samuel Dantas
@since   18/12/2018
@version 1.0
/*/
//-------------------------------------------------------------------
User Function IFAA0008()
    
    Private cCadastro := 'Modifica��es em pedidos de compra'

    Private aRotina := {{'Visualizar'           ,'AxVisual'     ,0,2} ,;
                       {'Alterar'               ,'AxAltera'     ,0,4} ,;
                       {'Alterar P/ corrigido'  ,'U_IFAA008B'   ,0,2} ,;
                       {'Log de Altera��es'     ,'U_IFAA008C'   ,0,2} ,;
                       {'Legenda'               ,'U_IFAA008A'   ,0,2}}

    Private cDelFunc := '.T.' // Validacao para a exclusao. Pode-se utilizar ExecBlock
    
    Private cString := 'Z07'
    Private aCores := {}

    AADD(aCores,{"Z07_STATUS == '1'"	,"BR_VERDE"	 })
	AADD(aCores,{"Z07_STATUS == '2'"	,"BR_VERMELHO"})
    
    DbSelectArea(cString)
    DbSetOrder(1)
    DbSelectArea(cString)

    mBrowse( 6,1,22,75,cString,,,,,,aCores)

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} IFAA008A
Monta legenda do status
@author  Samuel Dantas  
@since   18/12/2018
/*/
//-------------------------------------------------------------------
User Function IFAA008A()
	Local aLegenda := {}
	AADD(aLegenda,{"BR_VERDE","Corre��o pendente"})	
	AADD(aLegenda,{"BR_VERMELHO","Corrigido"})							
	BrwLegenda(cCadastro, "Legenda", aLegenda)
	
Return Nil

//-------------------------------------------------------------------
/*/{Protheus.doc} function
Altera status modifica��es de pedido de compra
para corrigido
@author  Samuel Dantas
@since   18/12/2018
/*/
//-------------------------------------------------------------------
User Function IFAA008B ()
    
    If Z07->Z07_STATUS <> '2'

         cStatusPara    := u_X3_CBOX("Z07_STATUS", '2')
         cStatusDe      := u_X3_CBOX("Z07_STATUS", Z07->Z07_STATUS)

        If MsgYesNo( "Confirma altera��o do status para "+cStatusPara+"?", "Altera��o de status" )

            RecLock('Z07', .F.)
                Z07->Z07_STATUS := '2' 
            Z07->(MsUnLock())

            RecLock('Z08',.T.)
                Z08->Z08_FILIAL := xFilial('Z08')
                Z08->Z08_DATA   := dDataBase
                Z08->Z08_HORA   := Time()
                Z08->Z08_DE     := cStatusDe
                Z08->Z08_PARA   := cStatusPara
                Z08->Z08_USUARI := __cUserID + ' - ' + cUserName
                Z08->Z08_CODZ07 := Z07->Z07_CODIGO
            Z08->(MsUnLock())

        EndIf

    Endif
Return

User Function IFAA008c

    Private cCadastro := 'Log de mudan�a de status de a��es'

    Private aRotina := {{'Pesquisar' ,'AxPesqui',0,1} ,;
                       {'Visualizar' ,'AxVisual',0,2} ,;
                       {'Excluir' ,'AxDeleta',0,5} }

    Private cDelFunc := '.T.' // Validacao para a exclusao. Pode-se utilizar ExecBlock

    Private cString := 'Z08'

    DbSelectArea(cString)
    DbSetOrder(1)
    DbSelectArea(cString)
    
    Set Filter To Z08->Z08_CODZ07 == Z07->Z07_CODIGO
    mBrowse( 6,1,22,75,cString)
    Set Filter To 

Return