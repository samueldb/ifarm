#INCLUDE 'Rwmake.ch'
#INCLUDE 'Protheus.ch'
#INCLUDE 'TbIconn.ch'
#INCLUDE 'Topconn.ch'
#INCLUDE "FILEIO.CH"

/*/{Protheus.doc} IFAF0004
    Fun��es gen�ricas utilizadas no WEBSERVICE
    @type  IFAF0004
    @author Samuel Dantas
    @since 15/03/2019
    @version version
/*/
User Function IFAF0004 ( cRotina, cFunction, cTipo, cJson, cRetWS, cDiretorio )
    
    Local cArquivo      := ""
    Local cMsg          := "" 
    
    Default cDiretorio  := "\log\requisicoes\"
    Default cRotina     := "" 
    Default cFunction   := "" 
    Default cTipo       := "" 
    Default cJson       := "" 
    Default cRetWS      := "" 

    If Empty(FunName())
        PREPARE ENVIRONMENT EMPRESA '01' FILIAL '01'
    EndIf
    
    cArquivo      := DtoS(Date())+"T"+Time()+"_"+cTipo+"_"+cRotina+".txt"

    //Se for um log das requisicoes, criar subpastas com a data
    If 'log\requisicoes' $ cDiretorio 
        
        If !ExistDir( cDiretorio := cDiretorio + cValToChar(Year(Date()) ) + "\" )
            MakeDir(cDiretorio)
        EndIf

        If !ExistDir(cDiretorio := cDiretorio + StrZero(Month(Date()),2) + "\" )
            MakeDir(cDiretorio)
        EndIf

        If !ExistDir( cDiretorio := cDiretorio + StrZero(Day(Date()),2) + "\" )
            MakeDir(cDiretorio)
        EndIf

    ElseIf !ExistDir( cDiretorio )
        MakeDir(cDiretorio)
    EndIf
   
    cMsg += "Rotina: "  + cRotina               + CRLF
    cMsg += "Fun��o: "  + cFunction             + CRLF
    cMsg += "Tipo: "    + cTipo                 + CRLF
    cMsg += "Date: "    + DtoS(Date())+Time()   + CRLF
    cMsg += cRetWS      + CRLF
    
    cArquivo := StrTran(cArquivo,":","-")

    MemoWrite(cDiretorio+cArquivo, cMsg + cJson)
    
Return 

/*/{Protheus.doc} IFAF0004
    Essa fun��o:
    � chamada pelo job (SENDERRO) 
    Pega os arquivos de erros da pasta mail 
    Envia o conteudo por e-mail (e-mails cadastrados em parametro)
    Move o arquivo para a pasta enviados
    @type  IFAF0004
    @author Gilson Silva
    @since 15/03/2019
    @version version
/*/
User Function IFAF004A()

    Local cDiretorio := "\log\mail\"
    Local aFiles     := {}
    Local aSizes     := {}
    Local cString    := ""
    Local nTamanho   := 0
    Local cEmails    := "" 

    If Empty(FunName())
        PREPARE ENVIRONMENT EMPRESA '01' FILIAL '01'
    EndIf    
    
    cEmails := AllTrim(SuperGetMv("MS_TECMAIL",.F.,"samuel.batista@agisrn.com;sidney.sales@agisrn.com"))  

    aEmails	:= StrTokArr2( cEmails, ";" )

    If ExistDir( cDiretorio )
        aFiles := Directory(cDiretorio + '*.*')
    Endif
    
    For i:= 1 to Len(aFiles)

        nHandle := fopen(cDiretorio + aFiles[i,1])

        nTamanho := Fseek(nHandle,0,FS_END)
        FSeek(nHandle,0,FS_SET)
        FRead(nHandle,@cString,nTamanho)
        FClose(nHandle)

        For j := 1 To len(aEmails)

            U_EnviaEmail("iFarm - Log de erro.", aEmails[j], cString,'','')
            FRename(cDiretorio + aFiles[i,1] , cDiretorio + 'enviado\' + aFiles[i,1])

        Next

    Next

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} IFAF004B
Funcao schedulada que fica verifcando os emails que entraram e ainda
nao foi enviado email informando que o pedido foi incluido.
@author  Sidney Sales
@since   03/04/2019
@version 1.0
/*/
//-------------------------------------------------------------------

User Function IFAF004B
    Local cQuery, cEmails, i 
    Local cTestes 
    If Empty(FunName())
        PREPARE ENVIRONMENT EMPRESA '01' FILIAL '0101'
    EndIf    
    cTestes := Alltrim(SuperGetMv("MS_USRTEST",.F., "'000000210', '000000189'"))
    cEmails := AllTrim(SuperGetMv("MS_MAILPED",.F.,"sidney.sales@agisrn.com;georgeallan@gmail.com"))  

    aEmails	:= StrTokArr2( cEmails, ";" )

    cQuery := " SELECT R_E_C_N_O_ RECNOSC5 FROM " + RetSqlTab('SC5')
    cQuery += " WHERE " + RetSqlDel('SC5')
    cQuery += " AND C5_YEMAIL <> 'S' "
    
    TcQuery cQuery New Alias 'QRYSC5' 

    While QRYSC5->(!Eof())

        SC5->(DbGoto(QRYSC5->RECNOSC5))
        SA1->(DbSetOrder(1))
        SA1->(DbSeek(xFilial('SA1') + SC5->C5_CLIENTE + SC5->C5_LOJACLI))

        If !(Alltrim(SA1->A1_COD) $ (cTestes))
            cTexto      := 'Aten��o, um novo pedido foi realizado. ' + Chr(13) + Chr(10) + Chr(13) + Chr(10)
            cTexto      += U_IFAR0001(.T.)

            For i := 1 to Len(aEmails)
                U_EnviaEmail("iFarm - Novo Pedido", aEmails[i], cTexto,'','')            
            Next
        EndIf

        Reclock('SC5', .F.)
            SC5->C5_YEMAIL := 'S'
        SC5->(MsUnlock())

        QRYSC5->(DbSkip())
        
        sendTelegram(cTexto)
        
        //sendWatts(cTexto)
        
        
        sendWatts(cTexto, .T.)
        
    EndDo

    cQuery := " SELECT R_E_C_N_O_ RECNOSA1 FROM " + RetSqlTab('SA1')
    cQuery += " WHERE " + RetSqlDel('SA1')
    cQuery += " AND A1_YENVTEL <> 'S' "
    
    TcQuery cQuery New Alias 'QRYSA1' 

    While QRYSA1->(!Eof())

        SA1->(DbGoto(QRYSA1->RECNOSA1))
    
        cTexto := 'Aten��o, novo cliente cadastrado.' + chr(13) + chr(10)
        cTexto += '*Cliente:* ' + Alltrim(SA1->A1_NOME)   + chr(13) + chr(10)
        cTexto += '*Email:* '   + Alltrim(SA1->A1_YEMAIL) + chr(13) + chr(10)
        cTexto += '*Telefone:* '   + Alltrim(SA1->A1_TEL) + chr(13) + chr(10)
        
        //envia a mensagem
        sendTelegram(cTexto)        
        sendWatts(cTexto, .T.,.T.)
        
        //Grava o envio
        RecLock('SA1', .F.)
            SA1->A1_YENVTEL := 'S'
        SA1->(MsUnLock())
        
        QRYSA1->(DbSkip())
    EndDo
    
    QRYSC5->(DbCloseArea())
    QRYSA1->(DbCloseArea())

Return


//-------------------------------------------------------------------
/*/{Protheus.doc} sendTelegram
Funcao auxiliar para enviar o pedido via telegram
@author  Sidney Sales
@since   24/06/19
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function sendTelegram(cTexto)
    Local oHttp := FWRest():new( 'https://api.telegram.org' )
    Local cIDChat   := SuperGetMv('MS_CHATTEL', .F., '-297864899')
    Local cIDBOt    := SuperGetMv('MS_BOTTELE', .F., 'bot647523778:AAFsBWBBarVi994GXE0rl439ByRd5LkRXP8')
    
    cTexto          := StrTran(cTexto, '#',': ')
    cTexto          := StrTran(cTexto, chr(13)+chr(10),'%0A')
    cTexto          := Alltrim(EncodeUTF8(cTexto))
    
    oHttp:SetPath( '/'+cIDBOt+'/sendMessage?parse_mode=markdown&use_aliases=true&chat_id='+cIDChat+'&text=' + Escape(cTexto) )

    If oHttp:get()
        conout("mensagem enviada com sucesso")
    EndIf

Return


//-------------------------------------------------------------------
/*/{Protheus.doc} sendWatts
Envia uma mensgaem via wattsapp para o grupo inforamdo no parametro
@author  Sidney Sales
@since   10/07/19
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function sendWatts(cTexto, lInterna, lSoInterna)
    Local cInstancia    := SuperGetMv('MS_INSTAWA',.F., 'instance49121')
    Local cToken		:= Alltrim(SuperGetMv('MS_TOKWAPP',.F.,'oiiojqorkkj64ezm'))
    Local cUrl          := Alltrim(SuperGetMv('MS_URLWAPP',.F.,'https://eu14.chat-api.com/')) + cInstancia
    Local cPath		    := '/sendMessage'     
    Local aHeader		:= {}
    Local cResponsavel  := Alltrim(SuperGetMv('MS_RESWAPP',.F., 'Cleide'))
    Local cTelRespon    := Alltrim(SuperGetMv('MS_TELRES',.F., '558492123315'))
    Local cCodMsg       := Alltrim(SuperGetMv('MS_MSGPED', .F., '000010')) // CODIGO DA MSG NA Z33
    Local cTelAux       := Alltrim(SuperGetMv('MS_TELAUX', .F., '558494540153')) // CODIGO DA MSG NA Z33
    Private cMsg := ""
    Default lInterna    := .F.
    Default lSoInterna  := .F.

    aAdd(aHeader, 'Content-Type: application/json')

    If Empty(SA1->A1_YGROUP) .AND. ! lSoInterna               

        oRest   := FwRest():new(cUrl)          
        cPath   := '/group'        
        oJson   := JsonObject():new()

        nPos     := At(' ',SA1->A1_NOME)
        cNomeCli := Alltrim(SubStr(SA1->A1_NOME, 1, nPos))

        Z33->(DbSetOrder(1))
        //Busca Msg do Pedido       
        If Z33->(DbSeek(xFilial("Z33") + cCodMsg))
            cMsg := &(Alltrim(Z33->Z33_MSG))
        EndIf    
        
        If ValType(cMsg) == "C"
            oJson["messageText"]:= cMsg
            
            oJson["groupName"]  := Left('FeirinhaOrg�nica-' + cNomeCli,25)
            
            aRemove := {'(',')','-'}
            
            cTelCli := '55' + Alltrim(SA1->A1_TEL)
            
            For i := 1 to aRemove
                cTelCli := StrTran(cTelCli, aRemove[i], '')
            Next
            
            aFones  := {cTelRespon, cTelCli}
            aAuxiliar := STRTOKARR( cTelAux, "," )
            
            For nJ := 1 To len(aAuxiliar)
                aAdd(aFones,aAuxiliar[nJ])
            Next
            
            oJson["phones"] := aFones

            oRest:setPath(cPath + '?token=' + cToken )

            oRest:SetPostParams(EncodeUtf8(oJson:ToJson()))
            
            oResult := JsonObject():new()
            
            If oRest:Post(aHeader)        
                cResult := oRest:GetResult()

                oResult:FromJson(cResult)

                If oResult['created']
                    
                    cChatId := oResult["chatId"]
                    
                    If ValType(cChatID) <> 'U'
                        
                        RecLock('SA1', .F.)
                            SA1->A1_YGROUP := cChatID
                        SA1->(MsUnLock())

                        oRest   := FwRest():new(cUrl)          
                        cPath   := '/promoteGroupParticipant'        
                        oJson   := JsonObject():new()
                        
                        oJson['groupId']            := cChatID
                        oJson['participantChatid']  := cTelRespon
                        oJson['participantPhone']   := cTelRespon
                        
                        oRest:setPath(cPath + '?token=' + cToken )
                        oRest:SetPostParams(oJson:ToJson())
                        oRest:Post(aHeader)  
                    EndIf
                
                EndIf
            EndIf
        EndIf
        

    EndIf

    If lInterna
        cPath   := '/sendMessage'     
        oRest   := FwRest():new(cUrl)

        oRest:setPath(cPath + '?token=' + cToken )

        cChatID := SuperGetMv('MS_CHATID', .F., "558498079516-1562704350@g.us")
        oJson   := JsonObject():new()
        
        oJson["chatId"]	:= Alltrim(cChatID)
        oJson["body"]	:= cTexto

        oRest:SetPostParams(EncodeUtf8(oJson:ToJson()))
        oRest:Post(aHeader)
        //ConOut('PARA O BOT>>>> ' + oRest:GetResult())
    EndIf
    
    If ! Empty(SA1->A1_YGROUP) .AND. ! lSoInterna   
        cChatID := SA1->A1_YGROUP        
        oRest   := FwRest():new(cUrl)

        oRest:setPath(cPath + '?token=' + cToken )

        oJson := JsonObject():new()
        oJson["chatId"]	:= Alltrim(cChatID)
        oJson["body"]	:= cTexto

        oRest:SetPostParams(EncodeUtf8(oJson:ToJson()))
        oRest:Post(aHeader)
        //ConOut('PARA O CLIENTE>>>> ' + oRest:GetResult())
    EndIf

Return

User Function IFAF004C
    Local cTexto

    SA1->(DbSetOrder(1))
    SA1->(DbSeek(xFilial('SA1') + SC5->C5_CLIENTE + SC5->C5_LOJACLI))

    cTexto      := 'Aten��o, seu pedido foi *alterado*.' + Chr(13) + Chr(10) + Chr(13) + Chr(10)
    cTexto      += U_IFAR0001(.T.)

    sendWatts(cTexto)

Return

User Function TESTEWS

    If Empty(FunName())
        PREPARE ENVIRONMENT EMPRESA '01' FILIAL '0101'
    EndIf
    
    SA1->(DbGoto(94))

    sendWatts('teste', .T.)

Return
