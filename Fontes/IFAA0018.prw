//-------------------------------------------------------------------
/*/{Protheus.doc} IFAA0018
Dias de entrega
@author  Samuel Dantas
@since   08/01/2019
@version 1.0
/*/
//-------------------------------------------------------------------
User Function IFAA0018()

    Private cCadastro := 'Dias de entrega'

    Private aRotina := {{'Pesquisar' ,'AxPesqui',0,1} ,;
                       {'Visualizar' ,'AxVisual',0,2} ,;
                       {'Incluir' ,'AxInclui',0,3} ,;
                       {'Alterar' ,'AxAltera',0,4} ,;
                       {'Excluir' ,'AxDeleta',0,5} }


    Private cDelFunc := '.T.' // Validacao para a exclusao. Pode-se utilizar ExecBlock

    Private cString := 'Z22'
    DbSelectArea(cString)
    DbSetOrder(1)
    DbSelectArea(cString)

    mBrowse( 6,1,22,75,cString)

Return
