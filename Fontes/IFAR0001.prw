#INCLUDE 'Rwmake.ch'
#INCLUDE 'Protheus.ch'
#INCLUDE 'TbIconn.ch'
#INCLUDE 'Topconn.ch'
#Include "RPTDEF.CH"
//-------------------------------------------------------------------
/*/{Protheus.doc} IFAR0001
Impress�o de DANFE - Impressora n�o fiscal         
@author  Samuel Dantas
@since   18/01/2019
@version 1.0
/*/
//-------------------------------------------------------------------
User Function IFAR0001(lEmail, aRecSC5, lLista)
    Local cTexto        := ""  
    Local cPorta        := ALLTRIM(SuperGetMv("MS_YIMPPOR",.F., "COM7"))
    Local cImpressora   := ALLTRIM(SuperGetMv("MS_YIMPFIS",.F., "BEMATECH TH"))
    Local lUsaFis       := ALLTRIM(SuperGetMv("MS_YUSAFIS",.F., .F.))
    Local lSpool        := SuperGetMv("MS_SPOOL",.F., .T.)
    Local aPedidos      := {}
    Local i
    
    Default lEmail      := .F.
    Default aRecSC5     := {SC5->(RecNo())}
    Default lLista      := .F.
    
    For i := 1 to Len(aRecSC5)        
        SC5->(DbGoTo(aRecSC5[i]))
        aAdd(aPedidos, retDados(lLista))    
    Next
    
    If lSpool .and. ! lEmail
        impSpool(aPedidos)
    Else

        For i := 1 to Len(aPedidos)
            For j := 1 to Len(aPedidos[i])
                cTexto += aPedidos[i][j]
            Next
            cTexto += CRLF
        Next

        If lEmail
            Return cTexto
        Else
            nHdlECF := INFAbrir( cImpressora,cPorta ) 

            If nHdlECF > 0
                INFTexto(cTexto)
            Else
                MsgAlert('Erro na abertura de comunica��o com a impressao. Defina no par�metro MS_YIMPPOR a porta COM utilizada pela impressora.', 'Aten��o' )
            EndIf
        EndIf

    EndIf

Return

Static Function retDados(lLista)
    Local aTexto := {}
    Local cTexto := ""
    Local aAreaZ18 := Z18->(GetArea())
    Local i

    Z18->(DbSetOrder(4))//Filial + Codigo do Produto

    Z01->(DbSetOrder(1))
    Z01->(DbSeek(xFilial("Z01")))

    SA1->(DbSetOrder(1))
    SA1->(DbSeek(xFilial("SA1") + SC5->C5_CLIENTE))

    //Monta texto para impress�o
    aAdd(aTexto, "Pedido #"  + SC5->C5_NUM + CRLF)
    aAdd(aTexto, "Emissao: " + DtoC(SC5->C5_EMISSAO) + CRLF)
    aAdd(aTexto, "Dias de entrega:  " )
    aAdd(aTexto, ALLTRIM(SC5->C5_YDIAENT) + " " + CRLF)
    aAdd(aTexto, "Hor�rios de entrega: ")
    aAdd(aTexto, ALLTRIM(SC5->C5_YHRENT) + " "+ CRLF)
    aAdd(aTexto, Replicate("-",48) + CRLF)
    aAdd(aTexto, "Cliente: ")
    aAdd(aTexto, ALLTRIM(SA1->A1_NOME) + CRLF)
    aAdd(aTexto, "Telefone: "+Transform(ALLTRIM(SA1->A1_TEL),"@R (99) 99999-9999") + CRLF)

    Z09->(DbSetOrder(2))
    cTexto := ""
    If  Z09->(DbSeek(xFilial("Z09") + SC5->C5_YCODZ09 ))
        cTexto += ALLTRIM(Z09->Z09_PSRECE) + CRLF
    EndIf

    aAdd(aTexto, "Rua " + ALLTRIM(Z09->Z09_RUA)+ ", " + Z09->Z09_NUMERO  + CRLF)
    aAdd(aTexto,"Bairro: "+ ALLTRIM(Z09->Z09_BAIRRO) + " - " + ALLTRIM(Z09->Z09_MUN)+"/"+ALLTRIM(Z09->Z09_EST)+ CRLF)
    aAdd(aTexto, "Complemento: " + ALLTRIM(Z09->Z09_COMPLE) + CRLF)

    aAdd(aTexto, "Pessoa a receber: " + cTexto)
    aAdd(aTexto, "REF: " + ALLTRIM(Z09->Z09_REF) + CRLF)

    SC6->(DbSetOrder(1))
    SC6->(DbSeek(cSeek:= xFilial("SC6") + SC5->(C5_NUM)))

    aAdd(aTexto,Replicate("-",48) + CRLF)

    aAdd(aTexto, "ITEM   "+PADR("DESCRICAO",30) + CRLF)
    aAdd(aTexto, "       QTD"+SPACE(4)+"   VLR.UNI  "+"VLR.TOT" + CRLF)
    nSubtot := 0

    aAdd(aTexto,Replicate("-",48) + CRLF)

    aRecSC6 := {}

    While (SC6->(!EOF())) .AND. cSeek == SC6->(C6_FILIAL + C6_NUM)                
        
        Z18->(DbSeek(xFilial('Z18') + SC6->C6_PRODUTO))
        aAdd(aRecSC6, {SC6->(RecNo()), Z18->(RecNo()), Z18->Z18_CODZ21 + lower(Z18->Z18_DESC)})        
        SC6->(DbSkip())

    EndDo

    If lLista
        aRecSC6 := aSort(aRecSC6, , , { | x,y | x[3] < y[3] } )    
    EndIf

    For i :=  1 to Len(aRecSC6)
    
        SC6->(DbGoTo(aRecSC6[i][1]))
        Z18->(DbGoTo(aRecSC6[i][2]))
        
        If Z18->(DbSeek(xFilial('Z18') + SC6->C6_PRODUTO))
            cDescri := Alltrim(Z18->Z18_DESC)
        Else
            cDescri := Alltrim(SC6->C6_DESCRI)
        EndIf        
                
        aAdd(aTexto,  ALLTRIM(SC6->C6_ITEM)+"   "+ cDescri +" " + CRLF)
        aAdd(aTexto, SPACE(5)+ " "+Alltrim(Transform(Round(SC6->C6_QTDVEN,2),"@E 9,999.99")) +" x R$ "+Transform(Round(SC6->C6_PRCVEN,2),"@E 999.99") + SPACE(3)+" R$ "+Transform((Round(SC6->C6_QTDVEN,2)*Round(SC6->C6_PRCVEN,2)),"@E 9,999.99") + CRLF)
        nSubtot += Round(SC6->C6_QTDVEN,2)*Round(SC6->C6_PRCVEN,2)
    
    Next



    nTotal := Round(nSubtot,2) + Round(SC5->(C5_FRETE - C5_YDESCFR),2)
    aAdd(aTexto,Replicate("-",48) + CRLF)
    aAdd(aTexto, "Sub-total     "+SPACE(10)+" R$ "+Transform(nSubtot,"@E 9,999.99")         + CRLF)
    aAdd(aTexto, "Desc. Frete   "+SPACE(10)+" R$ "+Transform(SC5->C5_YDESCFR,"@E 9,999.99") + CRLF)
    aAdd(aTexto, "Desc. Pedido  "+SPACE(10)+" R$ "+Transform(SC5->C5_DESC1,"@E 9,999.99")   + CRLF)
    aAdd(aTexto, "Total         "+SPACE(10)+" R$ "+Transform(nTotal,"@E 9,999.99")          + CRLF)
    
    If SC5->C5_YTROCO > 0
        aAdd(aTexto, "Troco         "+SPACE(10)+" R$ "+Transform(SC5->C5_YTROCO - nTotal ,"@E 999,999.99") + CRLF)
    EndIf

    nCredito := getCreditos()

    If nCredito > 0
        aAdd(aTexto, "Cr�dido:      "+SPACE(10)+" R$ "+Transform(nCredito,"@E 9,999.99") + CRLF)
    EndIf

    aAdd(aTexto,Replicate("-",48) + CRLF)
            
    Z15->(DbSetOrder(1))
    Z15->(DbSeek(xFilial("Z15") + SC5->C5_YFORMPG ))
    
    aAdd(aTexto, "Forma de pagamento: " + Alltrim(Z15->Z15_NOME) + CRLF )

    aAdd(aTexto, "Observa��es   "+SPACE(21)+" " + CRLF)
    aAdd(aTexto, IIF( EMPTY(SC5->C5_YOBS), "Nenhuma observa��o.",SC5->C5_YOBS) + CRLF)
    aAdd(aTexto, CRLF)

    RestArea(aAreaZ18)

Return aTexto

Static Function getCreditos
    Local cQuery
    Local nRet := 0

    cQuery := " SELECT SUM(E1_SALDO) CREDITO FROM " + RetSqlTab('SE1') 
    cQuery += " WHERE " + RetSqlDel('SE1')
    cQuery += " AND E1_CLIENTE = '" + SC5->C5_CLIENTE + "' "
    cQuery += " AND E1_LOJA    = '" + SC5->C5_LOJACLI + "' "
    cQuery += " AND E1_TIPO = 'NCC'"

    If Select('QRYSE1') > 0
        QRYSE1->(DbCloseArea())
    EndIf

    TcQuery cQuery New Alias 'QRYSE1'

    nRet := QRYSE1->CREDITO

Return nRet

Static Function impSpool(aPedidos)
   	Local nLinha	:= 150
	Local nLeft		:= 20	
    Local nTamText  := 0
	Local nLargura	:= 2100 
	Local nWidthMax	:= 800
	Local nHeightMax	:= Val(SuperGetMv("MS_MAXHREL",.F.,"2350"))
	Local nHCupomMax	:= Val(SuperGetMv("MS_MAXHCUP",.F.,"3200"))
    Local oPrint 	:= FWMSPrinter():New('pedidos'+'_'+ DtoS(dDatabase) + '_' + StrTran(Time(),':', '_') + ".rel")  
    Local i
    
    oPrint:SetPaperSize(2)
	oPrint:SetMargin(0,0,0,0)

    oFont12	:= TFont():New("Courier new", 9, 12,.T.,.T., 5,.T., 5, .T., .F.)
	oFont14n	:= TFont():New("Arial", 9, 16,.T.,.T., 5,.T., 5, .T., .F.)

    For i := 1 to Len(aPedidos)
        nLinha	:= 150
        aTextos := aPedidos[i]
        oPrint:StartPage()

        For j := 1 To Len(aTextos)
            nLinha += 50
            cTexto := aTextos[j]
            If oPrint:nDevice == IMP_PDF
                oPrint:SayAlign(nLinha, nLeft, cTexto, oFont14n,2000,500,,0)
                nTamText := oPrint:getTextWidth(AllTrim(cTexto),oFont12)
                While nTamText > 2000
                    nLinha += 50
                    nTamText -= 2000
                EndDo   

                If nLinha > nHeightMax
                    oPrint:EndPage()
                    oPrint:StartPage()
                    nLinha	:= 0
                EndIf
            Else
                oPrint:SayAlign(nLinha, nLeft, cTexto, oFont12,nWidthMax,500,,0)
                nTamText := oPrint:getTextWidth(AllTrim(cTexto),oFont12)
                While nTamText > nWidthMax
                    nLinha += 50
                    nTamText -= nWidthMax
                EndDo   

                If nLinha > nHCupomMax
                    oPrint:EndPage()
                    oPrint:StartPage()
                    nLinha	:= 0
                EndIf

            EndIf
                  
        Next

        oPrint:EndPage()
    
    Next
    
	oPrint:Preview()    
Return
