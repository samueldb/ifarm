#INCLUDE 'Rwmake.ch'
#INCLUDE 'Protheus.ch'
#INCLUDE 'TbIconn.ch'
#INCLUDE 'Topconn.ch'

//-------------------------------------------------------------------
/*/{Protheus.doc} IFAF0001
Funcao para calculo da quantidade a partir do produto vinculado(Z06)
@author  Sidney Sales
@since   17/12/2018
@version 1.0
/*/
//-------------------------------------------------------------------
User Function IFAF0001(cCodZ06)
    Local aAreaSB1  := SB1->(GetArea())
    Local aAreaZ06  := Z06->(GetArea())
    Local nQtd      := 0
    
    If Z06->(DbSeek(xFilial('Z06') + cCodZ06)) .AND. SB1->(DbSeek(xFilial('SB1') + Z06->Z06_CODSB1))
        If SB1->B1_TIPCONV == 'D'
            nQtd    := Z06->Z06_QTD / SB1->B1_CONV
        ElseIf SB1->B1_TIPCONV == 'M'
            nQtd    := Z06->Z06_QTD * SB1->B1_CONV
        EndIf
        nQtd := Round(nQtd,2)
    EndIf
    
    RestArea(aAreaZ06)
    RestArea(aAreaSB1)

Return nQtd


//-------------------------------------------------------------------
/*/{Protheus.doc} IFAF001A
Funcao para validar se um armazem ja esta em uso por outro fornecedor
@author  Sidney Sales
@since   17/12/2018
@version 1.0
/*/
//-------------------------------------------------------------------

User Function IFAF001A(cArmazem)
    Local lRet := .T.
    Local cCampo := Right(ReadVar(),10)
    Local cValor := &(ReadVar())
    Default cArmazem := M->A2_YLOCAL

    If "A2_YLOCAL " = cCampo .AND. !Empty(cValor)
        If cValor == M->A2_YLOCAL1 .OR. cValor == M->A2_YLOCAL2
            MsgAlert('Este armaz�m ' + cArmazem + ' j� foi cadastrado para este fornecedor. ')
            Return .F.
        EndIf
    ElseIf "A2_YLOCAL1" = cCampo .AND. !Empty(cValor)
        If cValor == M->A2_YLOCAL .OR. cValor == M->A2_YLOCAL2
            MsgAlert('Este armaz�m ' + cArmazem + ' j� foi cadastrado para este fornecedor. ')
            Return .F.
        EndIf
    ElseIf "A2_YLOCAL2" = cCampo .AND. !Empty(cValor)
        If cValor == M->A2_YLOCAL1 .OR. cValor == M->A2_YLOCAL
                MsgAlert('Este armaz�m ' + cArmazem + ' j� foi cadastrado para este fornecedor. ')  
            Return .F.
        EndIf
    EndIF

    cQuery := " SELECT A2_COD, A2_LOJA, A2_NREDUZ FROM " +  RetSqlTab('SA2') 
    cQuery += " WHERE " + RetSqlDel('SA2')
    cQuery += " AND A2_COD+A2_LOJA <>  '"  + M->A2_COD+ M->A2_LOJA  + "' "
    cQuery += " AND ( A2_YLOCAL = '" + cArmazem   + "' "
    cQuery += " OR  A2_YLOCAL1  = '" + cArmazem   + "' "
    cQuery += " OR  A2_YLOCAL2  = '" + cArmazem   + "' ) "

    If Select('QRY') > 0
        QRY->(DbCloseArea())
    EndIF

    TcQuery cQuery New Alias 'QRY'

    If QRY->(!Eof())
        MsgAlert('O armaz�m ' + cArmazem + ' j� foi definido para o fornecedor ' + QRY->A2_COD + '/'+QRY->A2_LOJA + ' - ' + Alltrim(QRY->A2_NREDUZ) + ;
            '. Escolha outro armaz�m. ', 'Aten��o')
        lRet := .F.
    EndIf
    
Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} IFAF001C
Retorna total em estoque de acordo com produto e/ou fornecedor
@author  Samuel Dantas
@since   30/01/2019
@param   cCodProd, caractere, codigo do produto
@param   cCodFor, caractere, codigo do fornecedor
@param   lPedidos, l�gico, usada para definir se conta qtd em pedidos de venda
@version version
/*/
//-------------------------------------------------------------------
User Function IFAF001C(cCodProd,cCodFor, lPedidos )
    Local cSeek := ""
    Local nQtd  := 0
    Default lPedidos := .F.

    cCodProd := PADR(cCodProd,TAMSX3("B1_COD")[1])
    cCodFor  := PADR(cCodFor,TAMSX3("A2_COD")[1])
    
    Z01->(DbSetOrder(1))
    Z01->(DbSeek(xFilial("Z01")))

    If !EMPTY(cCodFor)
        SA2->(DbSetOrder(1))
        SA2->(DbSeek(xFilial("SA2") + cCodFor))
        
        SB2->(DbSetOrder(1))
        SB2->(DbSeek(xFilial("SB2") + cCodProd + SA2->A2_YLOCAL))
        //Verifica se o produto � fornecido
        Z05->(DbSetOrder(1))
        If Z05->(DbSeek(xFilial("Z05") + SA2->(A2_COD + A2_LOJA) + cCodProd))
            nQtd += SaldoSb2()
        EndIf
    Else
        Z02->(DbSetOrder(1))
        Z02->( DbSeek( cSeek:= xFilial("Z02") + Z01->Z01_CODIGO))

        While Z02->(!EOF()) .AND. cSeek == Z02->(Z02_FILIAL + Z02_CODZ01)
            
            SA2->(DbSetOrder(1))
            SA2->(DbSeek(xFilial("SA2") + Z02->Z02_CODSA2))
            
            SB2->(DbSetOrder(1))
            SB2->(DbSeek(xFilial("SB2") + cCodProd + SA2->A2_YLOCAL))
            //Verifica se o produto � fornecido
            Z05->(DbSetOrder(1))
            If Z05->(DbSeek(xFilial("Z05") + SA2->(A2_COD + A2_LOJA) + cCodProd))
                nQtd += SaldoSb2()
            EndIf

            Z02->(DbSkip())
        EndDo
    EndIf

    If lPedidos
        nQtd -= Reservados(cCodProd)
    EndIf
    
Return nQtd

//-------------------------------------------------------------------
/*/{Protheus.doc} Reservados
Verifica nos pedidos de venda a quantidade do produto que est� reservado
@author  Samuel Dantas 
@since   07/01/2019
@param   cProd, caractere, Codigo do produto
/*/
//-------------------------------------------------------------------
Static function Reservados (cProd)
    
    nQtdRes := 0

    cQuery := " SELECT SUM(C6_QTDVEN) as QTD FROM "  + RetSqlName('SC6') + " SC6"
    cQuery += " INNER JOIN "+RetSqlName('SC5')+ " SC5 ON C5_FILIAL = C6_FILIAL AND C5_NUM = C6_NUM "
    cQuery += " WHERE SC6.C6_FILIAL = '"+xFilial("SC6")+"' "
    cQuery += " AND SC6.C6_PRODUTO = '" +cProd+"' "
    cQuery += " AND SC5.C5_YSTATUS <> 'F' "
    cQuery += " AND SC6.D_E_L_E_T_ <> '*' "
    cQuery += " AND SC5.D_E_L_E_T_ <> '*' "
    
    If Select('QRY') > 0
        QRY->(dbclosearea())
    EndIf
    
    TcQuery cQuery New Alias 'QRY'
    
    If QRY->(!Eof())
        
        nQtdRes += QRY->QTD
    EndIf
    
Return nQtdRes

//-------------------------------------------------------------------
/*/{Protheus.doc} IFAF001D
Fun��o para valida��o de estoque na tela de pedido de venda
no campo C6_QTDVEN
@author  Samuel Dantas
@since   06/02/2019
@version version
/*/
//-------------------------------------------------------------------
User function IFAF001D ()
    Local nEstoque := U_IFAF001C(GdFieldGet("C6_PRODUTO"),"",.T. )
    Local lRet := .T.
    
    If Alltrim(FunName()) == "MATA410"
        If nEstoque < M->C6_QTDVEN
            Aviso("Quantidade em estoque", "Quantidade em estoque � "+cValToChar(nEstoque)+", menor que quantidade solicitada . Contando com os pedidos que n�o foram finalizados.")
        EndIf
    Else
        If nEstoque < M->C6_QTDVEN
            lRet := .F.
        EndIf
    EndIf
        

Return lRet


User Function TestSA1
    
    If Empty(FunName())
        PREPARE ENVIRONMENT EMPRESA '01' FILIAL '01'
    EndIf
    cNome := ""
    cNomes := ""

    cQuery := " SELECT R_E_C_N_O_ AS RECNO FROM "  + RetSqlName('SA1') + " SA1"
    cQuery += " WHERE SA1.D_E_L_E_T_ <> '*'"
    
    If Select('QRY') > 0
        QRY->(dbclosearea())
    EndIf
    
    TcQuery cQuery New Alias 'QRY'
    
    While QRY->(!Eof())
        SA1->(DbGoTo(QRY->RECNO))
        cNome := AllTrim(DecodeUtf8(SA1->A1_NOME)) 
        If !EMPTY(cNome)
            RecLock('SA1', .F.)
                SA1->A1_NOME := cNome
            SA1->(MsUnLock())
        EndIf
        
        QRY->(dbSkip())
    EndDo
    MsgAlert(cNomes)
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} IFAF001E
Fun��o usada para retorna o total da venda
 na tela de pedidos
@author  Samuel Dantas
@since   14/06/2019
@version version
/*/
//-------------------------------------------------------------------
User Function IFAF001E()
    Local aAreaSC6 := SC6->(GetArea())
    Local nRet := 0
    Local cSeek := xFilial("SC6") + SC5->C5_NUM

    SC6->(DbSetOrder(1))
    While SC6->(!EoF()) .AND. SC6->(C6_FILIAL+C6_NUM) == cSeek
        nRet += SC6->C6_VALOR
        SC6->(DbSkip())
    EndDo
    SC6->(RestArea(aAreaSC6))
Return nRet