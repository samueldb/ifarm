//-------------------------------------------------------------------
/*/{Protheus.doc} IFAA0013
Formas de pagamento da associação
@author  Samuel Dantas
@since   27/12/2018
@version 1.0
/*/
//-------------------------------------------------------------------
User Function IFAA0013()

    Private cCadastro := 'Formas de pagamento da associação '

    Private aRotina := {{'Pesquisar' ,'AxPesqui',0,1} ,;
                       {'Visualizar' ,'AxVisual',0,2} ,;
                       {'Incluir' ,'AxInclui',0,3} ,;
                       {'Alterar' ,'AxAltera',0,4} ,;
                       {'Excluir' ,'AxDeleta',0,5} }


    Private cDelFunc := '.T.' // Validacao para a exclusao. Pode-se utilizar ExecBlock

    Private cString := 'Z15'
    DbSelectArea(cString)
    DbSetOrder(1)
    DbSelectArea(cString)

    mBrowse( 6,1,22,75,cString)

Return
