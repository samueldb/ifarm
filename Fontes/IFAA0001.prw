#INCLUDE 'Rwmake.ch'
#INCLUDE 'Protheus.ch'
#INCLUDE 'TbIconn.ch'
#INCLUDE 'Topconn.ch'

//-------------------------------------------------------------------
/*/{Protheus.doc} IFAA0001
Cadastro de associa��es 
@author  Samuel Dantas
@since   07/12/2018
@version 1.0
/*/
//-------------------------------------------------------------------
User Function IFAA0001()
    Local aCores := {}
    // Aadd(aCores,{" _STATUS == 'A'" ,"BR_VERDE" })

    Private cCadastro   := 'Fornecedor/Associado'

    Private aRotina     := {{'Pesquisar'        ,'AxPesqui'                     ,0,1} ,;
                            {'Visualizar'       ,"u_modeloaba(2,'IFAA001A')"    ,0,2} ,;
                            {'Incluir'          ,"u_modeloaba(3,'IFAA001A')"    ,0,3} ,;
                            {'Alterar'          ,"u_modeloaba(4,'IFAA001A')"    ,0,4} ,;
                            {'Visualizar imagem',"U_IFAA001G"    ,0,4} ,;
                            {"Historico"        ,"U_IFAA0002()"                  ,0,4}}

    Private cDelFunc    := '.F.' // Validacao para a exclusao. Pode-se utilizar ExecBlock
    Private cString     := 'Z01'

    DbSelectArea(cString)
    DbSetOrder(1)

    SetPrvt(u_modeloaba(0)) // declara as variaveis como private

    U_IFAA001A() // preenche os valores
    dbSelectArea(cString)
    DbSetOrder(1)

    mBrowse(6,1,22,75,cString,,,,,,/*aCores*/)
    
    Set Filter To

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} IFAA001A
Fun��o que preenche as variaveis que sao utilizadas no modeloaba
@author  author
@since   07/12/2018
@version 1.0
/*/
//-------------------------------------------------------------------
User Function IFAA001A()
    aPrivates := u_modeloaba(1)

    For i:= 1 to Len(aPrivates)
        &(aPrivates[i][1]) := &(aPrivates[i][2])
    Next

    //Pai
    cCabec := 'Z01'
    cRepCabec:= 'Z03'
    cTitulo := cCadastro

    //Filhas
    aAdd(aFilhas, 'Z02')
    aAdd(aRepFilhas, 'Z04')
    aAdd(aNomes , 'Fornecedores associados')
    aAdd(aOrdem , 1)
    aAdd(_aSeek , "Z01->Z01_FILIAL + Z01->Z01_CODIGO")
    aAdd(aWhile , {|| Z02_FILIAL + Z02_CODZ01 }) //While tabela filha
    aAdd(aIniPos, '+Z02_CODIGO')
    aAdd(aOrdItem, 1) // Z02_FILIAL + Z02_CODZ02 + Z02_CODIGO
    aLinha := {}
    aAdd(aLinha , {'Z02_FILIAL', "xFilial('Z02')"})
    aAdd(aLinha , {'Z02_CODZ01', "M->Z01_CODIGO"})
    aAdd(aLinha , {'Z02_CODIGO', "GdFieldGet('Z02_CODIGO', n)"})
    aAdd(_aChave, aLinha)
    aAdd(aReadOnly, .F.)

    aAdd(aPe, {'AfterPost'		,"U_IFAA001B()" })
    aAdd(aPe, {'LinOk01'	    ,"u_IFAA001C()"})
    aAdd(aPe, {'AntesdeTudo'    ,'U_IFAA001E()'})
    aAdd(aPe, {'TudoOk'         ,'U_IFAA001F()'})
    
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} IFAA001A
Fun��o para preenchimento de tabelas de hist�rico: Z03 E Z04
@author  Samuel Dantas
@since   07/12/2018
@version 1.0
/*/
//-------------------------------------------------------------------
User Function IFAA001B()
    Local lRet := .F.
    lRet := u_modeloAba(9, 'IFAA001A')
Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} IFAA001c
Ponto de entrada para valida��o de linha
@author  Samuel Dantas  
@since   10/12/2018
/*/
//-------------------------------------------------------------------
User Function IFAA001c 
Return 'U_IFAA001D()'

User Function IFAA001D
	Local aCampoUnico := {}
	Local lResult 		:= .T.
    
	aAdd(aCampoUnico,{'Z02_CODSA2',gdFieldGet('Z02_CODSA2', oGet01:nAt,.F., oGet01:aHeader, oGet01:aCols)})	
    //Atualiza campo de percentual  total ao alter linha
	u_CalcTotal("Z01_PERCEN", "Z02_PERCUS"	, oGet01:aHeader, oGet01:aCols)
    //verifica fornecedor �nico
	If !U_fCampoUnq(aCampoUnico, oGet01:aHeader, oGet01:aCols)
		lResult := .F.
		ApMsgInfo('J� existe este item na grid.')
	EndIf	

Return lResult

//-------------------------------------------------------------------
/*/{Protheus.doc} IFAA001E
Metodo utilizado para atualizar tela antes de atualizar o Dialog
@author  Samuel Dantas  
@since   10/12/2018
/*/
//-------------------------------------------------------------------
User Function IFAA001E()
    Local lResult := .T.
    If INCLUI
        Z01->(DbSeek(xFilial('Z01')))
        If Z01->(!Eof())
            lResult := .F.
            MsgAlert("J� existe um cadadstro de associa��o, s� � permitido um cadastro por filial.", "Aten��o")
        EndIf
    EndIf
Return lResult

//-------------------------------------------------------------------
/*/{Protheus.doc} IFAA001E
Ponto de entrada para validar os resultados antes de salvar
@author  Samuel Dantas  
@since   10/12/2018
/*/
//-------------------------------------------------------------------
User Function IFAA001F ()
	Local aCampoUnico := {}
	Local lResult 		:= .T.
    //Realiza a soma dos valores novamente
	u_CalcTotal("Z01_PERCEN", "Z02_PERCUS"	, oGet01:aHeader, oGet01:aCols)
    For i := 1 To len(oGet01:aCols)
        aCampoUnico := {}

        aAdd(aCampoUnico,{'Z02_CODSA2',gdFieldGet('Z02_CODSA2', i,.F., oGet01:aHeader, oGet01:aCols)})	
        //Verifica se o c�digo de fornecedor � unico
        If !U_fCampoUnq(aCampoUnico, oGet01:aHeader, oGet01:aCols)
            lResult := .F.
            ApMsgInfo("O item '" + aCampoUnico[1][2] + "' est� duplicado no cadastro." )
        EndIf
    Next
    If M->Z01_PERCEN > 100
        lResult := .F.
        ApMsgInfo("Percentual de custo est� excendo o limite " )
    EndIf

Return lResult


//-------------------------------------------------------------------
/*/{Protheus.doc} IFAA001G
Mostra a imagem cadastrada para associa��o
@author  Sidney Sales
@since   11/12/18
@version 1.0
/*/
//-------------------------------------------------------------------
User Function IFAA001G
	Local oDlgImg
	Local cCodZ01	:= Z01->Z01_CODIGO
	Local oLayer:= FWLayer():New()
	Local oPanel01, oPanel02
	Private cDirWeb        := Alltrim(SuperGetMV('MS_DIRIMGS',.F.,'/web/'))
    //Posiciona imagem relacionada a associa��o
    Z14->(DbSetOrder(2)) // Z14_FILIAL + Z14_CODZ01
	Z14->(DbSeek(xFilial('Z14') + cCodZ01))


	DEFINE DIALOG oDlgImg TITLE "Imagem da Associa��o - " + Z01->Z01_DESC FROM 180,180 TO 630,680 PIXEL	   

	oLayer:init(oDlgImg,.F.)

	//inclui uma linha(id, percentual da altura da linha, tamanho fixo da linha?)
	oLayer:addLine("linha01",92,.F.)
	oLayer:addCollumn("coluna01",100,.F.,"linha01")
   	oLayer:addWindow("coluna01","janela01",'',100,.F.,.F.,{|| }, "linha01",{|| })
    
    oPanel01 := oLayer:getWinPanel("coluna01","janela01", "linha01")

	//Se tiver imagem preenchida, copia para a pasta temporaria para mostrar ao usuario
	If !Empty(Z14->Z14_BASE64)
		// cFile		:= GetTempPath() + '\associacao' + Alltrim(Z01->Z01_CODIGO) + '.' + Z14->Z14_EXT
		// nH 			:= fCreate(cFile)
		// cImgBuffer	:= Decode64(Z14->Z14_BASE64) 
		// nSaved 		:= fWrite(nH,cImgBuffer)
		// fClose(nH)
		cFile := cDirWeb +"imgs/"+Alltrim(Z14->Z14_URLWEB)
		// CpyT2S( cFile, cDirWeb+"imgs/associacao/" )
	Else
		cFile		:= '\sem_foto.png'		
	EndIf
	
	oTBitmap1 := TBitmap():New(10,10,260,184,,cFile,.T.,oPanel01,{||  },,.F.,.F.,,,.F.,,.T.,,.F.)
	oTBitmap1:Align := CONTROL_ALIGN_ALLCLIENT
	oTBitmap1:lAutoSize := .T.	
	
	TButton():New(208, 010, "Buscar"	, oPanel02,{|| gravarImg(3), oDlgImg:End(), u_IFAA001G()  },45,015,,,.F.,.T.,.F.,,.F.,,,.F. )    
	TButton():New(208, 060, "Excluir"	, oPanel02,{|| gravarImg(5,cCodZ01), oDlgImg:End(), u_IFAA001G() },45,015,,,.F.,.T.,.F.,,.F.,,,.F. )    
	TButton():New(208, 200, "Fechar"	, oPanel02,{|| oDlgImg:End()},45,015,,,.F.,.T.,.F.,,.F.,,,.F. )    
	
	ACTIVATE DIALOG oDlgImg CENTERED 		

Return


//-------------------------------------------------------------------
/*/{Protheus.doc} gravaImg
Funcao para gravar a imagem na estrutura
@author  Sidney 
@since   11/12/18
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function gravarImg(nTipo, cCodZ01)
	Local cImgFile
	Local cImgWeb
	Local cExt	:= ''
	Local lInclui := .F.
	Local cTime := Time()
	Default cCodZ01:= ''	

    //Verifica se j� est� cadastrado
    lInclui := IIF(Empty(Z14->Z14_FILIAL),.T.,.F.) 

	//Se for inclusao

	If nTipo == 3
		
		//Localiza a imagem
		cImgFile := cGetFile("Imagem JPEG|*.jpg|Imagem PNG|*.png","Abrir imagem do disco",0,"\",.F., NIL ,.T.)
		cExt := Alltrim(aTail(StrTokArr(cImgFile,".")))
		CpyT2S( cImgFile, cDirWeb+"imgs/associacao/" )
		cImgWeb := cDirWeb+"imgs/associacao/"+Alltrim(Z01->Z01_CODIGO)+StrTran(cTime,":","")+"."+Right(cExt,3)
		FRename( cDirWeb+"imgs/associacao/"+AllTrim(aTail(StrTokArr(cImgFile,"\"))), cImgWeb )
        ConOut( "Buscando arquivo")
		//Se o arquivo existe, le e convertem em base64
		If ! Empty(cImgFile) 
			
			nH := Fopen(cImgFile,0)
			nSize := fSeek(nH,0,2)
			fSeek(nH,0)
			cImgBuffer := space(nSize)
			nRead := fRead(nH,@cImgBuffer,nSize)           
			fClose(nH)

			SplitPath(cImgFile,,,,@cExt)

			cImg64 := Encode64(cImgBuffer)
			ConOut( "Salvando BASE64 na Z14")
			//Grava o resultado e a extensao
			RecLock('Z14', lInclui )
				Z14->Z14_FILIAL     := xFilial("Z14")
				Z14->Z14_NOME       := aTail(StrTokArr(cImgFile,"\"))
				Z14->Z14_STORAG     := "base64"
				Z14->Z14_BASE64     := cImg64
				Z14->Z14_TIPO       := IIF(Right(cExt,3) == "jpg", "Imagem JPEG", "Imagem PNG")
				Z14->Z14_EXT        := Right(cExt,3)
				Z14->Z14_TAM        := nSize
				Z14->Z14_URL        := "data:image/"+StrTran(cExt,".","")+";base64," + cImg64
				Z14->Z14_URLWEB     := "associacao/"+Alltrim(Z01->Z01_CODIGO)+StrTran(cTime,":","") + "."+Right(cExt,3)
				Z14->Z14_CODZ01     := Z01->Z01_CODIGO
			Z14->(MsUnLock())
            ConOut( "Z14 inclu�da com sucesso.")
			//Atualiza o componente					
			oTBitmap1:Load(, cImgWeb)
			oTBitmap1:Refresh()
			
		Else
            ConOut( "Arquivo n�o encontrado")
			MsgAlert('Opera��o cancelada.', 'Aten��o')
		EndIf

	ElseIf MsgYesNo('Confirma a exclus�o da imagem?')	
		Z14->(DbSeek(xFilial('Z14') + cCodZ01))					
		RecLock('Z14', .F.)
			    Z14->Z14_NOME       := ""
				Z14->Z14_STORAG     := ""
				Z14->Z14_BASE64     := ""
				Z14->Z14_TIPO       := ""
				Z14->Z14_EXT        := ""
				Z14->Z14_TAM        := 0
				Z14->Z14_URL        := ""
				Z14->Z14_CODZ01     := ""
				Z14->Z14_URLWEB 	:= ""
		Z14->(MsUnLock())
	EndIf

Return