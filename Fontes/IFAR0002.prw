#INCLUDE 'Rwmake.ch'
#INCLUDE 'Protheus.ch'
#INCLUDE 'TbIconn.ch'
#INCLUDE 'Topconn.ch'
#Include "RPTDEF.CH"

//-------------------------------------------------------------------
/*/{Protheus.doc} IFAR0002
Relatorio de Impressao de Romaneio de Separacao
@author  Sidney Sales
@since   04/04/2019
@version 1.0
/*/
//-------------------------------------------------------------------
User Function IFAR0002(_l80Cols)
    Local aDados := {}
    Private oReport
    Private l80Cols := .F.
    Default _l80Cols 		:= '.F.'  

	l80Cols := &_l80Cols
    //Prepara relatorio
    If l80Cols
        aAdd(aDados, PrintImp())
        // impSpool(aDados)
    Else
        ReportDef()
        //Monta tela de impressao
        oReport:PrintDialog()
    EndIF
    

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} ReportDef
Configuracoes para a impressao do relatorio
@author  Sidney Sales
@since   04/04/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ReportDef
    Local oSection1

    //Cria o relatorio
    oReport := TReport():New('IFAR0002','Relat�rio Impressao de Romaneio de Separacao','',{|oReport| PrintReport(oReport)},'Relat�rio Impressao de Romaneio de Separacao')

    //Seta o relatorio em modo retrato
    oReport:SetLandscape(.T.)

    //Seta o totalizador em colunas
    oReport:SetTotalInLine(.F.)

    //Cria a secao do relatorio
    oSection1 := TRSection():New(oReport, 'Impressao de Romaneio ' + Z11->Z11_CODIGO, {'QRY'})

    //Celulas que irao conter na primeira secao do relatorio
    TRCell():New( oSection1, "C7_PRODUTO"           , "QRY",'Produto'/*X3Titulo*/,/*Picture*/, /*Tamanho*/)
    TRCell():New( oSection1, "C7_DESCRI"            , "QRY",'Descricao'/*X3Titulo*/,/*Picture*/, /*Tamanho*/)
    TRCell():New( oSection1, "C7_QUANT"             , "QRY",'Quantidade'/*X3Titulo*/,/*Picture*/, /*Tamanho*/)
    TRCell():New( oSection1, ""                     , "QRY",'Quant. Coletada',,,,{|| "________________"},,, )

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} PrintReport
Montagem da query para impressao do relatorio
@author  Sidney Sales
@since   04/04/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function PrintReport(oReport)
    Local cQuery    := ""
    Local oSection1 := oReport:Section(1)

    cQuery    := GetQuery()
    If Select("QRY") > 0
        QRY->(DbCloseArea())
    EndIf

    TcQuery cQuery New Alias "QRY"


	
    oSection1:init()

    cChaveAtu := ''

    // Percorre a Secao 1
    While QRY->(!Eof())
        If cChaveAtu <> QRY->(C7_FILIAL + C7_NUM + C7_FORNECE)
            If !Empty(cChaveAtu)
                oReport:StartPage()
                oSection1:init()
            EndIf

            SA2->(DbSetOrder(1))
            SA2->(DbSeek(xFilial('SA2') + QRY->(C7_FORNECE + C7_LOJA)))
            cChaveAtu := QRY->(C7_FILIAL + C7_NUM + C7_FORNECE)

            oReport:PrintText(Space(2) + 'Fornecedor: ' + SA2->A2_NOME)
            oReport:SkipLine()		
            oReport:SkipLine()		

        EndIf
        oSection1:PrintLine()
        QRY->(DbSkip())
        If cChaveAtu <> QRY->(C7_FILIAL + C7_NUM + C7_FORNECE)
            oReport:EndPage()
        EndIf

    EndDo

    oSection1:Finish()

    oReport:EndPage()
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} ValidPerg
Criacao das perguntas do relatorio
@author  Sidney Sales
@since   04/04/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ValidPerg
    Local aRegs    := {}
    Local aAreaSX1 := SX1->(GetArea())
    Local i, j

    SX1->(DbSetOrder(1))

    // Numeracao dos campos:
    // 01 -> X1_GRUPO   02 -> X1_ORDEM    03 -> X1_PERGUNT  04 -> X1_PERSPA  05 -> X1_PERENG
    // 06 -> X1_VARIAVL 07 -> X1_TIPO     08 -> X1_TAMANHO  09 -> X1_DECIMAL 10 -> X1_PRESEL
    // 11 -> X1_GSC     12 -> X1_VALID    13 -> X1_VAR01    14 -> X1_DEF01   15 -> X1_DEFSPA1
    // 16 -> X1_DEFENG1 17 -> X1_CNT01    18 -> X1_VAR02    19 -> X1_DEF02   20 -> X1_DEFSPA2
    // 21 -> X1_DEFENG2 22 -> X1_CNT02    23 -> X1_VAR03    24 -> X1_DEF03   25 -> X1_DEFSPA3
    // 26 -> X1_DEFENG3 27 -> X1_CNT03    28 -> X1_VAR04    29 -> X1_DEF04   30 -> X1_DEFSPA4
    // 31 -> X1_DEFENG4 32 -> X1_CNT04    33 -> X1_VAR05    34 -> X1_DEF05   35 -> X1_DEFSPA5
    // 36 -> X1_DEFENG5 37 -> X1_CNT05    38 -> X1_F3       39 -> X1_GRPSXG

    aAdd(aRegs, {cPerg, "01", "Data de?"  , "", "", "mv_ch1", 'D', 8, 0, 0, 'G', "", "MV_PAR01", "",  "", "", "", "", "",    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""})
    aAdd(aRegs, {cPerg, "02", "Data at�?" , "", "", "mv_ch2", 'D', 8, 0, 0, 'G', "", "MV_PAR02", "",  "", "", "", "", "",    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""})

    For i := 1 To Len(aRegs)
        If ! SX1->(DbSeek(cPerg+aRegs[i,2]))
            RecLock("SX1", .T.)

            For j :=1 to SX1->(FCount())
                If j <= Len(aRegs[i])
                    SX1->(FieldPut(j,aRegs[i,j]))
                EndIf
            Next

            SX1->(MsUnlock())
        EndIf
    Next

    SX1->(RestArea(aAreaSX1))
Return


Static Function GetQuery()
    Local cQuery
    
    cQuery := " SELECT * FROM " + RetSqlTab('SC7')
    cQuery += " WHERE " + RetSqlDel('SC7')
    cQuery += " AND C7_YCODZ11 = '" + Z11->Z11_CODIGO + "' "
    cQuery += " AND C7_FILIAL = '" + xFilial('SC7') + "' "
    IF l80Cols
        cQuery += " ORDER BY C7_FILIAL, C7_FORNECE, C7_NUM, C7_DESCRI "
    Else
        cQuery += " ORDER BY C7_FILIAL, C7_NUM, C7_FORNECE, C7_DESCRI "
    EndIF
    

Return cQuery


Static Function PrintImp()
    Local cQuery := ""
    Local cTexto := ""
    Local cPorta        := ALLTRIM(SuperGetMv("MS_YIMPPOR",.F., "COM7"))
    Local cImpressora   := ALLTRIM(SuperGetMv("MS_YIMPFIS",.F., "BEMATECH TH"))
    Local lUsaFis       := ALLTRIM(SuperGetMv("MS_YUSAFIS",.F., .F.))
    Local lSpool        := SuperGetMv("MS_SPOOL",.F., .T.)
    Local aTextos        := {}
    Local nLeft		:= 20	
    Local oFont14n	:= TFont():New("Arial", 9, 16,.T.,.T., 5,.T., 5, .T., .F.)
    Local oFont14	:= TFont():New("Arial", 9, 16,.T.,.F., 5,.T., 5, .T., .F.)
    Local nHeightMax	:= Val(SuperGetMv("MS_MAXHREL",.F.,"2350"))
	Local nHCupomMax	:= Val(SuperGetMv("MS_MAXHCUP",.F.,"3200"))
	Local cForAnt	:= ""
	Local cForAtu	:= ""

    cQuery    := GetQuery()
    
    If Select("QRY") > 0
        QRY->(DbCloseArea())
    EndIf

    TcQuery cQuery New Alias "QRY"
    
    If QRY->(!EoF())
        
        oPrint 	:= FWMSPrinter():New('romaneio'+'_'+ DtoS(dDatabase) + '_' + StrTran(Time(),':', '_') + ".rel")  
        oPrint:SetPaperSize(2)
        oPrint:SetMargin(0,0,0,0)
        oPrint:StartPage()
        
        nLinha	:= 150
        oPrint:SayAlign(nLinha, nLeft, 'Impressao de Romaneio '+ Z11->Z11_CODIGO, oFont14n,2000,100,,0)
        
        // aAdd(aTextos,"Produto "+Space(3)+" Descricao ")
        // aAdd(aTextos,SPACE(10)+"Qtd"+SPACE(3)+" Qtd. Coletada")
        // aAdd(aTextos,Replicate("-",48) )
        nLinha += 50
        While QRY->(!EoF())
            cForAtu := QRY->(C7_FORNECE+C7_LOJA)
            If Empty(cForAnt) .OR. cForAnt != cForAtu
                nLinha += 50
                // aAdd(aTextos,'Impressao de Romaneio ' + Z11->Z11_CODIGO)
                SA2->(DbSetOrder(1))
                SA2->(DbSeek(xFilial('SA2') + QRY->(C7_FORNECE + C7_LOJA)))
                
                oPrint:SayAlign(nLinha, nLeft, 'Fornecedor: ' + ALLTRIM(SA2->A2_NOME), oFont14n,2000,100,,0)
                nLinha += 75
                oPrint:SayAlign(nLinha, nLeft, "Produto"+Space(36)+"Qtd", oFont14n,2000,100,,0)
                nLinha  += 50
            EndIf

            cQuant := cValToChar(QRY->C7_QUANT)//Transform(Round(QRY->C7_QUANT,2),"@E 999")
            oPrint:SayAlign(nLinha, nLeft, Lower(PADR(QRY->C7_DESCRI,27)), oFont14,2000,100,,0)
            oPrint:SayAlign(nLinha, nLeft+560, Iif(len(cQuant) <= 2 , StrZero(QRY->C7_QUANT,2),StrZero(QRY->C7_QUANT,2) ), oFont14,200,100,,0)
            If nLinha > nHeightMax
                oPrint:EndPage()
                oPrint:StartPage()
                nLinha	:= 0
            EndIf
            nLinha  += 50

            cForAnt := QRY->(C7_FORNECE+C7_LOJA)
            QRY->(DbSkip())
        EndDo
        
        oPrint:EndPage()
        oPrint:Preview()    
    EndIf
    
    
Return aTextos

Static Function impSpool(aDados)
   	Local nLinha	:= 150
	Local nLeft		:= 20	
    Local nTamText  := 0
	Local nLargura	:= 2100 
	Local nWidthMax	:= 800
	Local nHeightMax	:= Val(SuperGetMv("MS_MAXHREL",.F.,"2350"))
	Local nHCupomMax	:= Val(SuperGetMv("MS_MAXHCUP",.F.,"3200"))
    Local i

    oPrint 	:= FWMSPrinter():New('romaneio'+'_'+ DtoS(dDatabase) + '_' + StrTran(Time(),':', '_') + ".rel")  
    oPrint:SetPaperSize(2)
	oPrint:SetMargin(0,0,0,0)

    oFont12	:= TFont():New("Courier new", 9, 12,.T.,.T., 5,.T., 5, .T., .F.)
	oFont14n	:= TFont():New("Arial", 9, 16,.T.,.T., 5,.T., 5, .T., .F.)

    For i := 1 to Len(aDados)
        nLinha	:= 150
        aTextos := aDados[i]
        oPrint:StartPage()

        For j := 1 To Len(aTextos)
            nLinha += 50
            cTexto := aTextos[j]
            If oPrint:nDevice == IMP_PDF
                oPrint:SayAlign(nLinha, nLeft, cTexto, oFont14n,2000,500,,0)
                nTamText := oPrint:getTextWidth(AllTrim(cTexto),oFont12)
                While nTamText > 2000
                    nLinha += 50
                    nTamText -= 2000
                EndDo   

                If nLinha > nHeightMax
                    oPrint:EndPage()
                    oPrint:StartPage()
                    nLinha	:= 0
                EndIf
            Else
                oPrint:SayAlign(nLinha, nLeft, cTexto, oFont12,nWidthMax,500,,0)
                nTamText := oPrint:getTextWidth(AllTrim(cTexto),oFont12)
                While nTamText > nWidthMax
                    nLinha += 50
                    nTamText -= nWidthMax
                EndDo   

                If nLinha > nHCupomMax
                    oPrint:EndPage()
                    oPrint:StartPage()
                    nLinha	:= 0
                EndIf

            EndIf
                  
        Next

        oPrint:EndPage()
    
    Next
    
	oPrint:Preview()    
Return