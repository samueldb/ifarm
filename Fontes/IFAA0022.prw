//-------------------------------------------------------------------
/*/{Protheus.doc} IFAA0022
description
@author  Samuel Dantas
@since   04/06/2019
@version 1.0
/*/
//-------------------------------------------------------------------
User Function IFAA0022()

    Private cCadastro := 'Log de erros'

    Private aRotina := {{'Visualizar' ,'AxVisual',0,2} ,;
                        {'Alterar' ,'AxAltera',0,4}}


    Private cDelFunc := '.T.' // Validacao para a exclusao. Pode-se utilizar ExecBlock

    Private cString := 'Z28'
    DbSelectArea(cString)
    DbSetOrder(1)
    DbSelectArea(cString)

    mBrowse( 6,1,22,75,cString)

Return