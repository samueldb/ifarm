#INCLUDE 'Rwmake.ch'
#INCLUDE 'Protheus.ch'
#INCLUDE 'TbIconn.ch'
#INCLUDE 'Topconn.ch'

//-------------------------------------------------------------------
/*/{Protheus.doc} Fun��o para cadastro de fotos de produtos
Fun��o para cadastro de fotos do produto
@author  Samuel Dantas
@since   24/01/2019
@version version
/*/
//-------------------------------------------------------------------
User Function IFAF0003 (cCodProd, cZ18_CODZ21, cZ18_CODZ17, cZ18_ITEM)
                        
    // Private cEnd           := "http://192.168.200.90:9052"
    Local cDirWeb        := Alltrim(SuperGetMV('MS_DIRIMGS',.F.,'/web/'))
    Local cDirLocal      := StrTran(cDirWeb, '/','\')
    
    Local aAreaZ18       := Z18->(GetArea())
    Local cProd        
    Private cEnd           := Alltrim(SuperGetMV('MS_ENDHTTP',.F.,'http://187.111.253.19:9051'))  
    Private cProdPadr        
    Private cProdOld        
    Private lNotFound := .F.
    Private cNomeImagem 
    Private lAberto     := .T.
    
    While lAberto
        lAberto := .F.
        Z18->(DbSetOrder(2)) //FILIAL+CODZ21+CODZ17+ITEM
        If Z18->(DbSeek(xFilial('Z18') + cZ18_CODZ21 + cZ18_CODZ17 + cZ18_ITEM ))

            cProd       := Alltrim(cFilAnt + '-' + cCodProd + cZ18_CODZ21 + cZ18_CODZ17 + cZ18_ITEM    )
            cProdPadr       := Alltrim(cFilAnt + '-' + cCodProd + cZ18_CODZ21 + cZ18_CODZ17 + cZ18_ITEM    )
            cProdOld        := AllTrim(cFilAnt + '-' + cCodProd)
            cNomeImagem := cProd + '.png'        
            
            If Empty(Z18->Z18_URLIMG)        
                RecLock('Z18', .F.)
                    Z18->Z18_URLIMG := cProd
                Z18->(MsUnLock())
                __CopyFile(cDirLocal + '\imgs\' + cFilAnt + '-' + cCodProd + ".png" ,cDirLocal + '\imgs\' +  cProd + '.png')        
            Else
                cProd := AllTrim(Z18->Z18_URLIMG)
            EndIf

            cNomeImagem := cProd + '.png'  

            cPagina     := cEnd + '/pages/' + cProd + ".html"
            cNotFound   := cEnd + '/imgs/'+ "404.png"
            
            criahtml(cDirLocal, cEnd, cProd)
                
            DEFINE DIALOG oDlg TITLE "Imagem" FROM 180,180 TO 550,700 PIXEL      
                
                If File(cDirLocal + 'imgs\' + cNomeImagem)
                    oTIBrowser := TIBrowser():New(0,0,260,170,cPagina,oDlg)	
                Else
                    oTIBrowser := TIBrowser():New(0,0,260,170,cNotFound,oDlg)	
                EndIf
                
                oTIBrowser:Align := CONTROL_ALIGN_ALLCLIENT

                TButton():New( 172, 052, "Inserir/Alterar", oDlg,;
                    {|| InserirAlterar(cDirLocal), oTIBrowser:Navigate(cPagina) },40,010,,,.F.,.T.,.F.,,.F.,,,.F. )
                    
                TButton():New( 172, 102, "Excluir", oDlg,;
                    {|| Excluir(cDirLocal) },40,010,,,.F.,.T.,.F.,,.F.,,,.F. )
                    
                // Dispon�vel apenas em vers�es superiores a 7.00.170117A - 17.2.0.2
                TButton():New( 172, 152, "Sair", oDlg,;
                    {|| Sair() },40,010,,,.F.,.T.,.F.,,.F.,,,.F. )
                    
            ACTIVATE DIALOG oDlg CENTERED
        Else
            lAberto := .F.
            ApMsgInfo('O produto ainda n�o foi salvo, salve o cadastro antes de alterar a imagem.')
        EndIf
    EndDo
    RestArea(aAreaZ18)

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} inseriralterar
Insere ou altera uma imagem de um produto
@author Sidney Sales
@since   17/01/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function inseriralterar(cDir)
    local cPathPdr      := Alltrim(GETMV("MS_PATHPDR", .F., "C:\TEMP"))
    Local cImgFile      := ""
    Local cProd         := ""
    
    Private cPerg := PadR('U_IFAF0003', 10)
    
    If !ExistDir("C:\TEMP")
        MakeDir( "C:\TEMP")
    EndIf

    cImgFile      := cGetFile("Imagem PNG|*.png","Abrir imagem do disco",0, cPathPdr,.t., 16 ,.T.)
    
    If !Empty(cImgFile)
        aAux := STRTOKARR( cImgFile, "\" )
        cFileAux := aAux[len(aAux)]
        cPathPdr := LEFT(cImgFile,LEN(cImgFile) - LEN(cFileAux))
    EndIF

    PUTMV("MS_PATHPDR", cPathPdr)

    If !EMPTY(cImgFile)
        cNomeAux            := StrTran(Lower(cNomeImagem), '.png','')
        cNomeImagem         := cValToChar(Month(dDataBase)) + cValToChar(Day(dDataBase)) + StrTran(Time(),":","") + ".png"
        cProd               := StrTran(Lower(cNomeImagem), '.png','')
        RecLock('Z18', .F.)
            Z18->Z18_URLIMG := cProd
        Z18->(MsUnLock())    
        __CopyFile(cImgFile,cDir + '\imgs\' + cNomeImagem)
        cPagina     := cEnd + '/pages/' + cNomeAux + ".html"
        oDlg:End()
        lAberto := .T.
    EndIf
    
Return 

Static Function criahtml(cDirLocal, cEnd, cProd)
    Local cHtml
    
    cHtml := '<!DOCTYPE HTML>'
    cHtml += '<html>'
    cHtml += '<head>'
    cHtml += '<title>Title of the document</title>'
    cHtml += '</head>'
    cHtml += '<body>'
    cHtml += '<img src="'+cEnd+'/imgs/'+cProd + '.png">'
    cHtml += '</body>'
    cHtml += '</html>'

    MemoWrit(cDirLocal + 'pages\' + cProd + '.html', cHtml)

return

Static function Excluir(cDir)
    oDlg:End()
    lAberto := .F.
    FErase( cDir + 'pages\' + StrTran(cNomeImagem,"png","html"))
    FErase( cDir + 'imgs\' + cNomeImagem)
    //Imagem inicial
    FErase( cDir + 'pages\' +cProdPadr+".html")
    FErase( cDir + 'imgs\' +cProdPadr+".png")
    //Imagens salvas antes de ir para produ��o
    FErase( cDir + 'pages\' +cProdOld+".html")
    FErase( cDir + 'imgs\' +cProdOld+".png")

    RecLock('Z18', .F.)
        Z18->Z18_URLIMG := ''
    Z18->(MsUnLock())
Return

Static function Sair()
    oDlg:End()
    lAberto := .F.
Return

//-------------------------------------------------------------------
/*/{Protheus.doc} ValidPerg
Criacao das perguntas do relatorio
@author  author
@since   22/06/2020
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ValidPerg
    Local aRegs    := {}
    Local aAreaSX1 := SX1->(GetArea())
    Local i, j

    SX1->(DbSetOrder(1))

    // Numeracao dos campos:
    // 01 -> X1_GRUPO   02 -> X1_ORDEM    03 -> X1_PERGUNT  04 -> X1_PERSPA  05 -> X1_PERENG
    // 06 -> X1_VARIAVL 07 -> X1_TIPO     08 -> X1_TAMANHO  09 -> X1_DECIMAL 10 -> X1_PRESEL
    // 11 -> X1_GSC     12 -> X1_VALID    13 -> X1_VAR01    14 -> X1_DEF01   15 -> X1_DEFSPA1
    // 16 -> X1_DEFENG1 17 -> X1_CNT01    18 -> X1_VAR02    19 -> X1_DEF02   20 -> X1_DEFSPA2
    // 21 -> X1_DEFENG2 22 -> X1_CNT02    23 -> X1_VAR03    24 -> X1_DEF03   25 -> X1_DEFSPA3
    // 26 -> X1_DEFENG3 27 -> X1_CNT03    28 -> X1_VAR04    29 -> X1_DEF04   30 -> X1_DEFSPA4
    // 31 -> X1_DEFENG4 32 -> X1_CNT04    33 -> X1_VAR05    34 -> X1_DEF05   35 -> X1_DEFSPA5
    // 36 -> X1_DEFENG5 37 -> X1_CNT05    38 -> X1_F3       39 -> X1_GRPSXG

    aAdd(aRegs, {cPerg, "01", "File?"  , "", "", "mv_ch1", 'C', 8, 0, 0, 'F', "", "MV_PAR01", "",  "", "", "", "", "",    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""})

    For i := 1 To Len(aRegs)
        If ! SX1->(DbSeek(cPerg+aRegs[i,2]))
            RecLock("SX1", .T.)

            For j :=1 to SX1->(FCount())
                If j <= Len(aRegs[i])
                    SX1->(FieldPut(j,aRegs[i,j]))
                EndIf
            Next

            SX1->(MsUnlock())
        EndIf
    Next

    SX1->(RestArea(aAreaSX1))
Return
