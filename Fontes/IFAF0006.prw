#INCLUDE 'Rwmake.ch'
#INCLUDE 'Protheus.ch'
#INCLUDE 'TbIconn.ch'
#INCLUDE 'Topconn.ch'

//-------------------------------------------------------------------
/*/{Protheus.doc} IFAF0006
description
@author  Samuel Dantas
@since   05/06/2019
@version version
/*/
//-------------------------------------------------------------------
User Function IFAF0006()
    Local cQuery        := ""
    Local cMsg          := ""
    Local cAssunto      := ""
    Local cEmail        := ""
    Local nX            := 1

    If Empty(FunName())
        PREPARE ENVIRONMENT EMPRESA '01' FILIAL '0101'
    EndIf

    cEmails := AllTrim(SuperGetMv("MS_TECMAIL",.F.,"samuel.batista@agisrn.com;sidney.sales@agisrn.com"))  

    aEmails	:= StrTokArr2( cEmails, ";" )
    ConOut( "Chamada a IFAF0006")
    
    cQuery := " SELECT R_E_C_N_O_ AS RECNO FROM "  + RetSqlName('Z28') + " Z28"
    cQuery += " WHERE Z28.D_E_L_E_T_ <> '*' "
    cQuery += " AND Z28_STATUS = 'I' AND Z28_ENVIAD = 'F' "
    
    If Select('QRY') > 0
        QRY->(dbclosearea())
    EndIf
    
    TcQuery cQuery New Alias 'QRY'
    If QRY->(!Eof())
        cAssunto := " IFARM - Rotinas n�o conclu�das "
        cMsg += PADR("Rotina - ",len(Z28->Z28_ROTINA))+Space(2)+PADR("Data - ",8)+Space(2)+PADR("Hora - ",len(Z28->Z28_TMPENT))+Space(2)+PADR("Fonte ",len(Z28->Z28_FONTE)) + CRLF
        While QRY->(!Eof())
            Z28->(DbGoTo(QRY->RECNO))
            cMsg += AllTrim(Z28->Z28_ROTINA)+Space(2)+" - "+DtoC(Z28->Z28_DATA)+Space(2)+" - "+Z28->Z28_TMPENT+Space(2)+" - "+Alltrim(Z28->Z28_FONTE) + CRLF
            
            RecLock('Z28', .F.)
                Z28->Z28_ENVIAD := .T.
            Z28->(MsUnLock())
            QRY->(dbSkip())
        EndDo

        For nX := 1 To len(aEmails)
            U_EnviaEmail(cAssunto, aEmails[nX], cMsg,'','') 
        Next
    EndIf
Return


User Function IFAF006A(cTexto, cStatus, cRotina, cFonte, lAtualiza)
    Default cRotina := ""
	Default cStatus := "I"
	Default cTexto := ""
	Default lAtualiza := .F.
	Default cFonte := ""

    If lAtualiza
        AtualizarLog(cTexto, cStatus)
    Else
        SalvarLog( cTexto, cStatus,cRotina,cFonte)
    EndIf

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} SalvarLog
@author  Samuel Dantas
@since   05/06/2019
@version version
/*/
//-------------------------------------------------------------------
Static Function SalvarLog( cTexto, cStatus,cRotina,cFonte)
	Default cRotina := ""
	Default cStatus := "I"
	Default cTexto := ""
	Default cFonte := ""

	RecLock('Z28', .T.)
		Z28->Z28_FILIAL := xFilial('Z28')
		Z28->Z28_ROTINA := cRotina
		Z28->Z28_DATA 	:= Date()
		Z28->Z28_TMPENT	:= Time()
		Z28->Z28_ENTRAD := cTexto
		Z28->Z28_STATUS := cStatus
		Z28->Z28_FONTE := cFonte
	Z28->(MsUnLock())

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} AtualizarLog
Atualiza a tabela de log caso tenha saida
@author  Samuel Dantas
@since   05/06/2019
@version version
/*/
//-------------------------------------------------------------------
Static Function AtualizarLog(cTexto, cStatus)
	
	RecLock('Z28', .F.)
		Z28->Z28_SAIDA  := cTexto
		Z28->Z28_STATUS  := cStatus
		Z28->Z28_TMPSAI  := Time()
	Z28->(MsUnLock())

Return