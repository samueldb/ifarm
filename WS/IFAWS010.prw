#INCLUDE "TOTVS.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "TBICONN.CH"
#INCLUDE "PROTHEUS.CH"
//-------------------------------------------------------------------
/*/{Protheus.doc} deliveryTimes
@author  Samuel Dantas
@since   19/12/2018
/*/
//-------------------------------------------------------------------
WSRESTFUL deliveryTimes DESCRIPTION "Rest para consulta horarios de entrega"

    WSMETHOD GET   DESCRIPTION "Consulta horarios de entrega" WSSYNTAX "/deliveryTimes/getDates/{Associate}"

END WSRESTFUL 

WSMETHOD GET WSRECEIVE _cFil WSSERVICE deliveryTimes          
	Local aResponse := {}		
    Local lRet       := .F.
    Local _cFil    := ''
    Local dDataEntrega 
    
	// define o tipo de retorno do metodo
	::SetContentType("application/json")   	
	
    If Len(::aURLParms) < 2
		SetRestFault(400, "Informe o codigo da associacao")
		return .F.
	Else
		_cFil := ::aURLParms[2]
        cFilAnt := _cFil
        If Upper(::aURLParms[1]) == "GETDATES"
            aResponse := getDates()
            lRet := aResponse[1]
        ElseIf Upper(::aURLParms[1]) == "GETTIMES"
            dDataEntrega := StoD(::aURLParms[3])
            aResponse := GetTimes(dDataEntrega)
            lRet := aResponse[1]
        Else
            SetRestFault(400, EncodeUTF8("A��o ainda n�o est� preparada"))
		    return .F.
        EndIf
    EndIf
    
    If ! lRet
        SetRestFault(400, EncodeUTF8(aResponse[2]))
    Else
        self:SetResponse(EncodeUTF8(aResponse[2]))
    EndIf

Return lRet 

//-------------------------------------------------------------------
/*/{Protheus.doc} getDates
M�todo para retorno de JSON com todas as data dispon�veis
@author  Samuel Dantas
@since   26/06/2019
@version version
/*/
//-------------------------------------------------------------------
Static Function getDates()
    Local oDia := JsonObject():New()
    Local aAreaZ30  := Z30->(GetArea())
    Local nDiaAtu   := 0
    Local nDiaAnt   := 0
    Local cQuery    := ""
    Local dData     := Date()
    Local aSemana  := {}
    Local lValido   := .F.
    Local oResult   := JsonObject():New()

    oResult['type']     := "success"
    oResult['status']   := 200
    oResult['message']  := "Datas Encontradas"
    oResult['result'] := {}

    //Array de semana
    aAdd(aSemana, {"1","","Domingo"} )
    aAdd(aSemana, {"2","","Segunda-feira"} )
    aAdd(aSemana, {"3","","Ter�a-feira"} )
    aAdd(aSemana, {"4","","Quarta-feira"} )
    aAdd(aSemana, {"5","","Quinta-feira"} )
    aAdd(aSemana, {"6","","Sexta-feira"} )
    aAdd(aSemana, {"7","","S�bado"} )
    
    cQuery := " SELECT DISTINCT Z30_DIA FROM "  + RetSqlName('Z30') + " Z30 "
    cQuery += " WHERE Z30.D_E_L_E_T_ <> '*' "
    cQuery += " AND Z30_ATIVO = 'T' "
    
    If Select('QRYZ30') > 0
        QRYZ30->(dbclosearea())
    EndIf
    
    TcQuery cQuery New Alias 'QRYZ30'
    If QRYZ30->(!Eof())
        While QRYZ30->(!Eof())
            
            dData   := Date()
            lValido := .F.
            cDiaAtu := QRYZ30->Z30_DIA
            //Se o dia da semana � o dia atual
            If Val(cDiaAtu) = Dow(Date())
                //Valida disponibilidade de horario
                If ExistHrDisp(cDiaAtu, cDiaAtu)
                    If !ExistHrDisp(cDiaAtu, cDiaAtu, LEFT(Time(),5))
                        dData := dData + 7
                        lValido := ExistLimit(dData,cDiaAtu) .AND. !VldFeriado(dData)
                    EndIf
                ElseIf !ValidaHorario(cDiaAtu)
                    dData := dData + 7
                    lValido := ExistLimit(dData,cDiaAtu) .AND. !VldFeriado(dData)
                EndIf
                
                //Valida limite de vendas por dia. Tamb�m valida feriados
                While !lValido
                    lValido := ExistLimit(dData,cDiaAtu) .AND. !VldFeriado(dData)
                    If !lValido
                        dData := dData + 7 // Pula uma semana
                    EndIf
                EndDo
            Else
                //Busca a data referente a dia de semana
                dData := GetProxDate( dData, cDiaAtu )
                
                If !ValidaHorario(cDiaAtu) .AND. !ExistHrDisp(cDiaAtu, cValToChar(Dow(Date())), LEFT(Time(),5))
                    dData := dData + 7
                    lValido := ExistLimit(dData,cDiaAtu) .AND. !VldFeriado(dData)
                EndIf

                //Valida limite de vendas por dia. Tamb�m valida feriados
                While !lValido
                    lValido := ExistLimit(dData,cDiaAtu) .AND. !VldFeriado(dData)
                    If !lValido
                        dData := dData + 7 // Pula uma semana
                    EndIf
                EndDo
            EndIf

            nPos := aScan(aSemana,{|x| AllTrim(x[1]) == cDiaAtu })
            If nPos > 0
                aSemana[nPos][2] := DtoC(dData)
            EndIf
            QRYZ30->(dbSkip())
        EndDo  
        //Monta Json
        For nI := 1 To len(aSemana)
            oDia := JsonObject():New()
            cCodigo := aSemana[nI][1]
            cData := aSemana[nI][2]
            cDescricao := aSemana[nI][3]
            If !EMPTY(aSemana[nI][2])
                oDia['codigo']  := cCodigo
                oDia['data']    := cData
                oDia['label']   := cDescricao
                aAdd(oResult['result'],oDia)
            EndIf
        Next
        oResult['result'] := OrdernarObj(oResult)
    Else
        oResult['type']     := "Error"
        oResult['status']   := 400
        oResult['message']  := "Nenhum dia encontrado."
        oResult['result'] := {}
    EndIf
    
Return { .T.,oResult:toJson() }

//-------------------------------------------------------------------
/*/{Protheus.doc} OrdernarObj
Ordernar Json com dias de entrega por data (DD/MM/YYYY)
@author  Samuel Dantas
@since   18/06/2019
@version version
/*/
//-------------------------------------------------------------------
Static Function OrdernarObj(oResult)
    Local aDiasEntrega := oResult['result']
    Local aRet := {}
    Local nI := 1
    Local dDtMenor := CtoD("")
    Local nIndiceMenor := 1

    While len(aDiasEntrega) > 0
        
        For nI := 1 To len(aDiasEntrega) 
            If EMPTY(dDtMenor)
                dDtMenor := CtoD(aDiasEntrega[nI]['data'])
                nIndiceMenor := nI
            ElseIf dDtMenor > CtoD(aDiasEntrega[nI]['data'])
                nIndiceMenor := nI
                dDtMenor := CtoD(aDiasEntrega[nI]['data'])
            EndIf
        Next

        aAdd(aRet,aDiasEntrega[nIndiceMenor])
        aDel(aDiasEntrega,nIndiceMenor)
        aSize(aDiasEntrega,len(aDiasEntrega) - 1)
        dDtMenor := CtoD("")
    EndDo

Return aRet

//-------------------------------------------------------------------
/*/{Protheus.doc} OrdernarObj
M�todo para valida��o de veriados
@author  Samuel Dantas
@since   18/06/2019
@version version
/*/
//-------------------------------------------------------------------
Static Function VldFeriado(dData)
    Local aAreaZ31  := Z31->(GetArea())
    Local lRet      := .F.
    
    Z31->(DbSetOrder(1))
    If Z31->(DbSeek(xFilial("Z31") + DtoS(dData)))
        lRet := .T.
    EndIf
    Z31->(RestArea(aAreaZ31))
Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} ExistLimit
Verifica se h� pelo menos um horario com capacidade de pedidos para 
a data especificada.
@author  Samuel Dantas
@since   21/06/2019
@version version
/*/
//-------------------------------------------------------------------
Static Function ExistLimit(dDataEntrega, cDia)
    Local oDia := JsonObject():New()
    Local aAreaZ30 := Z30->(GetArea())
    Local cSeek := ""
    Local lRet := .F.
    
    cSeek := xFilial("Z30") + cDia

    Z30->(DbSetOrder(1))
    Z30->(DbSeek(cSeek))
    While Z30->(!EOF()) .AND. cSeek == Z30->(Z30_FILIAL + Z30_DIA)
        If Z30->Z30_ATIVO
            lRet := VldSC5(dDataEntrega, Z30->Z30_CODZ12)
            If lRet
                Exit
            EndIf
        EndIf
        Z30->(DbSkip())
    EndDo
    Z30->(RestArea(aAreaZ30))
Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} VldSC5
Verifica se excedeu o limite de pedidos para a data de entrega e horario
especificado
@author  Samuel Dantas
@since   21/06/2019
@version version
/*/
//-------------------------------------------------------------------
Static Function VldSC5(dDataEntrega, cCodZ12)
   Local lRet := .F.
   Local cHrs := "" 

   cQuery := " SELECT DISTINCT COUNT(*) AS TOTAL FROM "  + RetSqlName('SC5') + " SC5 "
   cQuery += " INNER JOIN "+RetSqlName('Z12') + " Z12 ON Z12.D_E_L_E_T_ = SC5.D_E_L_E_T_ "
   cQuery += " WHERE SC5.D_E_L_E_T_ <> '*' "
   cQuery += " AND C5_YDTENTR = '"+DtoS(dDataEntrega)+"' "
   cQuery += " AND C5_YSTATUS <> 'F' "
   cQuery += " AND C5_YHRSZ12 LIKE '%"+cCodZ12+"%' "
   cQuery += " GROUP BY Z12_CODIGO "


   If Select('QRY') > 0
       QRY->(dbclosearea())
   EndIf
   
   TcQuery cQuery New Alias 'QRY'
    
    If QRY->(!Eof())    
        If Z30->Z30_CAPPED > QRY->TOTAL
            lRet := .T. 
        EndIF
    Else
        lRet := .T. 
    EndIf
    
    
Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} GetTimes
Retorna todos os horarios dispon�veis para a data solicitada.
@author  Samuel Dantas
@since   24/06/2019
@version version
/*/
//-------------------------------------------------------------------
Static Function GetTimes(dDataEntrega)
    Local aHorarios := {}
    Local aAreaZ30 := Z30->(GetArea())
    Local oResult := JsonObject():New()
    Local oHorario := JsonObject():New()

    oResult['type']     := "success"
    oResult['status']   := 200
    oResult['message']  := "Horarios Encontradas"
    oResult['result'] := {}

    cSeek := xFilial("Z30") + cValToChar(Dow(dDataEntrega))
    Z30->(DbSetOrder(1))
    Z30->(DbSeek(cSeek))
    
    While Z30->(!Eof()) .AND. cSeek == Z30->(Z30_FILIAL+Z30_DIA) 
        lValido := .F.
        If Z30->Z30_ATIVO
            oHorario := JsonObject():New()
            lValido := VldSC5(dDataEntrega, Z30->Z30_CODZ12) .AND. !VldFeriado(dDataEntrega) 
            If !lValido
                Z30->(DbSkip())
                loop
            Else
                If Val(Z30->Z30_DIAATE ) == Dow(Date())
                    If Z30->Z30_HORATE >= LEFT(Time(),5) 
                        oHorario['codigo']  := Z30->Z30_CODZ12
                        oHorario['horario'] := Z30->Z30_HORARI
                        aAdd(oResult['result'], oHorario )
                    ElseIf DateDiffDay( Date(), dDataEntrega ) >= 7 //Se a data � da pr�xima semana, considera todos os horarios.
                        oHorario['codigo']  := Z30->Z30_CODZ12
                        oHorario['horario'] := Z30->Z30_HORARI
                        aAdd(oResult['result'], oHorario )
                    EndIf
                ElseIf DateDiffDay( Date(), dDataEntrega ) >= 7 //Se a data � da pr�xima semana, considera todos os horarios.
                    oHorario['codigo']  := Z30->Z30_CODZ12
                    oHorario['horario'] := Z30->Z30_HORARI
                    aAdd(oResult['result'], oHorario )
                Else    
                    If ValidaZ30(Z30->(Recno()))
                        oHorario['codigo']  := Z30->Z30_CODZ12
                        oHorario['horario'] := Z30->Z30_HORARI
                        aAdd(oResult['result'], oHorario )
                    EndIF
                EndIf
            EndIf
        EndIf
        Z30->(DbSkip())
    EndDo
    
    If len(oResult['result']) < 1
        oResult['type']     := "Error"
        oResult['status']   := 400
        oResult['message']  := "Nenhum hor�rio foi encontrado para a data "+DtoC(dDataEntrega)+". "
        oResult['result'] := {}
    EndIf

    Z30->(RestArea(aAreaZ30))
Return {.T., oResult:toJson()}

//-------------------------------------------------------------------
/*/{Protheus.doc} ExistHrDisp
Valida se h� PELO MENOS UM horario disponivel de entrega para a data
Usado com quando o diaate � igual a data de hoje
@author  Samuel Dantas
@since   19/06/2019
@version version
/*/
//-------------------------------------------------------------------
Static Function ExistHrDisp(cDia, cDiaAte, cHrAtu)
    Local cQuery    := ""
    Local lRet      := .F.
    Local lVldTime   := .F.
    Default cHrAtu   := ""

    cQuery := " SELECT R_E_C_N_O_ AS RECNO FROM "  + RetSqlName('Z30') + " Z30"
    cQuery += " WHERE Z30.D_E_L_E_T_ <> '*' "
    cQuery += " AND Z30_DIA = '"+cDia+"' "
    cQuery += " AND Z30_DIAATE = '"+cDiaAte+"' "

    If !EMPTY(cHrAtu)
        cQuery += " AND Z30_HORATE >= '"+cHrAtu+"' "
    EndIf
    cQuery += " AND Z30_ATIVO = 'T' "

    If Select('QRY') > 0
        QRY->(dbclosearea())
    EndIf

    TcQuery cQuery New Alias 'QRY'

    If QRY->(!Eof())
        lRet := .T.
    EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} GetProxDate
Busca a proxima data para o dia da semana
@author  Samuel Dantas
@since   24/06/2019
@version version
/*/
//-------------------------------------------------------------------
Static Function GetProxDate(dData,cDiaSeman)
    While Dow(dData) != Val(cDiaSeman)
        dData := dData + 1
    EndDo
Return dData

//-------------------------------------------------------------------
/*/{Protheus.doc} ValidaHorario
Valida se existe PELO MENOS UM horario dispon�vel para a semana.
@author  Samuel Dantas
@since   26/06/2019
@param cDia, caractere, Dia da semana passado no json 
Ex: 1 -> Domingo, 2->segunda ...
@version version
/*/
//-------------------------------------------------------------------
Static Function ValidaHorario(cDia)

    Local cDiaAtu := cValToChar(Dow(Date())) //Dia hoje
    Local lRet  := .F.

    cQuery := " SELECT R_E_C_N_O_ AS RECNO FROM "  + RetSqlName('Z30') + " Z30"
    cQuery += " WHERE Z30.D_E_L_E_T_ <> '*' "
    cQuery += " AND Z30_DIA =  '"+cDia+"' "

    cQuery += " AND Z30_DIAATE <> '"+cDiaAtu+"' "
    cQuery += " AND Z30_ATIVO = 'T' "

    If Select('QRY') > 0
        QRY->(dbclosearea())
    EndIf
    
    TcQuery cQuery New Alias 'QRY'
    
    While QRY->(!Eof())
        If ValidaZ30(QRY->RECNO)
            lRet := .T.
            exit
        Endif
        
        QRY->(dbSkip())
    EndDo
    
Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} ValidaZ30
valida disponibilidade para o dia atual de uma Z30 espec�fica
@author  Samuel Dantas
@since   26/06/2019
@version version
/*/
//-------------------------------------------------------------------
Static Function ValidaZ30(nRecno)
    
    Local aAreaZ30  := Z30->(GetArea())
    Local cDiaAtu   := cValToChar(Dow(Date()))
    Local lRet      := .F.
    
    Z30->(DbGoTo(nRecno))
    If Z30->(!EoF())
        If Z30->Z30_DIA = Z30->Z30_DIAATE
            lRet := .T.
        ElseIf Z30->Z30_DIA > Z30->Z30_DIAATE
            If  cDiaAtu < Z30->Z30_DIA .AND. cDiaAtu <= Z30->Z30_DIAATE
                lRet := .T.   
            ElseIf cDiaAtu > Z30->Z30_DIA .AND. cDiaAtu >= Z30->Z30_DIAATE
                lRet := .T.        
            EndIf
        Else
            If cDiaAtu > Z30->Z30_DIA .AND. cDiaAtu <= Z30->Z30_DIAATE
                lRet := .T.    
            ElseIf cDiaAtu < Z30->Z30_DIA .AND. cDiaAtu >= Z30->Z30_DIAATE
                lRet := .T.    
            EndIf
        EndIf
    EndIf
    
    Z30->(RestArea(aAreaZ30))

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} IFAWS010
Fun��o de testes
@author  Samuel Dantas
@since   21/06/2019
@version version
/*/
//-------------------------------------------------------------------
User Function IFAWS010
    Local aRet := {}
    
    If Empty(FunName())
        PREPARE ENVIRONMENT EMPRESA '01' FILIAL '01'
    EndIf
    aRet := GETDATES()
    getTimes(StoD("20190625"))
Return

