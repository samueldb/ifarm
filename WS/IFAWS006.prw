#INCLUDE "TOTVS.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "TBICONN.CH"
//-------------------------------------------------------------------
/*/{Protheus.doc} IFAWS006
Webservice para get de categorias
@author  Jerry Junior
@since   08/01/2019
/*/
//-------------------------------------------------------------------
WSRESTFUL resource DESCRIPTION "Rest para Consulta das Categorias"
    WSMETHOD GET    DESCRIPTION "Consulta Categorias"   WSSYNTAX "/categories/{}"
END WSRESTFUL 


WSMETHOD GET WSRECEIVE cCod WSSERVICE resource     

	Local aResponse := {}		
    Local lRet       := .F.
    Local cCod    := ''
    
	// define o tipo de retorno do metodo
	::SetContentType("application/json")   	

	aResponse := categories()
    lRet := aResponse[1]
    U_IFAF0004("IFAWS0006", "categories", "GET", aResponse[2], " " )
    If ! lRet
        SetRestFault(400, EncodeUTF8(aResponse[2]))
    Else
        self:SetResponse(EncodeUTF8(aResponse[2]))
    EndIf

Return lRet 


//-------------------------------------------------------------------
/*/{Protheus.doc} categories
Funcao respons�vel por retornar as categorias - Z16
@author  Jerry Junior
@since   08/01/2019
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function categories()

    Local oResult
    Local oCliente
    Local oEndereco
    Local oDevices
    Local aRet := {}
    Local aResult := {}
    oResult := JsonObject():new()
    
    oResult['type']     := "success"
    oResult['status']   := 200
    oResult['message']  := "G�neros encontrados"

    aAreaZ16 := Z16->(GetArea())

    Z16->(dbGoTop())
    While !Z16->(EoF())
        oCategoria := JsonObject():new()
        oCategoria['descricao'] := ALLTRIM(Z16->Z16_NOME)
        oCategoria['id']        := ALLTRIM(Z16->Z16_NOME)
        aAdd(aResult,oCategoria)
        Z16->(dbSkip())
    EndDo 
    Restarea(aAreaZ16)      
    oResult['result'] := aResult
    aRet := {.T., oResult:toJson() }    

Return aRet


//funcoes de testes
User Function ifateste

    PREPARE ENVIRONMENT EMPRESA '01' FILIAL '01'
    aRet := categories()
    MsgAlert(aRet[2])

Return