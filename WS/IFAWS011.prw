#INCLUDE "TOTVS.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "TBICONN.CH"
//-------------------------------------------------------------------
/*/{Protheus.doc} customers
Webservice para inclusao de cadastros de clientes relacionado 
ao vendedor no IFARM
SA1 - Clientes

@author  Samuel Dantas
@since   19/12/2018
/*/
//-------------------------------------------------------------------
WSRESTFUL clientes DESCRIPTION "Rest para Inclusao de clientes Protheus"
    WSMETHOD GET    DESCRIPTION "Consulta cliente"   WSSYNTAX "/clientes/getById/{}"
END WSRESTFUL 

WSMETHOD GET WSRECEIVE cCodVend WSSERVICE clientes     

	Local aRet := {}		
    Local lRet       := .F.
    Local cCodVend    := ''
    Local oResponse := JsonObject():new()   
	
    // define o tipo de retorno do metodo
	::SetContentType("application/json")   	
	
    If Len(::aURLParms) < 2
		SetRestFault(400, "Informe o codigo da associacao")
		return .F.
	Else
		cCodVend := ::aURLParms[2]
	EndIf

	aRet := getById(cCodVend)
    lRet := aRet[1]
    
    If ! lRet    
		oResponse['type']		:= "error"
		oResponse['status']		:= 404
		oResponse['message']	:= aRet[2]
        self:SetResponse(EncodeUTF8(oResponse:toJson()))
        return .T.
    Else
        self:SetResponse(EncodeUTF8(aRet[2]))
    EndIf

Return lRet 

//-------------------------------------------------------------------
/*/{Protheus.doc} GetbyId
Metodo responsavel por retornar campos do cliente a partir do codigo
@author  Samuel Dantas
@since   08/01/2019
@param   cCodCli, caractere, codigo do cliente
/*/
//-------------------------------------------------------------------
Static function GetbyId(cCodVend)
    
    Local oResult
    Local oCliente
    Local oEndereco
    Local oDevices
    Local aRet := {}
    Local cQrySA1 := ""

    oResult := JsonObject():new()
    oResult['type']     := "success"
    oResult['status']   := 200
    oResult['message']  := "Cliente encontrado"
    oResult['result']   := {}
    cQrySA1 := " SELECT R_E_C_N_O_ AS RECNO FROM "  + RetSqlName('SA1') + " SA1"
    cQrySA1 += " WHERE SA1.D_E_L_E_T_ <> '*'"
    cQrySA1 += " AND A1_VEND = '"+cCodVend+"' "
    
    If Select('QRYSA1') > 0
        QRYSA1->(dbclosearea())
    EndIf
    
    TcQuery cQrySA1 New Alias 'QRYSA1'
    
    If !EMPTY(cCodVend)
        If QRYSA1->(!Eof())
            While QRYSA1->(!Eof())
                SA1->(DbGoTo(QRYSA1->RECNO))
                
                cQuery := " SELECT A2_FILIAL, A2_COD FROM "  + RetSqlName('SA2') + " SA2"
                cQuery += " WHERE SA2.D_E_L_E_T_ <> '*' AND A2_CLIENTE = '"+SA1->A1_COD+"' " 
                cQuery += " AND A2_LOJCLI = '"+SA1->A1_LOJA+"' " 
                
                If Select('QRY') > 0
                    QRY->(dbclosearea())
                EndIf
                
                TcQuery cQuery New Alias 'QRY'
                
                oCliente := JsonObject():new()
                oCliente['uid']                     := ALLTRIM(SA1->A1_YFIREB)
                oCliente['nome']                    := ALLTRIM(SA1->A1_NOME)
                oCliente['id']                      := ALLTRIM(SA1->A1_COD)
                oCliente['fornecedor']              := IIF( QRY->(!Eof()), .T., .F.)
                oCliente['associacao']              := IIF( QRY->(!Eof()), QRY->A2_FILIAL, "")

            
                aAdd(oResult['result'], oCliente)
                
                QRYSA1->(dbSkip())
            EndDo
        Else 
            cQrySA1 := " SELECT R_E_C_N_O_ AS RECNO FROM "  + RetSqlName('SA1') + " SA1 "
            cQrySA1 += " WHERE SA1.D_E_L_E_T_ <> '*' "
            
            If Select('QRYSA1') > 0
                QRYSA1->(dbclosearea())
            EndIf
            
            TcQuery cQrySA1 New Alias 'QRYSA1'

            While QRYSA1->(!Eof())

                SA1->(DbGoTo(QRYSA1->RECNO))
                
                cQuery := " SELECT A2_FILIAL, A2_COD FROM "  + RetSqlName('SA2') + " SA2"
                cQuery += " WHERE SA2.D_E_L_E_T_ <> '*' AND A2_CLIENTE = '"+SA1->A1_COD+"' " 
                cQuery += " AND A2_LOJCLI = '"+SA1->A1_LOJA+"' " 
                
                If Select('QRY') > 0
                    QRY->(dbclosearea())
                EndIf
                
                TcQuery cQuery New Alias 'QRY'
                
                oCliente := JsonObject():new()
                oCliente['uid']                     := ALLTRIM(SA1->A1_YFIREB)
                oCliente['nome']                    := ALLTRIM(SA1->A1_NOME)
                oCliente['id']                      := ALLTRIM(SA1->A1_COD)
                oCliente['fornecedor']              := IIF( QRY->(!Eof()), .T., .F.)
                oCliente['associacao']              := IIF( QRY->(!Eof()), QRY->A2_FILIAL, "")

            
                aAdd(oResult['result'], oCliente)
                
                QRYSA1->(dbSkip())
            EndDo
        EndIf
    EndIf

    aRet := {.T., oResult:toJson() }
Return aRet

//-------------------------------------------------------------------
/*/{Protheus.doc} IFAWS11A
Fun��o para testes
@author  Samuel Dantas
@since   25/07/2019
@version version
/*/
//-------------------------------------------------------------------
User Function IFAWS11A

    PREPARE ENVIRONMENT EMPRESA '01' FILIAL '01'
   
    aRet := GetbyId("Q1mpI7VkUzl6PrAJxlbH")
    MsgAlert(aRet[2])

Return

