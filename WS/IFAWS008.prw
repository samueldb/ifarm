#INCLUDE "TOTVS.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "tbiconn.ch"

//-------------------------------------------------------------------
/*/{Protheus.doc} app_marketplace
Inclusao de estoques do produtor 
@author  Sidney Sales
@since   31/01/19
@version 1.0
/*/
//-------------------------------------------------------------------

WSRESTFUL estoqueprodutor DESCRIPTION "Inclusao de Estoques - Produtor"
	WSDATA fornecedor as String
    WSMETHOD POST  	DESCRIPTION "Inclusao"  WSSYNTAX "/estoqueprodutor{fornecedor}"
END WSRESTFUL

WSMETHOD POST WSRECEIVE fornecedor WSSERVICE estoqueprodutor          
    Local cJson         := DecodeUtf8(self:getContent())
    Local lRet          := .T.
    Local oResponse     := JsonObject():new()
    Private lMsErroAuto := .F.
	// define o tipo de retorno do metodo
	::SetContentType("application/json")   	

	U_IFAF0004("IFAWS0008", "SalvarPed", "ENTRADA", cJson )
    U_IFAF006A(cJson,"I","Inclus�o de estoque","IFAWS008",.F.)

    SA1->(DBOrderNickname("A1YIDUSER"))
    lExist := SA1->(DbSeek(xFilial("SA1") + Alltrim(::fornecedor)))
    If !lExist
        SA1->(DBOrderNickname("A1YFIREB"))
        lExist := SA1->(DbSeek(xFilial("SA1") + Alltrim(::fornecedor)))
    EndIf

    If lExist
        
        cQuery := " SELECT A2_FILIAL, A2_COD, A2_LOJA FROM "  + RetSqlName('SA2') + " SA2"
        cQuery += " WHERE SA2.D_E_L_E_T_ <> '*' AND A2_CLIENTE = '"+SA1->A1_COD+"' " 
        cQuery += " AND A2_LOJCLI = '"+SA1->A1_LOJA+"' " 
        
        If Select('QRY') > 0
            QRY->(dbclosearea())
        EndIf
        
        TcQuery cQuery New Alias 'QRY'

        //Verifica se localiza o fornecedor vinculado
        If ! QRY->(Eof())
            cFilAnt := QRY->A2_FILIAL
            aRet    := MovimentoInterno(cJson, QRY->A2_COD, QRY->A2_LOJA)
            U_IFAF0004("IFAWS0008", "SalvarPed", "SAIDA", cJson, aRet[2] )
            lRet    := aRet[1]
        Else
            aRet := {.F.,'Fornecedor n�o localizado'}            
        EndIf
        
        //Verifica o retorno da rotina e define o retorno para o ws
        If ! lRet
            oResponse['type'] 	:= 'error'
            oResponse['status'] := 400
            oResponse['message']:= aRet[2]
            oResponse['result'] := ""
            
            If lMsErroAuto
                U_IFAF006A(aRet[2],"I",,,.T.)
            Else
                U_IFAF006A(aRet[2],"E",,,.T.)
            EndIf
            SetRestFault(400, aRet[2])
            lRet := .F.
        Else

            U_IFAF0004("IFAWS0008", "POSTESTOQUE", "SAIDA", cJson, aRet[2] )
            oResponse['type'] 	:= 'success'
            oResponse['status'] := 200
            oResponse['message']:= aRet[2]
            oResponse['result'] := ""
            U_IFAF006A(aRet[2],"S",,,.T.)

            self:SetResponse(EncodeUTF8(oResponse:toJson()))
        EndIf
    Else
  		SetRestFault(400, "Fornecedor nao localizado")
        U_IFAF0004("IFAWS0008", "POSTESTOQUE", "SAIDA", cJson, aRet[2] )
		return .F.
    EndIf
    
Return lRet

Static Function MovimentoInterno(cJsonAux, cCodSA2, cLojaSA2)
    Local oJson     := JsonObject():new()
    Local cMovEntr	:= PADR(AllTrim(SuperGetMv("MS_MVENTR",.F.,"001")),3) //Parametro para TM de entrada
    Local cMovSaid	:= PADR(AllTrim(SuperGetMv("MS_MVSAID",.F.,"501")),3) //Parametro para TM de saida
    Local cArqErro	:= "erroauto.txt"
    Local lRet      := .T.
    Local cMsg      := 'Movimento Incluido com Sucesso'
    Local aItens    := {}
    Local cEmails	:= AllTrim(SuperGetMv("MS_TECMAIL",.F.,"samuel.batista@agisrn.com;sidney.sales@agisrn.com"))
	Local aEmails	:= {}
    Local aAux, aCab    
    

    oJson:fromJson(cJsonAux)

    If ValType(oJson) == 'U'
        Return {.F., 'Json com formato inv�lido. Verifique o JSON enviado.'}
    EndIf
            
    //pegar o de para do protheus x firebase
    SA2->(DbSetOrder(1))
    SA2->(DbSeek(xFilial('SA2') + cCodSA2 + cLojaSA2))

    BeginTran()

    For i := 1 to Len(oJson['produtos'])
                
        cProduto    := oJson['produtos'][i]['produto']
        nQuant      := oJson['produtos'][i]['quantidade']

        If SB1->(DbSeek(xFilial('SB1') + cProduto))

            If ! SB2->(DbSeek(xFilial('SB2') + cProduto + SA2->A2_YLOCAL))
                CriaSB2(cProduto,SA2->A2_YLOCAL)
            EndIf

            //Retorna o saldo
            nSaldoSB2 := SaldoSb2()

            //Se houver diferenca entre os estoques, ira fazer a movimetacao
            If nSaldoSB2 <> nQuant
                lMovimenta := .T.
                //Entrada
                If nSaldoSB2 < nQuant
                    cTM			:= cMovEntr
                    nQuant  	:= nQuant - nSaldoSB2
                //Saida
                Else
                    cTM			:= cMovSaid
                    nQuant	    := nSaldoSB2 - nQuant 
                Endif

                //Cabecalho
                aCab := {	{ "D3_TM"		, cTM       , Nil },;
                            {"D3_EMISSAO" 	,ddatabase  , NIL}} 

                aAux    := {}
                aItens  := {}

                aadd( aAux, { "D3_COD"   	, cProduto          , Nil } ) 
                aadd( aAux, { "D3_UM"    	, SB1->B1_UM        , Nil } ) 
                aadd( aAux, { "D3_QUANT" 	, nQuant            , Nil } ) 
                aadd( aAux, { "D3_LOCAL" 	, SA2->A2_YLOCAL    , Nil } )

                aAdd(aItens, aAux)
        
                If lMovimenta 
                    MSExecAuto( { |x,y,z| MATA241(x ,y,z) }, aCab, aItens, 3 ) 
                EndIf
                
                If lMsErroAuto
                    RollBackSX8()
                    MostraErro( GetSrvProfString("Startpath","") , cArqErro )	                
                    
                    cMsg := " Erro na movimentacao dos produtos. Operacao cancelada " + chr(13)+chr(10)
                    cMsg += MemoRead( GetSrvProfString("Startpath","") + '\' + cArqErro )
                    
                    U_IFAF0004("IFAWS0008", "MovimentoInterno", "SAIDA", cJsonAux+chr(13)+chr(10)+"Saida:"+cMsg , nil , '\log\mail\')
                    
                    lRet := .F.
                    Exit
                Else
                    cMsg := 'Movimento Incluido com Sucesso'
                EndIf

            EndIf

        Else
            lRet    := .F.
            cMsg    := 'Produto ' + cProduto + ' nao localizado no cadastro. Operacao cancelada'
        EndIf
    
    Next

    If lRet
        EndTran()
    Else    
        DisarmTransaction()
    EndIF

Return {lRet, cMsg }


User Function testamov()

    If Empty(FunName())
        PREPARE ENVIRONMENT EMPRESA '01' FILIAL '01'
    EndIf
    
    cJson := '{'
    cJson += '"produtos": ['
    cJson += '    {'
    cJson += '    "produto": "000000000000004",'
    cJson += '    "quantidade": 1'
    cJson += '    },'
    cJson += '    {'
    cJson += '    "produto": "000000000000003",'
    cJson += '    "quantidade": 1'
    cJson += '    },'
    cJson += '    {'
    cJson += '    "produto": "000000000000005",'
    cJson += '    "quantidade": 1'
    cJson += '    }'
    cJson += ']'
    cJson += '}'

    aRet := MovimentoInterno(cJson, '000000003', '0001')

    x:=1

Return
















