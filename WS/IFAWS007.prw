#INCLUDE "TOTVS.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "TBICONN.CH"
#INCLUDE "PROTHEUS.CH"
//-------------------------------------------------------------------
/*/{Protheus.doc} deliveryPoints
Webservice para inclusao de cadastros de avaliac�es no IFARM
Z20 - avaliac�es

@author  Samuel Dantas
@since   19/12/2018
/*/
//-------------------------------------------------------------------
WSRESTFUL deliveryPoints DESCRIPTION "Rest para consulta de enderecos de estabelecimentos"

    WSMETHOD GET   DESCRIPTION "Consulta de enderecos de estabelecimentos"    WSSYNTAX "/deliveryPoints/getByAssociate/{Associate}"

END WSRESTFUL 

WSMETHOD GET WSRECEIVE _cFil WSSERVICE deliveryPoints          
	Local aResponse := {}		
    Local lRet       := .F.
    Local _cFil    := ''
    
	// define o tipo de retorno do metodo
	::SetContentType("application/json")   	
	
    If Len(::aURLParms) < 2
		SetRestFault(400, "Informe o codigo da associacao")
		return .F.
	Else
		_cFil := ::aURLParms[2]
	EndIf

	aResponse := getByAssociate(_cFil)
    lRet := aResponse[1]
    U_IFAF0004("IFAWS0007", "GETDELIVERY", "GET", aResponse[2], " " )
    If ! lRet
        SetRestFault(400, EncodeUTF8(aResponse[2]))
    Else
        self:SetResponse(EncodeUTF8(aResponse[2]))
    EndIf

Return lRet      

//-------------------------------------------------------------------
/*/{Protheus.doc} getByAssociate
Funcao respons�vel por retornar endereco da associacao (Z20)
@author  Samuel Dantas 
@since   07/01/2019
@version version
/*/
//-------------------------------------------------------------------
Static function getByAssociate(_cFil)

    Local aRet := {}
    Local oEndereco 
    Local oResult

    oResult := JsonObject():new()
    
    
    oResult['type']     := "success"
    oResult['status']   := 200
    oResult['message']  := "Ponto de entrega encontrado"
    oResult['result'] := {}

    //Busca avaliac�es
    Z20->(DbSetOrder(1))
    
    If Z20->(DbSeek( PADR(_cFil,TAMSX3("Z20_FILIAL")[1]) ))
        While  Z20->(!EOF()) .AND. Z20->Z20_FILIAL == _cFil
            oEndereco := JsonObject():new()
            oEndereco["id"]             := ALLTRIM(Z20->Z20_CODIGO)
            oEndereco["cep"]            := ALLTRIM(Z20->Z20_CEP)
            oEndereco["retirar"]        := .T.
            oEndereco["descricao"]      := ALLTRIM(Z20->Z20_DESC)
            oEndereco["estado"]         := ALLTRIM(Z20->Z20_EST)
            oEndereco["complemento"]    := ALLTRIM(Z20->Z20_COMPLE)
            oEndereco["numero"]         := ALLTRIM(Z20->Z20_NUMERO)
            oEndereco["bairro"]         := ALLTRIM(Z20->Z20_BAIRRO)
            oEndereco["rua"]            := ALLTRIM(Z20->Z20_RUA)
            oEndereco["id_associado"]   :=_cFil
            oEndereco["cidade"]         := ALLTRIM(Z20->Z20_MUN)
            
            aAdd(oResult['result'],oEndereco)
            Z20->(DbSkip())
        EndDo
        aRet := {.T., oResult:toJson() }
    Else
        aRet := {.F., "Nenhuma endereco foi encontrado para essa associacao." }
    EndIf
Return aRet

//funcoes de testes
User Function IFAWS07A

    PREPARE ENVIRONMENT EMPRESA '01' FILIAL '01'

    aRet := getByAssociate("0101")
    
    MsgAlert(aRet[2])

Return
