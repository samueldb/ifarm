#INCLUDE "TOTVS.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "TBICONN.CH"
#INCLUDE "PROTHEUS.CH"
//-------------------------------------------------------------------
/*/{Protheus.doc} menus
Webservice para inclusao de cadastros de avaliac�es no IFARM
Z17 - CARD�PIO

@author  Samuel Dantas
@since   19/12/2018
/*/
//-------------------------------------------------------------------
WSRESTFUL menus DESCRIPTION "Rest para Inclusao de avaliac�es Protheus"
    
    WSDATA fornecedor AS STRING 
    WSDATA cliente    AS STRING 
    // WSMETHOD POST  DESCRIPTION "Inclusao/Alteracao" 	WSSYNTAX "menus/getByIdAssociateAtivos/{Associado}"
    WSMETHOD GET   DESCRIPTION "Avaliacao do pedido"    WSSYNTAX "menus/getByIdAssociateAtivos/{Associado}"

END WSRESTFUL 

WSMETHOD GET WSRECEIVE fornecedor, cliente WSSERVICE menus          
	Local aRet          := {}		
    Local lRet          := .F.
    Local _cFil         := ''
    Local oResponse     := JsonObject():new()
    Local lTemCli       := .F.
    Local lExist       := .F.

    Private aUltProds   := {}
	// define o tipo de retorno do metodo
	::SetContentType("application/json")   	
    
    If !EMPTY(::fornecedor)
        If Len(::aURLParms) == 2
            If ::aURLParms[1] == 'getByCategory'
                aRet := getProducts(::fornecedor,.T.,::aURLParms[2])
            EndIf
        Else
            aRet := getProducts(::fornecedor,.F.,"")
        EndIf
        
        lRet := aRet[1]
    ElseIf Len(::aURLParms) < 2
        SetRestFault(400, "Informe o codigo da associacao")
        return .F.
	Else
        
        _cFil := ::aURLParms[2]

        If !Empty(::cliente)
            SA1->(DBOrderNickname("A1YIDUSER"))
            
            lExist := SA1->(DbSeek(xFilial("SA1") + Alltrim(::cliente)))
            If !lExist
                SA1->(DBOrderNickname("A1YFIREB"))
                lExist := SA1->(DbSeek(xFilial("SA1") + Alltrim(::cliente)))
            EndIf

            If lExist
                aUltProds := getUltimosProdutos(_cFil)
                lTemCli := .T.
            EndIf
        EndIf

        aRet := getmenus(_cFil, lTemCli)
        lRet := aRet[1]
	EndIf
    U_IFAF0004("IFAWS0005", "menus", "GET", aReT[2], " " )
    If ! lRet
        SetRestFault(400, EncodeUTF8(aRet[2]))
    Else
        self:SetResponse(EncodeUTF8(aRet[2]))
    EndIf

Return lRet    
  
//-------------------------------------------------------------------
/*/{Protheus.doc} getmenus
Metodo para retorno de json com informac�es de card�pio da filial 
solicitada
@author  Samuel Dantas
@since   07/01/2019
@param   _cFil, caractere, Informada via rest 
@version version
/*/
//-------------------------------------------------------------------
Static function getmenus(_cFil, lTemCli)

    Local aRet := {}
    Local oMenu 
    Local oResult
    Local oCategoria 
    Local oProduto 
    Local nEstoqDisp    := 0
    Default lTemCli     := .F.

    cFilAnt := _cFil
    
    oResult     := JsonObject():new()
    oMenu       := JsonObject():new()
    oCategoria  := JsonObject():new()
    oProduto    := JsonObject():new()
    
    oResult['type']     := "success"
    oResult['status']   := 200
    oResult['message']  := "Resultados encontrados"
    oResult['result'] := {}
    
    //Busca card�pios
    Z17->(DbSetOrder(1)) // Z17_FILIAL + Z17_CODIGO
    Z17->(DbGoTop())
    Z17->(DbSeek(_cFil))
    If Z17->(!EOF())
        While  Z17->(!EOF()) .AND. Z17->Z17_FILIAL == _cFil
            
            oMenu := JsonObject():new()
            oMenu['descricao']           := ALLTRIM(Z17->Z17_DESC)
            oMenu['created']             := ALLTRIM(Z17->Z17_CREAT)
            oMenu['ordem']               := ALLTRIM(Z17->Z17_ORDEM)
            oMenu['status']              := ALLTRIM(Z17->Z17_STATUS)
            oMenu['updated']             := ALLTRIM(Z17->Z17_UPDATE)
            oMenu['id']                  := ALLTRIM(Z17->Z17_CODIGO)
            oMenu['codigo_associado']    := _cFil
            oMenu['categorias']          := {}
            oMenu['plano']               := ''
            
            Z21->(DbSetOrder(1)) //Z21_FILIAL + Z21_CODZ17 + Z21_CODIGO
            Z21->(DbGoTop())
            Z21->(DbSeek(cSeekZ21 := _cFil + Z17->Z17_CODIGO))

            aUltimos    := {}
            aAuxCategs  := {}

            While Z21->(!EOF()) .AND. cSeekZ21 ==  Z21->Z21_FILIAL + Z21->Z21_CODZ17
                
                oCategoria  := JsonObject():new()
                oCategoria['id']        := Z21->Z21_CODSBM
                oCategoria['updated']   := FWTIMESTAMP(3, Z21->Z21_DTCRIA, Z21->Z21_HRCRI)
                oCategoria['created']   := FWTIMESTAMP(3, Z21->Z21_DTATU, Z21->Z21_HRATU)
                oCategoria['status']    := "Ativo"
                oCategoria['descricao'] := ALLTRIM(Z21->Z21_NOME)
                oCategoria['produtos']  := {}
                oCategoria['destaque']  := Z21->Z21_DESTAQ

                Z18->(DbSetOrder(3)) //Z18_FILIAL + Z18_CODZ17 + Z18_CODZ21
                Z18->(DbGoTop())
                Z18->(DbSeek(cSeekZ18 := _cFil + Z17->Z17_CODIGO + Z21->Z21_ITEM))
                
                While Z18->(!EOF()) .AND. cSeekZ18 ==  Z18->Z18_FILIAL + Z18->Z18_CODZ17 + Z18->Z18_CODZ21
                    
                    nEstoqDisp := 0
                    SB1->(DbSetOrder(1)) //SB1_FILIAL + B1_COD
                    SB1->(DbSeek(_cFil + Z18->Z18_CODSB1))
                    nEstoqDisp := U_IFAF001C(Z18->Z18_CODSB1,"",.T.)

                    If nEstoqDisp <= 0 
                        Z18->(DbSkip())
                        loop
                    EndIf 

                    oProduto    := JsonObject():new()
                    oProduto['created']                 := FWTIMESTAMP(3, DATE(), TIME())
                    oProduto['preco']                   := Round(Z18->Z18_PRCDE, 2)                     
                    oProduto['valor']                   := Round(Z18->Z18_PRECO, 2)
                    oProduto['valorreal']               := Round(Z18->Z18_PRECO, 2)

                    If lTemCli
                        nPercPlano                      := getPercPlano(Z18->Z18_CODSB1, Z21->Z21_CODSBM, .F.)                        
                        If nPercPlano > 0                            
                            nPreco                      := Z18->Z18_PRECO - Z18->Z18_PRECO * nPercPlano / 100   
                            oProduto['valor']           := Round(nPreco, 2)
                            //Nesse momento o Z25 ta setado, pois passou pela funcao do getPercPlano
                            oMenu['plano']              := Z25->Z25_CODZ24
                        EndIf
                    EndIf

                    oProduto['produto']                 := ALLTRIM(Z18->(Z18_CODSB1))
                    oProduto['status']                  := "Ativo"
                    oProduto['usos']                    := ALLTRIM(SB1->B1_YUSO)
                    oProduto['id']                      := ALLTRIM(Z18->Z18_CODSB1)
                    oProduto['descricao']               := Z18->Z18_DESC
                    oProduto['validacao_cardapio']      := ""
                    oProduto['nome']                    := ALLTRIM(SB1->B1_DESC)
                    oProduto['updated']                 := FWTIMESTAMP(3, DATE(), TIME())
                    oProduto['quantidade_disponivel']   := nEstoqDisp
                    oProduto['desconto']                := oProduto['preco'] - oProduto['valor'] 
                    oProduto['id_cardapio']             := ALLTRIM(Z17->Z17_CODIGO)
                    oProduto['quantidade']              := 999
                    oProduto['selo']                    := SB1->B1_YSELO
                    
                    If Empty(Z18->Z18_URLIMG)
                        oProduto['urlimagem'] := Alltrim(Z18->Z18_FILIAL + '-' + Z18->Z18_CODSB1)
                    Else
                        oProduto['urlimagem'] := Alltrim(Z18->Z18_URLIMG)
                    EndIf

                    aAdd(oCategoria['produtos'],oProduto)

                    If aScan(aUltProds, Z18->Z18_CODSB1) <> 0
                        aAdd(aUltimos,oProduto)
                    EndIf

                    Z18->(DbSkip())
                EndDo
                Z21->(DbSkip())
                
                aAdd(aAuxCategs,oCategoria)
            EndDo

            If Len(aUltimos) > 0
                oCategoria  := JsonObject():new()
                oCategoria['id']        := 'ULTI'
                oCategoria['updated']   := FWTIMESTAMP(3, dDataBase, Time())
                oCategoria['created']   := FWTIMESTAMP(3, dDataBase, Time())
                oCategoria['status']    := "Ativo"
                oCategoria['descricao'] := 'Ultimos Pedidos'
                oCategoria['produtos']  := aUltimos
                oCategoria['destaque']  := "N"
                aAdd(oMenu['categorias'],oCategoria)
                
            EndIf

            For i := 1 to Len(aAuxCategs)
                aAdd(oMenu['categorias'],aAuxCategs[i])                
            Next

            aAdd(oResult['result'],oMenu)
            Z17->(DbSkip())

        EndDo
        aRet := {.T., oResult:toJson() }
    Else
        aRet := {.F., "Nenhum Card�pio foi encontrado para essa associacao." }
    EndIf
    
    
Return aRet

User Function IFAWS05 ()
    If Empty(FunName())
        PREPARE ENVIRONMENT EMPRESA '01' FILIAL '01'
    EndIf
    
    getProducts()
Return

Static function getProducts(cCliente, lRetProd, cItem)

    Local aRet := {}
    Local oMenu 
    Local oResult
    Local oCategoria 
    Local oProduto 
    Local nEstoqDisp    := 0
    Local aProdutos     := {}

    Default lRetProd    := .T.
    Default cItem       := ""

    oResult     := JsonObject():new()
    oMenu       := JsonObject():new()
    oCategoria  := JsonObject():new()
    oProduto    := JsonObject():new()
    
    oResult['type']     := "success"
    oResult['status']   := 200
    oResult['message']  := "Resultados encontrados"
    oResult['result'] := {}
    
    SA1->(DBOrderNickname("A1YIDUSER"))
    lExist := SA1->(DbSeek(xFilial("SA1") + Alltrim(cCliente)))
    If !lExist
        SA1->(DBOrderNickname("A1YFIREB"))
        lExist := SA1->(DbSeek(xFilial("SA1") + Alltrim(cCliente)))
    EndIf

    If lExist
        cQuery := " SELECT A2_FILIAL, A2_COD, A2_LOJA FROM "  + RetSqlName('SA2') + " SA2"
        cQuery += " WHERE SA2.D_E_L_E_T_ <> '*' AND A2_CLIENTE = '"+SA1->A1_COD+"' " 
        cQuery += " AND A2_LOJCLI = '"+SA1->A1_LOJA+"' " 
        
        If Select('QRY') > 0
            QRY->(dbclosearea())
        EndIf
        
        TcQuery cQuery New Alias 'QRY'
        
        If QRY->(!EOF())
            cFilAnt := QRY->A2_FILIAL
            cCodA2  := QRY->A2_COD
            cCodLoja := QRY->A2_LOJA
            //Busca card�pios
            Z17->(DbSetOrder(1)) // Z17_FILIAL + Z17_CODIGO
            Z17->(DbGoTop())
            If Z17->(!EOF())
                While  Z17->(!EOF()) .AND. Z17->Z17_FILIAL == xFilial("Z17")
                    
                    oMenu := JsonObject():new()
                    oMenu['descricao']           := ALLTRIM(Z17->Z17_DESC)
                    oMenu['created']             := ALLTRIM(Z17->Z17_CREAT)
                    oMenu['ordem']               := ALLTRIM(Z17->Z17_ORDEM)
                    oMenu['status']              := ALLTRIM(Z17->Z17_STATUS)
                    oMenu['updated']             := ALLTRIM(Z17->Z17_UPDATE)
                    oMenu['id']                  := ALLTRIM(Z17->Z17_CODIGO)
                    oMenu['codigo_associado']    := xFilial("Z17")
                    oMenu['categorias']          := {}

                    Z21->(DbSetOrder(1)) //Z21_FILIAL + Z21_CODZ17 + Z21_CODIGO
                    Z21->(DbSeek(cSeekZ21 := xFilial("Z21") + Z17->Z17_CODIGO + cItem))
                    While Z21->(!EOF()) .AND. cSeekZ21 ==  Z21->Z21_FILIAL + Z21->Z21_CODZ17 + IIF(Empty(cItem), cItem, Z21->Z21_ITEM)
                        aProdutos := {}
                        If lRetProd
                            Z18->(DbSetOrder(3)) //Z18_FILIAL + Z18_CODZ17 + Z18_CODZ21
                            Z18->(DbGoTop())
                            Z18->(DbSeek(cSeekZ18 := xFilial("Z18") + Z17->Z17_CODIGO + Z21->Z21_ITEM))
                            
                            While Z18->(!EOF()) .AND. cSeekZ18 ==  Z18->Z18_FILIAL + Z18->Z18_CODZ17 + Z18->Z18_CODZ21                   
                                nEstoqDisp := 0
                                SB1->(DbSetOrder(1)) //SB1_FILIAL + B1_COD
                                SB1->(DbSeek(xFilial("Z18") + Z18->Z18_CODSB1))
                                
                                Z05->(DbSetOrder(1))
                                If Z05->(DbSeek(xFilial("Z05") + cCodA2 + cCodLoja + Z18->Z18_CODSB1))
                                    nEstoqDisp := U_IFAF001C(Z18->Z18_CODSB1,cCodA2,.F.)

                                    oProduto    := JsonObject():new()
                                    oProduto['created']                 := FWTIMESTAMP(3, DATE(), TIME())
                                    oProduto['preco']                   := Round(Z18->Z18_PRCDE, 2) 
                                    oProduto['valor']                   := Round(Z18->Z18_PRECO, 2)
                                    oProduto['valorreal']               := Round(Z18->Z18_PRECO, 2)
                                    oProduto['produto']                 := ALLTRIM(Z18->(Z18_CODSB1))
                                    oProduto['status']                  := "Ativo"
                                    oProduto['usos']                    := ALLTRIM(SB1->B1_YUSO)
                                    oProduto['id']                      := ALLTRIM(Z18->Z18_CODSB1)
                                    oProduto['descricao']               := ALLTRIM(Z18->Z18_DESC)
                                    oProduto['validacao_cardapio']      := ""
                                    oProduto['nome']                    := ALLTRIM(SB1->B1_DESC)
                                    oProduto['updated']                 := FWTIMESTAMP(3, DATE(), TIME())
                                    oProduto['quantidade_disponivel']   := nEstoqDisp
                                    oProduto['desconto']                := oProduto['preco'] - oProduto['valor'] 
                                    oProduto['id_cardapio']             := ALLTRIM(Z17->Z17_CODIGO)
                                    oProduto['quantidade']              := 999
                                    oProduto['selo']                    := SB1->B1_YSELO
                                    If Empty(Z18->Z18_URLIMG)
                                        oProduto['urlimagem'] := Alltrim(Z18->Z18_FILIAL + '-' + Z18->Z18_CODSB1)
                                    Else
                                        oProduto['urlimagem'] := Alltrim(Z18->Z18_URLIMG)
                                    EndIf
                                    aAdd(aProdutos,oProduto)
                                    
                                EndIf

                                Z18->(DbSkip())
                            EndDo
                        EndIf
            
                        If len(aProdutos) > 0 .OR. !lRetProd
                            oCategoria  := JsonObject():new()
                            oCategoria['id']        := Z21->Z21_CODSBM
                            oCategoria['updated']   := FWTIMESTAMP(3, Z21->Z21_DTCRIA, Z21->Z21_HRCRI)
                            oCategoria['created']   := FWTIMESTAMP(3, Z21->Z21_DTATU, Z21->Z21_HRATU)
                            oCategoria['status']    := "Ativo"
                            oCategoria['descricao'] := ALLTRIM(Z21->Z21_NOME)
                            oCategoria['item'] := ALLTRIM(Z21->Z21_ITEM)
                            oCategoria['produtos']  := {}
                            If lRetProd
                                For nI := 1 To len(aProdutos)
                                    aAdd(oResult['result'],aProdutos[nI])
                                Next
                            EndIf
                            aAdd(oMenu['categorias'],oCategoria)
                        EndIf

                        Z21->(DbSkip())

                    EndDo
                    If !lRetProd
                        aAdd(oResult['result'],oMenu)
                    EndIf
                    
                    Z17->(DbSkip())
                EndDo
                aRet := {.T., oResult:toJson() }
            Else
                aRet := {.F., "Nenhum Cardapio foi encontrado para essa associacao." }
            EndIf
        Else
            aRet := {.F., "O usuario nao e um fornecedor." }
        EndIf
    Else
        aRet := {.F., "O usuario nao encontrado." }
    EndIf

Return aRet

//-------------------------------------------------------------------
/*/{Protheus.doc} getUltimosProdutos
Retorna os ultimos produtos pedidos pelo cliente
@author  Sidney 
@since   05/04/19
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function getUltimosProdutos(_cFil)
    Local cQuery 
    Local nQtdTopPro    := SuperGetMv('MS_QTDTOP', .F., 10)
    Local aRet          := {}

    If nQtdTopPro <= 0
        nQtdTopPro := 1
    EndIf

    cQuery := "SELECT TOP " +cValToChar(nQtdTopPro)+ "C6_PRODUTO FROM ( "
    cQuery += "        SELECT C6_PRODUTO, MAX(R_E_C_N_O_) R_E_C_N_O_ FROM "+RetSqlTab('SC6') 
    cQuery += " WHERE D_E_L_E_T_ <> '*' AND C6_CLI = '"+SA1->A1_COD +"' AND C6_LOJA = '"+SA1->A1_LOJA+"' AND C6_FILIAL = '"+_cFil+"'
    cQuery += " GROUP BY C6_PRODUTO ) AS X "
    cQUery += " ORDER BY R_E_C_N_O_ DESC "

    TcQuery cQuery New Alias 'QRYC6'

    While QRYC6->(!Eof())
        aAdd(aRet, QRYC6->C6_PRODUTO)
        QRYC6->(DbSkip())
    EndDo

    QRYC6->(dbclosearea())

Return aRet


//-------------------------------------------------------------------
/*/{Protheus.doc} getPercPlano
Retorna o percentual de desconto para o cliente
@author  Sidney 
@since   10/04/19
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function getPercPlano(cProduto, cGrupo, lFrete)
    Local aRet          := {}
    Local nPercFrete    := 0
    Local nPercProd     := 0
    Local aAreaZ26      := Z26->(GetArea())
    Default lFrete      := .T.  
    
    Z24->(DbSetOrder(1))
    Z25->(DbSetOrder(1))

    //Posiciona o plano de desconto pelo cliente
    If Z25->(DbSeek(xFilial('Z25') + SA1->A1_COD))
        
        //Posiciona o cabecalho do plano
        Z24->(DbSeek(xFilial('Z24') + Z25->Z25_CODZ24))        
        
        //Percentual de desconto do frete e desconto do pedido
        nPercFrete  := Z24->Z24_DESCFR
        nPercProd   := Z24->Z24_DESPED
        
        //Se tiver desconto no produto, assume o desconto do produto
        Z26->(DbSetOrder(1))//Z26_FILIAL+Z26_CODSB1+Z26_CODZ24
        If Z26->(DbSeek(xFilial('Z26') + cProduto + Z25->Z25_CODZ24))
            nPercProd := Z26->Z26_DESCON
        EndIf
        
        //Se tiver desconto do grupo de produto e o do grupo for maior, assume o do grupo
        Z26->(DbSetOrder(2))//Z26_FILIAL+Z26_CODSBM+Z26_CODZ24
        If Z26->(DbSeek(xFilial('Z26') + cGrupo + Z25->Z25_CODZ24)) .AND. Z26->Z26_DESCON > nPercProd
            nPercProd := Z26->Z26_DESCON
        EndIf

    EndIf

    RestArea(aAreaZ26) 

    //Retorna o desconto de acordo com o solicitado
    If lFrete
        nRet := nPercFrete
    Else
        nRet := nPercProd    
    EndIf

Return nRet
