#INCLUDE "TOTVS.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "TBICONN.CH"
#INCLUDE "PROTHEUS.CH"
//-------------------------------------------------------------------
/*/{Protheus.doc} testecartao
Rota para testes de cartao 
@author  Sidney Sales
@since   18/06/19
@version version
/*/
//-------------------------------------------------------------------
WSRESTFUL testecartao DESCRIPTION "Rest para testes de cartao"

    WSMETHOD POST   DESCRIPTION "Rota para testes de cartao de credito"  WSSYNTAX "/testecartao/{Associate}"

END WSRESTFUL 

WSMETHOD POST WSSERVICE testecartao          
    Local cJson         := self:getContent()
    Local lRet          := .T.
    Local oResponse     := JsonObject():new()

	// define o tipo de retorno do m�todo
	::SetContentType("application/json")   	
	
    If Empty(::aURLParms)
		SetRestFault(400, "Informe o codigo da associa��o")
		return .F.
	Else
		cFilAnt := ::aURLParms[1]
	EndIf

    aRet := testecartao(cJson)

    //Verifica o retorno da rotina e define o retorno para o ws
    If ! aRet[1]
        SetRestFault(400, aRet[2])
        lRet := .F.
    Else
        oResponse['type'] 	:= 'success'
        oResponse['status'] := 200
        oResponse['message']:= aRet[2]
        oResponse['result'] := ""
        self:SetResponse(oResponse:toJson())
    EndIf

Return lRet      

Static Function testecartao(cJson)
    Local oJson     := JsonObject():New()
    Local oJsonPost := JsonObject():New()
    Local aHeader   := {}    
    Local cID, cToken
    Local lRet, cRet, i
    oJson:fromJson(cJson)    

    //Cabecalho da requisicao
    aAdd(aHeader, "Content-Type : application/json")
    
    //Token de acesso do mercado pago
    cToken  := Alltrim(SuperGetMV("MS_TOKENMP"  , .F., "TEST-7863235948118309-042310-d45196dae2d5f2ce1c56417fbb9677a2-319192752"))

    //URI Base para api do mercado pago
    cURLMP  := Alltrim(SuperGetMV("MS_URLMP"    , .F., "https://api.mercadopago.com/v1/payments"))    

    //Cria o objeto rest de comunicacao na URL
    oRest   := FwRest():new(cURLMP)

    //Prepara o json para o post        
    oJsonPost['token']                  := oJson['token']
    oJsonPost['payment_method_id']      := oJson['bandeira']
    oJsonPost['transaction_amount']     := 1
    oJsonPost['installments']           := 1
    
    //Objeto do pagador
    oPayer  :=  JsonObject():new()
    oPayer['email']     := oJson['email']
    oJsonPost['payer']  := oPayer

    //Prepara o json para o post
    cJson   := oJsonPost:ToJson()
    
    oRest:SetPostParams(cJson)
    oResult := JsonObject():new()

    //Seta o token de acesso
    oRest:setPath("?access_token="+cToken)

    //Realiza o pagamento de testes no valor de 1 real
    If oRest:Post(aHeader)
        
        //Obtem o resultado
        oResult:FromJson(oRest:GetResult())

        //Pega o ID do retorno
        cID     := cValToChar(oResult['id'])
        cStatus := oResult['status']        

        //Faz tres tentativas para ver se autorizou
        For i := 1 to SuperGetMV('MS_QTDTENT', .F.,3)
            
            cRet    := ''
            aRet    := autoriza(aHeader, oRest,cID,cStatus,cToken,cURLMP)
            
            //Verifica se autorizou ou se nao autorizou
            If aRet[1] .OR. aRet[2] == 'N�o autorizado'
                lRet := aRet[1]
                cRet := aRet[2]
                Exit
            Else
            //Pega o status para tentar novamente
                lRet    := aRet[1]
                cStatus := aRet[2]
            EndIf
        Next
        
        //Se nao preencheu o cret e poque nao obteve sucesso no retorno
        If Empty(cRet)
            cRet := cStatus
        EndIf

    Else        
        
        //Verifica o erro
        conout(oRest:GetResult())
        oResult:FromJson(oRest:GetResult())
        cRet := oResult['message']

        //Caso nao tenha erro, tenta capturar o erro do objeto
        If Empty(cRet)
            cRet := oRest:GetLastError()
        EndIf
        
        lRet := .F.
        cRet := 'Erro de comunica��o ' + cRet
    
    EndIf

Return {lRet, cRet}

Static Function autoriza(aHeader, oRest,cID,cStatus,cToken,cURLMP)
    Local lRet  := .F.
    Local cRet  := ''
    Local cPatch, cAux

    If Alltrim(cStatus) $ Alltrim(SuperGetMv('MS_STATPAG', .F., 'approved,authorized'))
        lRet   := .T.
        cRet   := 'Autorizado'
        
        
        oRest   := FwRest():new(cURLMP)
        
        cPath := "/"+cID+"/refunds?access_token="+cToken
        
        //Seta o token de acesso
        oRest:setPath(cPath)
        
        If oRest:Post(aHeader)
            //gravar aqui para cancelar depois            
            cAux := 'Cancelamento de cart�o teste de cart�o conclu�do' + chr(13) + chr(10)
            cAux += 'Url: ' + cURLMP + chr(13) + chr(10)
            cAux += 'Path:' + cPath + chr(13) + chr(10)
            cAux += oRest:GetResult()
            oResult:FromJson(oRest:GetResult())
            MemoWrit('\logcartao\cancelado\' + cID + '.txt', cAux)
        Else
            //gravar aqui para cancelar depois            
            cAux := 'Cancelamento de cart�o teste de cart�o n�o conclu�do' + chr(13) + chr(10)
            cAux += 'Url: ' + cURLMP + chr(13) + chr(10)
            cAux += 'Path:' + cPath + chr(13) + chr(10)
            cAux += oRest:GetLastError()            
            MemoWrit('\logcartao\naocancelado\' + cID + '.txt', cAux)
        EndIf
        
    ElseIf Alltrim(cStatus) == 'rejected'
        lRet := .F.
        cRet := 'N�o autorizado'
    Else
        //Seta o ID + token de acesso
        oRest:setPath("/"+cID+"/?access_token="+cToken )    

        //Realiza o get, para verificar o status atual do pagamento
        If oRest:get(aHeader)
            oResult:FromJson(oRest:GetResult())                
            cRet    := oResult['status']                     
            lRet    := .F.            
        Endif
    EndIf

Return {lRet, cRet}

User Function canccart
    Local oJson
    Local aHeader := {}

    aAdd(aHeader, "Content-Type : application/json")
    
    If Empty(FunName())
        PREPARE ENVIRONMENT EMPRESA '01' FILIAL '0101'
    EndIf
    /*    
    oJson := JsonObject():New()
    oJson['token']      := 'a5dd3c9cb4d624dec4db8fb0e1422d97'
    oJson['bandeira']   := 'visa'
    oJson['email']      := 'usuario@teste.com'
    */
    //testecartao(oJson:toJson())
    
    cToken  := Alltrim(SuperGetMV("MS_TOKENMP"  , .F., "TEST-7863235948118309-042310-d45196dae2d5f2ce1c56417fbb9677a2-319192752"))

    //URI Base para api do mercado pago
    cURLMP  := Alltrim(SuperGetMV("MS_URLMP"    , .F., "https://api.mercadopago.com/v1/payments"))    

    oRest   := FwRest():new(cURLMP)
    cID     := '4889922109'
        
    cPath   := "/"+cID+"/refunds?access_token="+cToken
        
    oRest:setPath(cPath)
        
    If oRest:Post(aHeader)
        cRet := oRest:GetResult()
    Else
        cRet := oRest:GetLastError()              
    EndIf

    MsgAlert(cRet)

Return


