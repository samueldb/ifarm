#INCLUDE "TOTVS.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "TBICONN.CH"
//-------------------------------------------------------------------
/*/{Protheus.doc} customers
Webservice para inclusao de cadastros de clientes no IFARM
SA1 - Clientes

@author  Samuel Dantas
@since   19/12/2018
/*/
//-------------------------------------------------------------------
WSRESTFUL customers DESCRIPTION "Rest para Inclusao de clientes Protheus"
    WSMETHOD GET    DESCRIPTION "Consulta cliente"   WSSYNTAX "/customers/getById/{}"
    WSMETHOD POST   DESCRIPTION "Inclusao/Alteracao" WSSYNTAX "/customers/create/{}"
    WSMETHOD PUT    DESCRIPTION "Atualizacao" 	     WSSYNTAX "/customers/update/{}"
    WSMETHOD DELETE DESCRIPTION "Exclusao" 	         WSSYNTAX "/customers/getById/{}"
END WSRESTFUL 

WSMETHOD POST WSSERVICE customers
    
    Local cJson     := DecodeUtf8(self:getContent())
    Local lRet      := .T.
    Local cCodCli   := ""
    Local aRet      := {}
    Local oResponse := JsonObject():new()

    //Registra log de entrada
    U_IFAF0004("IFAWS0001", "customers", "ENTRADA", cJson )
    U_IFAF006A(cJson,"I","Inclusao usuarios ","IFAWS001",.F.)
    // define o tipo de retorno do metodo
	::SetContentType("application/json")   	

    If Len(::aURLParms) == 0
		SetRestFault(400, "Informe o codigo do cliente")
		return .F.
	Else   
        Do Case
            Case Len(::aURLParms) == 1
                If ::aURLParms[1] == "create"
                    //Chama funcao que realizara a inclusao 
                    aRet := customers(cJson)
                    U_IFAF0004("IFAWS0001", "customers", "SAIDA", cJson, aRet[2] )
                    lRet := aRet[1]            
                EndIF
            Case Len(::aURLParms) >= 2
                If ::aURLParms[1] == "create"
                    //Chama funcao que realizara a inclusao 
                    aRet := customers(cJson)
                    U_IFAF0004("IFAWS0001", "customers", "SAIDA", cJson, aRet[2] )
                    lRet := aRet[1]      
                ElseIf ::aURLParms[2] == "create_address" 
                    cCodCli :=  ::aURLParms[1]
                    aRet    :=  SaveAddress(cCodCli,cJson)
                    U_IFAF0004("IFAWS0001", "SaveAddress", "SAIDA", cJson, aRet[2] )
                    lRet    := aRet[1]         
                ElseIf ::aURLParms[2] == "remove_address" 
                    cCodCli     := ::aURLParms[1]
                    cCodEnd     := ::aURLParms[3]
                    aRet        := SaveAddress(cCodCli,cJson, cCodEnd, .T.)
                    U_IFAF0004("IFAWS0001", "SaveAddress", "SAIDA", cJson, aRet[2] )
                    lRet        := aRet[1]           
                EndIF
        End Case
	EndIf
    
    //Verifica o retorno da rotina e define o retorno para o ws
    If ! lRet
        oResponse["type"]       := "error"
        oResponse["status"]     := 400
        oResponse["message"]    := aRet[2]
        Conout(aRet[2])
        self:SetResponse(EncodeUTF8(oResponse:toJson()))
        lRet:= .T.
        U_IFAF006A(aRet[2],"E",,,.T.)
    Else
        U_IFAF006A(aRet[2],"S",,,.T.)
        //Inicia o objeto principal
        self:SetResponse(EncodeUTF8(aRet[2]))
    EndIf

Return lRet

WSMETHOD GET WSRECEIVE cCliente WSSERVICE customers     

	Local aResponse := {}		
    Local lRet       := .F.
    Local cCliente    := ''
    Local oResponse := JsonObject():new()   
	
    // define o tipo de retorno do metodo
	::SetContentType("application/json")   	
	
    If Len(::aURLParms) < 2
		SetRestFault(400, "Informe o codigo da associacao")
		return .F.
	Else
		cCliente := ::aURLParms[2]
	EndIf

	aResponse := getById(cCliente)
    lRet := aResponse[1]
    
    U_IFAF0004("IFAWS0001", "customers", "GET", aResponse[2], " " )
    
    If ! lRet    
		oResponse['type']		:= "error"
		oResponse['status']		:= "404"
		oResponse['message']	:= aResponse[2]
        self:SetResponse(EncodeUTF8(oResponse:toJson()))
        return .T.
        //SetRestFault(404EncodeUTF8(, oResponse:toJso)n())
    Else
        self:SetResponse(EncodeUTF8(aResponse[2]))
    EndIf

Return lRet 

WSMETHOD PUT WSRECEIVE cCliente WSSERVICE customers    
    
    Local cJson     := DecodeUtf8(self:getContent())
    Local lRet      := .T.
    Local aRet      := {}
    Local cCliente    := ''
    Local oResponse := JsonObject():new()
	// define o tipo de retorno do metodo
	::SetContentType("application/json")   	
     U_IFAF0004("IFAWS0001", "customers", "ENTRADA", cJson )
	    
    If Len(::aURLParms) < 2
		SetRestFault(400, "Informe o codigo da associacao")
		return .F.
	ElseIf ::aURLParms[2] == "update_address" 
        cCodCli     := ::aURLParms[1]
        cCodEnd     := ::aURLParms[3]
        aRet   := SaveAddress(cCodCli,cJson, cCodEnd)
        U_IFAF0004("IFAWS0001", "SaveAddress", "SAIDA", cJson, aRet[2] )
        lRet        := aRet[1]            
    ElseIf ::aURLParms[2] == "remove_address" 
        cCodCli     := ::aURLParms[1]
        cCodEnd     := ::aURLParms[3]
        aRet   := SaveAddress(cCodCli,cJson, cCodEnd, .T.)
        U_IFAF0004("IFAWS0001", "SaveAddress", "SAIDA", cJson, aRet[2] )
        lRet        := aRet[1]            
    Else
		cCliente := ::aURLParms[2]
        aRet   := customers(cJson,cCliente,4)
        U_IFAF0004("IFAWS0001", "customers", "SAIDA", cJson, aRet[2] )
        lRet        := aRet[1]
	EndIf

    If ! lRet
        oResponse['type'] 	:= 'error'
        oResponse['status'] 	:= 400
        oResponse['message'] := aRet[2]
        oResponse['result']  := ""
        self:SetResponse(EncodeUTF8(oResponse:toJson()))
        lRet:= .T.
    Else
        self:SetResponse(EncodeUTF8(aRet[2]))
    EndIf

Return lRet

WSMETHOD DELETE WSRECEIVE cCliente WSSERVICE customers    
    
    Local cJson     := DecodeUtf8(self:getContent())
    Local lRet      := .T.
    Local aRet      := {}
    Local cCliente    := ''
    Local oResponse := JsonObject():new()

	// define o tipo de retorno do metodo
	::SetContentType("application/json")   	
     U_IFAF0004("IFAWS0001", "customers", "ENTRADA", cJson )
	    
    If Len(::aURLParms) < 2
		SetRestFault(400, "Informe o codigo da associacao")
		return .F.
    ElseIf ::aURLParms[2] == "remove_address" 
        cCodCli     := ::aURLParms[1]
        cCodEnd     := ::aURLParms[3]
        aRet        := SaveAddress(cCodCli,cJson, cCodEnd, .T.)
        U_IFAF0004("IFAWS0001", "SaveAddress", "SAIDA", cJson, aRet[2] )
        lRet        := aRet[1]            
	EndIf

    If ! lRet
        oResponse['type'] 	:= 'error'
        oResponse['status'] 	:= 400
        oResponse['message'] := aRet[2]
        oResponse['result']  := ""
        self:SetResponse(EncodeUTF8(oResponse:toJson()))
        lRet:= .T.
    Else
        self:SetResponse(EncodeUTF8(aRet[2]))
    EndIf

Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} Funcao responsavel por salvar o novo cliente
@author  Samuel Dantas 
@since   19/12/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function customers(cJsonAux, cCodCli, nOpc)

    Local cJsonAux
    Local oCliente
    Local oCliResult    := JsonObject():new()
    Local cArqErro	    := "erroauto.txt"
    Local aVetor        := {}
    Local xConteudo
    Local cMsg          := ""
    Local nI            := 1
    Local aNoAttr       := {}
    Local nTamEmail     := TAMSX3("A1_YEMAIL")[1]
    Local aAttrs        := {{"nome"     , "A1_NOME"    , TAMSX3("A1_NOME")[3]   },;
                            {"email"    , "A1_YEMAIL"  , TAMSX3("A1_YEMAIL")[3] },;
                            {"id"       , "A1_YFIREB"  , TAMSX3("A1_YFIREB")[3] }}
                            // {"uid"  , "A1_YIDUSER"  , TAMSX3("A1_YIDUSER")[3] },;
    Local oResponse
    Local oResult
    Local cTelefone     := ""
    Local lExist        := .F.

    Default cCodCli     := ""
    Default nOpc        := 3
    Private lMsErroAuto := .F.
    
    oResponse := JsonObject():new()
    oResult   := JsonObject():new()
    
    //Transforma o json em objeto
    FWJsonDeserialize(cJsonAux,@oCliente) 

    If ValType(oCliente) == 'U'
        Return {.F., 'Json com formato invalido. Verifique o JSON enviado.'}
    EndIf
    Conout(cJsonAux)
    //Verifica se os itens enviados via get estao setados
    For nI := 1 To len(aAttrs)
        cAttr   := aAttrs[nI][1]
        cCampo   := aAttrs[nI][2]
        cTipo   := aAttrs[nI][3] // Tipo no protheus

        If !AttIsMemberOf(oCliente,cAttr)
            aAdd(aNoAttr,aAttrs[nI][1]) //Adiciona ao array de atributos nao encontrados
        Else
            xConteudo := &("oCliente:"+cAttr)

            //Tratamento de tipos
            If cTipo == 'D' .AND. ValType(xConteudo) == 'C'
                xConteudo := CtoD(xConteudo) 
            ElseIf cTipo == 'D' .AND. ValType(xConteudo) != 'C'
                Return {.F., 'Erro ao comparar atributo '+cAttr+'. Tipo recebido : '+ValType(xConteudo)+ ' e tipo esperado: C (AAAAMMDD).'}
            ElseIf cTipo != ValType(xConteudo)
                Return {.F., 'Erro ao comparar atributo '+cAttr+'. Tipo recebido : '+ValType(xConteudo)+ ' e tipo esperado: '+ cTipo+'.'}
            EndIf 
            
            aAdd( aVetor, { cCampo, xConteudo, NIL } )
        EndIf
    Next
    
    //Caso todos o atributos estejam no JSON
    If len(aNoAttr) == 0    
        oCliente:cpf := Alltrim(StrTran(StrTran(oCliente:cpf,".",""),"-",""))
        If !ValidaCpf(oCliente:cpf) 
            return {.F., " Numero de CPF invalido. "}
        EndIf
        cTelefone := StrTran(StrTran(StrTran(StrTran(oCliente:telefone,"(",""),")",""),"-","")," ","")
        If nOpc == 3
            
            SA1->(DbSetOrder(3))
            IF !EMPTY(oCliente:cpf) .AND. SA1->( DbSeek(xFilial("SA1") + PADR(ALLTRIM(oCliente:cpf),TAMSX3("A1_CGC")[1])) )
                Return {.F., 'CPF do cliente ja esta cadastrado no sistema.'}
            EndIf

            SA1->(DBOrderNickname("A1YEMAIL"))
            SA1->(DbGoTop())
            IF SA1->( DbSeek(xFilial("SA1") + PADR(ALLTRIM(oCliente:email),nTamEmail)) )
                Return {.F., 'Email do cliente ja esta cadastrado no sistema.'}
            EndIf

            SA1->(DBOrderNickname("A1YIDUSER"))
            ConOut(Alltrim(oCliente:id))
            lExist := AttIsMemberOf(oCliente, 'uid') .AND. SA1->(DbSeek(xFilial("SA1") + Alltrim(oCliente:uid)))
            If lExist
                Return {.F., 'Cliente ja esta cadastrado.'}
            Else
                SA1->(DBOrderNickname("A1YFIREB"))
                lExist := SA1->(DbSeek(xFilial("SA1") + Alltrim(oCliente:id)))
                If lExist
                    Return {.F., 'Cliente ja esta cadastrado.'}
                EndIf
            EndIf

            cCodCli := GetSXENUM("SA1","A1_COD")
            RecLock('SA1', .T.)
                SA1->A1_FILIAL  := xFilial("SA1")
                SA1->A1_COD     := cCodCli
                
                SA1->A1_NOME        := oCliente:nome
                SA1->A1_TIPO        := "F"
                SA1->A1_LOJA        := "0001"
                SA1->A1_YEMAIL      := oCliente:email
                SA1->A1_TEL         := cTelefone
                SA1->A1_YFIREB      := oCliente:id
                If (AttIsMemberOf(oCliente, 'uid'))
                    SA1->A1_YIDUSER     := oCliente:uid
                EndIf
                SA1->A1_YPSWORD     := oCliente:senha
                SA1->A1_CGC         := oCliente:cpf
                SA1->A1_NREDUZ      := oCliente:nome
                SA1->A1_DTCAD       := Date()

            SA1->(MsUnLock())
            //Adiciona itens obrigatorios que nao vem do JSON
        ElseIf nOpc == 4

            SA1->(DBOrderNickname("A1YIDUSER"))
            ConOut(Alltrim(oCliente:id))
            lExist := AttIsMemberOf(oCliente, 'uid') .AND. SA1->(DbSeek(xFilial("SA1") + Alltrim(oCliente:uid)))
            If !lExist
                SA1->(DBOrderNickname("A1YFIREB"))
                lExist := SA1->(DbSeek(xFilial("SA1") + Alltrim(oCliente:id)))
            EndIf

            ConOut(Alltrim(oCliente:id))
            If lExist
                If !EMPTY(SA1->A1_CGC) .AND.  ALLTRIM(SA1->A1_CGC) != ALLTRIM(oCliente:cpf)
                    Return {.F., 'CPF nao pode ser alterado.'}
                Else
                    ConOut(Alltrim("Alterando cliente"))
                    RecLock('SA1', .F.)
                        SA1->A1_NOME        := oCliente:nome
                        SA1->A1_NREDUZ      := UPPER(oCliente:nome)
                        SA1->A1_TIPO        := "F"
                        SA1->A1_LOJA        := "0001"
                        SA1->A1_YEMAIL      := oCliente:email
                        SA1->A1_CGC         := oCliente:cpf
                        SA1->A1_TEL         := cTelefone
                        SA1->A1_YFIREB      := oCliente:id
                        If (AttIsMemberOf(oCliente, 'uid'))
                            SA1->A1_YIDUSER     := oCliente:uid
                        EndIf
                        SA1->A1_YPSWORD     := oCliente:senha
                    SA1->(MsUnLock())
                EndIf
                
            EndIf
        EndIf
    
        ConfirmSX8()
        
    Else // Trata mensagem caso atributo nao tenha sido encontrado
        If len(aNoAttr) == 1 
            cMsg := "O campo " + aNoAttr[1] + " nao foi encontrado no json."
        Else
            cMsg += "Os campos: "
            For nI := 1 To len(aNoAttr)
                If nI == 1
                    cMsg += aNoAttr[nI]
                ElseIf nI == len(aNoAttr)
                    cMsg += " e "+ aNoAttr[nI]
                Else
                    cMsg += ", "+ aNoAttr[nI]
                EndIf
            Next
            cMsg += " nao foram encontrados no Json."
        EndIf
        
        Return {.F., cMsg}
    EndIf
    
    oCliResult['nome']      := oCliente:nome
    oCliResult['email']     := oCliente:email
    oCliResult['telefone']  := oCliente:telefone
    oCliResult['id']        := oCliente:id
    oCliResult['uid']   := Iif(AttIsMemberOf(oCliente, 'uid'), oCliente:uid, "" )  
    oCliResult['senha']     := oCliente:senha
    oCliResult['cpf']       := oCliente:cpf
    oCliResult['nome']      := oCliente:nome

    //Resposta em formato json
    oResponse["type"]       := "success"
    oResponse["status"]     := 200
    oResponse["message"]    := 'Cliente '+IIf(nOpc == 3,"Salvo","Alterado")+' com sucesso.'
    oResult['id']           := cCodCli
    oResponse["result"]     := oCliResult

Return {.T., oResponse:toJson()}

Static function ValidaCpf (cCPF)
    Local lRet := .F.
    ConOut(cCPF)
    
    lRet := EMPTY(cCPF) .OR. (CGC(cCPF))
        
Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} SaveAddress
Metodo responsavel por salvar enderecos do cliente 
@author  Samuel Dantas 
@since   08/01/2019
/*/
//-------------------------------------------------------------------
Static Function SaveAddress(cCodCli,cJson, cCodEnd, lDeleta) 
    Local oAddress
    Local nI
    Local aAttrs    := { "cep","rua","estado","numero","bairro","cidade" }
    Local aNoAttr   := {} //Campos que nao estao no JSON
    Local lInclui   := .T.
    Local lExist    := .F.
    Default cCodEnd :=  ''
    Default lDeleta :=  .F.
    

    oResponse := JsonObject():new()
    oResult   := JsonObject():new()

    FWJsonDeserialize(cJson,@oAddress) 
    
    If lDeleta
        
        cResposta := ''
        
        SA1->(DBOrderNickname("A1YIDUSER"))
        lExist :=  SA1->(DbSeek(xFilial("SA1") + Alltrim(cCodCli)))
        If !lExist
            SA1->(DBOrderNickname("A1YFIREB"))
            lExist := SA1->(DbSeek(xFilial("SA1") + Alltrim(cCodCli)))
        EndIf

        If lExist
            If !Empty(cCodEnd)
                Z09->(DbSetOrder(1)) 
                If Z09->(DbSeek(xFilial('Z09') + SA1->A1_COD + SA1->A1_LOJA + cCodEnd ))
                    RecLock('Z09', .F.)
                        Z09->(DbDelete())
                    Z09->(MsUnLock())
                Else
                    cResposta := 'Endereco nao foi localizado'
                EndIf
            Else    
                cResposta := 'Endereco nao foi enviado'
            EndIf          
        Else
            cResposta := 'Cliente nao localizado'
        EndIf

        If Empty(cResposta)
            //Resposta em formato json
            oResponse["type"]       := "success"
            oResponse["status"]     := 200
            oResponse["message"]    := 'Endereco excluido com sucesso.'        
            Return {.T., oResponse:toJson()}
        Else
            Return {.F., cResposta}
        EndIf
    EndIf
    
    //verifica se o objeto � valido
    If ValType(oAddress) == 'U'
        Return {.F., 'Json com formato invalido. Verifique o JSON enviado.'}
    EndIf

    //Verifica campos do JSON
    For nI := 1 To len(aAttrs)
        If !AttIsMemberOf(oAddress,aAttrs[nI])
            aAdd(aNoAttr,aAttrs[nI]) //Adiciona ao array de atributos nao encontrados
        EndIf
    Next
    
    If len(aNoAttr) == 0
        SA1->(DBOrderNickname("A1YIDUSER"))
        lExist := SA1->(DbSeek(xFilial("SA1") + Alltrim(cCodCli)))
        If !lExist
            SA1->(DBOrderNickname("A1YFIREB"))
            lExist := SA1->(DbSeek(xFilial("SA1") + Alltrim(cCodCli)))
        EndIf

        If lExist
            Conout(cCodCli)
            If !Empty(cCodEnd)
                Z09->(DbSetOrder(1)) 
                lInclui := ! Z09->(DbSeek(xFilial('Z09') + SA1->A1_COD + SA1->A1_LOJA + cCodEnd ))
                Conout(SA1->A1_COD + SA1->A1_LOJA + cCodEnd)
            EndIf          
            RecLock('Z09', lInclui)
                Z09->Z09_FILIAL := xFilial("Z09")
                Z09->Z09_CEP    := oAddress:cep
                Z09->Z09_RUA    := oAddress:rua
                Z09->Z09_EST    := oAddress:estado
                Z09->Z09_NUMERO := oAddress:numero 
                Z09->Z09_BAIRRO := oAddress:bairro
                Z09->Z09_MUN    := oAddress:cidade
                Z09->Z09_REF    := oAddress:referencia
                Z09->Z09_COMPLE := oAddress:complemento
                Z09->Z09_PSRECE := oAddress:pessoa_recebe
                If lInclui
                    Z09->Z09_CODIGO := u_uGetSXENUM("Z09","Z09_CODIGO")
                EndIf
                Z09->Z09_CODSA1 := SA1->A1_COD
                Z09->Z09_LOJA   := SA1->A1_LOJA
            Z09->(MsUnLock())
        Else
            Return {.F., 'Cliente de codigo '+cCodCli+' nao esta cadastrado.'}
        EndIf
    Else
        If len(aNoAttr) == 1 
            cMsg := "O campo " + aNoAttr[1] + " nao foi encontrado no json."
        Else
            cMsg += "Os campos: "
            For nI := 1 To len(aNoAttr)
                If nI == 1
                    cMsg += aNoAttr[nI]
                ElseIf nI == len(aNoAttr)
                    cMsg += " e "+ aNoAttr[nI]
                Else
                    cMsg += ", "+ aNoAttr[nI]
                EndIf
            Next
            cMsg += " nao foram encontrados no Json."
        EndIf
        Return {.F., cMsg}
    EndIf

    //Resposta em formato json
    oResponse["type"]       := "success"
    oResponse["status"]     := 200
    oResponse["message"]    := 'Endereco ' + IIf(lInclui, 'cadastrado', 'alterado') + ' com sucesso.'

    If lInclui
        oResult['id']           := Z09->Z09_CODIGO
        oResponse["result"]     := oResult
    EndIf
    
Return {.T., oResponse:toJson()}

//-------------------------------------------------------------------
/*/{Protheus.doc} GetbyId
Metodo responsavel por retornar campos do cliente a partir do codigo
@author  Samuel Dantas
@since   08/01/2019
@param   cCodCli, caractere, codigo do cliente
/*/
//-------------------------------------------------------------------
Static function GetbyId(cCodCli)
    
    Local oResult
    Local oCliente
    Local oEndereco
    Local oDevices
    Local aRet := {}
    Local lExist := .F.

    oResult := JsonObject():new()
    
    oResult['type']     := "success"
    oResult['status']   := 200
    oResult['message']  := "Cliente encontrado"

    SA1->(DBOrderNickname("A1YIDUSER"))
    lExist := SA1->(DbSeek(xFilial("SA1") + PADR(cCodCli,TAMSX3("A1_YIDUSER")[1])))
    If !lExist
        SA1->(DBOrderNickname("A1YFIREB"))
        lExist := SA1->(DbSeek(xFilial("SA1") + Alltrim(cCodCli)))
    EndIf

    If lExist
        
        cQuery := " SELECT A2_FILIAL, A2_COD FROM "  + RetSqlName('SA2') + " SA2"
        cQuery += " WHERE SA2.D_E_L_E_T_ <> '*' AND A2_CLIENTE = '"+SA1->A1_COD+"' " 
        cQuery += " AND A2_LOJCLI = '"+SA1->A1_LOJA+"' " 
        
        If Select('QRY') > 0
            QRY->(dbclosearea())
        EndIf
        
        TcQuery cQuery New Alias 'QRY'
        
        oCliente := JsonObject():new()
        oCliente['uid']                     := ALLTRIM(SA1->A1_YFIREB)
        oCliente['uid']                     := ALLTRIM(SA1->A1_YIDUSER)
        oCliente['updated']                 := FWTimeStamp(3,Date(),TIME())
        oCliente['cpf']                     := ALLTRIM(SA1->A1_CGC)
        oCliente['created']                 := FWTimeStamp(3,Date(),TIME())
        oCliente['receber_notificacoes']    := NIL
        oCliente['email']                   := ALLTRIM(SA1->A1_YEMAIL)
        oCliente['comprador']               := ""//Criar campo
        oCliente['id_vendedor']             := Alltrim(SA1->A1_YCODSA3) //Criar campo
        oCliente['telefone']                := ALLTRIM(SA1->A1_TEL)
        oCliente['id_plano']                := ""
        oCliente['senha']                   := ALLTRIM(SA1->A1_YPSWORD)
        oCliente['nome']                    := ALLTRIM(SA1->A1_NOME)
        oCliente['id']                      := ALLTRIM(SA1->A1_COD)
        oCliente['enderecos']               := {}
        oCliente['devices_tokens']          := {""}
        oCliente['fornecedor']              := IIF( QRY->(!Eof()), .T., .F.)
        oCliente['associacao']              := IIF( QRY->(!Eof()), QRY->A2_FILIAL, "")

        Z09->(DbSetOrder(1))
        If Z09->(DbSeek( cSeek := xFilial("Z09") + SA1->(A1_COD+A1_LOJA)))
            While Z09->(!EOF()) .AND. cSeek == xFilial("Z09") + Z09->(Z09_CODSA1+Z09_LOJA)
                oEndereco := JsonObject():new()
                oEndereco['bairro']         := ALLTRIM(Z09->Z09_BAIRRO)
                oEndereco['complemento']    := ALLTRIM(Z09->Z09_COMPLE)
                oEndereco['rua']            := ALLTRIM(Z09->Z09_RUA)
                oEndereco['endereco']       := ""
                oEndereco['alterar']        := .T.
                oEndereco['cep']            := ALLTRIM(Z09->Z09_CEP)
                oEndereco['estado']         := ALLTRIM(Z09->Z09_EST)
                oEndereco['referencia']     := ALLTRIM(Z09->Z09_REF)
                oEndereco['numero']         := ALLTRIM(Z09->Z09_NUMERO)
                oEndereco['cidade']         := ALLTRIM(Z09->Z09_MUN)
                oEndereco['id']             := ALLTRIM(Z09->Z09_CODIGO)
                oEndereco['pessoa_recebe']  := ALLTRIM(Z09->Z09_PSRECE)
                aAdd(oCliente['enderecos'] ,oEndereco)                
                Z09->(DbSkip())
            EndDo
        EndIf
        oResult['result'] := oCliente
        aRet := {.T., oResult:toJson() }
    Else
        return {.F., "Nao foi encontrado cliente com o codigo "+cCodCli+". "}
    EndIF

Return aRet

//funcoes de testes
User Function IFAWS01A

    PREPARE ENVIRONMENT EMPRESA '01' FILIAL '01'
   
    aRet := GetbyId("Q1mpI7VkUzl6PrAJxlbH")
    MsgAlert(aRet[2])

Return


//-------------------------------------------------------------------
/*/{Protheus.doc} SalvarFire
Salvar conteudo da tabela do protheus no firebase
@author  Samuel Dantas
@since   date
@version version
/*/
//-------------------------------------------------------------------
Static Function SalvarMailChimp(nRecnoSA1)
        
        Local cEndereco         := ""
        Local aHeader           := {}
        Local cChvMChimp        := Alltrim(SuperGetMv("MS_MCHIMP", .F., "cabaec6a7b577c11fea8a495fdd5dd13-us3"))
        Local cIdList           := Alltrim(SuperGetMv("MS_CHMPLIST", .F., "66d5e35937"))
        Local cEndereco         := Alltrim(SuperGetMv('MS_URLCHIMP', .F.,"https://us3.api.mailchimp.com/3.0"))
        Local aHeader           := {}
        Local cPath             := "/lists/"+cIdList+"/members"
        Local aName             := ""
        Local cJson             := ""
        Local oResult           := JsonObject():New()
        Local oMergeFields      := JsonObject():New()
        Local oResponse         := JsonObject():New()
        Local aAreaSA1 := SA1->(GetArea())
        
        aAdd(aHeader, 'Content-Type: application/json')
        aAdd(aHeader, 'Authorization: Basic '+Encode64('user:'+cChvMChimp)+'')
        cPath     := "/lists/"+cIdList+"/members/"

        SA1->(DbGoTop())
        While SA1->(!EoF())
            oResult           := JsonObject():New()
            oMergeFields      := JsonObject():New()
            oResponse         := JsonObject():New()

            If !EMPTY(SA1->A1_YEMAIL) .AND. VerificaEmail(AllTrim(SA1->A1_YEMAIL)) .AND. !SA1->A1_YCHIMP
                aName       := Strtokarr2( SA1->A1_NOME, " ")
                
                oResult['email_address']    := Alltrim(SA1->A1_YEMAIL)
                oResult['status']           := "subscribed"
                oMergeFields['FNAME']       := Iif( len(aName) >= 1, aName[1], "" )
                oMergeFields['LNAME']       := Iif( len(aName) > 1, aName[2], oMergeFields['FNAME'] )
                oMergeFields['PHONE']       := AllTrim(SA1->A1_TEL)
                oResult['merge_fields']     := oMergeFields
                
                cJson := oResult:toJson()
                cJson := EncodeUtf8(cJson)

                oRest   := fwRest():New(cEndereco)
                oRest:setPath(cPath)
                
                oRest:SetPostParams(cJson)
                If !oRest:Post(aHeader) 
                    // MsgAlert(oRest:getResult())
                Else
                    RecLock('SA1', .F.)
                        SA1->A1_YCHIMP := .T.
                    SA1->(MsUnLock())
                EndIf
                
                
            EndIf
            SA1->(DbSkip())
        EndDo

Return

//-------------------------------------------------------------------
/*/{Protheus.doc} SalvarFire
Salvar conteudo da tabela do protheus no firebase
@author  Samuel Dantas
@since   date
@version version
/*/
//-------------------------------------------------------------------
Static Function VerificaEmail(cEmail)
        
        Local cEndereco         := ""
        Local aHeader           := {}
        Local cChvMChimp        := Alltrim(SuperGetMv("MS_MCHIMP", .F., "a1007f46e7c962643d953c058087a57e-us7"))
        Local cIdList           := Alltrim(SuperGetMv("MS_CHMPLIST", .F., "ce19cea9e9"))
        Local cEndereco         := Alltrim(SuperGetMv('MS_URLCHIMP', .F.,"https://us7.api.mailchimp.com/3.0"))
        Local oRestClient       
        Local oResponse         := JsonObject():New()
        Local cPath         := "/search-members?query="

        Default cEmail          := ""
        cPath += EncodeUtf8(cEmail)

        oRestClient := FWRest():New(cEndereco)
        aAdd(aHeader, 'Content-Type: application/json')
        aAdd(aHeader, 'Authorization: Basic '+Encode64('user:'+cChvMChimp)+'')
        
        oRestClient:setPath(cPath)
        If oRestClient:Get(aHeader)
            oResponse:fromJson(oRestClient:GetResult())
            For nI := 1 To len(oResponse['exact_matches']['members'])
                cEmailAux := oResponse['exact_matches']['members'][nI]['email_address']
                If DecodeUtf8(cEmailAux) == cEmail
                    If !SA1->A1_YCHIMP
                        RecLock('SA1', .F.)
                            SA1->A1_YCHIMP := .T.
                        SA1->(MsUnLock())
                    EndIf
                    Return .F.
                EndIf
            Next
            ConOut(oRestClient:GetResult())
        Else
            Conout(oRestClient:GetLastError())
        Endif
        
Return .T.

User Function IFAWS1 ()

    If Empty(FunName())
        PREPARE ENVIRONMENT EMPRESA '01' FILIAL '01'
    EndIf
    // VerificaEmail()
    GetbyId("VPtc3Mj7LWhGblTGVw9ulPt2qWz1")

Return


User Function ImportJson(cJson)
    Local cFile     := ""
    Local oClientes := JsonObject():new()
    Private cText     := ""

    If Empty(FunName())
        PREPARE ENVIRONMENT EMPRESA '01' FILIAL '01'
    EndIf
    
    cFile := cGetFile('Arquivo JSON', 'Selecione a arquivo JSON',,, .F., 16)
    If !Empty(cFile)
        SA1->(DBOrderNickname("A1YEMAIL"))
        // cText := MemoRead(cFile)
        nHandle :=  FOpen( cFile )
        nSize :=  FRead( nHandle, cText, 9990000 )
        FClose(nHandle)
        oClientes   := oClientes:fromJson(cText)
        aClientes    := oClientes['users']

        For nI := 1 To len(aClientes)
            //YEMAIL
            IF SA1->(DbSeek(xFilial("SA1") + PADR(ALLTRIM(aClientes[nI]['email']),TAMSX3("A1_YEMAIL")[1])))
                RecLock('SA1', .F.)
                    // SA1->A1_YFIREB := aClientes[nI]['localId']
                    SA1->A1_YIDUSER := aClientes[nI]['localId']
                SA1->(MsUnLock())
            Endif
        Next
        
    EndIf

Return


User Function IFAWS001B(cJson)
    Local cFile     := ""
    Local oClientes := JsonObject():new()
    Private cText     := ""

    If Empty(FunName())
        PREPARE ENVIRONMENT EMPRESA '01' FILIAL '01'
    EndIf
   
    cFile       := cGetFile('Arquivo compactado(.gz)'   , 'Selecione a arquivo compactado(.gz)',,, .F., GETF_LOCALHARD+GETF_MULTISELECT )
    cFileOut    := cGetFile('Destino final'             , 'Selecione o destino final'          ,,, .F., GETF_LOCALHARD+GETF_RETDIRECTORY )
    
    If !ExistDir( "\temp" )
        MakeDir("\temp")
    EndiF
    If !ExistDir( "\temp\nfeproc" )
        MakeDir("\temp\nfeproc")
    EndiF
    If !ExistDir( "\temp\nfeproc\xml" )
        MakeDir("\temp\nfeproc\xml")
    EndiF

    CpyT2S( cFile, "\temp\nfeproc" )
    aFile := Strtokarr2(cFile,"\")
    aArquivos := Directory("\temp\nfeproc\xml\*") 
    
    //Exclui arquivos do servidor.
    For nI := 1 To len(aArquivos)
        FErase( "\temp\nfeproc\xml\"+aArquivos[nI][1] )
    Next
    
    FErase( "\temp\nfeproc\xml\" )

    //Descompacta arquivos para xml
    If GzDecomp( "\temp\nfeproc\"+aFile[len(aFile)], "\temp\nfeproc\xml\" )
        
        Conout("Arquivo descompactado.")
        aArquivos := Directory("\temp\nfeproc\xml\*") 
        For nI := 1 To len(aArquivos)
            //Renomeia com extens�o .xml
            FRename( "\temp\nfeproc\xml\"+aArquivos[nI][1], "\temp\nfeproc\xml\"+aArquivos[nI][1]+".xml" )
        Next
        
        //Copia os arquivos para o cliente
        CpyS2T( "\temp\nfeproc\xml\*.xml", cFileOut )
    Else
        x := 1 
    EndIf

Return