#INCLUDE "TOTVS.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "tbiconn.ch"

//-------------------------------------------------------------------
/*/{Protheus.doc} marketplace
Ws Rest para retornos de lista de estabelecimentos ativos
@author  Sidney Sales
@since   03/01/18
@version 1.0
/*/
//-------------------------------------------------------------------
WSRESTFUL marketplace DESCRIPTION "Retorna a lista de Estabelecimentos Ativos"

WSMETHOD GET DESCRIPTION "Retorno de lista de Estabelecimentos Ativos" WSSYNTAX "/gerenciamento/{getAtivos}"

END WSRESTFUL

WSMETHOD GET WSRECEIVE acao WSSERVICE marketplace          
	Local cRet := ''		
	Local cAcao

	// define o tipo de retorno do metodo
	::SetContentType("application/json")   	
	
	//Valida os parametros enviados
	If Len(::aURLParms) == 0
		SetRestFault(400, "Informe a acao que deseja utilizar")
		return .F.
	Else
		cAcao := ::aURLParms[2]
	EndIf

	//Ate o momento so foi criado o get para os ativos	
	If Upper(cAcao) == 'GETATIVOS'
		oJson := getEstabelecimentos(.T.)
		cRet := oJson:ToJson()
		U_IFAF0004("IFAWS0002", "marketplace", "GET", cRet, " " )
		::SetResponse(EncodeUTF8(cRet))
	ElseIf UPPER(cAcao) == 'GET'
		oJson := getEstabelecimentos(.F.)
		cRet := oJson:ToJson()
		U_IFAF0004("IFAWS0002", "marketplace", "GET", cRet, " " )
		::SetResponse(EncodeUTF8(cRet))
	ElseIf UPPER(cAcao) == 'GETBYID'
		oJson := getById(::aURLParms[3])
		cRet  := oJson:ToJson()
		U_IFAF0004("IFAWS0002", "marketplace", "GET", cRet, " " )
		::SetResponse(EncodeUTF8(cRet))
	ElseIf UPPER(cAcao) <> 'GETATIVOS'
		U_IFAF0004("IFAWS0002", "marketplace", "GET", "Acao nao preparada para uso", " " )
		SetRestFault(401, "Acao nao preparada para uso")
		return .F.
	ElseIf Empty(cAcao) 
		U_IFAF0004("IFAWS0002", "marketplace", "GET", "Informe a acao que deseja utilizar", " " )
		SetRestFault(400, "Informe a acao que deseja utilizar")
		return .F.
	endif

Return .T.                     

//-------------------------------------------------------------------
/*/{Protheus.doc} getEstabelecimentos
Funcao que retorna a lista de estabelecimentos associacoes que sera
cada SM0(FILIAL)
@author  Sidney Sales
@since   03/01/18
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function getEstabelecimentos(lAtivos)

	Local aAreaSM0 	:= SM0->(GetArea())
	Local aJson		:= {}
	Local aAux		:= {}
	Local oJsonFim	:= JsonObject():new()

	//Inicia o objeto principal
	oJsonFim['type'] 	:= 'success'
	oJsonFim['status'] 	:= 200
	oJsonFim['message']	:= 'Doc(s) encontrados'
	oJsonFim['result']	:= {}
	
	//Percorre SM0 gerando um objeto para cada filial/associacao
	While SM0->(!Eof())

		cFilAnt := SM0->M0_CODFIL
		Z22->(DbSetOrder(1))
		//Pesquisa a associal na filial corrente
		If Z01->(DbSeek(xFilial('Z01')))	

			//Se for ativo e nao for solicitado
			If lAtivos .AND. Z01->Z01_STATUS <> 'A'
				SM0->(DbSkip())
				loop
			EndIf
			
			//Cria o objeto da associacao
			oJson 		:= JsonObject():new()

			oJson['nome_fantasia'] 			:= Alltrim(Z01->Z01_DESC)
			oJson['telefone']				:= Alltrim(Z01->Z01_TEL)
			oJson['raio_entrega']			:= Alltrim(Z01->Z01_RAIO)
			oJson['id_associado']			:= cFilAnt
			oJson['valor_frete']			:= Z01->Z01_FRETE
			oJson['horarioEntrega']			:= {}
			Z12->(DbGoTop())
			Z12->(DbSeek(xFilial("Z12")))
			While Z12->(!Eof())
				If Z12->Z12_ATIVO
					aAdd(oJson['horarioEntrega'],ALLTRIM(Z12->Z12_HORARI)) 
				EndIf
				Z12->(DbSkip())
			EndDo
			
			oJson['categorias']				:= getTabela('Z23','Z23_DESC')	
			oJson['descricao_atividade']	:=  Alltrim(Z01->Z01_DSCATV)	
			oJson['raio_entrega']			:=	Z01->Z01_RAIO
			
			//Objeto da geolocalizacao
			oJsonGeo :=  JsonObject():new()
			oJsonGeo['latitude'] 	:= Z01->Z01_LAT
			oJsonGeo['longitude'] 	:= Z01->Z01_LNG

			oJson['geocoding']				:= oJsonGeo

			oJson['pedido_minimo']			:= Z01->Z01_PEDMIN
			oJson['status'] 				:= u_X3_CBOX('Z01_STATUS',Z01->Z01_STATUS)
			oJson['padrao_entrega'] 		:= Z01->Z01_CODZ20
			oJson['formaspagamento'] 		:= getFormas()
			oJson['formas_pagamento'] 		:= getFormas2()

			oJson['updated']				:= FWTimeStamp(3,DATE(),TIME())
			oJson['horarios']				:= getHorarios()
			oJson['informacao']				:= Alltrim(Z01->Z01_INFO)
			oJson['id']						:= cFilAnt
			oJson['foto_ecommerce']			:= { getFoto() }
			If Z14->(DBSeek(xFilial('Z14')))
				oJson['imagem_ecommerce']		:= Alltrim(Z14->Z14_URLWEB)
			Else
				oJson['imagem_ecommerce']		:= ""
			EndIf
			oJson['diasEntrega']			:= {}
			oJson['tokenapimp']				:= SuperGetMv('MS_TAPIMP', .F., 'TEST-e6745f1c-9fa3-4db4-8edd-3b095c0f8363')

			Z22->(DbGoTop())
			Z22->(DbSeek(xFilial("Z22")))
			While Z22->(!Eof())
				If Z22->Z22_ATIVO
					aAdd(oJson['diasEntrega'],ALLTRIM(Z22->Z22_DESC)) 
				EndIf
				Z22->(DbSkip())
			EndDo

			//Adiciona ao array o objeto json atual
			aAdd(oJsonFim['result'], oJson)

			FreeObj(oJson)
			
		EndIf
		
		SM0->(DbSkip())

	EndDo

	RestArea(aAreaSM0)

Return oJsonFim


Static Function getFoto

	Local oJson	:= JsonObject():new()

	If Z14->(DBSeek(xFilial('Z14')))

		oJson['url'] 	:= ''
		oJson['name'] 	:= ''
		oJson['storage']:= ''
		oJson['type']	:= ''
		oJson['originalName'] := ''
		oJson['data']	:= ''
		oJson['size']	:= Z14->Z14_TAM
	EndIf	

Return oJson

//-------------------------------------------------------------------
/*/{Protheus.doc} getHorarios
Funcao que retorna o objeto com os horarios cadastrados
@author  Sidney Sales
@since   03/01/18
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function getHorarios
	Local oJson	:= JsonObject():new()
	Local oJsonDia,oJsonInt
	
	Z13->(DbSeek(xFilial('Z13')))
	
	//Cria o objeto 'semanal'		
	oJson['semanal'] := {}

	While Z13->(!Eof()) .AND. Z13->Z13_FILIAL == cFilAnt

		//Cria o objeto que tera os intervalos e o dia da semana
		oJsonDia := JsonObject():new()
		oJsonDia['dia_semana'] 	:= u_X3_CBOX('Z13_DIASEM',Z13->Z13_DIASEM)
		
		//Objeto com o intervalo
		oJsonInt := JsonObject():new()
		oJsonInt['fim'] 		:= Z13->Z13_FIM
		oJsonInt['inicio'] 		:= Z13->Z13_INICIO
		
		oJsonDia['intervalos'] 	:= {oJsonInt}

		aAdd(oJson['semanal'], oJsonDia)

		FreeObj(oJsonDia)
		FreeObj(oJsonInt)

		Z13->(DbSkip())
	EndDo

Return oJson

//-------------------------------------------------------------------
/*/{Protheus.doc} getTabela	
Funcao auxiliar para retornar um array de acordo com os paraemtros
enviados
@author  Sidney Sales	
@since   03/01/18
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function getTabela(cTab, cCampo)
	Local aRet := {}
	
	(cTab)->(DbSeek(xFilial(cTab)))

	While (cTab)->(!Eof()) .AND. (cTab)->&(cTab+'_FILIAL') == cFilAnt
		aAdd(aRet, Alltrim((cTab)->&(cCampo)))
		(cTab)->(DbSkip())
	EndDo

Return aRet

//-------------------------------------------------------------------
/*/{Protheus.doc} getFormas
Funcao auxiliar para criar um objeto com as formas de pagamento existentes
@author  Sidney Sales
@since   03/01/18
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function getFormas
	Local oJson	:= JsonObject():new()

	Z15->(DbSeek(xFilial('Z15')))
	While Z15->(!Eof()) .AND. Z15->Z15_FILIAL == cFilAnt
		oJson[Alltrim(Z15->Z15_DESC)] := Z15->Z15_ATIVO
		Z15->(DbSkip())
	EndDo

Return oJson

Static Function getFormas2
	Local oJson
	Local aRet	:= {}

	Z15->(DbSeek(xFilial('Z15')))

	While Z15->(!Eof()) .AND. Z15->Z15_FILIAL == cFilAnt
		oJson	:= JsonObject():new()
		oJson['codigo'] 	:= Z15->Z15_CODIGO
		oJson['descricao'] 	:= Alltrim(Z15->Z15_DESC)
		oJson['nome'] 		:= Alltrim(Z15->Z15_NOME)
		oJson['ativo'] 		:= Z15->Z15_ATIVO
		aAdd(aRet, oJson )
		Z15->(DbSkip())

	EndDo

Return aRet


Static Function getById(_cFil)
	
	Local aJson		:= {}
	Local aAux		:= {}
	Local oJsonFim	:= JsonObject():new()
	Local cFilAux	:= cFilAnt
	//Inicia o objeto principal
	oJsonFim['type'] 	:= 'success'
	oJsonFim['status'] 	:= 200
	oJsonFim['message']	:= 'Marketplace encontrado'
	oJsonFim['result']	:= {}

	cFilAnt := _cFil
	
	//Pesquisa a associal na filial corrente
	If Z01->(DbSeek(xFilial('Z01')))	
		
		//Cria o objeto da associacao
		oJson 		:= JsonObject():new()

		oJson['nome_fantasia'] 			:= Alltrim(Z01->Z01_DESC)
		oJson['telefone']				:= Alltrim(Z01->Z01_TEL)
		oJson['raio_entrega']			:= Alltrim(Z01->Z01_RAIO)
		oJson['id_associado']			:= cFilAnt
		oJson['valor_frete']			:= Z01->Z01_FRETE
		oJson['horarioEntrega']			:= {}
		Z12->(DbGoTop())
		Z12->(DbSeek(xFilial("Z12")))
		While Z12->(!Eof())
			If Z12->Z12_ATIVO
				aAdd(oJson['horarioEntrega'],ALLTRIM(Z12->Z12_HORARI)) 
			EndIf
			Z12->(DbSkip())
		EndDo

		oJson['categorias']				:= getTabela('Z23','Z23_DESC')	
		oJson['descricao_atividade']	:=  Alltrim(Z01->Z01_DSCATV)	
		oJson['raio_entrega']			:=	Z01->Z01_RAIO
		
		//Objeto da geolocalizacao
		oJsonGeo :=  JsonObject():new()
		oJsonGeo['latitude'] 	:= Z01->Z01_LAT
		oJsonGeo['longitude'] 	:= Z01->Z01_LNG

		oJson['geocoding']				:= oJsonGeo

		oJson['pedido_minimo']			:= Z01->Z01_PEDMIN
		oJson['status'] 				:= u_X3_CBOX('Z01_STATUS',Z01->Z01_STATUS)
		oJson['padrao_entrega'] 		:= Z01->Z01_CODZ20
		oJson['formaspagamento'] 		:= getFormas()
		oJson['formas_pagamento'] 		:= getFormas2()

		oJson['horarios']				:= getHorarios()
		oJson['informacao']				:= Alltrim(Z01->Z01_INFO)
		oJson['id']						:= cFilAnt
		oJson['foto_ecommerce']			:= { getFoto() }
		oJson['diasEntrega']			:= {}
		
		Z22->(DbSetOrder(1))
		Z22->(DbSeek(xFilial("Z22")))
		While Z22->(!Eof())
			aAdd(oJson['diasEntrega'],ALLTRIM(Z22->Z22_DESC)) 
			Z22->(DbSkip())
		EndDo

		oJsonFim['result'] := oJson

		FreeObj(oJson)
		
	EndIf
	
	cFilAnt = cFilAux
Return oJsonFim

User Function tstGet

	If Empty(FunName())
		PREPARE ENVIRONMENT EMPRESA '01' FILIAL '0101'
	EndIf

	oJson := getEstabelecimentos(.T.)
	cRet := oJson:ToJson()


Return