#INCLUDE "TOTVS.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "tbiconn.ch"

//-------------------------------------------------------------------
/*/{Protheus.doc} orders/getByCliente
Ws Rest para retornos de lista de de pedidos por cliente
Ws Rest para para salvar pedidos de venda
@author  Sidney Sales
@since   03/01/18
@version 1.0
/*/
//-------------------------------------------------------------------

WSRESTFUL orders DESCRIPTION "Retorna a lista de Pedidos Por Cliente"
	WSMETHOD GET 		DESCRIPTION "Retorna a lista de Pedidos Por Cliente" WSSYNTAX "/orders/getByCliente/{Cliente}"
	WSMETHOD POST  		DESCRIPTION "Inclusao/Alteracao" 				  	 WSSYNTAX "/orders/create/{Cliente}"
	WSMETHOD DELETE  	DESCRIPTION "Exclusao" 				  	 			 WSSYNTAX "/orders/{numpedido}"
END WSRESTFUL

WSMETHOD DELETE WSRECEIVE acao WSSERVICE orders          
	Local cRet 		:= ''		
	Local cAcao		:= ''
	Local cPedido 	:= ''
	Local oResponse	:= JsonObject():new()
	
	// define o tipo de retorno do metodo
	::SetContentType("application/json")   	
	
	//Valida os parametros enviados
	If Len(::aURLParms) < 1
		SetRestFault(400, "Parametros nao enviados")
		return .F.
	Else
		//Recupera o pedido e chama a funcao de exclusao
		cPedido 	:= ::aURLParms[1]
		aRet 		:= ExcluirPedido(cPedido) 	
		lRet 		:= aRet[1]
		
		//Retorno o servico
		If ! lRet
			oResponse['type']		:= "error"
			oResponse['status']		:= "404"
			oResponse['message']	:= aRet[2]
			SetRestFault(400, EncodeUTF8(oResponse:toJson()))			
		Else
			self:SetResponse(EncodeUTF8(aRet[2]))			
		EndIf

	EndIf

Return lRet 

WSMETHOD GET WSRECEIVE acao WSSERVICE orders          
	Local cRet := ''		
	Local cAcao
	Local cCliente := ''

	// define o tipo de retorno do metodo
	::SetContentType("application/json")   	
	
	//Valida os parametros enviados
	If Len(::aURLParms) < 2
		SetRestFault(400, "Parametros nao enviados")
		return .F.
	Else
		cAcao 		:= ::aURLParms[1]
		cCliente 	:= ::aURLParms[2]
	EndIf

	//Ate o momento so foi criado o get para os ativos	
	If Upper(cAcao) == 'GETBYCLIENTE'
		oJson := getPedidos(cCliente)
		cRet := oJson:ToJson()
		U_IFAF0004("IFAWS0004", "orders", "GET", cRet, " " )
		::SetResponse(EncodeUTF8(cRet))
	ElseIf UPPER(cAcao) <> 'GETBYCLIENTE'
		U_IFAF0004("IFAWS0004", "orders", "GET", cAcao, " " )
		SetRestFault(401, "Acao("+cAcao+") nao preparada para uso")
		return .F.
	ElseIf Empty(cAcao) 
		U_IFAF0004("IFAWS0004", "orders", "GET", cAcao, " " )
		SetRestFault(400, "Informe a acao que deseja utilizar")
		return .F.
	endif

Return .T.  

WSMETHOD POST WSSERVICE orders
	Local cJson         := DecodeUtf8(self:getContent())
    Local lRet          := .T.
	Local oResponse		:= JsonObject():new()
	PRIVATE lMsErroAuto := .F.
	::SetContentType("application/json")   
    //Chama funcao que realizara a inclusao 
    conout(cJson)
	U_IFAF0004("IFAWS0004", "SalvarPed", "ENTRADA", cJson )
	U_IFAF006A(cJson,"I","Pedido de venda","IFAWS004",.F.)

	aRet := SalvarPed(cJson)
	U_IFAF0004("IFAWS0004", "SalvarPed", "SAIDA", cJson, aRet[2] )
    
	lRet := aRet[1]
	conout(aRet[2])
    //Verifica o retorno da rotina e define o retorno para o ws
    If ! lRet
		oResponse['type']		:= "error"
		oResponse['status']		:= "404"
		oResponse['message']	:= aRet[2]
        If lMsErroAuto
			U_IFAF006A(aRet[2],"I",,,.T.)
		Else
			U_IFAF006A(aRet[2],"E",,,.T.)
		EndIf
		SetRestFault(400, EncodeUTF8(oResponse:toJson()))
    Else
	
		U_IFAF006A(aRet[2],"S",,,.T.)	
        self:SetResponse(EncodeUTF8(aRet[2]))
    EndIf
    
Return lRet

//-------------------------------------------------------------------
/*/{Protheus.doc} SalvarPed
Metodo respons�vel por salvar os pedidos de venda
@author  Samuel Dantas	
@since   07/01/2019
@version version
/*/
//-------------------------------------------------------------------
Static Function SalvarPed (cJsonAux)
	
	Local aCabec 	:= {}
	Local aItens 	:= {}
	Local aLinha 	:= {}
	Local nX     	:= 0
	Local cDoc   	:= ""
	Local oPedido
	Local nTamProd 	:= TAMSX3("C6_PRODUTO")[1]
	Local cArqErro	:= "erroauto.txt"
	Local _cFil		:= ""
	Local aValida   := {}
	Local oResponse	:= JsonObject():new()
	Local oResult	:= JsonObject():new()
	Local cEmails	:= AllTrim(SuperGetMv("MS_TECMAIL",.F.,"samuel.batista@agisrn.com;sidney.sales@agisrn.com"))
	Local cCodZ12	:= ""
	Local dDataEntr	:= CtoD("")
	Local aEmails	:= {}
	Local lExist	:= .F.
	
	

	//Inicia o objeto principal
	oResponse['type'] 	:= 'success'
	oResponse['status'] 	:= 200
	
	//Transforma o json em objeto
    FWJsonDeserialize(cJsonAux,@oPedido) 

    If ValType(oPedido) == 'U'
        Return {.F., 'Json com formato inv�lido. Verifique o JSON enviado.'}
    EndIf
		
	_cFil := PADR(oPedido:id_associado, TAMSX3("Z01_FILIAL")[1])
	
	//Validacao de filial
	If !Z01->(DbSeek(_cFil))
		Return {.F., 'A filial '+_cFil+' nao est� cadastrada.'}
	EndIf
	
	//Validacao de cliente
	SA1->(DBOrderNickname("A1YIDUSER"))
	lExist := SA1->(DbSeek(xFilial("SA1") + Alltrim(oPedido:id_cliente)))
	If !lExist
		SA1->(DBOrderNickname("A1YFIREB"))
		lExist := SA1->(DbSeek(xFilial("SA1") + Alltrim(oPedido:id_cliente)))
	EndIf

	If !lExist		
		Return {.F., 'O cliente de codigo'+oPedido:id_cliente+'nao est� cadastrado.'}
	Else	
		nRecnoSA1 := SA1->(RecNo())
	EndIf

	SA1->(DbSetOrder(1))
	SA1->(DbGoTo(nRecnoSA1))

	
	cDoc := u_uGetSXENum("SC5","C5_NUM")

	aCabec := {}
	aItens := {}
	aadd(aCabec,{"C5_NUM"   	,cDoc														,Nil})
	aadd(aCabec,{"C5_TIPO" 		,"N"														,Nil})
	aadd(aCabec,{"C5_CLIENTE"	,SA1->A1_COD												,Nil})
	aadd(aCabec,{"C5_LOJACLI"	,SA1->A1_LOJA												,Nil})
	aadd(aCabec,{"C5_LOJAENT"	,SA1->A1_LOJA												,Nil})
	aadd(aCabec,{"C5_CONDPAG"	,"001"														,Nil})
	aadd(aCabec,{"C5_YCODZ01"	,Z01->Z01_CODIGO											,Nil})
	aadd(aCabec,{"C5_YOBS"		,oPedido:obs												,Nil})

	aadd(aCabec,{"C5_YFORMPG"	,oPedido:pagamento:codigo																	,Nil})
	aadd(aCabec,{"C5_YTROCO"	,Iif(ValtYpe(oPedido:pagamento:troco)=='N', oPedido:pagamento:troco, 0)						,Nil})
	aadd(aCabec,{"C5_YTOKEN"	,Iif(ValtYpe(oPedido:pagamento:token)=='C', oPedido:pagamento:token, '')					,Nil})		
	aadd(aCabec,{"C5_YMETOD"	,Iif(ValtYpe(oPedido:pagamento:paymentMethodId)=='C', oPedido:pagamento:paymentMethodId, ''),Nil})		
	aadd(aCabec,{"C5_YPARCEL"	,Iif(ValtYpe(oPedido:pagamento:installments)=='N', oPedido:pagamento:installments, 0)		,Nil})		

	aadd(aCabec,{"C5_YSTATUS"	,"P"														,Nil})
	aadd(aCabec,{"C5_YCODZ09"	,oPedido:endereco:id										,Nil})
	aadd(aCabec,{"C5_FRETE"		,oPedido:frete - oPedido:desconto_frete						,Nil})
	aadd(aCabec,{"C5_TPFRETE"	,IIF((oPedido:frete - oPedido:desconto_frete) == 0,'C','F')	,Nil})
	aadd(aCabec,{"C5_DESC1"		,oPedido:desconto_pedido									,Nil})
	aadd(aCabec,{"C5_YDESCFR"	,oPedido:desconto_frete										,Nil})
	aadd(aCabec,{"C5_YDESCPE"	,oPedido:desconto_pedido									,Nil})
	aadd(aCabec,{"C5_YENTREG"	,oPedido:entrega											,Nil})
	aadd(aCabec,{"C5_YCODZ24"	,oPedido:plano												,Nil})
	
	cYHRENT := ""
	cYDIAENT := ""
	cYHRSZ12 := ""

	For nI := 1 To len(oPedido:horarioEntrega)
		If oPedido:horarioEntrega[nI]:checked
			cYHRENT += Alltrim(oPedido:horarioEntrega[nI]:horario) + " "
			cCodZ12 := Alltrim(oPedido:horarioEntrega[nI]:codigo)
			cYHRSZ12 += IIF( Empty(cYHRSZ12), Alltrim(oPedido:horarioEntrega[nI]:codigo), ","+ Alltrim(oPedido:horarioEntrega[nI]:codigo) ) 
		EndIf
	Next

	For nI := 1 To len(oPedido:diasEntrega)
		If oPedido:diasEntrega[nI]:checked
			cYDIAENT += Alltrim(oPedido:diasEntrega[nI]:label) + " "
			dDataEntr := CtoD(Alltrim(oPedido:diasEntrega[nI]:data))
		EndIf
	Next
	aadd(aCabec,{"C5_YHRENT"	,cYHRENT									,Nil})
	aadd(aCabec,{"C5_YDIAENT"	,cYDIAENT									,Nil})
	aadd(aCabec,{"C5_YDTENTR"	,dDataEntr									,Nil})
	aadd(aCabec,{"C5_YCODZ12"	,cCodZ12									,Nil})
	aadd(aCabec,{"C5_YHRSZ12"	,cYHRSZ12									,Nil})

	//Valida cabecalho
	aValida := ValidaSX3(aCabec)
	If aValida[1] == .F.
		return aValida
	EndIf

	For nX := 1 To len(oPedido:produtos)

		//Separa codigo da E SB1
		cCodProd := SubStr( oPedido:produtos[nX]:id_produto , 1, nTamProd )
		
		SB1->(DbSetOrder(1)) //B1_FILIAL + B1_COD
		If !SB1->(DbSeek(_cFil + cCodProd))
			Return {.F., 'O Produto ' +cCodProd+' nao encontrado.'}
		EndIf
		
		aLinha := {}
		aadd(aLinha,{"C6_ITEM"		,StrZero(nX,2)										   					,Nil})
		aadd(aLinha,{"C6_PRODUTO"	,cCodProd											   					,Nil})
		aadd(aLinha,{"C6_QTDVEN" 	,oPedido:produtos[nX]:quantidade					   					,Nil})
		aadd(aLinha,{"C6_QTDLIB" 	,oPedido:produtos[nX]:quantidade					   					,Nil})
		If AttIsMemberOf(oPedido:produtos[nX], 'valorreal') 
			aadd(aLinha,{"C6_PRUNIT" 	,Round(oPedido:produtos[nX]:valorreal,2)					   		,Nil})
		EndIf
		aadd(aLinha,{"C6_PRCVEN" 	,Round(oPedido:produtos[nX]:valor,2)				   					,Nil})
		
		aadd(aLinha,{"C6_VALOR"  	,Round(oPedido:produtos[nX]:valor,2)*oPedido:produtos[nX]:quantidade	,Nil})
		aadd(aLinha,{"C6_YVALOR" 	,Round(oPedido:produtos[nX]:valor,2)									,Nil})
		aadd(aLinha,{"C6_YQTD"   	,oPedido:produtos[nX]:quantidade										,Nil})
		aadd(aLinha,{"C6_TES"		,"501"																	,Nil})
		
		//Voltar 
		//nPRUNIT := Iif(ValtYpe(oPedido:produtos[nX]:valorreal)=='N', oPedido:produtos[nX]:valorreal, oPedido:produtos[nX]:valor)
		nPRUNIT := oPedido:produtos[nX]:valor
		aadd(aLinha,{"C6_PRUNIT"	,Round(nPRUNIT,2)								    ,Nil})
		
		aadd(aItens,aLinha)
		
		//Valida item
		aValida := ValidaSX3(aLinha)
		If aValida[1] == .F.
			return aValida
		EndIf

	Next nX

	//Inclui pedido de venda
	MSExecAuto({|x,y,z|mata410(x,y,z)},aCabec,aItens,3)
	If !lMsErroAuto
		ConfirmSX8()
		ConOut("Incluido com sucesso! "+cDoc)
	Else
		
		RollBackSX8()
		MostraErro( GetSrvProfString("Startpath","") , cArqErro )
		cMsg := " Error ao salvar pedido do cliente " + SA1->A1_COD + " - "+SA1->A1_NOME+" - "+ CRLF
		cMsg += MemoRead( GetSrvProfString("Startpath","") + '\' + cArqErro )
		
		U_IFAF0004("IFAWS0004", "SalvarPed", "SAIDA", cJsonAux+chr(13)+chr(10)+"Saida:"+cMsg , nil , '\log\mail\')

		Return {.F., cMsg }
	EndIf

	oResponse['message']	:= 'Pedido '+ cDoc+' enviado com sucesso.'
	oResult['num_pedido']  	:= cDoc
	oResponse['result']		:= oResult

Return { .T., oResponse:ToJson() }

//-------------------------------------------------------------------
/*/{Protheus.doc} getPedidos
Funcao que retorna a lista de Pedidos do cliente
cada SM0(FILIAL)
@author  Sidney Sales
@since   03/01/18
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function getPedidos(cCodCli)

	Local aAreaSM0 	:= SM0->(GetArea())
	Local oJsonFim	:= JsonObject():new()
	Local oJson, cQuery, cSeek		
	Local lExist := .F.		
	
	//Inicia o objeto principal
	oJsonFim['type'] 	:= 'success'
	oJsonFim['status'] 	:= 200
	oJsonFim['message']	:= 'Pedidos Encontrados'
	oJsonFim['result']	:= {}
	
	SA1->(DBOrderNickname("A1YIDUSER"))
	lExist := SA1->(DbSeek(xFilial("SA1") + Alltrim(cCodCli)))
	If !lExist
		SA1->(DBOrderNickname("A1YFIREB"))
		lExist := SA1->(DbSeek(xFilial("SA1") + Alltrim(cCodCli)))
	EndIf

	If !lExist	
		oJsonFim['type'] 	:= 'error'
		oJsonFim['status'] 	:= 400
		oJsonFim['message']	:= 'O cliente de codigo'+cCodCli+'nao est� cadastrado.'
		oJsonFim['result']	:= {}
		
		Return oJsonFim
	Else	
		nRecnoSA1 := SA1->(RecNo())
	EndIf

	SA1->(DbSetOrder(1))
	SA1->(DbGoTo(nRecnoSA1))

	cQuery := " SELECT R_E_C_N_O_ SC5RECNO FROM " + RetSqlTab("SC5") 
	cQuery += " WHERE " + RetSqlDel("SC5")
	cQuery += " AND C5_CLIENTE = '" + SA1->A1_COD + "' "

	If Select('QRY') > 0
		QRY->(DbCloseArea())
	EndIf

	TcQuery cQuery New Alias 'QRY'
	
	//Percorre SM0 gerando um objeto para cada filial/associacao
	While QRY->(!Eof())

		SC5->(DbGoTo(QRY->SC5RECNO))
		
		SC6->(DbSetOrder(1))
		SC6->(DbSeek(cSeek := SC5->C5_FILIAL + SC5->C5_NUM))

		Z17->(DbSetOrder(1))
		Z17->(DbSeek(SC5->C5_FILIAL))

		Z01->(DbSeek(SC5->C5_FILIAL))

		oJsonPed := JsonObject():new()

		If Z19->(DbSeek(SC5->C5_FILIAL + SC5->C5_NUM))
			cAvalProd := Z19->Z19_NOTA 
			cAvalReco := Z19->Z19_NOTREC
			cComentCli:= Z19->Z19_CMTCLI
			cDtAvalia := FwTimeStamp(3, Z19->Z19_DATA,'00:00')
			cNomeCli  := Z19->z19_NOMECL
		Else
			cAvalProd := ''
			cAvalReco := ''
			cComentCli:= ''
			cDtAvalia := ''
			cNomeCli  := ''
		EndIf

		oJsonPed['avaliacao_recomendacao'] 			:= cAvalReco
		oJsonPed['avaliacao_comentario_cliente'] 	:= Alltrim(cComentCli)
		oJsonPed['avaliacao_produto'] 				:= cAvalProd
		oJsonPed['avaliacao_data']					:= cDtAvalia
		oJsonPed['avaliacao_nome_cliente']			:= Alltrim(cNomeCli)
		oJsonPed['avaliacao_resposta_empresa']		:= ''
		oJsonPed['avaliacao_status']				:= 'A'
		oJsonPed['avaliacao_pedido_moderacao']		:= ''
		oJsonPed['avaliacao_motivo_moderacao']		:= ''

		oJsonPed['created']							:= SC5->C5_YDTCRIA
		oJsonPed['associado_nome']					:= Alltrim(Z01->Z01_DESC)
		oJsonPed['status']							:= SC5->C5_YSTATUS
		oJsonPed['desconto_frete']					:= SC5->C5_YDESCFR
		oJsonPed['desconto_pedido']					:= SC5->C5_YDESCPE
		oJsonPed['motivo_rejeicao']					:= ''
		oJsonPed['id_associado']					:= cFilAnt
		oJsonPed['raio_entrega']					:= Z01->Z01_RAIO
		oJsonPed['frete']							:= SC5->C5_FRETE
		oJsonPed['id_cliente']						:= SC5->C5_CLIENTE
		oJsonPed['obs']								:= Alltrim(SC5->C5_YOBS)
		oJsonPed['troco	']							:= SC5->C5_YTROCO
		oJsonPed['token_cartao']					:= ''
		oJsonPed['entrega']							:= SC5->C5_YENTREG
		oJsonPed['forma_pagamento'	]				:= SC5->C5_YFORMPG
		oJsonPed['num_pedido']						:= SC5->C5_FILIAL + SC5->C5_NUM
		oJsonPed['id_plano']						:= SC5->C5_YCODZ24
		Z24->(DbSetOrder(1))
		If Z24->(DbSeek(xFilial("Z24") + SC5->C5_YCODZ24))
			oJsonPed['titulo_plano']						:= Z24->Z24_TITULO
		Else
			oJsonPed['titulo_plano']						:= Z24->Z24_TITULO
		EndIf
		oJsonPed['token_device']					:= ''
		oJsonPed['id']								:= SC5->C5_FILIAL + SC5->C5_NUM
		oJsonPed['status_cartao']					:= Alltrim(SC5->C5_YSTATMP) $ Alltrim(SuperGetMv('MS_STATPAG', .F., 'approved,authorized'))
		oJsonPed['retorno_cartao']					:= getStatus(Alltrim(SC5->C5_YSTATMP))
		oJsonPed['produtos']						:= {}
		
		oJsonEnd := JsonObject():new()
		oJsonEnd['cidade']	:= SC5->C5_YMUN
		oJsonEnd['id']		:= SC5->C5_YCODZ09
		oJsonEnd['cep']		:= SC5->C5_YCEP
		oJsonEnd['estado']	:= SC5->C5_YEST
		oJsonEnd['complemento']	:= SC5->C5_YCOMPLE
		oJsonEnd['numero']	:= SC5->C5_YNUMERO
		oJsonEnd['bairro']	:= SC5->C5_YBAIRRO
		oJsonEnd['rua']		:= SC5->C5_YRUA
		
		oJsonGeo := JsonObject():new()
		oJsonGeo['lat']		:= SC5->C5_YLAT
		oJsonGeo['lng']		:= SC5->C5_YLAT
		oJsonGeo['status'] 	:= 'OK'
		oJsonEnd['geocoding']	:= oJsonGeo
		
		SB1->(DbSetOrder(1))
		
		nTotal := 0
		While SC6->(!Eof()) .AND. cSeek == SC6->C6_FILIAL + SC6->C6_NUM

			nTotal += SC6->C6_VALOR

			//Cria o objeto dos pedidos
			oJson 		:= JsonObject():new()

			oJson['descricao'] 			:= SC6->C6_DESCRI		
			oJson['preco'] 				:= Round(SC6->C6_PRCVEN,2)
			oJson['valor'] 				:= Round(SC6->C6_PRCVEN,2)
			oJson['quantidade']			:= SC6->C6_QTDVEN
			oJson['validacao_cardapio']	:= 'manual'
			oJson['id_produto']			:= SC6->C6_PRODUTO
			oJson['id_cardapio']		:= Z17->Z17_CODIGO

			aAdd(oJsonPed['produtos'], oJson)

			FreeObj(oJson)

			SC6->(DbSkip())		
		EndDo

		oJsonPed['total'] := nTotal							

		//Adiciona ao array o objeto json atual
		aAdd(oJsonFim['result'], oJsonPed)

		QRY->(DbSkip())

	EndDo

	RestArea(aAreaSM0)

Return oJsonFim


User Function tstPed
	If Empty(FunName())
		PREPARE ENVIRONMENT EMPRESA '01' FILIAL '0101'
	EndIf
	cJson := ''
	cJson += '{  '
	cJson += '   "num_pedido":"",'
	cJson += '   "id_associado":"0101",'
	cJson += '   "geocoding":{  '
	cJson += '      "latitude":-5.80972,'
	cJson += '      "longitude":-35.197488'
	cJson += '   },'
	cJson += '   "raio_entrega":20,'
	cJson += '   "horarioEntrega":[  '
	cJson += '      {  '
	cJson += '         "horario":"12h - 14h",'
	cJson += '         "checked":false'
	cJson += '      },'
	cJson += '      {  '
	cJson += '         "horario":"14h - 16h",'
	cJson += '         "checked":true'
	cJson += '      }'
	cJson += '   ],'
	cJson += '   "diasEntrega":[  '
	cJson += '      {  '
	cJson += '         "label":"Segunda-feira",'
	cJson += '         "checked":true'
	cJson += '      }'
	cJson += '   ],'
	cJson += '   "frete":0,'
	cJson += '   "id_plano":"",'
	cJson += '   "desconto_pedido":0,'
	cJson += '   "desconto_frete":0,'
	cJson += '   "endereco":{  '
	cJson += '      "pessoa_recebe":"",'
	cJson += '      "numero":"5100",'
	cJson += '      "bairro":"Candelária",'
	cJson += '      "id":"000194",'
	cJson += '      "alterar":true,'
	cJson += '      "estado":"RN",'
	cJson += '      "endereco":"",'
	cJson += '      "rua":"Rua Jaguarari",'
	cJson += '      "complemento":"Casa 114",'
	cJson += '      "cep":"59064500",'
	cJson += '      "referencia":"",'
	cJson += '      "cidade":"Natal"'
	cJson += '   },'
	cJson += '   "entrega":false,'
	cJson += '   "id_cliente":"iVsNz3nhhg5qndcwehpc",'
	cJson += '   "produtos":[  '
	cJson += '      {  '
	cJson += '         "id_produto":"000000000000114",'
	cJson += '         "quantidade":1,'
	cJson += '         "descricao":"Queijo Ricota JUCURUTU - aprox. 500g (UND) - CONV                                                                                                                                                                                                             ",'
	cJson += '         "valor":12.24,'
	cJson += '         "quantidade_disponivel":1,'
	cJson += '         "validacao_cardapio":"",'
	cJson += '         "id_cardapio":"000007",'
	cJson += '         "valorreal":16.3'
	cJson += '      },'
	cJson += '      {  '
	cJson += '         "id_produto":"000000000000078",'
	cJson += '         "quantidade":1,'
	cJson += '         "descricao":"Ovo Caipira - Bandeja com 15 unidades (UND) - CONV                                                                                                                                                                                                            ",'
	cJson += '         "valor":15.3,'
	cJson += '         "valorreal":16.3,'
	cJson += '         "quantidade_disponivel":6,'
	cJson += '         "validacao_cardapio":"",'
	cJson += '         "id_cardapio":"000007"'
	cJson += '      },'
	cJson += '      {  '
	cJson += '         "id_produto":"000000000000127",'
	cJson += '         "quantidade":1,'
	cJson += '         "descricao":"Ameixa sem Caroco - 150g (UND) - CONV                                                                                                                                                                                                                         ",'
	cJson += '         "valor":6.67,'
	cJson += '         "quantidade_disponivel":20,'
	cJson += '         "validacao_cardapio":"",'
	cJson += '         "id_cardapio":"000007"'
	cJson += '      },'
	cJson += '      {  '
	cJson += '         "id_produto":"000000000000132",'
	cJson += '         "quantidade":1,'
	cJson += '         "descricao":"Castanha de Cajú Torrada- 100g (UND) - CONV                                                                                                                                                                                                                   ",'
	cJson += '         "valor":9.56,'
	cJson += '         "quantidade_disponivel":11,'
	cJson += '         "validacao_cardapio":"",'
	cJson += '         "id_cardapio":"000007"'
	cJson += '      },'
	cJson += '      {  '
	cJson += '         "id_produto":"000000000000143",'
	cJson += '         "quantidade":1,'
	cJson += '         "descricao":"Semente de Girassol - 100g (UND) - CONV                                                                                                                                                                                                                       ",'
	cJson += '         "valor":2.99,'
	cJson += '         "quantidade_disponivel":7,'
	cJson += '         "validacao_cardapio":"",'
	cJson += '         "id_cardapio":"000007"'
	cJson += '      },'
	cJson += '      {  '
	cJson += '         "id_produto":"000000000000145",'
	cJson += '         "quantidade":2,'
	cJson += '         "descricao":"Uva Passas Escuras - 100g (UND) - CONV                                                                                                                                                                                                                        ",'
	cJson += '         "valor":3.06,'
	cJson += '         "quantidade_disponivel":20,'
	cJson += '         "validacao_cardapio":"",'
	cJson += '         "id_cardapio":"000007"'
	cJson += '      }'
	cJson += '   ],'
	cJson += '   "pagamento":{  '
	cJson += '      "token":"",'
	cJson += '      "nome":"Dinheiro",'
	cJson += '      "forma_pagamento":"cash",'
	cJson += '      "codigo":"000001",'
	cJson += '      "troco":0,'
	cJson += '      "bandeira":null,'
	cJson += '      "paymentMethodId":"",'
	cJson += '      "installments":1'
	cJson += '   },'
	cJson += '   "obs":"Falta entregar apenas os ovos. ",'
	cJson += '   "status":"",'
	cJson += '   "total":52.88'
	cJson += '}'

	SalvarPed(cJson)

Return


Static Function validaSX3 (aItens)
	Local nI := 1
	Local cCampo
	Local xConteudo
	Local aRet := {.T.}

	For nI := 1 To len(aItens)
		cCampo 		:= aItens[nI][1]
		xConteudo 	:= aItens[nI][2]
		aSX3 		:=TAMSX3(cCampo)
		cTipo 		:= aSX3[3]

		If cTipo == 'D' .AND. ValType(xConteudo) == 'C'
			aItens[nI][2] := StoD(xConteudo)
		ElseIf cTipo == 'M' .AND. ValType(xConteudo) == 'C'

		ElseIf cTipo != ValType(xConteudo)
			aRet := {.F., "Conte�do do campo "+cCampo+" inv�lido. Tipo esperado "+cTipo+" Tipo enviado "+ValType(xConteudo)+"."}
		EndIf
		
	Next
return aRet

Static Function getStatus(cStatus)
	Local cRet		:= 'N�o processado'
	Local aStatus	:= {}
	Local nPos
	
	aAdd(aStatus, {'pending'		, 'Pendente'	})
	aAdd(aStatus, {'approved'		, 'Aprovado'	})
	aAdd(aStatus, {'authorized'		, 'Autorizado'	})
	aAdd(aStatus, {'in_process'		, 'Em processo'	})
	aAdd(aStatus, {'in_mediation'	, 'Em media��o'	})
	aAdd(aStatus, {'rejected'		, 'N�o autorizado'})
	aAdd(aStatus, {'cancelled'		, 'Cancelado'	})
	aAdd(aStatus, {'refunded'		, 'Pendente'	})
	aAdd(aStatus, {'charged_back'	, 'Estornado'	})

	If !Empty(cStatus)
		nPos := aScan(aStatus, {|x| x[1] == cStatus})
		If nPos <> 0
			cRet := aStatus[nPos][2]
		EndIf
	EndIf

Return cRet


//-------------------------------------------------------------------
/*/{Protheus.doc} ExcluirPedido
Funcao para exclusao do pedido
@author  Sidney
@since   29/07/19
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function ExcluirPedido(cNumPed) 	
	Local aCabec		:= {}
	Local aItens		:= {}
	Local aLinha		:= {}
	Local cArqErro		:= 'erroauto.txt'
	Private lMsErroAuto	:= .F.
	
	Default cNumPed		:= '004213'
		
	SC5->(DbSetOrder(1))

	If SC5->(DbSeek(xFilial('SC5') + cNumPed))

		aadd(aCabec, {"C5_NUM",     SC5->C5_NUM,      Nil})
		aadd(aCabec, {"C5_TIPO",    SC5->C5_TIPO,       Nil})
		aadd(aCabec, {"C5_CLIENTE", SC5->C5_CLIENTE,    Nil})
		aadd(aCabec, {"C5_LOJACLI", SC5->C5_LOJACLI,   Nil})

		SC6->(DbSetOrder(1))
		SC6->(DbSeek(cSeekSC6 := xFilial('SC6') + SC5->C5_NUM ))

		While SC6->(!Eof()) .AND. SC6->(C6_FILIAL + C6_NUM) == cSeekSC6
			
			aLinha := {}
			aadd(aLinha,{"C6_ITEM",    SC6->C6_ITEM		, Nil})
			aadd(aLinha,{"C6_PRODUTO", SC6->C6_PRODUTO	, Nil})
			aadd(aLinha,{"C6_QTDVEN",  SC6->C6_QTDVEN	, Nil})
			aadd(aLinha,{"C6_PRCVEN",  SC6->C6_PRCVEN	, Nil})
			aadd(aLinha,{"C6_PRUNIT",  SC6->C6_PRUNIT	, Nil})
			aadd(aLinha,{"C6_QTDLIB",  0				, Nil})
			aadd(aLinha,{"C6_VALOR",   SC6->C6_VALOR	, Nil})
			aadd(aLinha,{"C6_TES",     SC6->C6_TES		, Nil})
			
			aadd(aItens, aLinha)

			SC6->(DbSkip())
		EndDo
		
		//Deslibera o pedido
		SC9->(DbSeek(cSeekSC9 := xFilial("SC9") + cNumPed))
		While !SC9->(Eof()) .And. SC9->(C9_FILIAL + C9_PEDIDO) == cSeekSC9
			SC9->(a460Estorna())
			SC9->(dbSkip())
		Enddo

		MSExecAuto({|a, b, c| MATA410(a, b, c)}, aCabec, aItens, 5)

		If lMsErroAuto
			MostraErro( GetSrvProfString("Startpath","") , cArqErro )
			cMsg := MemoRead( GetSrvProfString("Startpath","") + '\' + cArqErro )
			aRet := {.F., cMsg}
		Else
			aRet := {.T., 'Pedido ' + cNumPed + ' apagado com sucesso.'}
		EndIf

	Else
		aRet := {.F., 'Pedido ' + cNumPed + ' n�o localizado.'}
	EndIf

Return aRet
