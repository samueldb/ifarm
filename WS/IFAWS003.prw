#INCLUDE "TOTVS.CH"
#INCLUDE "RESTFUL.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "TBICONN.CH"
#INCLUDE "PROTHEUS.CH"
//-------------------------------------------------------------------
/*/{Protheus.doc} evaluation
Webservice para inclusao de cadastros de avaliac�es no IFARM
Z19 - avaliac�es

@author  Samuel Dantas
@since   19/12/2018
/*/
//-------------------------------------------------------------------
WSRESTFUL evaluation DESCRIPTION "Rest para Inclusao de avaliac�es Protheus"

    WSMETHOD POST  DESCRIPTION "Inclusao/Alteracao" 	WSSYNTAX "/evaluation/create/{Evaluation}"
    WSMETHOD GET   DESCRIPTION "Avaliacao do pedido"    WSSYNTAX "/evaluation/marketplace/{getEvaluation}"

END WSRESTFUL 

WSMETHOD POST WSRECEIVE cCod WSSERVICE evaluation
    Local cJson         := DecodeUtf8(self:getContent())
    Local lRet          := .T.
    Local cCodPed       := ""
    Local oResponse      := JsonObject():new()
    ::SetContentType("application/json")   
    
    //Inicia o objeto principal
	

    If Len(::aURLParms) == 0
		SetRestFault(400, "Informe o codigo do cliente")
		return .F.
	Else
		cCodPed := ::aURLParms[2]
	EndIf
    U_IFAF006A(cJson,"I","Incluir avaliacao","IFAWS003",.F.)
    U_IFAF0004("IFAWS0004", "evaluation", "ENTRADA", cJson )
    //Chama funcao que realizara a inclusao 
    aRet := evaluation(cCodPed,cJson)
    U_IFAF0004("IFAWS0004", "evaluation", "SAIDA", cJson, aRet[2] )
    lRet := aRet[1]

    //Verifica o retorno da rotina e define o retorno para o ws
    If ! lRet
        U_IFAF006A(aRet[2],"E",,,.T.)
        If aRet[3] == 200 
            oResponse['type'] 	:= 'conflict'
            oResponse['status'] 	:= aRet[3]
        Else
            oResponse['type'] 	:= 'error'
            oResponse['status'] 	:= aRet[3]
        EndIf
        
        oResponse['message'] := aRet[2]
        oResponse['result']  := ""
        If aRet[3] == 200 
            // SetRestFault(200, EncodeUTF8(oResponse:toJson()))
            self:SetResponse(EncodeUTF8(oResponse:toJson()))
            lRet := .T.
        Else
            SetRestFault(400, EncodeUTF8(oResponse:toJson()))
        EndIf
    Else
        U_IFAF006A(aRet[2],"S",,,.T.)
        oResponse['type'] 	:= 'success'
        oResponse['status'] 	:= 201
        oResponse['message'] := aRet[2]
        oResponse['result']  := ""
        self:SetResponse(EncodeUTF8(oResponse:toJson()))
    EndIf
    
Return lRet

WSMETHOD GET WSRECEIVE _cFil WSSERVICE evaluation          
	Local aResponse := {}		
    Local lRet       := .F.
    Local _cFil    := ''
    
	// define o tipo de retorno do metodo
	::SetContentType("application/json")   	
	
    If Len(::aURLParms) < 2
		SetRestFault(400, "Informe o codigo da associacao")
		return .F.
	Else
		_cFil := ::aURLParms[2]
	EndIf

	aResponse := getEvaluation(_cFil)
    lRet := aResponse[1]
    U_IFAF0004("IFAWS0003", "evaluation", "SAIDA", aResponse[2], " " )
    self:SetResponse(EncodeUTF8(aResponse[2]))

Return .T.      

//-------------------------------------------------------------------
/*/{Protheus.doc} Funcao respons�vel por salvar a avaliacao do pedido
@author  Samuel Dantas 
@since   19/12/2018
@version 1.0
/*/
//-------------------------------------------------------------------
Static Function evaluation(cCodPed,cJsonAux)

    Local cJsonAux
    Local oAvaliacao
    Local cMsg          := ""
    Local nI            := 1
    Local aNoAttr       := {}
    Local nC5Num        := TAMSX3("C5_NUM")[1]
    Local _cFil         := ""
    // {Attr do json, Campo no protheus, tipo do campo}
    Local aAttrs        := {{"nivel_recomendacao", "Z19_NOTREC"}, {"nivel_produto","Z19_NOTA"}, {"comentario_cliente","Z19_CMTCLI"}}
     
    //Transforma o json em objeto
    FWJsonDeserialize(cJsonAux,@oAvaliacao) 

    If ValType(oAvaliacao) == 'U'
        Return {.F., 'Json com formato inv�lido. Verifique o JSON enviado.', 404 }
    EndIf

    _cFil := LEFT(cCodPed,TAMSX3("Z19_FILIAL")[1])
    cCodC5 := RIGHT(cCodPed,nC5Num)

    //Busca o pedido de venda
    SC5->(DbSetOrder(1)) //C5_FILIAL + C5_NUM
    If !SC5->(DbSeek(_cFil + PADR(cCodC5, nC5Num)))
        Return {.F., 'O pedido nao foi encontrado. Verifique o numero de pedido.', 404 }
    EndIf

    Z19->(DbSetOrder(1)) // Z19_FILIAL + Z19_NUMSC5 
    If Z19->(DbSeek(_cFil + SC5->C5_NUM ))
        Return { .F., 'Ja existe uma avaliacao cadastrada para esse pedido.',200 }
    EndIf

    For nI := 1 To len(aAttrs)

        //Verificacao de tipo de dados
        cAttr       := aAttrs[nI][1]
        cCampo      := aAttrs[nI][2]
        cTipo       := TAMSX3(cCampo)[3]
        xConteudo   := &("oAvaliacao:" + cAttr)
        If !(ValType(xConteudo) == cTipo)
            If cTipo == 'D' .AND. ValType(xConteudo) == 'C'
                &("oAvaliacao:" + cAttr) := StoD(xConteudo)
            Else 
                Return {.F., 'Tipo de dado inv�lido para o atributo: ' + cAttr + ". Tipo enviado '" +ValType(xConteudo) +"' e tipo esperado '"+cTipo+"' .",500} 
            EndIf
        EndIf

        If !AttIsMemberOf(oAvaliacao,cAttr)
            aAdd(aNoAttr,cAttr) //Adiciona ao array de atributos nao encontrados
        EndIf

    Next

    //Busca cliente relacionado ao pedido
    SA1->(DbSetOrder(1))
    If !SA1->(DbSeek(xFilial("SA1") + SC5->C5_CLIENTE))
        Return {.F., 'Nao foi possivel encontrar o cliente. Nao existe cliente relacionado ao pedido.',404}
    EndIf

    //Caso todos o atributos estejam no JSON
    If len(aNoAttr) == 0    

        RecLock('Z19', .T.)
            Z19->Z19_FILIAL := _cFil
            Z19->Z19_CODIGO := GetSXENUM("Z19","Z19_CODIGO")
            Z19->Z19_NOMECL := SA1->A1_NOME                     //Nome do cliente
            Z19->Z19_STATUS := "P"                 
            Z19->Z19_DATA   := DATE()                           // Data da avaliacao
            Z19->Z19_HORARI := TIME()                           // Horario
            Z19->Z19_NOTA   := oAvaliacao:nivel_produto         // Nota da avaliacao
            Z19->Z19_NOTREC := oAvaliacao:nivel_recomendacao    // Nota de recomendacao
            Z19->Z19_CMTCLI := oAvaliacao:comentario_cliente    // Coment�rio do cliente
            Z19->Z19_NUMSC5 := cCodC5                          // Codigo do pedido
        Z19->(MsUnLock())
    Else // Trata mensagem caso atributo nao tenha sido encontrado
        If len(aNoAttr) == 1 
            cMsg := "O campo " + aNoAttr[1] + " nao foi encontrado no json."
        Else
            cMsg += "Os campos: "
            For nI := 1 To len(aNoAttr)
                If nI == 1
                    cMsg += aNoAttr[nI]
                ElseIf nI == len(aNoAttr)
                    cMsg += " e "+ aNoAttr[nI]
                Else
                    cMsg += ", "+ aNoAttr[nI]
                EndIf
            Next
            cMsg += " nao foram encontrados no Json."
        EndIf
        
        Return {.F., cMsg,404}
    EndIf

Return {.T., 'Avaliacao salva com sucesso.'}

//-------------------------------------------------------------------
/*/{Protheus.doc} getEvaluation
Funcao respons�vel por retornar avaliacao refente as filiais
@author  Samuel Dantas 
@since   07/01/2019
@version version
/*/
//-------------------------------------------------------------------
Static function getEvaluation(_cFil)

    Local aRet := {}
    Local oAvaliacao 
    Local oResult

    oResult := JsonObject():new()
    oResult['result']   := {}
     //Busca avaliac�es
    Z19->(DbSetOrder(1))
    Z19->(DbSeek(_cFil))
    If Z19->(!EOF())
        While  Z19->(!EOF()) .AND. Z19->Z19_FILIAL == _cFil
            oAvaliacao := JsonObject():new()
            
            oAvaliacao['id_pedido']                     := ALLTRIM(Z19->Z19_CODIGO)
            oAvaliacao['avaliacao_nome_cliente']        := ALLTRIM(Z19->Z19_NOMECL)
            oAvaliacao['avaliacao_data']                := FWTIMESTAMP(3, Z19->Z19_DATA, Z19->Z19_HORARI)
            oAvaliacao['avaliacao_produto']             := Z19->Z19_NOTA
            oAvaliacao['avaliacao_recomendacao']        := Z19->Z19_NOTREC
            oAvaliacao['avaliacao_comentario_cliente']  := ALLTRIM(Z19->Z19_CMTCLI)
            oAvaliacao['avaliacao_resposta_empresa']    := ALLTRIM(Z19->Z19_AVEMP)
            oAvaliacao['avaliacao_status']              := ALLTRIM(Z19->Z19_STATUS)
            oAvaliacao['avaliacao_motivo_moderacao']    := NIL
            oAvaliacao['avaliacao_pedido_moderacao']    := NIL
            
            aAdd(oResult['result'],oAvaliacao)
            Z19->(DbSkip())
        EndDo

        oResult['type']     := "success"
        oResult['status']   := 200
        oResult['message']  := "Resultados encontrados"        
        aRet := {.T., oResult:toJson() }
    Else
        oResult['type']     := "error"
        oResult['status']   := 400
        oResult['message']  := "Nenhuma avaliacao foi encontrada para essa associacao." 
        oResult['result']   := {}
        aRet := {.F., oResult:toJson()}
    EndIf
Return aRet

//funcoes de testes
User Function testeEx

    cCodPed := "000007"
    cJson := '{'
    cJson += '"nivel_recomendacao": 5,'
    cJson += '"nivel_produto": 5,'
    cJson += '"comentario_cliente": "teste"'
    cJson += '}'

    PREPARE ENVIRONMENT EMPRESA '01' FILIAL '01'

    aRet := evaluation(cCodPed, cJson)
    
    MsgAlert(aRet[2])

Return
